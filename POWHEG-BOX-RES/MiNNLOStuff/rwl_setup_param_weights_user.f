c     This subroutine is called bpowheginput("#rwl_group_events") the hook extraweightshook.h in
c     rwl_setup_param_weights.f.  It allows to perform stage4 on-the-fly
c     reweighting for parameters other than PDF and (muR,muF) scale
c     variation.

c     If you want to use it, rwl_setup_param_extraweights.f should be
c     compiled by the Makefile, and the hook file should be present in
c     your local process directory.
      
c     In this particular case, it deals with the parameter kappaq that
c     we use in setlocalscales.f

c     Howto:
c     1) include common_kappaQ in setlocalscales
c     2) use kappaQ where needed
      
c     kappar = st_renfact
c     kappaf = st_facfact
c     if(ckappaq.gt.0) kappaq = ckappaq

      subroutine rwl_setup_params_weights_user(count)
      implicit none
      include 'minnlo_flg.h'
      include 'pwhg_flg.h'
      integer count
      logical rwl_keypresent
      real * 8 val
      real *8 kappaq
      common/common_kappaQ/ kappaq
      real*8, save :: stored_kappaq
      double precision, save :: stored_modlog_p
      logical, save :: stored_kill_H2, stored_kill_D3
      logical kill_D3,kill_H2
      common/common_kill/kill_D3,kill_H2
      integer run_mode
      logical, save :: ini = .true.
      logical, save :: first_time = .true.
      real*8 powheginput
      external powheginput
      ! If flg_rwl_add is true, the variables to reweight for
      ! need to be set according to the reweighting infos, not 
      ! to the input (even at the very first call)
      logical rwl_modlog_p, rwl_kappaq, rwl_run_mode, recompute_sudakov, recompute_virtuals
      common/minnlo_rwl_add/rwl_modlog_p, rwl_kappaq, rwl_run_mode, recompute_sudakov, recompute_virtuals

      rwl_modlog_p = .false.
      rwl_kappaq = .false.
      rwl_run_mode = .false.
      recompute_sudakov = .false.
      recompute_virtuals = .false.

      ! If flg_rwl_add = .true., additional weights are added to existing
      ! event files. At the first time this subroutine is called, common block
      ! variables have not been initialized yet, so that they should not be stored.
      if(count==0 .and. (.not.flg_rwl_add) ) then
c-----Store the initial settings
         stored_kappaq = kappaq
         stored_kill_H2 = kill_H2
         stored_kill_D3 = kill_D3
         stored_modlog_p = modlog_p
      elseif(count == -1 .and. (.not.flg_rwl_add) ) then
c-----Restore the initial settings
         kappaq = stored_kappaq
         kill_H2 = stored_kill_H2
         kill_D3 = stored_kill_D3
         modlog_p = stored_modlog_p
      elseif(flg_rwl_add.and.(count==0.or.count==-1))then
         return
      else
c-----Do on-the-fly reweighting         
         if(rwl_keypresent(count,'kappaQ',val)) then
            kappaq = val
            if(ini) rwl_kappaq = .true.
         endif

         if(rwl_keypresent(count,'modlog_p',val)) then
            modlog_p = val
            if(ini) rwl_modlog_p = .true.
         endif

         if(rwl_keypresent(count,'run_mode',val)) then
            if(ini)then
               rwl_run_mode = .true.
               if(powheginput('#minnlo').le.0)then
                  write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                  write(*,*) " Event reweighting by run_mode keyword   "
                  write(*,*) " only available if minnlo is on ...      "
                  write(*,*) " setting flg_minnlo to true!!!           "
                  write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                  flg_minnlo=.true.
               endif
               if(powheginput("#rwl_group_events").ne.1)then
                  ! Check that the number of stored events in
                  ! the reweighting is set to 1 for 2loop reweighting
                  write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                  write(*,*) " Error in rwl_setup_param_weights_user:  "
                  write(*,*) " rwl_group_events in the input card      "
                  write(*,*) " must be set to 1 for 2-loop reweighting "
                  write(*,*) " ... fix this and restart!"
                  write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
                  stop
               endif
            endif
            if(val .eq. 1) then
               kill_D3 = .false.
               kill_H2 = .false.
            elseif(val .eq. 2) then
               kill_D3 = .true.
               kill_H2 = .true.
            elseif(val .eq. 4) then
               kill_D3 = .false.
               kill_H2 = .true.
            endif
         endif
         
         if(ini) ini=.false.
      endif
      
      end


