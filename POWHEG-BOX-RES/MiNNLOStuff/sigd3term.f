      subroutine sigd3term(res)
      use MiNLOdebug
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_br.h'
      include 'pwhg_st.h'
      include 'pwhg_flg.h'
      include 'pwhg_pdf.h'
      include 'minnlo_flg.h'
      real * 8 uubsigma ! Cross section in the underlying Born of the Born
      real * 8 pdf1(-pdf_nparton:pdf_nparton),
     1         pdf2(-pdf_nparton:pdf_nparton)
      real * 8 rescfac,tot0
      real * 8 res0(flst_nborn), res(flst_nborn)
      integer bornmult
      real * 8 d3terms,uubjakob
      common/d3terms/d3terms
      integer j
      real * 8 mufact2(flst_nborn)
      integer iborn
      integer, save :: counter=0 
      real * 8 my_mufact2
      common/my_iborn/my_mufact2,iborn
      real *8 tmp, bornAP,resweight
      logical test_rapidity
      logical,save ::ini=.true.
      real * 8  :: powheginput
      external powheginput
      logical nores
      save nores
      real*8 resj_bound
      common/MiNNLO_outliers/resj_bound
      logical makecuts
      real*8 Zajp(0:3,5)

      if(ini)then
         !******************* DEBUG TOOLS ***********************
         ! We need to pass this flag by means of a module or common block:                                                   
         ! the inclusion of phwg_flg.h in subroutines of pdf_tools.f90                                             
         ! will not work for compatibility reasons between F77 and F90                                             
         ! (specifically, the way of splitting a line of code in two lines)                                        
         ! INSTRUCTIONS: Switch this flag on only if you are running with                                          
         ! btildeborn=btildereal=btildecoll=softmismch=0, but btildevirt=1                                         
         ! distribute_by_ub=distribute_by_ub_AP=0, but uubornonly=1                                                
         ! (A) This flag computes the contribution of the D3terms: the result of the                               
         !     integration is just the integral of the D3 part of the cross section                                
         uubornonlyD3=flg_uubornonly
         ! (B) This flag computes the contribution of the Born by using the D1 terms:                              
         !     this check is useful to see if the MiNNLOPS machinery is correctly working                          
         if(flg_uubornonly) then
            write(*,*)
            write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            write(*,*) "!!!!!!!!!!! MiNNLOPS DEBUG !!!!!!!!!!!!"
            uubornonlyD1=.false. !.true. -> Set it by hand if you want to use it                     
            if(uubornonlyD1)then
               ! If you are just computing the Born contribution of the Dterms,
               ! you can choose whether to test the phsp integration of the 
               ! 0-jet system ...
               dbg_phsp=.false. !-> Set it by hand !!! 
               if(dbg_phsp) then
                  write(*,*)"!!  Performing phase space integral  !!"
                  dbg_phsp_with_cuts=.true. !-> Set it by hand !!! 
                  if(dbg_phsp_with_cuts)then
                     ! Use cuts in the projected phsp
                     dbg_phspproj=.true. !-> Set it by hand !!!
                     if(dbg_phspproj) then
                        write(*,*)"!!          using cuts               !!"
                        write(*,*)"!! in the projected UUB phase space  !!"
                     else
                        write(*,*)"!!     using analysis cuts           !!"
                     endif
                  endif
               endif
               ! ... or directly the integral of the matrix element
               if(dbg_phsp)then
                  dbg_ME=.false.
               else
                  write(*,*)"!!  Performing the LO UUB integral   !!"
                  dbg_ME=.true.
                  if(dbg_ME)then
                     ! Use cuts in the projected phsp
                     dbg_phspproj=.false. !-> Set it by hand !!!    
                     if(dbg_phspproj) then
                        write(*,*)"!!          using cuts               !!"
                        write(*,*)"!! in the projected UUB phase space  !!"
                     else
                        write(*,*)"!!     using analysis cuts           !!"
                     endif 
                  endif
               endif
            else
               write(*,*)"!!  Computing D3 terms contribution  !!"
                  
               ! If you are here, you will perform the test over the D3 terms
               ! The D3 terms can be computed by switching the flag flg_uubornonly
               ! on. This will compute them with a uniform distribution over the 
               ! Born+j phsp. If you switch on flg_uubornonly and the flags 
               ! flg_distribute_by_ub (flg_distribute_by_ub_AP) simultaneously,
               ! the d3terms will be distributed accordingly.

               ! You can try to integrate the d3terms by using the cuts of
               ! the projected phase space
               dbg_phspproj=.false. !-> Set it by hand !!!    
            endif
            write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            write(*,*)
         endif
         !******************** END DEBUG ***********************

         if(powheginput("#nores") == 1) then
            nores = .true.
         else
            nores = .false.
         endif

         ini=.false.

      endif

      if(kn_jacborn.eq.0d0) then
         res=0d0
         return
      endif

      test_rapidity=powheginput("#test_rapidity").eq.1

      tot0=0d0

      if(dbg_phsp)goto 111

      do j=1,flst_nborn
         ! The D3 terms are actually initialized once and for all
         ! at the first call of setlocalscales. The sum over the different
         ! flavous has already been done. The result of this call with
         ! imode 3 is put into the common block variable d3terms
         call setlocalscales(j,3,rescfac)

         ! Remember the scales used for each subprocess;
         ! they are needed in the flg_distribute_by_ub case
         mufact2(j)=st_mufact2
         if(flg_distribute_by_ub)then
            call pdfcall(1,kn_xb1,pdf1)
            call pdfcall(2,kn_xb2,pdf2)
            res0(j)=br_born(j) *
     1           pdf1(flst_born(1,j))*pdf2(flst_born(2,j))*kn_jacborn
            tot0=tot0+res0(j)
         endif
      enddo

      if(flg_distribute_by_ub) then
         if(flg_distribute_by_ub_AP) then
            ! The virtual contribution to which d3terms are added is multiplied
            ! by a global weight factor accoutning for the resonance history:
            ! in the ratio res0(j)/br_born(j) this factor is cancelled out and 
            ! so it should be multiplied back. It can be computed outside the loop
            ! since the res0(j) not corresponding to the current resonance
            ! will be zero 
            call bornresweight(flst_ibornresgroup,resweight)
         endif
         ! Arbitrarily choose mufact2(1) to fill my_mufact2 (needed by
         ! evaluubjakob), as in any case, for now, all the entries of mufact2
         ! are the same. For sure this can be coded better.
         my_mufact2 = mufact2(1)
         ! Evaluate uubjakob. If one wants to include d3 in only some
         ! partonic channel, the "iborn" variable (see common block above) can be
         ! used to pass this information to the subroutines in ProjKinematics.f
         if(test_rapidity) then
            uubjakob=1d0
         else
            do j=1,flst_nborn
               if(res0(j) .ne. 0 .and. d3terms .ne. 0d0)then
                  call evaluubjakob(uubjakob,j)
                  goto 9
               endif
            enddo
         endif
 9       continue
         if (uubjakob .eq. -1d10 .and. d3terms .ne. 0d0) then
            counter=counter+1
            write(*,*) 'ERROR: uubjakob not correct, counter:', counter, d3terms
         endif
         ! Uubjakob includes, in the denominator, the sum over all underlying
         ! Born (pp->B+j) channels.  Now supply the d3 terms to each channel,
         ! accordingly to itas underlying Born matrix element.
         do j=1,flst_nborn
            if(uubornonlyD3) res(j)=0d0
            if (res0(j) .ne. 0 .and. (d3terms .ne. 0)) then
               if (uubjakob .ne. -1d10) then
                  if(flg_distribute_by_ub_AP) then
                     call setbornAPfrombrphsp(flst_born(:,j),bornAP)
                     if(.not.uubornonlyD3)then
                        res(j) = res(j) + d3terms * uubjakob * res0(j)/br_born(j)*bornAP * resweight * flst_bornmult(j)
                     else
                        res(j) = d3terms * uubjakob * res0(j)/br_born(j)*bornAP * resweight * flst_bornmult(j)
                     endif
                  else
                     if(.not.uubornonlyD3)then
                        res(j) = res(j) + d3terms * uubjakob * res0(j)
                     else
                        res(j) = d3terms * uubjakob * res0(j)
                     endif
                  endif
               endif
            endif

            
            if (dabs(res(j)) > resj_bound .and. .not.uubornonlyD3) then
               write(*,*) "WARNING:"
               write(*,*) "res(j) too large; usually < ~",resj_bound,":", res(j)
               write(*,*) "setting res(j)=0 for this event/flavour"
               write(*,*) "D3 =",d3terms, " uubjakob =", uubjakob," res0 =", res0(j)
               res(j) = 0d0 
            endif
            if ((res(j)+1 .eq. res(j)) .or. (res(j)-1 .eq. res(j))) then
               write(*,*) "WARNING:"
               write(*,*) "res(j) NaN or Infinity", res(j)
               write(*,*) "setting res(j)=0 for this event/flavour"
               write(*,*) "D3 =",d3terms, " uubjakob =", uubjakob," res0 =", res0(j)
               res(j) = 0d0
            endif

         enddo

      else

 111     continue
         

         do j=1,flst_nborn
            if (res0(j) .ne. 0)then
               call evaluubjakob(uubjakob,j)
               if(uubjakob .eq. -1d10)then
                  write(*,*) 'ERROR: uubjakob not correct'
                  res(:)=0d0
                  return
               endif
               goto 99
            endif
         enddo
 99      continue
         if (.not. flg_uubornonly) then
            res(:) = res(:) + res0(:)*(d3terms/tot0*kn_jacborn*uubjakob)
         else
            ! The resweight*flst_bornmult(j) is necessary, in order to compensate for the 
            ! factor of vol=1/(wind(ind+1)-wind(ind))=flst_nbornresgroup in integration 
            ! subroutine of mint
            ! OLD form of the test  res(:) =res0(:)*(d3terms/tot0*kn_jacborn*uubjakob)

            call bornresweight(flst_ibornresgroup,resweight)
            bornmult=flst_bornmult(flst_ibornresgroup)
            if(uubornonlyD1.and.dbg_phsp)then
               do j=1,flst_nborn
                  if(flst_ibornresgroup.eq.flst_bornresgroup(j))then
                     if(dbg_ptb.lt.sqrt(kn_sborn)/8d0)then 
                        if(nores)then
                           res(j) =kn_jacborn*uubjakob/(sqrt(kn_sborn)/8d0)/flst_nborn 
                        else
                           res(j) =kn_jacborn*uubjakob/(sqrt(kn_sborn)/8d0)/(flst_nborn/flst_nbornresgroup)*resweight* bornmult
                        endif
                     else
                        res(j)=0d0
                     endif
                  else
                     res(j)=0d0
                  endif
               enddo
            elseif(uubornonlyD1.and.dbg_ME)then  
               if(dbg_ptb.lt.dbg_mb)then 
                  if(nores)then
                     res(:)=d3terms*kn_jacborn*uubjakob/dbg_mb/flst_nborn
                  else
                     res(:)=d3terms*kn_jacborn*uubjakob/dbg_mb/flst_nborn*resweight*bornmult
                  endif
               else
                  res(:)=0d0
               endif
            elseif(uubornonlyD3)then
               if(nores)then
                  res(:)=d3terms*kn_jacborn*uubjakob/flst_nborn
               else
                  res(:)=d3terms*kn_jacborn*uubjakob/flst_nborn*resweight*bornmult
               endif
            else
               write(*,*) "Unknown test for debug. Make sure that you"
               write(*,*) "have set all necessary flags to the proper"
               write(*,*) "value ... "
               stop
            endif

         endif
      endif

      end
