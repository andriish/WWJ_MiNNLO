!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!  This file is important to set all process-specific things, like flavour scheme     !!!!!!
!!!!!!  (setting nf=4 for 4FS or nf=5 for 5FS for instance), additional phase-space cuts,  !!!!!!
!!!!!!  0,1,2-loop matrix elements for pp->F etc...                                        !!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      function get_nf_for_process()
      implicit none
      integer get_nf_for_process, nf
      write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      write(*,*) "get_nf_for_process():
     f process_dependent_minnlo not implemented."
      write(*,*) "-> using nf=5 "
      write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      nf=5
      get_nf_for_process = nf
      end
      
      function impose_born_cut(p)
      implicit none
      include 'nlegborn.h'
      integer, parameter :: nlegs=nlegbornexternal-1
      logical impose_born_cut
      real * 8 p(0:3,nlegs)
      ! if your process requires does not require any additional cut in the FJ phase space,
      ! just comment out the following lines:
      write(*,*) "impose_born_cut(p): 
     f process_dependent_minnlo not implemented. Exiting"
      stop
      ! and uncomment:
!      impose_born_cut = .false.
      end
      
      subroutine cuts_on_uuborn(p_uub,p_ub,pt_a_cut,cuts_passed)
      implicit none
      include 'nlegborn.h'
      integer, parameter :: nlegs=nlegbornexternal-1
      real*8 p_uub(0:3,nlegs),p_ub(0:3,nlegs)
      logical cuts_passed
      real*8 pt_a_cut
      ! if your process requires does not require any additional cut in the projected (uub)
      ! F phase space,just comment out the following lines:
      write(*,*) "cuts_on_uuborn: 
     f process_dependent_minnlo not implemented. Exiting"
      stop
      ! and uncomment:
!      cuts_passed=.true.
      end

      subroutine init_process_dependent()
      implicit none
      real*8 resj_bound
      common/MiNNLO_outliers/resj_bound
      ! Set upper bound for values of res(j) for them to be consider as anomalous ones
      resj_bound=1d99 ! need to set this value in case of instabilities/outliers !!!

      call init_masses_Dterms()
      
      ! use this routine to initialize anything apart from the amplitudes
      write(*,*) "init_process_dependent(): DUMMY SUBROUTINE"
      stop
      end

      subroutine init_masses_Dterms()
      use internal_parameters, pi_hoppet => pi, cf_hoppet => cf, ca_hoppet => ca, tf_hoppet => tf
      implicit none
      include 'PhysPars.h'
      write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      write(*,*) "init_masses_Dterms(): DUMMY SUBROUTINE"
      write(*,*) "-> using Zmass for initialization"
      write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      call set_masses(ph_Zmass)
      !stop      
      end

c DUMMY routine for bmass used in setlocalscales.f for Higgs
      subroutine bmass_in_minlo(bfact,alphas)
      implicit none
      double precision bfact, alphas
      write(*,*) "bmass_in_minlo(bfact,alphas): DUMMY SUBROUTINE"
      stop      
      end

      function get_M_for_init_Dterms()
      implicit none
      double precision get_M_for_init_Dterms
      include 'PhysPars.h'
      write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      write(*,*) "get_M_for_init_Dterms(): DUMMY SUBROUTINE"
      write(*,*) "-> using Zmass for initialization"
      write(*,*) "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      get_M_for_init_Dterms = ph_zmass
      !stop      
      end

      subroutine get_uub_flav(flav,uub_flav)
      implicit none
      include 'nlegborn.h'
      integer flav(1:nlegborn)
      integer uub_flav(1:nlegbornexternal-1)
      write(*,*) "get_uub_flav_qqb(flav,uub_flav): DUMMY SUBROUTINE"
      stop
      end

      subroutine uub_phasespace_check(p,check)
      implicit none
      logical check
      include 'nlegborn.h'
      integer, parameter :: nlegs=nlegbornexternal-1
      real*8 p(0:3,nlegs)
      write(*,*) "uub_phasespace_check(p,check): DUMMY SUBROUTINE"
      stop
      end

c     matrix elements to test UUB projection (blank for DY)
      subroutine uub_for_minnlo(pphy,iborn,amp2)
      implicit none
      include 'pwhg_math.h'
      include 'pwhg_st.h'
      include 'PhysPars.h'
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      real* 8 amp2
      integer nlegs
      parameter (nlegs=4)
      integer  iborn
      real *8 pphy(0:3,nlegs)
      write(*,*) "uub_for_minnlo(pphy,iborn,amp2): 
     f process_dependent_minnlo not implemented. Exiting"
      stop
      end

!!!!!! most relevant routines to be added to this file for 0,1,2-loop virtuals of pp->F !!!!!!
!      --> add extra routuines if needed
      
      subroutine init_amplitude()
      implicit none
      ! use this for initialization, can remain dummy routine for trivial processes like DY or Higgs
      return
      end

      subroutine get_B_V1_V2(pborn_UUB,msqB,msqV1,msqV2,iflav)
      implicit none
      include 'nlegborn.h'
      integer, parameter :: nlegs=nlegbornexternal-1
      integer, parameter :: nflav=5
      double precision msqB(-nflav:nflav,-nflav:nflav), msqV1(-nflav:nflav,-nflav:nflav)             
      double precision,optional::  msqV2(-nflav:nflav,-nflav:nflav)
      integer,optional :: iflav
      double precision pborn_UUB(0:3,nlegs)
      write(*,*) "get_B_V1_V2(pborn_UUB,msqB,msqV1,msqV2,iflav): 
     f process_dependent_minnlo not implemented. Exiting"
      stop
      end

