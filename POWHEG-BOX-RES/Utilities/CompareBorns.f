c Compare born, bmunu, bornjk,    b,bmunu,bjk  with  cb,cbmunu,cbjk
      subroutine compareBorns(n,flav,p,b,bmunu,bjk, cb,cbmunu,cbjk)
      implicit none
      integer n
      real * 8 p(0:3,n),b,cb,bjk(n,n),cbjk(n,n),
     1      bmunu(0:3,0:3,n),cbmunu(0:3,0:3,n)
      integer flav(n)
      real * 8, parameter:: tiny=1d-6
      integer j,k,mu,nu
      integer mu1,mu2,mu3,iep1,iep2
      real * 8 eperp1(0:3),eperp2(0:3),eperp(0:3,2)
      equivalence (eperp1(0),eperp(0,1)),(eperp2(0),eperp(0,2))
      real * 8 pr,cpr,tmp,dotp
      logical is_coloured
      
      if(.not. comparenums(b,cb)) then
         write(*,*) ' compareBorns: bors differ: ',b,cb
      endif
      do j=1,n
         do k=1,n
c The POWHEG BOX does not use the diagonal entries            
            if(j/=k) then
               if(is_coloured(flav(j)) .and. is_coloured(flav(k))) then
                  if(.not. comparenums(bjk(j,k),cbjk(j,k)) ) then
                     write(*,100) ' compareBorns: b(',j,',',k,') differ: ',
     1                    bjk(j,k),cbjk(j,k)
 100                 format(a,i1,a,i1,a,d14.8,1x,d14.8)
                  endif
               endif
            endif
         enddo
      enddo
c     Compare bmunu; keep in mind that they may differ by gauge transformation.
c     So, we build two spacelike vectors ep1 and ep2, orthogonal among them
c     and orthogonal to the momentum, and make sure that the
c     bmunu's contracted with ep1-ep1, ep2-ep2, ep1-ep2, ep2-ep1 give the same result.
      do j=1,n
         if( flav(j) == 0 .or. flav(j) == 22 ) then
c     First make sure that bmunu and cbmunu are transverse in both indices
            do mu=0,3
               if( .not. (   zerodotp(bmunu(mu,:,j),p(:,j)) 
     1              .and. zerodotp(bmunu(:,mu,j),p(:,j)) ) ) then
                  write(*,*) ' compareBorns: bmunu is not transverse'
               endif
               if( .not. (   zerodotp(cbmunu(mu,:,j),p(:,j)) 
     1              .and. zerodotp(cbmunu(:,mu,j),p(:,j)) ) ) then
                  write(*,*) ' compareBorns: cbmunu is not transverse'
               endif
            enddo
c find the largest component of the j momentum               
            mu3=1
            do mu=1,3
               if(abs(p(mu,j)) > abs(p(mu3,j))) then
                  mu3 = mu
               endif
            enddo
c     find the other two indices            
            mu1=-1
            do mu=1,3
               if(mu /= mu3) then
                  if(mu1 == -1) then
                     mu1 = mu
                  else
                     mu2 = mu
                  endif
               endif
            enddo
c     this typically happens for initial state gluons
            if(p(mu1,j)==0 .and. p(mu2,j) == 0) then
               eperp1 = 0
               eperp2 = 0
               eperp1(mu1)=1
               eperp2(mu2)=1
            else
c     Build eperp1 orthogonal to p(:,j), only space components               
c     make sure components are not too small
               eperp1 = 0
               tmp = abs(p(mu2,j))+abs(p(mu1,j))
               eperp1(mu1) = - p(mu2,j)/tmp
               eperp1(mu2) = p(mu1,j)/tmp
c     take eperp2 equal to the cross product of p(1:3,j)/p(3,j) and eperp1;
c     remember that eperp1(3)=0. Take the order mu1,mu2,mu3 (it doesn't matter)
               eperp2=0
c     mu1 = mu2 mu3 - mu3 mu2
               eperp2(mu1) = eperp1(mu2)
c     mu2 = mu3 mu1 - mu1 mu3
               eperp2(mu2) = -eperp1(mu1)
c     mu3 = mu1 mu2 - mu2 mu1
               eperp2(mu3) = (eperp1(mu1)*p(mu2,j)-eperp1(mu2)*p(mu1,j))
     1              /p(mu3,j)
c     check for orthogonality:
               tmp = abs(dotp(eperp1,eperp2))+
     1               abs(dotp(eperp1,p(:,j)))/p(mu3,j)+
     2              abs(dotp(eperp2,p(:,j)))/p(mu3,j)
               if(tmp>1d-10) then
                  write(*,*) ' orthogonality check did non work'
                  write(*,*) eperp1
                  write(*,*) eperp2
                  write(*,*) p(:,j)
               endif
            endif
c     eperp is equivalenced to eperp1, eperp2
c     eperp(:,1)=eperp1
c     eperp(:,2)=eperp2
            do iep1=1,2
               do iep2=1,2
                  pr=mult(bmunu(:,:,j),eperp(:,iep1),eperp(:,iep2))
                  cpr=mult(cbmunu(:,:,j),eperp(:,iep1),eperp(:,iep2))
                  if(.not. comparenums(pr,cpr)) then
                     write(*,100) ' compareBorns: bmunu(',iep1,',',iep2,
     1               ') differ: ',pr,cpr
                     write(*,101) 'ep1 ',eperp1(1:3)
                     write(*,101) 'ep2 ',eperp2(1:3)
                     write(*,101) 'p   ',p(1:3,j)
                     write(*,*) 'j=',j,' flav:',flav
 101                 format(a,3(d10.4,2x))
                  endif
               enddo
            enddo
         endif
      enddo
      contains
      logical function comparenums(a,b)
      real * 8 a,b
      real * 8 absum
      absum = abs(a)+abs(b)
      comparenums = .true.
      if(absum /=0) then
         if(abs(a-b)/absum > 1d-6) then
            comparenums = .false.
         endif
      endif
      end function comparenums
      real * 8 function mult(tens,v1,v2)
      real * 8 tens(0:3,0:3),v1(0:3),v2(0:3)
      integer mu,nu
      mult = 0
      do mu=1,3
         do nu=1,3
            mult = mult + tens(mu,nu)*v1(mu)*v2(nu)
         enddo
      enddo
      end function mult
      logical function zerodotp(v1,v2)
c     Checks that v1.v2 is zero,
      real * 8 v1(0:3),v2(0:3),res
      res = v1(0)*v2(0)-v1(1)*v2(1)-v1(2)*v2(2)-v1(3)*v2(3)
      if(res == 0) then
         zerodotp = .true.
      else
c Normalize so that result is generally of order 1
         res = abs(res)/(sum(abs(v1))*sum(abs(v2)))
         if(res < 1d-10) then
            zerodotp = .true.
         else
            zerodotp = .false.
            write(*,*) ' zerodotp:',res,v1,v2
         endif
      endif
      end function zerodotp
      end
      
