In the POWHEG-BOX-RES framework, there is a mechanism to identify
equivalent amplitudes (for example, differing only by irrelevant
flavour labels). By default, when the program starts, POWHEG checks if
two or more amplitudes are equal or proportional, by comparing their
values on the first 20 set of momenta, randomly chosen by the Monte
Carlo.

Starting with revision 3808 some options have been added in order to
fix the equivalent-amplitude information at compile time. The
tolerance parameters to determine if two amplitudes are related can be
overridden by the user, by adding the lines

compare_vecsb_ep <value> ! tolerance parameter for born amplitudes
compare_vecsr_ep <value> ! tolerance parameter for real amplitudes
compare_vecsv_ep <value> ! tolerance parameter for virtual amplitudes

The default for each parameter above is 1d-10.

In addition, the equivalence relations can now be hardcoded once and for all
in the generated code.  In order to do this, the developer of a process under
the POWHEG-BOX-RES should run the code with

writeequivfile 1

in the powheg.input file

When the program calls the Born, the real and the virtual matrix elements,
the computed equivalence relations are written in the following files (in the
run directory)

sigequiv_hook-born-XXXX.f
sigequiv_hook-virt-XXXX.f
sigequiv_hook-btl-XXXX.f
sigequiv_hook-rad-XXXX.f

The -XXXX label stands for the seed number in case of a "manyseeds"
runs. For single runs this label is absent. Meanwhile, the files

sigborn_equiv-XXXX
sigvirtual_equiv-XXXX
sigreal_btl0_equiv-XXXX
sigreal_rad_equiv-XXXX

contain a (more human-readable) list of the equivalent configurations.  By
inspecting the sigequiv*.f file it should be clear what is going on.

Please note that, in order for these files to be created, the program has to
run with enough phase-space points.

It can happen that the equivalent relations found in one run are different
from those in another run, due to the different phase-space points where the
amplitudes are evaluated during the comparison stage.

The developer should then inspect the files of the human-readable equivalent
configurations and decide which one has to be hardcoded in the main
code. This should be done for the Born, the real and the virtual amplitudes.

Once this is done, the subroutines in the runs that have produced the
"correct" equivalent relations should be copied in the file sigequiv_hook.f,
in the process directory.
This file will supersede the file in the POWHEG-BOX-RES directory at
compilation time.


If the developer-generated sigequiv_hook.f is present, in subsequent runs no
*equiv* files will be generated at all.

