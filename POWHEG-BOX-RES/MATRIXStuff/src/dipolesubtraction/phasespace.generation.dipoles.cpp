#include "header.hpp"

void phasespace_set::ac_psp_RA_dipoles(){
  static Logger logger("phasespace_set::ac_psp_RA_dipoles");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (switch_off_RS_mapping){return;}
  
  if ((*RA_dipole)[RA_x_a].type_dipole() == 1 && !switch_off_RS_mapping_ij_k){
    if ((*RA_dipole)[RA_x_a].massive() == 1){ac_psp_RA_group_ij_k_massive((*RA_dipole), RA_x_a);}
    else {ac_psp_RA_group_ij_k((*RA_dipole), RA_x_a);}
  }
  else if ((*RA_dipole)[RA_x_a].type_dipole() == 2 && !switch_off_RS_mapping_ij_a){
    if ((*RA_dipole)[RA_x_a].massive() == 1){ac_psp_RA_group_ij_a_massive((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);}
    else {ac_psp_RA_group_ij_a((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);}
  }
  else if ((*RA_dipole)[RA_x_a].type_dipole() == 3 && !switch_off_RS_mapping_ai_k){
    if ((*RA_dipole)[RA_x_a].massive() == 1){ac_psp_RA_group_ai_k_massive((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);}
    else {ac_psp_RA_group_ai_k((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);}
  }
  else if ((*RA_dipole)[RA_x_a].type_dipole() == 5){
    ac_psp_RA_group_ai_b((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);
  }
  
  /*
  if      ((*RA_dipole)[RA_x_a].type_dipole() == 1 && (*RA_dipole)[RA_x_a].massive() == 1){ac_psp_RA_group_ij_k_massive((*RA_dipole), RA_x_a);}
  else if ((*RA_dipole)[RA_x_a].type_dipole() == 2 && (*RA_dipole)[RA_x_a].massive() == 1){ac_psp_RA_group_ij_a_massive((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);}
  else if ((*RA_dipole)[RA_x_a].type_dipole() == 3 && (*RA_dipole)[RA_x_a].massive() == 1){ac_psp_RA_group_ai_k_massive((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);}
  else if ((*RA_dipole)[RA_x_a].type_dipole() == 1 && (*RA_dipole)[RA_x_a].massive() == 0){ac_psp_RA_group_ij_k((*RA_dipole), RA_x_a);}
  else if ((*RA_dipole)[RA_x_a].type_dipole() == 2 && (*RA_dipole)[RA_x_a].massive() == 0){ac_psp_RA_group_ij_a((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);}
  else if ((*RA_dipole)[RA_x_a].type_dipole() == 3 && (*RA_dipole)[RA_x_a].massive() == 0){ac_psp_RA_group_ai_k((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);}
  else if ((*RA_dipole)[RA_x_a].type_dipole() == 5){ac_psp_RA_group_ai_b((*RA_dipole), dipole_sinx_min[RA_x_a], RA_x_a);}
  */
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void phasespace_set::ag_psp_RA_dipoles(){
  static Logger logger("phasespace_set::ag_psp_RA_dipoles");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (switch_off_RS_mapping){return;}
  
  for (int i_a = 1; i_a < n_dipoles; i_a++){
    if ((*RA_dipole)[i_a].type_dipole() == 1 && !switch_off_RS_mapping_ij_k){
      if ((*RA_dipole)[i_a].massive() == 1){ag_psp_RA_group_ij_k_massive((*RA_dipole), i_a);}
      else {ag_psp_RA_group_ij_k((*RA_dipole), i_a);}
    }
    else if ((*RA_dipole)[i_a].type_dipole() == 2 && !switch_off_RS_mapping_ij_a){
      if ((*RA_dipole)[i_a].massive() == 1){ag_psp_RA_group_ij_a_massive(dipole_sinx_min[i_a], (*RA_dipole), i_a);}
      else {ag_psp_RA_group_ij_a(dipole_sinx_min[i_a], (*RA_dipole), i_a);}
    }
    else if ((*RA_dipole)[i_a].type_dipole() == 3 && !switch_off_RS_mapping_ai_k){
      if ((*RA_dipole)[i_a].massive() == 1){ag_psp_RA_group_ai_k_massive(dipole_sinx_min[i_a], (*RA_dipole), i_a);}
      else {ag_psp_RA_group_ai_k(dipole_sinx_min[i_a], (*RA_dipole), i_a);}
    }
    else if ((*RA_dipole)[i_a].type_dipole() == 5 && !switch_off_RS_mapping_ai_b){
      ag_psp_RA_group_ai_b(dipole_sinx_min[i_a], (*RA_dipole), i_a);
    }

    /*
    if      ((*RA_dipole)[i_a].type_dipole() == 1 && (*RA_dipole)[i_a].massive() == 1){ag_psp_RA_group_ij_k_massive((*RA_dipole), i_a);}
    else if ((*RA_dipole)[i_a].type_dipole() == 2 && (*RA_dipole)[i_a].massive() == 1){ag_psp_RA_group_ij_a_massive(dipole_sinx_min[i_a], (*RA_dipole), i_a);}
    else if ((*RA_dipole)[i_a].type_dipole() == 3 && (*RA_dipole)[i_a].massive() == 1){ag_psp_RA_group_ai_k_massive(dipole_sinx_min[i_a], (*RA_dipole), i_a);}
    else if ((*RA_dipole)[i_a].type_dipole() == 1 && (*RA_dipole)[i_a].massive() == 0){ag_psp_RA_group_ij_k((*RA_dipole), i_a);}
    else if ((*RA_dipole)[i_a].type_dipole() == 2 && (*RA_dipole)[i_a].massive() == 0){ag_psp_RA_group_ij_a(dipole_sinx_min[i_a], (*RA_dipole), i_a);}
    else if ((*RA_dipole)[i_a].type_dipole() == 3 && (*RA_dipole)[i_a].massive() == 0){ag_psp_RA_group_ai_k(dipole_sinx_min[i_a], (*RA_dipole), i_a);}
    else if ((*RA_dipole)[i_a].type_dipole() == 5){ag_psp_RA_group_ai_b(dipole_sinx_min[i_a], (*RA_dipole), i_a);}
    */
    
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
