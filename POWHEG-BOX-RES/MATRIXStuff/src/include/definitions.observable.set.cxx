#define osi_process_class (oset.csi->process_class)
#define osi_decay (oset.csi->decay)
#define osi_type_perturbative_order (oset.csi->type_perturbative_order)
#define osi_type_contribution (oset.csi->type_contribution)
#define osi_type_correction (oset.csi->type_correction)
#define osi_contribution_order_alpha_s (oset.csi->contribution_order_alpha_s)
#define osi_contribution_order_alpha_e (oset.csi->contribution_order_alpha_e)
#define osi_contribution_order_interference (oset.csi->contribution_order_interference)
#define osi_subprocess (oset.csi->subprocess)

#define osi_n_particle (oset.csi->n_particle)
#define osi_type_parton (oset.csi->type_parton)
#define osi_process_type (oset.csi->process_type)

/*
#define osi_process_class (oset.csi.process_class)
#define osi_decay (oset.csi.decay)
#define osi_type_perturbative_order (oset.csi.type_perturbative_order)
#define osi_type_contribution (oset.csi.type_contribution)
#define osi_type_correction (oset.csi.type_correction)
#define osi_contribution_order_alpha_s (oset.csi.contribution_order_alpha_s)
#define osi_contribution_order_alpha_e (oset.csi.contribution_order_alpha_e)
#define osi_contribution_order_interference (oset.csi.contribution_order_interference)
#define osi_subprocess (oset.csi.subprocess)
*/
/*
#define osi_object_list (oset.esi.object_list)
#define osi_n_observed_min (oset.esi.n_observed_min)
#define osi_n_observed_max (oset.esi.n_observed_max)
#define osi_define_pT (oset.esi.define_pT)
#define osi_define_ET (oset.esi.define_ET)
#define osi_define_eta (oset.esi.define_eta)
#define osi_define_y (oset.esi.define_y)

#define osi_relevant_object_list (oset.esi.relevant_object_list)
#define osi_relevant_n_observed_min (oset.esi.relevant_n_observed_min)
#define osi_relevant_n_observed_max (oset.esi.relevant_n_observed_max)
#define osi_relevant_define_pT (oset.esi.relevant_define_pT)
#define osi_relevant_define_ET (oset.esi.relevant_define_ET)
#define osi_relevant_define_eta (oset.esi.relevant_define_eta)
#define osi_relevant_define_y (oset.esi.relevant_define_y)
*/
/*
#define osi_process_class (oset.process_class)
//#define osi_decay (oset.decay)
#define osi_subprocess (oset.subprocess)
#define osi_type_perturbative_order (oset.type_perturbative_order)
#define osi_type_contribution (oset.type_contribution)
#define osi_type_correction (oset.type_correction)
#define osi_contribution_order_alpha_s (oset.contribution_order_alpha_s)
#define osi_contribution_order_alpha_e (oset.contribution_order_alpha_e)
#define osi_contribution_order_interference (oset.contribution_order_interference)
*/
#define osi_object_list (oset.object_list)
#define osi_n_observed_min (oset.n_observed_min)
#define osi_n_observed_max (oset.n_observed_max)
#define osi_define_pT (oset.define_pT)
#define osi_define_ET (oset.define_ET)
#define osi_define_eta (oset.define_eta)
#define osi_define_y (oset.define_y)

#define osi_relevant_object_list (oset.relevant_object_list)
#define osi_relevant_n_observed_min (oset.relevant_n_observed_min)
#define osi_relevant_n_observed_max (oset.relevant_n_observed_max)
#define osi_relevant_define_pT (oset.relevant_define_pT)
#define osi_relevant_define_ET (oset.relevant_define_ET)
#define osi_relevant_define_eta (oset.relevant_define_eta)
#define osi_relevant_define_y (oset.relevant_define_y)




//#define oset (pset.oset())
//#define osi_model (oset.model)
#define osi_msi (oset.msi)

#define osi_switch_qTcut (oset.switch_qTcut)
#define osi_n_qTcut (oset.n_qTcut)
#define osi_min_qTcut (oset.min_qTcut)
#define osi_step_qTcut (oset.step_qTcut)
#define osi_value_qTcut (oset.value_qTcut)
#define osi_selection_qTcut_distribution (oset.selection_qTcut_distribution)
#define osi_no_qTcut_distribution (oset.no_qTcut_distribution)
#define osi_value_qTcut_distribution (oset.value_qTcut_distribution)

#define osi_scale_ren (oset.scale_ren)
#define osi_scale_fact (oset.scale_fact)

#define osi_needed_scale2_ren (oset.needed_scale2_ren)
#define osi_needed_scale2_fact (oset.needed_scale2_fact)

#define osi_switch_TSV (oset.switch_TSV)

#define osi_n_set_TSV (oset.n_set_TSV)
#define osi_n_extended_set_TSV (oset.n_extended_set_TSV)
#define osi_name_set_TSV (oset.name_set_TSV)
#define osi_name_extended_set_TSV (oset.name_extended_set_TSV)
#define osi_central_scale_ren_TSV (oset.central_scale_ren_TSV)
#define osi_central_scale_fact_TSV (oset.central_scale_fact_TSV)
#define osi_relative_central_scale_ren_TSV (oset.relative_central_scale_ren_TSV)
#define osi_relative_central_scale_fact_TSV (oset.relative_central_scale_fact_TSV)
#define osi_n_scale_ren_TSV (oset.n_scale_ren_TSV)
#define osi_n_scale_fact_TSV (oset.n_scale_fact_TSV)
#define osi_factor_scale_ren_TSV (oset.factor_scale_ren_TSV)
#define osi_factor_scale_fact_TSV (oset.factor_scale_fact_TSV)
#define osi_dynamic_scale_ren_TSV (oset.dynamic_scale_ren_TSV)
#define osi_dynamic_scale_fact_TSV (oset.dynamic_scale_fact_TSV)
#define osi_min_qTcut_TSV (oset.min_qTcut_TSV)
#define osi_max_qTcut_TSV (oset.max_qTcut_TSV)
#define osi_switch_distribution_at_all_TSV (oset.switch_distribution_at_all_TSV)
#define osi_switch_distribution_TSV (oset.switch_distribution_TSV)
#define osi_max_n_integrand_TSV (oset.max_n_integrand_TSV)
#define osi_min_qTcut_distribution_TSV (oset.min_qTcut_distribution_TSV)
#define osi_max_qTcut_distribution_TSV (oset.max_qTcut_distribution_TSV)
#define osi_switch_moment_TSV (oset.switch_moment_TSV)
#define osi_no_central_scale_ren_TSV (oset.no_central_scale_ren_TSV)
#define osi_no_central_scale_fact_TSV (oset.no_central_scale_fact_TSV)
#define osi_relative_scale_ren_TSV (oset.relative_scale_ren_TSV)
#define osi_relative_scale_fact_TSV (oset.relative_scale_fact_TSV)
#define osi_filename_integration_TSV (oset.filename_integration_TSV )
#define osi_filename_result_TSV (oset.filename_result_TSV)
#define osi_filename_distribution_TSV (oset.filename_distribution_TSV)
#define osi_filename_moment_TSV (oset.filename_moment_TSV)

#define osi_n_scale_TSV (oset.n_scale_TSV)
#define osi_max_n_scale_ren_TSV (oset.max_n_scale_ren_TSV)
#define osi_max_n_scale_fact_TSV (oset.max_n_scale_fact_TSV)
#define osi_n_dynamic_scale_TSV (oset.n_dynamic_scale_TSV)
#define osi_switch_dynamic_scale_TSV (oset.switch_dynamic_scale_TSV)
#define osi_dynamic_scale_TSV (oset.dynamic_scale_TSV)
#define osi_relative_central_scale_TSV (oset.relative_central_scale_TSV)
#define osi_factor_scale_TSV (oset.factor_scale_TSV)
#define osi_central_scale_TSV (oset.central_scale_TSV)
//#define osi_name_TSV (oset.name_TSV)


/*
#define osi_switch_qTcut (oset.switch_qTcut())
#define osi_n_qTcut (oset.n_qTcut())
#define osi_min_qTcut (oset.min_qTcut())
#define osi_step_qTcut (oset.step_qTcut())
#define osi_value_qTcut (oset.value_qTcut())

#define osi_scale_ren (oset.scale_ren())
#define osi_scale_fact (oset.scale_fact())

#define osi_switch_TSV (oset.switch_TSV())

#define osi_n_set_TSV (oset.n_set_TSV())
#define osi_name_set_TSV (oset.name_set_TSV())
#define osi_central_scale_ren_TSV (oset.central_scale_ren_TSV())
#define osi_central_scale_fact_TSV (oset.central_scale_fact_TSV())
#define osi_relative_central_scale_ren_TSV (oset.relative_central_scale_ren_TSV())
#define osi_relative_central_scale_fact_TSV (oset.relative_central_scale_fact_TSV())
#define osi_n_scale_ren_TSV (oset.n_scale_ren_TSV())
#define osi_n_scale_fact_TSV (oset.n_scale_fact_TSV())
#define osi_factor_scale_ren_TSV (oset.factor_scale_ren_TSV())
#define osi_factor_scale_fact_TSV (oset.factor_scale_fact_TSV())
#define osi_dynamic_scale_ren_TSV (oset.dynamic_scale_ren_TSV())
#define osi_dynamic_scale_fact_TSV (oset.dynamic_scale_fact_TSV())
#define osi_min_qTcut_TSV (oset.min_qTcut_TSV())
#define osi_max_qTcut_TSV (oset.max_qTcut_TSV())
#define osi_switch_distribution_at_all_TSV (oset.switch_distribution_at_all_TSV())
#define osi_switch_distribution_TSV (oset.switch_distribution_TSV())
#define osi_max_n_integrand_TSV (oset.max_n_integrand_TSV())
#define osi_min_qTcut_distribution_TSV (oset.min_qTcut_distribution_TSV())
#define osi_max_qTcut_distribution_TSV (oset.max_qTcut_distribution_TSV())
#define osi_switch_moment_TSV (oset.switch_moment_TSV())
#define osi_no_central_scale_ren_TSV (oset.no_central_scale_ren_TSV())
#define osi_no_central_scale_fact_TSV (oset.no_central_scale_fact_TSV())
#define osi_relative_scale_ren_TSV (oset.relative_scale_ren_TSV())
#define osi_relative_scale_fact_TSV (oset.relative_scale_fact_TSV())
#define osi_filename_integration_TSV (oset.filename_integration_TSV ())
#define osi_filename_result_TSV (oset.filename_result_TSV())
#define osi_filename_distribution_TSV (oset.filename_distribution_TSV())
#define osi_filename_moment_TSV (oset.filename_moment_TSV())
*/

#define osi_filename_result (oset.filename_result)
#define osi_filename_moment (oset.filename_moment)
#define osi_filename_integration (oset.filename_integration)
#define osi_filename_maxevent (oset.filename_maxevent)
#define osi_filename_comparison (oset.filename_comparison)
#define osi_filename_gnuplot (oset.filename_gnuplot)
#define osi_filename_makegnuplot (oset.filename_makegnuplot)
#define osi_filename_proceeding (oset.filename_proceeding)
#define osi_filename_proceeding_2 (oset.filename_proceeding_2)
#define osi_filename_execution (oset.filename_execution)
#define osi_filename_distribution_all_CV (oset.filename_distribution_all_CV)
#define osi_filename_integration_CV (oset.filename_integration_CV)
#define osi_filename_distribution (oset.filename_distribution)
#define osi_filename_distribution_CV (oset.filename_distribution_CV)
#define osi_filename_dddistribution (oset.filename_dddistribution)
#define osi_filename_dddistribution_CV (oset.filename_dddistribution_CV)

//  Logger *logger;
//#define osi_n_particle (oset.n_particle)

#define osi_n_ps (oset.n_ps)
#define osi_n_pc (oset.n_pc)
#define osi_n_pz (oset.n_pz)
#define osi_relation_pc_ps (oset.relation_pc_ps)

#define osi_cut_ps (oset.cut_ps)
#define osi_first_non_cut_ps (oset.first_non_cut_ps)

#define osi_n_moments (oset.n_moments)
#define osi_QCD_order (oset.QCD_order)
#define osi_active_qTcut (oset.active_qTcut)
#define osi_output_n_qTcut (oset.output_n_qTcut)

#define osi_counter_killed_qTcut (oset.counter_killed_qTcut)
#define osi_counter_acc_qTcut (oset.counter_acc_qTcut)

//#define osi_n_qTcut (oset.n_qTcut)

#define osi_alpha_S (oset.alpha_S)

#define osi_switch_VI (oset.switch_VI)
#define osi_switch_VI_bosonic_fermionic (oset.switch_VI_bosonic_fermionic)
#define osi_switch_KP (oset.switch_KP)
#define osi_switch_CM (oset.switch_CM)
#define osi_switch_OL (oset.switch_OL)
#define osi_switch_RCL (oset.switch_RCL)
#define osi_switch_H1gg (oset.switch_H1gg)
#define osi_switch_H2 (oset.switch_H2)

#define osi_switch_polenorm (oset.switch_polenorm)

#define osi_switch_old_qT_version (oset.switch_old_qT_version)

#define osi_switch_RS (oset.switch_RS)
#define osi_switch_RS_mapping (oset.switch_RS_mapping)

#define osi_switch_testcut (oset.switch_testcut)

//#define osi_switch_resum (oset.switch_resum) // !!! check if replaced !!!
#define osi_switch_resum (oset.switch_resummation)
#define osi_switch_resummation (oset.switch_resummation)

//#define osi_switch_distribution (oset.switch_distribution)

#define osi_switch_result (oset.switch_result)
#define osi_switch_distribution (oset.switch_distribution)
#define osi_switch_moment (oset.switch_moment)

#define osi_switch_output_execution (oset.switch_output_execution)
#define osi_switch_output_integration (oset.switch_output_integration)
#define osi_switch_output_maxevent (oset.switch_output_maxevent)
#define osi_switch_output_comparison (oset.switch_output_comparison)
#define osi_switch_output_gnuplot (oset.switch_output_gnuplot)
#define osi_switch_output_proceeding (oset.switch_output_proceeding)
#define osi_switch_output_weights (oset.switch_output_weights)
#define osi_switch_output_result (oset.switch_output_result)
#define osi_switch_output_distribution (oset.switch_output_distribution)

#define osi_switch_output_cancellation_check (oset.switch_output_cancellation_check)
#define osi_switch_output_testpoint (oset.switch_output_testpoint)
#define osi_switch_output_cutinfo (oset.switch_output_cutinfo)




#define osi_max_dyn_ren (oset.max_dyn_ren)
#define osi_max_dyn_fact (oset.max_dyn_fact)
#define osi_max_scale_dyn_ren (oset.max_scale_dyn_ren)
#define osi_max_scale_dyn_fact (oset.max_scale_dyn_fact)
#define osi_n_scale_dyn_ren (oset.n_scale_dyn_ren)
#define osi_n_scale_dyn_fact (oset.n_scale_dyn_fact)

#define osi_no_value_ren_TSV (oset.no_value_ren_TSV)
#define osi_no_value_fact_TSV (oset.no_value_fact_TSV)

#define osi_value_no_central_scale_fact (oset.value_no_central_scale_fact) // where is 'central' scale in each DS set
#define osi_value_central_scale_fact (oset.value_central_scale_fact)
#define osi_value_central_logscale2_fact (oset.value_central_logscale2_fact)




#define osi_value_relative_scale_ren (oset.value_relative_scale_ren)
#define osi_value_relative_scale2_ren (oset.value_relative_scale2_ren)
#define osi_value_scale_ren (oset.value_scale_ren)
#define osi_value_scale2_ren (oset.value_scale2_ren)
#define osi_value_alpha_S_TSV (oset.value_alpha_S_TSV)
#define osi_value_relative_factor_alpha_S (oset.value_relative_factor_alpha_S)

#define osi_value_relative_scale_fact (oset.value_relative_scale_fact)
#define osi_value_relative_scale2_fact (oset.value_relative_scale2_fact)
#define osi_value_relative_logscale2_fact (oset.value_relative_logscale2_fact)
#define osi_value_scale_fact (oset.value_scale_fact)
#define osi_value_scale2_fact (oset.value_scale2_fact)
#define osi_value_pdf_factor (oset.value_pdf_factor)

#define osi_pointer_scale_ren (oset.pointer_scale_ren)
#define osi_pointer_scale2_ren (oset.pointer_scale2_ren)
#define osi_pointer_factor_alpha_S (oset.pointer_factor_alpha_S)
#define osi_pointer_scale_fact (oset.pointer_scale_fact)
#define osi_pointer_scale2_fact (oset.pointer_scale2_fact)
#define osi_pointer_pdf_factor (oset.pointer_pdf_factor)
#define osi_pointer_relative_factor_alpha_S (oset.pointer_relative_factor_alpha_S)

#define osi_ps_integrand_TSV (oset.ps_integrand_TSV)
#define osi_ps_integrand_qTcut_TSV (oset.ps_integrand_qTcut_TSV)
#define osi_integrand_TSV (oset.integrand_TSV)
#define osi_sum_weight_TSV (oset.sum_weight_TSV)
#define osi_sum_weight2_TSV (oset.sum_weight2_TSV)
#define osi_fullsum_weight_TSV (oset.fullsum_weight_TSV)
#define osi_fullsum_weight2_TSV (oset.fullsum_weight2_TSV)
#define osi_sum_weight_qTcut_TSV (oset.sum_weight_qTcut_TSV)
#define osi_sum_weight2_qTcut_TSV (oset.sum_weight2_qTcut_TSV)
#define osi_fullsum_weight_qTcut_TSV (oset.fullsum_weight_qTcut_TSV)
#define osi_fullsum_weight2_qTcut_TSV (oset.fullsum_weight2_qTcut_TSV)

#define osi_ps_moment_TSV (oset.ps_moment_TSV)
#define osi_moment_TSV (oset.moment_TSV)
#define osi_sum_moment_TSV (oset.sum_moment_TSV)
#define osi_sum_moment2_TSV (oset.sum_moment2_TSV)
#define osi_fullsum_moment_TSV (oset.fullsum_moment_TSV)
#define osi_fullsum_moment2_TSV (oset.fullsum_moment2_TSV)
#define osi_sum_moment_qTcut_TSV (oset.sum_moment_qTcut_TSV)
#define osi_sum_moment2_qTcut_TSV (oset.sum_moment2_qTcut_TSV)
#define osi_fullsum_moment_qTcut_TSV (oset.fullsum_moment_qTcut_TSV)
#define osi_fullsum_moment2_qTcut_TSV (oset.fullsum_moment2_qTcut_TSV)

#define osi_value_ME2term (oset.value_ME2term)
#define osi_value_ME2term_ren (oset.value_ME2term_ren)
#define osi_value_ME2term_fact (oset.value_ME2term_fact)
#define osi_pointer_ME2term (oset.pointer_ME2term)

#define osi_value_logscale2_fact_papi (oset.value_logscale2_fact_papi)
#define osi_data_K (oset.data_K)
#define osi_value_data_P (oset.value_data_P)
#define osi_value_ME2_KP (oset.value_ME2_KP)

#define osi_bin_count_TSV (oset.bin_count_TSV)
#define osi_bin_weight_TSV (oset.bin_weight_TSV)
#define osi_bin_weight2_TSV (oset.bin_weight2_TSV)

#define osi_no_min_qTcut_TSV (oset.no_min_qTcut_TSV)
#define osi_no_max_qTcut_TSV (oset.no_max_qTcut_TSV)
#define osi_no_min_qTcut_distribution_TSV (oset.no_min_qTcut_distribution_TSV)
#define osi_no_max_qTcut_distribution_TSV (oset.no_max_qTcut_distribution_TSV)



#define osi_M (oset.M)
#define osi_M2 (oset.M2)


#define osi_name_process (oset.name_process)

//#define osi_type_parton (oset.type_parton)
#define osi_mass_parton (oset.mass_parton)
#define osi_mass2_parton (oset.mass2_parton)

#define osi_massive_QCD (oset.massive_QCD)
#define osi_massive_QEW (oset.massive_QEW)

#define osi_p_parton (oset.p_parton)
#define osi_particle_event (oset.particle_event)
#define osi_n_object (oset.n_object)

#define osi_start_p_parton (oset.start_p_parton)
#define osi_start_particle_event (oset.start_particle_event)
#define osi_start_n_object (oset.start_n_object)

#define osi_recombination_history (oset.recombination_history)

#define osi_access_object (oset.access_object)

#define osi_observed_object (oset.observed_object)
#define osi_object_category (oset.object_category)
#define osi_n_partonlevel (oset.n_partonlevel)
#define osi_n_parton_nu (oset.n_parton_nu)
/*
#define osi_ (oset.)
#define osi_ (oset.)
*/


#define osi_runtime_jet_recombination (oset.runtime_jet_recombination)
#define osi_runtime_photon_recombination (oset.runtime_photon_recombination)
#define osi_runtime_photon_isolation (oset.runtime_photon_isolation)
#define osi_runtime_original (oset.runtime_original)
#define osi_runtime_missing (oset.runtime_missing)

#define osi_runtime_object (oset.runtime_object)
#define osi_runtime_order (oset.runtime_order)
#define osi_runtime_order_inverse (oset.runtime_order_inverse)
#define osi_equivalent_object (oset.equivalent_object)
#define osi_equivalent_no_object (oset.equivalent_no_object)
#define osi_no_relevant_object (oset.no_relevant_object)

#define osi_frixione_isolation (oset.frixione_isolation)
#define osi_frixione_n (oset.frixione_n)
#define osi_frixione_epsilon (oset.frixione_epsilon)
#define osi_frixione_delta_0 (oset.frixione_delta_0)
#define osi_frixione_jet_removal (oset.frixione_jet_removal)

#define osi_photon_recombination (oset.photon_recombination)
#define osi_photon_R_definition (oset.photon_R_definition)
#define osi_photon_R (oset.photon_R)
#define osi_photon_R2 (oset.photon_R2)
#define osi_photon_E_threshold_ratio (oset.photon_E_threshold_ratio)
#define osi_photon_jet_algorithm (oset.photon_jet_algorithm)
#define osi_photon_photon_recombination (oset.photon_photon_recombination)
#define osi_photon_photon_recombination_R (oset.photon_photon_recombination_R)

#define osi_jet_algorithm (oset.jet_algorithm)
#define osi_jet_R_definition (oset.jet_R_definition)
#define osi_jet_R (oset.jet_R)
#define osi_jet_R2 (oset.jet_R2)
#define osi_parton_y_max (oset.parton_y_max)
#define osi_parton_eta_max (oset.parton_eta_max)

#define osi_bin (oset.bin)
#define osi_bin_max (oset.bin_max)

#define osi_bin_counts (oset.bin_counts)
#define osi_bin_weight (oset.bin_weight)
#define osi_bin_weight2 (oset.bin_weight2)
#define osi_bin_weight_CV (oset.bin_weight_CV)
#define osi_bin_weight2_CV (oset.bin_weight2_CV)

#define osi_dat (oset.dat)
#define osi_dddat (oset.dddat)


#define osi_var_A_integrand_cut (oset.var_A_integrand_cut)
#define osi_var_R_integrand_cut (oset.var_R_integrand_cut)
#define osi_var_R_integrand_cut_incl (oset.var_R_integrand_cut_incl)

#define osi_var_A_integrand_moment_cut (oset.var_A_integrand_moment_cut)
#define osi_var_R_integrand_moment_cut (oset.var_R_integrand_moment_cut)
#define osi_var_R_integrand_moment_cut_incl (oset.var_R_integrand_moment_cut_incl)


#define osi_ME2 (oset.ME2)
#define osi_integrand (oset.integrand)
#define osi_integrand_CV (oset.integrand_CV)

#define osi_moment (oset.moment)
#define osi_directed_moment (oset.directed_moment)
#define osi_max_integrand (oset.max_integrand)
#define osi_variation_CV (oset.switch_CV)
#define osi_switch_CV (oset.switch_CV)
#define osi_n_scales_CV (oset.n_scales_CV)
#define osi_variation_mu_fact_CV (oset.variation_mu_fact_CV)
#define osi_variation_mu_ren_CV (oset.variation_mu_ren_CV)
#define osi_variation_factor_CV (oset.variation_factor_CV)
#define osi_central_scale_CV (oset.central_scale_CV)
#define osi_no_central_scale_CV (oset.no_central_scale_CV)
#define osi_dynamic_scale (oset.dynamic_scale)
#define osi_prefactor_reference (oset.prefactor_reference)
#define osi_dynamic_scale_CV (oset.dynamic_scale_CV)
#define osi_prefactor_CV (oset.prefactor_CV)
#define osi_mu_fact (oset.mu_fact)
#define osi_mu_fact_CV (oset.mu_fact_CV)
#define osi_mu_ren (oset.mu_ren)
#define osi_mu_ren_CV (oset.mu_ren_CV)
#define osi_integrand_D (oset.integrand_D)
#define osi_integrand_D_CV (oset.integrand_D_CV)





#define osi_this_psp_weight (oset.this_psp_weight)
#define osi_this_psp_weight2 (oset.this_psp_weight2)
#define osi_this_psp_weight_CV (oset.this_psp_weight_CV)
#define osi_this_psp_weight2_CV (oset.this_psp_weight2_CV)
#define osi_step_sum_weight (oset.step_sum_weight)
#define osi_step_sum_weight2 (oset.step_sum_weight2)
#define osi_step_sum_weight_CV (oset.step_sum_weight_CV)
#define osi_step_sum_weight2_CV (oset.step_sum_weight2_CV)
#define osi_full_sum_weight (oset.full_sum_weight)
#define osi_full_sum_weight2 (oset.full_sum_weight2)
#define osi_full_sum_weight_CV (oset.full_sum_weight_CV)
#define osi_full_sum_weight2_CV (oset.full_sum_weight2_CV)

#define osi_this_psp_moment (oset.this_psp_moment)
#define osi_this_psp_moment_CV (oset.this_psp_moment_CV)
#define osi_this_psp_moment2 (oset.this_psp_moment2)
#define osi_this_psp_moment2_CV (oset.this_psp_moment2_CV)
#define osi_step_sum_moment (oset.step_sum_moment)
#define osi_step_sum_moment_CV (oset.step_sum_moment_CV)
#define osi_step_sum_moment2 (oset.step_sum_moment2)
#define osi_step_sum_moment2_CV (oset.step_sum_moment2_CV)
#define osi_full_sum_moment (oset.full_sum_moment)
#define osi_full_sum_moment2 (oset.full_sum_moment2)
#define osi_full_sum_moment_CV (oset.full_sum_moment_CV)
#define osi_full_sum_moment2_CV (oset.full_sum_moment2_CV)

/*
#define osi_ (oset.)
#define osi_ (oset.)
#define osi_ (oset.)
#define osi_ (oset.)
*/






#define osi_id_scales (oset.id_scales)
#define osi_map_value_scale_fact (oset.map_value_scale_fact)
#define osi_map_value_scale_fact_CV (oset.map_value_scale_fact_CV)
#define osi_value_mu_fact_central (oset.value_mu_fact_central)
#define osi_value_mu_fact_rel (oset.value_mu_fact_rel)
#define osi_value_mu_fact (oset.value_mu_fact)
#define osi_map_value_scale_ren (oset.map_value_scale_ren)
#define osi_map_value_scale_ren_CV (oset.map_value_scale_ren_CV)
#define osi_value_mu_ren_central (oset.value_mu_ren_central)
#define osi_value_mu_ren_rel (oset.value_mu_ren_rel)
#define osi_value_mu_ren (oset.value_mu_ren)
#define osi_value_alpha_S (oset.value_alpha_S)
#define osi_value_factor_alpha_S (oset.value_factor_alpha_S)


#define osi_pdf_factor (oset.pdf_factor)
#define osi_pdf_factor_CV (oset.pdf_factor_CV)



//#define osi_mu_fact (oset.mu_fact)
//#define osi_mu_ren (oset.mu_ren)

// needed externally ???
#define osi_rel_scale_factor_CV (oset.rel_scale_factor_CV)
#define osi_scale_fact_CV (oset.scale_fact_CV)
#define osi_scale_ren_CV (oset.scale_ren_CV)
//

#define osi_alpha_S_CV (oset.alpha_S_CV)
#define osi_rel_alpha_S_CV (oset.rel_alpha_S_CV)

#define osi_directory_name_scale_CV (oset.directory_name_scale_CV)
#define osi_rel_scale_factor_ren_CV (oset.rel_scale_factor_ren_CV)
#define osi_rel_scale_factor_fact_CV (oset.rel_scale_factor_fact_CV)

/*
#define osi_ (oset.)
#define osi_ (oset.)
#define osi_ (oset.)
*/

#define osi_mu_central (oset.mu_central)
#define osi_var_mu_ren (oset.var_mu_ren)
#define osi_var_mu_fact (oset.var_mu_fact)
#define osi_var_rel_alpha_S (oset.var_rel_alpha_S)
#define osi_var_mu_ren_CV (oset.var_mu_ren_CV)
#define osi_var_mu_fact_CV (oset.var_mu_fact_CV)
#define osi_var_rel_alpha_S_CV (oset.var_rel_alpha_S_CV)


#define osi_VA_ME2_cf (oset.VA_ME2_cf)
#define osi_VA_I_ME2_cf (oset.VA_I_ME2_cf)

#define osi_VA_b_ME2 (oset.VA_b_ME2)
#define osi_VA_V_ME2 (oset.VA_V_ME2)
#define osi_VA_X_ME2 (oset.VA_X_ME2)
#define osi_VA_I_ME2 (oset.VA_I_ME2)
#define osi_VA_X_ME2_CV (oset.VA_X_ME2_CV)
#define osi_VA_DeltaUV (oset.VA_DeltaUV)
#define osi_VA_DeltaIR1 (oset.VA_DeltaIR1)
#define osi_VA_DeltaIR2 (oset.VA_DeltaIR2)
#define osi_VA_delta_flag (oset.VA_delta_flag)

#define osi_CA_dipole_splitting (oset.CA_dipole_splitting)
#define osi_CA_ME2_cf (oset.CA_ME2_cf)
#define osi_CA_ME2_cf_fi (oset.CA_ME2_cf_fi)

#define osi_CA_value_log_mu2_fact (oset.CA_value_log_mu2_fact)
#define osi_CA_value_ME2_KP (oset.CA_value_ME2_KP)
#define osi_CA_value_pdf_factor (oset.CA_value_pdf_factor)
#define osi_CA_value_integrand (oset.CA_value_integrand)
#define osi_CA_value_integrand_D (oset.CA_value_integrand_D)
#define osi_CA_sum_value_integrand (oset.CA_sum_value_integrand)
#define osi_CA_sum_value_integrand_D (oset.CA_sum_value_integrand_D)

#define osi_xz_pdf (oset.xz_pdf)
#define osi_xz_coll (oset.xz_coll)
#define osi_xz_factor (oset.xz_factor)
#define osi_CX_value_pdf_factor (oset.CX_value_pdf_factor)
#define osi_CX_combination_pdf (oset.CX_combination_pdf)

#define osi_value_list_pdf_factor (oset.value_list_pdf_factor)
#define osi_list_combination_pdf (oset.list_combination_pdf)
#define osi_list_combination_pdf_emission (oset.list_combination_pdf_emission)

#define osi_multicollinear (oset.multicollinear)
#define osi_ncollinear (oset.ncollinear)


#define osi_RA_mu_ren (oset.RA_mu_ren)
#define osi_RA_mu_fact (oset.RA_mu_fact)
#define osi_RA_rel_alpha_S (oset.RA_rel_alpha_S)
#define osi_RA_mu_ren_CV (oset.RA_mu_ren_CV)
#define osi_RA_mu_fact_CV (oset.RA_mu_fact_CV)
#define osi_RA_rel_alpha_S_CV (oset.RA_rel_alpha_S_CV)
#define osi_RA_pdf_factor (oset.RA_pdf_factor)
#define osi_RA_pdf_factor_CV (oset.RA_pdf_factor_CV)
#define osi_RA_value_mu_fact (oset.RA_value_mu_fact)
#define osi_RA_value_mu_ren (oset.RA_value_mu_ren)
#define osi_RA_value_alpha_S (oset.RA_value_alpha_S)
#define osi_RA_value_factor_alpha_S (oset.RA_value_factor_alpha_S)



#define osi_RA_value_mu_fact (oset.RA_value_mu_fact)
#define osi_RA_value_mu_ren (oset.RA_value_mu_ren)
//#define osi_RA_value_alpha_S (oset.RA_value_alpha_S)
//#define osi_RA_value_factor_alpha_S (oset.RA_value_factor_alpha_S)

#define osi_RA_ME2 (oset.RA_ME2)
#define osi_var_RA_ME2 (oset.var_RA_ME2)
#define osi_var_RA_ME2_CV (oset.var_RA_ME2_CV)
#define osi_RA_ME2_moment (oset.RA_ME2_moment)
#define osi_RA_ME2_moment_CV (oset.RA_ME2_moment_CV)

#define osi_RA_integrand_moment (oset.RA_integrand_moment)
#define osi_RA_integrand_moment_CV (oset.RA_integrand_moment_CV)

#define osi_RA_techcut_integrand (oset.RA_techcut_integrand)
#define osi_RA_x_a (oset.RA_x_a)


#define osi_QT_initialstate_type (oset.QT_initialstate_type)
#define osi_QT_finalstate_massive_coloured (oset.QT_finalstate_massive_coloured)
#define osi_QT_correlationoperator (oset.QT_correlationoperator)
#define osi_QT_ME2_cf (oset.QT_ME2_cf)
#define osi_QT_ME2_loopcf (oset.QT_ME2_loopcf)
#define osi_QT_ME2_Born4cf (oset.QT_ME2_Born4cf)
 


#define osi_QT_pdf_factor_z1x2 (oset.QT_pdf_factor_z1x2)
#define osi_QT_pdf_factor_x1z2 (oset.QT_pdf_factor_x1z2)
#define osi_QT_pdf_factor_gx2  (oset.QT_pdf_factor_gx2)
#define osi_QT_pdf_factor_x1g  (oset.QT_pdf_factor_x1g)
#define osi_QT_pdf_factor_gg   (oset.QT_pdf_factor_gg)
#define osi_QT_pdf_factor_qx2  (oset.QT_pdf_factor_qx2)
#define osi_QT_pdf_factor_x1q  (oset.QT_pdf_factor_x1q)
#define osi_QT_pdf_factor_z1z2 (oset.QT_pdf_factor_z1z2)
#define osi_QT_pdf_factor_gz2  (oset.QT_pdf_factor_gz2)
#define osi_QT_pdf_factor_z1g  (oset.QT_pdf_factor_z1g)
#define osi_QT_pdf_factor_qbx2 (oset.QT_pdf_factor_qbx2)
#define osi_QT_pdf_factor_x1qb (oset.QT_pdf_factor_x1qb)
// new                          
#define osi_QT_pdf_factor_qq   (oset.QT_pdf_factor_qq)
#define osi_QT_pdf_factor_qz2  (oset.QT_pdf_factor_qz2)
#define osi_QT_pdf_factor_z1q  (oset.QT_pdf_factor_z1q)
  

#define osi_QT_pdf_factor_z1x2_CV (oset.QT_pdf_factor_z1x2_CV)
#define osi_QT_pdf_factor_x1z2_CV (oset.QT_pdf_factor_x1z2_CV)
#define osi_QT_pdf_factor_gx2_CV  (oset.QT_pdf_factor_gx2_CV)
#define osi_QT_pdf_factor_x1g_CV  (oset.QT_pdf_factor_x1g_CV)
#define osi_QT_pdf_factor_gg_CV   (oset.QT_pdf_factor_gg_CV)
#define osi_QT_pdf_factor_qx2_CV  (oset.QT_pdf_factor_qx2_CV)
#define osi_QT_pdf_factor_x1q_CV  (oset.QT_pdf_factor_x1q_CV)
#define osi_QT_pdf_factor_z1z2_CV (oset.QT_pdf_factor_z1z2_CV)
#define osi_QT_pdf_factor_gz2_CV  (oset.QT_pdf_factor_gz2_CV)
#define osi_QT_pdf_factor_z1g_CV  (oset.QT_pdf_factor_z1g_CV)
#define osi_QT_pdf_factor_qbx2_CV (oset.QT_pdf_factor_qbx2_CV)
#define osi_QT_pdf_factor_x1qb_CV (oset.QT_pdf_factor_x1qb_CV)
// new
#define osi_QT_pdf_factor_qq_CV  (oset.QT_pdf_factor_qq_CV)
#define osi_QT_pdf_factor_qz2_CV (oset.QT_pdf_factor_qz2_CV)
#define osi_QT_pdf_factor_z1q_CV (oset.QT_pdf_factor_z1q_CV)

  
#define osi_QT_value_pdf_factor      (oset.QT_value_pdf_factor)
#define osi_QT_value_pdf_factor_z1x2 (oset.QT_value_pdf_factor_z1x2)
#define osi_QT_value_pdf_factor_x1z2 (oset.QT_value_pdf_factor_x1z2)
#define osi_QT_value_pdf_factor_gx2  (oset.QT_value_pdf_factor_gx2)
#define osi_QT_value_pdf_factor_x1g  (oset.QT_value_pdf_factor_x1g)
#define osi_QT_value_pdf_factor_gg   (oset.QT_value_pdf_factor_gg)
#define osi_QT_value_pdf_factor_qx2  (oset.QT_value_pdf_factor_qx2)
#define osi_QT_value_pdf_factor_x1q  (oset.QT_value_pdf_factor_x1q)
#define osi_QT_value_pdf_factor_z1z2 (oset.QT_value_pdf_factor_z1z2)
#define osi_QT_value_pdf_factor_gz2  (oset.QT_value_pdf_factor_gz2)
#define osi_QT_value_pdf_factor_z1g  (oset.QT_value_pdf_factor_z1g)
#define osi_QT_value_pdf_factor_qbx2 (oset.QT_value_pdf_factor_qbx2)
#define osi_QT_value_pdf_factor_x1qb (oset.QT_value_pdf_factor_x1qb)
// new                               
#define osi_QT_value_pdf_factor_qq   (oset.QT_value_pdf_factor_qq)
#define osi_QT_value_pdf_factor_qz2  (oset.QT_value_pdf_factor_qz2)
#define osi_QT_value_pdf_factor_z1q  (oset.QT_value_pdf_factor_z1q)



#define osi_QT_sigma_qT (oset.QT_sigma_qT)
#define osi_QT_sigma_qT_CV (oset.QT_sigma_qT_CV)
  
#define osi_QT_sig_CV (oset.QT_sig_CV)
#define osi_QT_virt_CV (oset.QT_virt_CV)

#define osi_QT_A0 (oset.QT_A0)
#define osi_QT_A1 (oset.QT_A1)
#define osi_QT_A2 (oset.QT_A2)
#define osi_QT_H1_delta (oset.QT_H1_delta)
#define osi_QT_H2_delta (oset.QT_H2_delta)

#define osi_QT_Qres (oset.QT_Qres)






#define osi_start (oset.start)
#define osi_end (oset.end)
#define osi_h (oset.h)
#define osi_min (oset.min)
#define osi_sec (oset.sec)
#define osi_time_counter (oset.time_counter)


#define osi_int_end (oset.int_end)
#define osi_sigma_normalization (oset.sigma_normalization)
#define osi_sigma_normalization_deviation (oset.sigma_normalization_deviation)

#define osi_change_cut (oset.change_cut)


#define osi_coll_choice (oset.coll_choice) // also in psi
#define osi_N_f (oset.N_f)
#define osi_N_f_active (oset.N_f_active)

#define osi_N_quarks (oset.N_quarks)
#define osi_LHAPDFname (oset.LHAPDFname)
#define osi_LHAPDFsubset (oset.LHAPDFsubset)
#define osi_N_nondecoupled (oset.N_nondecoupled)

#define osi_user_switch_name (oset.user.switch_name)
#define osi_user_switch_value (oset.user.switch_value)
#define osi_user_switch_map (oset.user.switch_map)
#define osi_user_cut_name (oset.user.cut_name)
#define osi_user_cut_value (oset.user.cut_value)
#define osi_user_cut_map (oset.user.cut_map)
#define osi_user_int_name (oset.user.int_name)
#define osi_user_int_value (oset.user.int_value)
#define osi_user_int_map (oset.user.int_map)
#define osi_user_double_name (oset.user.double_name)
#define osi_user_double_value (oset.user.double_value)
#define osi_user_double_map (oset.user.double_map)
#define osi_user_string_name (oset.user.string_name)
#define osi_user_string_value (oset.user.string_value)
#define osi_user_string_map (oset.user.string_map)
#define osi_user_particle_name (oset.user.particle_name)
#define osi_user_particle_value (oset.user.particle_value)
#define osi_user_particle_map (oset.user.particle_map)

#define osi_OL_parameter (oset.OL_parameter)
#define osi_OL_value (oset.OL_value)

#define osi_RCL_parameter (oset.RCL_parameter)
#define osi_RCL_value (oset.RCL_value)

#define osi_path_to_main (oset.path_to_main)

#define osi_unit_calculation (oset.unit_calculation)
#define osi_unit_result (oset.unit_result)
#define osi_unit_distribution (oset.unit_distribution)

#define osi_unit_factor_calculation (oset.unit_factor_calculation)
#define osi_unit_factor2_calculation (oset.unit_factor2_calculation)
#define osi_unit_factor_result (oset.unit_factor_result)
#define osi_unit_factor_distribution (oset.unit_factor_distribution)

#define osi_Xsection (oset.Xsection)
#define osi_Xsection_delta (oset.Xsection_delta)
#define osi_Xsection_CV (oset.Xsection_CV)
#define osi_Xsection_delta_CV (oset.Xsection_delta_CV)

#define osi_temp_n_step (oset.temp_n_step)

#define osi_RA_dipole (*oset.RA_dipole)
#define osi_VA_ioperator (*oset.VA_ioperator)
//#define osi_CA_collinear (oset.CA_collinear)
#define osi_CA_collinear (*oset.CA_collinear)

#define osi_combination_pdf (oset.combination_pdf)

#define osi_CA_combination_pdf (oset.CA_combination_pdf)

#define osi_pdf_selection (oset.pdf_selection)
#define osi_pdf_disable (oset.pdf_disable)
#define osi_pdf_content_modify (oset.pdf_content_modify)
#define osi_allowed_all_pdf (oset.allowed_all_pdf)

#define osi_jet_algorithm_selection (oset.jet_algorithm_selection)
#define osi_jet_algorithm_disable (oset.jet_algorithm_disable)
#define osi_jet_algorithm_list (oset.jet_algorithm_list)

#define osi_photon_recombination_selection (oset.photon_recombination_selection)
#define osi_photon_recombination_disable (oset.photon_recombination_disable)
#define osi_photon_recombination_list (oset.photon_recombination_list)

//#define osi_combination_type_parton (oset.combination_type_parton)
//#define osi_combination_crossing (oset.combination_crossing)


// !!! overlap with phasespace_set !!!
#define osi_boost (oset.boost)
#define osi_x_pdf (oset.x_pdf)
#define osi_z_coll (oset.z_coll)

#define osi_process_id (oset.process_id)

#define osi_check_vanishing_ME2_end (oset.check_vanishing_ME2_end)
#define osi_flag_vanishing_ME2 (oset.flag_vanishing_ME2)
#define osi_n_event_vanishing_ME2 (oset.n_event_vanishing_ME2)

#define osi_ps_n_partonlevel (oset.ps_n_partonlevel)
#define osi_ps_relevant_n_partonlevel (oset.ps_relevant_n_partonlevel)
#define osi_ps_runtime_jet_algorithm (oset.ps_runtime_jet_algorithm)
#define osi_ps_runtime_photon (oset.ps_runtime_photon)
#define osi_ps_runtime_photon_recombination (oset.ps_runtime_photon_recombination)
#define osi_ps_runtime_photon_isolation (oset.ps_runtime_photon_isolation)

#define osi_esi (oset.esi)


/*
#define osi_ (oset.)
#define osi_ (oset.)
#define osi_ (oset.)
#define osi_ (oset.)
*/

/*
#define osi_ (oset.)
#define osi_ (oset.)
*/
