#include "header.hpp"
#include "definitions.observable.set.cxx"

void perform_event_selection(observable_set & oset, call_generic & generic){
  static Logger logger("perform_event_selection");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (Log::getLogThreshold() <= LOG_DEBUG_POINT){
    logger.newLine(LOG_DEBUG_POINT);
    logger << LOG_DEBUG_POINT // << "[" << setw(2) << i_a << "]"
	   << " New PSP: " 
	   << "   i_gen = " << setw(12) << oset.psi->i_gen 
	   << "   i_acc = " << setw(12) << oset.psi->i_acc 
	   << "   i_rej = " << setw(12) << oset.psi->i_rej 
	   << "   i_nan = " << setw(4) << oset.psi->i_nan 
	   << "   i_tec = " << setw(4) << oset.psi->i_tec 
	   << endl;
    logger.newLine(LOG_DEBUG_POINT);
    
    stringstream temp_psp;
    temp_psp << "Final-state parton-level momenta (partonic CMS frame):" << endl;
    for (int i_p = 1; i_p < osi_p_parton[0].size(); i_p++){
      temp_psp << "type_parton[" << setw(2) << 0 << "][" << i_p << "] = " 
	       << setw(3) << osi_type_parton[0][i_p] 
	       << " -> " 
	       << osi_p_parton[0][i_p] << endl; 
    }
    temp_psp << endl;
    temp_psp << "Final-state parton-level momenta (hadronic CMS = LAB frame):" << endl;
    for (int i_p = 3; i_p < osi_p_parton[0].size(); i_p++){
      temp_psp << "type_parton[" << setw(2) << 0 << "][" << i_p << "] = " 
	       << setw(3) << osi_type_parton[0][i_p] 
	       << " -> " 
	       << osi_particle_event[0][0][i_p].momentum << endl; 
      // << " m = " << osi_particle_event[0][0][i_p].m << " pT = " << osi_particle_event[0][0][i_p].pT << " eta = " << osi_particle_event[0][0][i_p].eta << endl;
    }
    
    //  cerr << temp_psp.str() << endl;
    logger << LOG_DEBUG_POINT << temp_psp.str() << endl;
    logger.newLine(LOG_DEBUG_POINT);
  }

  if (osi_switch_testcut == 0){
    for (int i_a = 0; i_a < osi_cut_ps.size(); i_a++){
      // should not be needed, as set to correct value later:
      osi_cut_ps[i_a] = 0;

      logger << LOG_DEBUG_POINT << "before: osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << endl;
      perform_event_selection_phasespace(i_a, oset);
      ///
      if (osi_switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after generic event selection, before user-defined particles   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}

      if (osi_cut_ps[i_a] == -1){continue;}

      if (oset.user.particle_name.size() > 0){generic.particles(i_a, oset);}
      ///
      if (osi_switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after user-defined particles, before general fiducial cuts   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}

      if (osi_cut_ps[i_a] == -1){continue;}

      // application of general cuts set via fiducialcut interface:
      logger << LOG_DEBUG_POINT << "osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << "   before fiducialcut." << endl;
      for (int i_f = 0; i_f < oset.esi.fiducial_cut.size(); i_f++){
	oset.esi.fiducial_cut[i_f].apply_fiducialcut(i_a);
	if (osi_switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after fiducialcut   " << oset.esi.fiducial_cut[i_f].name << "   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}

	if (osi_cut_ps[i_a] == -1){continue;}
      }

      if (osi_switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after general fiducial cuts, before individual event selection   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}

      generic.cuts(i_a, oset);
      ///
      if (osi_switch_output_cutinfo){logger << LOG_DEBUG_POINT << "[" << setw(2) << i_a << "]" << "   after individual cuts   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}
      logger << LOG_DEBUG_VERBOSE << "after:  osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << endl;
    }
  }
  else if (osi_switch_testcut == 1){
    generic.cuts_test(0, oset);
  }


  for (int i_a = 0; i_a < osi_cut_ps.size(); i_a++){
    logger << LOG_DEBUG_POINT << "osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << endl;
  }
  
}



void perform_event_selection_phasespace(int i_a, observable_set & oset){
  static Logger logger("perform_event_selection_phasespace");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  static int initialization = 1;
  static int n_original = 0;
  if (initialization == 1){
    // re-extract the number of particles that enter the event selection without modifications
    logger << LOG_DEBUG_VERBOSE << "initialization = " << initialization << endl;
    vector<int> temp_original;
    for (int i_g = 0; i_g < osi_runtime_original.size(); i_g++){
      logger << LOG_DEBUG_VERBOSE << "i_g = " << i_g << endl;
      int j_g = osi_runtime_original[i_g];
      for (int i_o = 0; i_o < osi_runtime_order[j_g].size(); i_o++){
        int i_p = osi_runtime_order[j_g][i_o];
        int flag = 0;
        for (int i = 0; i < temp_original.size(); i++){if (i_p == temp_original[i]){flag = 1; break;}}
        if (flag == 0){temp_original.push_back(i_p);}
      }
    }
    n_original = temp_original.size();
    for (int i = 0; i < temp_original.size(); i++){logger << LOG_DEBUG_VERBOSE << "temp_original[" << i << "] = " << temp_original[i] << endl;}
    logger << LOG_DEBUG_VERBOSE << "n_original = " << n_original << endl;
  
    initialization = 0;
  }



  stringstream info_cut;
  ///  static stringstream info_cut;
  if (osi_switch_output_cutinfo){
    info_cut.clear();
    info_cut.str("");
    info_cut << endl;
    info_cut << "[" << setw(2) << i_a << "]   generic event selection" << endl;
    info_cut << "[" << setw(2) << i_a << "]   parton-level momenta:" << endl;
    for (int i_p = 1; i_p < osi_p_parton[i_a].size(); i_p++){
      info_cut << "[" << setw(2) << i_a << "]   type_parton[" << i_p << "] = " << setw(3) << osi_type_parton[i_a][i_p] << " -> " << osi_particle_event[0][i_a][i_p].momentum << endl;//<< " pT = " << osi_particle_event[0][i_a][i_p].pT << " eta = " << osi_particle_event[0][i_a][i_p].eta << endl;
    }
    info_cut << endl;
  }
  
  
  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before (qT subtraction)):   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}

  if (oset.csi->class_contribution_qTcut_explicit){oset.event_selection_qTcut(i_a);}



  for (int i_p = 1; i_p < 3; i_p++){osi_particle_event[0][i_a][i_p] = particle(osi_p_parton[i_a][i_p].zboost(-osi_boost));}
  
  for (int i_p = 3; i_p < osi_p_parton[i_a].size(); i_p++){osi_particle_event[0][i_a][i_p] = particle(osi_p_parton[i_a][i_p].zboost(-osi_boost));}
  for (int i_p = 3; i_p < osi_p_parton[i_a].size(); i_p++){logger << LOG_DEBUG_VERBOSE << "osi_particle_event[0][" << i_a << "][" << i_p << "] = " << osi_particle_event[0][i_a][i_p] << endl;}

  



 
  for (int i_o = 1; i_o < osi_esi.object_list_selection.size(); i_o++){
    int j_o = osi_equivalent_no_object[i_o]; 
    // i_o -> number in relevant_object_list
    // j_o -> number in object_list
    //    logger << LOG_DEBUG_VERBOSE << "osi_particle_event[" << i_o << " -> " << j_o << "][" << i_a << "].size() = " << osi_particle_event[j_o][i_a].size() << "    n_object = " <<  osi_n_object[j_o][i_a] << endl;
    osi_particle_event[j_o][i_a].clear();
    osi_n_object[j_o][i_a] = 0;
    //    logger << LOG_DEBUG_VERBOSE << "osi_particle_event[" << i_o << " -> " << j_o << "][" << i_a << "].size() = " << osi_particle_event[j_o][i_a].size() << "    n_object = " <<  osi_n_object[j_o][i_a] << endl;
  }


  logger << LOG_DEBUG_VERBOSE << "photon_recombination" << endl;
  logger << LOG_DEBUG_VERBOSE << "osi_photon_recombination = " << osi_photon_recombination << endl;

  ///  vector<int> no_unrecombined_photon;
  //  static vector<int> no_unrecombined_photon;
  oset.no_unrecombined_photon.clear();

  //  for (int i_p = 3; i_p < osi_p_parton[i_a].size(); i_p++){logger << LOG_DEBUG_VERBOSE << "[" << setw(2) << i_a << "]   type_parton[" << i_p << "] = " << setw(3) << osi_type_parton[i_a][i_p] << " -> " << osi_particle_event[0][i_a][i_p].momentum << endl;


  ////////////////////////////
  //  photon recombination  //
  ////////////////////////////

  //  Recombinable particles are defined in  photon_recombination_list  (defined via  photon_recombination_selection  and  photon_recombination_disable ).
  //  If a particle is dressed with a photon, the combined momentum is stored at the position of its naked momentum, and the momentum of the recombined photon is empty.
  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   photon_recombination: " << "   photon_recombination = " << osi_photon_recombination << endl;}

  if (osi_photon_recombination){
    photon_recombination(oset.no_unrecombined_photon, i_a, oset);
    /*
    for (int i_c = 0; i_c < osi_ps_runtime_photon_recombination[i_a].size(); i_c++){
      //      logger << LOG_INFO << "osi_ps_runtime_photon_recombination[" << i_a << "][" << i_c << "] = " << osi_ps_runtime_photon_recombination[i_a][i_c] << "   pT2 = " << osi_particle_event[0][i_a][osi_ps_runtime_photon_recombination[i_a][i_c]].pT2  << endl;
      if (osi_particle_event[0][i_a][osi_ps_runtime_photon_recombination[i_a][i_c]].pT2 == 0.){
	logger << LOG_INFO << "i_a = " << i_a << "   Lepton " << osi_ps_runtime_photon_recombination[i_a][i_c] << " is removed in recombination procedure." << "   pT2 = " << osi_particle_event[0][i_a][osi_ps_runtime_photon_recombination[i_a][i_c]].pT2 << endl;
	
      }
    }
    */
  }
  else {oset.no_unrecombined_photon = osi_ps_runtime_photon[i_a];}
  //  Afterwards,  oset.no_unrecombined_photon  contains positions of un-recombined photons in  osi_particle_event[0][i_a]  (all photons if no dressing is done).

  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   oset.no_unrecombined_photon.size() = " << oset.no_unrecombined_photon.size() << endl;}
  if (osi_switch_output_cutinfo){
    if (oset.no_unrecombined_photon.size() < osi_ps_runtime_photon[i_a].size()){
      for (int i_p = 3; i_p < osi_p_parton[i_a].size(); i_p++){
	info_cut << "[" << setw(2) << i_a << "]   type_parton[" << i_p << "] = " << setw(3) << osi_type_parton[i_a][i_p] << " -> " << osi_particle_event[0][i_a][i_p].momentum << endl;//<< " pT = " << osi_particle_event[0][i_a][i_p].pT << " eta = " << osi_particle_event[0][i_a][i_p].eta << endl;
      }
    }
  }
  logger << LOG_DEBUG_VERBOSE << "oset.no_unrecombined_photon.size() = " << oset.no_unrecombined_photon.size() << endl;



  ////////////////////////
  //  runtime original  //
  ////////////////////////

  //  runtime_original   actually needed only once because "original" particles don't change on reduced phase spaces -> check evaluation of runtime_original !!!
  //  runtime_original   collects partons that enter event selection with their parton-level momenta (after photon dressing if applicable).
  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_original: " << "   osi_runtime_original.size() = " << osi_runtime_original.size() << endl;}

  logger << LOG_DEBUG_VERBOSE << "runtime_original" << "   osi_runtime_original.size() = " << osi_runtime_original.size() << endl;
  for (int i_g = 0; i_g < osi_runtime_original.size(); i_g++){
    // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_original
    int j_g = osi_runtime_original[i_g];  // j_g -> number in relevant_object_list
    int k_g = osi_equivalent_no_object[j_g];  // k_g -> number in object_list
    // later...   int k_g = j_g;
    
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   osi_esi.object_list_selection[runtime_original[" << i_g << "] = " << j_g << "] = " << osi_esi.object_list_selection[j_g] << endl;}

    logger << LOG_DEBUG_VERBOSE << "osi_runtime_original: i_g = " << i_g << "   j_g = " << j_g << " (" << osi_esi.object_list_selection[j_g] << ")   k_g = " << k_g << " (" << osi_esi.object_list[k_g] << ")" << endl;
    logger << LOG_DEBUG_VERBOSE << "osi_n_object[" << k_g << "][" << i_a << "] = " << osi_n_object[k_g][i_a] << endl;
    
    for (int i_o = 0; i_o < osi_runtime_order[j_g].size(); i_o++){
      int i_p = osi_runtime_order[j_g][i_o];  // i_p -> position in  osi_particle_event[0][i_a]  of parton belong to the respective object class
      if (osi_switch_output_cutinfo){info_cut << "runtime_original: " << i_g << "   osi_particle_event[0][" << i_a << "][" << i_p << "] = " << osi_particle_event[0][i_a][i_p] << endl;}
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
					      << "   y: " << setw(10) << abs(osi_particle_event[0][i_a][i_p].rapidity) << " < " << setw(8) << osi_esi.pds[j_g].define_y 
					      << "   eta:  " << setw(10) << abs(osi_particle_event[0][i_a][i_p].eta) << " < " << setw(8) << osi_esi.pds[j_g].define_eta 
					      << "   pT:  " << setw(10) << abs(osi_particle_event[0][i_a][i_p].pT) << " > " << setw(8) << osi_esi.pds[j_g].define_pT 
					      << "   ET:  " << setw(10) << abs(osi_particle_event[0][i_a][i_p].ET) << " > " << setw(8) << osi_esi.pds[j_g].define_ET 
					      << "   ---   " 
					      << (abs(osi_particle_event[0][i_a][i_p].rapidity) < osi_esi.pds[j_g].define_y && 
						  abs(osi_particle_event[0][i_a][i_p].eta) < osi_esi.pds[j_g].define_eta && 
						  osi_particle_event[0][i_a][i_p].pT >= osi_esi.pds[j_g].define_pT && 
						  osi_particle_event[0][i_a][i_p].ET >= osi_esi.pds[j_g].define_ET) << endl;}
      
      if (abs(osi_particle_event[0][i_a][i_p].rapidity) < osi_esi.pds[j_g].define_y && 
	  abs(osi_particle_event[0][i_a][i_p].eta) < osi_esi.pds[j_g].define_eta && 
	  osi_particle_event[0][i_a][i_p].pT >= osi_esi.pds[j_g].define_pT && 
	  osi_particle_event[0][i_a][i_p].ET >= osi_esi.pds[j_g].define_ET){
	osi_n_object[k_g][i_a]++;
	osi_particle_event[k_g][i_a].push_back(osi_particle_event[0][i_a][i_p]);
      }
    }
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   " << osi_esi.pds[j_g].n_observed_min << " <= osi_n_object[" << setw(2) <<k_g << "][" << setw(2) <<i_a << "] = " << osi_n_object[k_g][i_a] << " <= " << osi_esi.pds[j_g].n_observed_max << endl;}
	
    if ((osi_n_object[k_g][i_a] < osi_esi.pds[j_g].n_observed_min) || (osi_n_object[k_g][i_a] > osi_esi.pds[j_g].n_observed_max)){
      osi_cut_ps[i_a] = -1; 
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_original" << endl;}
      logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_original" << endl; 
      if (osi_switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
    }
    if (osi_n_object[k_g][i_a] > 1){sort(osi_particle_event[k_g][i_a].begin(), osi_particle_event[k_g][i_a].end(), greaterBypT);}
  }

  if (osi_switch_output_cutinfo){
    info_cut << endl;
    info_cut << "Summary after runtime_original:" << endl;
    for (int i_g = 0; i_g < osi_runtime_original.size(); i_g++){
      // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_original
      int j_g = osi_runtime_original[i_g];  // j_g -> number in relevant_object_list
      int k_g = osi_equivalent_no_object[j_g];  // k_g -> number in object_list
      //      info_cut << "[" << setw(2) << i_a << "]   osi_esi.object_list_selection[runtime_original[" << i_g << "] = " << j_g << "] = " << osi_esi.object_list_selection[j_g] << endl;
      info_cut << "[" << setw(2) << i_a << "]   osi_particle_event[" << setw(2) << k_g << " -> " << setw(10) << osi_esi.object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << osi_particle_event[k_g][i_a].size() << endl;
    }
    info_cut << endl;
  }


  ///////////////////////
  //  runtime missing  //
  ///////////////////////

  if (oset.ps_runtime_missing[i_a].size() > 0 ){
    //      runtime_missing   actually needed only once because "invisible" particles don't change on reduced phase spaces -> check evaluation of runtime_missing !!!
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_missing: " << "   oset.ps_runtime_missing[" << i_a << "].size() = " << oset.ps_runtime_missing[i_a].size() << endl;}
    //    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_missing: " << "   osi_runtime_missing.size() = " << osi_runtime_missing.size() << endl;}
    logger << LOG_DEBUG_VERBOSE << "runtime_missing" << "   osi_runtime_missing.size() = " << osi_runtime_missing.size() << endl;
    //    logger << LOG_INFO << "runtime_missing" << "   osi_runtime_missing.size() = " << osi_runtime_missing.size() << endl;
    //    logger << LOG_INFO << "runtime_missing" << "   oset.ps_runtime_missing[" << i_a << "].size() = " << oset.ps_runtime_missing[i_a].size() << endl;
    
    int j_g = osi_esi.observed_object_selection["missing"];  // j_g -> number in relevant_object_list
    int k_g = osi_esi.observed_object["missing"];  //  k_g -> number in object_list
    // later...   int k_g = j_g;
    //  cout << "j_g = " << j_g << "   k_g = " << k_g << endl;
    fourvector p_missing;
    for (int i_p = 0; i_p < oset.ps_runtime_missing[i_a].size(); i_p++){
      //    cout << "
      //      logger << LOG_INFO << "osi_particle_event[0][" << i_a << "][" << oset.ps_runtime_missing[i_a][i_p] << "].momentum = " << osi_particle_event[0][i_a][oset.ps_runtime_missing[i_a][i_p]].momentum << endl;
      p_missing = p_missing + osi_particle_event[0][i_a][oset.ps_runtime_missing[i_a][i_p]].momentum;
    }
    osi_particle_event[k_g][i_a].push_back(particle(p_missing));
    osi_n_object[k_g][i_a] = 1;
    
    //    logger << LOG_INFO << "osi_particle_event[" << k_g << "][" << i_a << "][0] = " << osi_particle_event[k_g][i_a][0].momentum << "   pT = " << osi_particle_event[k_g][i_a][0].pT << endl;
    
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
					    << "   pT:  " << setw(10) << osi_particle_event[k_g][i_a][0].pT << " > " << setw(8) << osi_esi.pds[j_g].define_pT 
					    << "   ET:  " << setw(10) << osi_particle_event[k_g][i_a][0].ET << " > " << setw(8) << osi_esi.pds[j_g].define_ET 
					    << "   ---   " 
					    << (osi_particle_event[k_g][i_a][0].pT < osi_esi.pds[j_g].define_pT ||
						osi_particle_event[k_g][i_a][0].ET < osi_esi.pds[j_g].define_ET) << endl;}
    
    if (osi_particle_event[k_g][i_a][0].pT < osi_esi.pds[j_g].define_pT || osi_particle_event[k_g][i_a][0].ET < osi_esi.pds[j_g].define_ET){
      osi_cut_ps[i_a] = -1; 
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_missing" << endl;}
      logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_missing" << endl; 
      if (osi_switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
    }
    
    if (osi_switch_output_cutinfo){
      info_cut << endl;
      info_cut << "Summary after runtime_missing:" << endl;
      //      for (int i_g = 0; i_g < osi_runtime_missing.size(); i_g++){
      for (int i_g = 0; i_g < oset.ps_runtime_missing[i_a].size(); i_g++){
	info_cut << "[" << setw(2) << i_a << "]   osi_particle_event[" << setw(2) << k_g << " -> " << setw(10) << osi_esi.object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << osi_particle_event[k_g][i_a].size() << endl;
      }
      info_cut << endl;
    }
    //    logger << LOG_INFO << "[" << setw(2) << i_a << "]   osi_particle_event[" << setw(2) << k_g << " -> " << setw(10) << osi_esi.object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << osi_particle_event[k_g][i_a].size() << endl;
  }

  /*
  //      runtime_missing   actually needed only once because "invisible" particles don't change on reduced phase spaces -> check evaluation of runtime_missing !!!
  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_missing: " << "   osi_runtime_missing.size() = " << osi_runtime_missing.size() << endl;}
  logger << LOG_DEBUG_VERBOSE << "runtime_missing" << "   osi_runtime_missing.size() = " << osi_runtime_missing.size() << endl;

  for (int i_g = 0; i_g < osi_runtime_missing.size(); i_g++){
    int j_g = osi_runtime_missing[i_g];       // j_g -> number in relevant_object_list
    int k_g = osi_equivalent_no_object[j_g];  // k_g -> number in object_list
    // later...   int k_g = j_g;

    fourvector p_missing;
    for (int i_o = 0; i_o < osi_runtime_order[j_g].size(); i_o++){
      int i_p = osi_runtime_order[j_g][i_o];
      p_missing = p_missing + osi_particle_event[0][i_a][i_p].momentum;
    }
    osi_particle_event[k_g][i_a].push_back(particle(p_missing));
        
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
					    << "   pT:  " << setw(10) << osi_particle_event[k_g][i_a][0].pT << " > " << setw(8) << osi_esi.pds[j_g].define_pT 
					    << "   ET:  " << setw(10) << osi_particle_event[k_g][i_a][0].ET << " > " << setw(8) << osi_esi.pds[j_g].define_ET 
					    << "   ---   " 
					    << (osi_particle_event[k_g][i_a][0].pT < osi_esi.pds[j_g].define_pT ||
						osi_particle_event[k_g][i_a][0].ET < osi_esi.pds[j_g].define_ET) << endl;}
    
    
    if (osi_particle_event[k_g][i_a][0].pT < osi_esi.pds[j_g].define_pT || osi_particle_event[k_g][i_a][0].ET < osi_esi.pds[j_g].define_ET){
      osi_cut_ps[i_a] = -1; 
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_missing" << endl;}
      logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_missing" << endl; 
      if (osi_switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
    }
  }
      
  if (osi_switch_output_cutinfo){
    info_cut << endl;
    info_cut << "Summary after runtime_missing:" << endl;
    for (int i_g = 0; i_g < osi_runtime_missing.size(); i_g++){
      // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_missing
      int j_g = osi_runtime_missing[i_g];  // j_g -> number in relevant_object_list
      int k_g = osi_equivalent_no_object[j_g];  // k_g -> number in object_list
      //      info_cut << "[" << setw(2) << i_a << "]   osi_esi.object_list_selection[runtime_missing[" << i_g << "] = " << j_g << "] = " << osi_esi.object_list_selection[j_g] << endl;
      info_cut << "[" << setw(2) << i_a << "]   osi_particle_event[" << setw(2) << k_g << " -> " << setw(10) << osi_esi.object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << osi_particle_event[k_g][i_a].size() << endl;
    }
    info_cut << endl;
  }

  */
      
  //////////////////////////////////
  //  determination of protojets  //
  //////////////////////////////////

  // particle_protojet  does not contain photons here - added later after Frixione isolation.
  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   jet_algorithm = " << osi_jet_algorithm << endl;}
  logger << LOG_DEBUG_VERBOSE << "jet algorithm: select protojet candidates" << endl;

  oset.particle_protojet.clear();
  oset.protojet_flavour.clear();
  oset.protojet_parton_origin.clear();

  logger << LOG_DEBUG_VERBOSE << "osi_ps_runtime_jet_algorithm.size() = " << osi_ps_runtime_jet_algorithm.size() << endl;
  for (int i_l = 0; i_l < osi_ps_runtime_jet_algorithm[i_a].size(); i_l++){
    logger << LOG_DEBUG_VERBOSE << "osi_ps_runtime_jet_algorithm[" << i_a << "].size() = " << osi_ps_runtime_jet_algorithm[i_a].size() << endl;
    int i_p = osi_ps_runtime_jet_algorithm[i_a][i_l];
    oset.particle_protojet.push_back(osi_particle_event[0][i_a][i_p]);

    oset.protojet_parton_origin.push_back(vector<int> (1, i_p));
    logger << LOG_DEBUG_VERBOSE << "osi_type_parton[" << i_a << "].size() = " << osi_type_parton[i_a].size() << endl;
    if (abs(osi_type_parton[i_a][i_p]) < 5){oset.protojet_flavour.push_back(vector<int> (1, 0));}  // treatment of light jets -> flavour numbers between -4 and +4  ->  0 (ljet)
    else {oset.protojet_flavour.push_back(vector<int> (1, osi_type_parton[i_a][i_p]));}
    logger << LOG_DEBUG_VERBOSE << "osi_ps_runtime_jet_algorithm[" << i_a << "].size() = " << osi_ps_runtime_jet_algorithm[i_a].size() << endl;
  }
  //  oset.particle_protojet  contains all particles relevant for Frixione isolation.

  for (int i_p = 0; i_p < oset.particle_protojet.size(); i_p++){
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   oset.particle_protojet[" << i_p << "]   pT = " << oset.particle_protojet[i_p].pT << "   ET = " << oset.particle_protojet[i_p].ET << "   flavour = " << oset.protojet_flavour[i_p][0] << "   origin = " << oset.protojet_parton_origin[i_p][0] << endl;}
  }
  logger << LOG_DEBUG_VERBOSE << "phasespace " << i_a << " " << oset.particle_protojet.size() << ": protojets" << endl;
  for (int i_p = 0; i_p < oset.particle_protojet.size(); i_p++){
    logger << LOG_DEBUG_VERBOSE << "oset.particle_protojet[" << i_p << "] = " << oset.particle_protojet[i_p] << endl << "   flavour = " << oset.protojet_flavour[i_p][0] << "   origin = " << oset.protojet_parton_origin[i_p][0] << endl;
  }
  


  //////////////////////////
  //  Frixione isolation  //
  //////////////////////////

  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before (osi_frixione_isolation)):   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}
  if (osi_frixione_isolation){
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   photon_isolation" << endl;}
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   photon_isolation:   runtime_photon_isolation.size() = " << osi_runtime_photon_isolation.size() << endl;}
    logger << LOG_DEBUG_VERBOSE << "runtime_photon_isolation" << endl;
    logger << LOG_DEBUG_VERBOSE << "osi_runtime_photon_isolation.size() = " << osi_runtime_photon_isolation.size() << endl;
    
    oset.particle_protojet_photon.clear();
    oset.protojet_flavour_photon.clear();
    oset.protojet_parton_origin_photon.clear();
    
    for (int i_g = 0; i_g < osi_runtime_photon_isolation.size(); i_g++){
      int j_g = osi_runtime_photon_isolation[i_g];
      int k_g = osi_equivalent_no_object[j_g];
      logger << LOG_DEBUG_VERBOSE << "oset.no_unrecombined_photon.size() = " << oset.no_unrecombined_photon.size() << endl;
      for (int i_u = 0; i_u < oset.no_unrecombined_photon.size(); i_u++){
	int i_p = oset.no_unrecombined_photon[i_u];
	if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
						<< "   y: " << setw(10) << abs(osi_particle_event[0][i_a][i_p].rapidity) << " < " << setw(8) << osi_esi.pds[j_g].define_y 
						<< "   eta:  " << setw(10) << abs(osi_particle_event[0][i_a][i_p].eta) << " < " << setw(8) << osi_esi.pds[j_g].define_eta 
						<< "   pT:  " << setw(10) << abs(osi_particle_event[0][i_a][i_p].pT) << " > " << setw(8) << osi_esi.pds[j_g].define_pT 
						<< "   ET:  " << setw(10) << abs(osi_particle_event[0][i_a][i_p].ET) << " > " << setw(8) << osi_esi.pds[j_g].define_ET 
						<< "   ---   " 
						<< (abs(osi_particle_event[0][i_a][i_p].rapidity) < osi_esi.pds[j_g].define_y && 
						    abs(osi_particle_event[0][i_a][i_p].eta) < osi_esi.pds[j_g].define_eta && 
						    osi_particle_event[0][i_a][i_p].pT >= osi_esi.pds[j_g].define_pT && 
						    osi_particle_event[0][i_a][i_p].ET >= osi_esi.pds[j_g].define_ET) << endl;}
	
	if (abs(osi_particle_event[0][i_a][i_p].rapidity) < osi_esi.pds[j_g].define_y && 
	    abs(osi_particle_event[0][i_a][i_p].eta) < osi_esi.pds[j_g].define_eta && 
	    osi_particle_event[0][i_a][i_p].pT >= osi_esi.pds[j_g].define_pT && 
	    osi_particle_event[0][i_a][i_p].ET >= osi_esi.pds[j_g].define_ET){
	  int temp_number_photon = osi_n_object[k_g][i_a];  //  temp_number_photon = number of identified photons before the one considered now (i_p)
	  logger << LOG_DEBUG_VERBOSE << "temp_number_photon = " << temp_number_photon << endl;
	  frixione_isolation(osi_n_object[k_g][i_a], osi_particle_event[k_g][i_a], osi_particle_event[0][i_a][i_p], oset.particle_protojet, oset);
	  // temporary: oset.particle_protojet_photon etc. could be directly filled...
	  if (temp_number_photon == osi_n_object[k_g][i_a] && osi_photon_jet_algorithm){  //  Happens if the considered photon is not identified. Non-identified photons will enter the jet algorithm afterwards.
	    logger << LOG_DEBUG_VERBOSE << "Add un-identified photons to jet candidates" << endl;
	    oset.particle_protojet_photon.push_back(osi_particle_event[0][i_a][i_p]);
	    oset.protojet_parton_origin_photon.push_back(vector<int> (1, i_p));
	    oset.protojet_flavour_photon.push_back(vector<int> (1, 22)); // photon
	    logger << LOG_DEBUG_VERBOSE << "oset.particle_protojet_photon.size() = " << oset.particle_protojet_photon.size() << endl;
	  }
	  logger << LOG_DEBUG_VERBOSE << "osi_particle_event[" << k_g << "][" << i_a << "].size() = " << osi_particle_event[k_g][i_a].size() << endl;
	  logger << LOG_DEBUG_VERBOSE << "osi_n_object[" << k_g << "][" << i_a << "] = " << osi_n_object[k_g][i_a] << endl;
	}
	else if (osi_photon_jet_algorithm){  //  Non-identified photons will enter the jet algorithm afterwards.
	  oset.particle_protojet_photon.push_back(osi_particle_event[0][i_a][i_p]);
	  oset.protojet_parton_origin_photon.push_back(vector<int> (1, i_p));
	  oset.protojet_flavour_photon.push_back(vector<int> (1, 22)); // photon
	  logger << LOG_DEBUG_VERBOSE << "oset.particle_protojet_photon.size() = " << oset.particle_protojet_photon.size() << endl;
	}
      }

      // Not fully generic if photon--photon recombination is applied!
      
      if (!osi_photon_photon_recombination){
	if ((osi_n_object[k_g][i_a] < osi_esi.pds[j_g].n_observed_min) || (osi_n_object[k_g][i_a] > osi_esi.pds[j_g].n_observed_max)){
	  osi_cut_ps[i_a] = -1; 
	  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_photon_isolation" << endl;}
	  logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_photon_isolation" << endl; 
	  if (osi_switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
	  return;
	}
	// shouldn't this be osi_particle_event[k_g][i_a].size() instead of osi_n_object[k_g][i_a] ???
	//      if (osi_n_object[k_g][i_a] > 1){sort(osi_particle_event[k_g][i_a].begin(), osi_particle_event[k_g][i_a].end(), greaterBypT);}
	// let's try:
	if (osi_particle_event[k_g][i_a].size() > 1){sort(osi_particle_event[k_g][i_a].begin(), osi_particle_event[k_g][i_a].end(), greaterBypT);}
	// Should be identical.
      }
      
      // added to let non-identified photons enter the jet algorithm
      if (osi_photon_jet_algorithm){  //  Non-identified photons are added to protojets.
	for (int i_p = 0; i_p < oset.particle_protojet_photon.size(); i_p++){
	  oset.particle_protojet.push_back(oset.particle_protojet_photon[i_p]);
	  oset.protojet_parton_origin.push_back(oset.protojet_parton_origin_photon[i_p]);
	  oset.protojet_flavour.push_back(oset.protojet_flavour_photon[i_p]);
	}
      }
      //
      else {
	// otherwise add to "photon" particle vector - without increasing photon number ???
      }
    }
    /*
    if (i_a == 0){
      logger << LOG_INFO << "photon_photon_recombination input (isolated photon: " << osi_particle_event[osi_access_object["photon"]][i_a].size() << " - rest: " << oset.particle_protojet_photon.size() << "): " << osi_particle_event[osi_access_object["photon"]][i_a].size() + oset.particle_protojet_photon.size() << endl;
    }
    */
    /*
    if (i_a == 0){
      for (int i_p = 3; i_p < osi_p_parton[i_a].size(); i_p++){
	logger << LOG_INFO << "[" << setw(2) << i_a << "]   type_parton[" << i_p << "] = " << setw(3) << osi_type_parton[i_a][i_p] << " -> " << osi_particle_event[0][i_a][i_p].momentum << endl;//<< " pT = " << osi_particle_event[0][i_a][i_p].pT << " eta = " << osi_particle_event[0][i_a][i_p].eta << endl;
      }


      logger << LOG_INFO << "photon_photon_recombination input (isolated photon: " << osi_particle_event[osi_access_object["photon"]][i_a].size() << " - rest: " << oset.particle_protojet_photon.size() << "):" << endl;
      //      logger << LOG_INFO << "photon.size() = " << osi_particle_event[osi_access_object["photon"]][i_a].size() << endl;
      for (int i_p = 0; i_p < osi_particle_event[osi_access_object["photon"]][i_a].size(); i_p++){
	logger << LOG_INFO << "photon[" << i_p << "] = " << osi_particle_event[osi_access_object["photon"]][i_a][i_p].momentum << endl;
      }
      //      logger << LOG_INFO << "oset.particle_protojet_photon.size() = " << oset.particle_protojet_photon.size() << endl;
      for (int i_p = 0; i_p < oset.particle_protojet_photon.size(); i_p++){
	logger << LOG_INFO << "oset.particle_protojet_photon[" << i_p << "] = " << oset.particle_protojet_photon[i_p].momentum << endl;
      }
      logger.newLine(LOG_INFO);
    }
    */
  }
  else {
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   No Frixione isolation performed" << endl;}
    logger << LOG_DEBUG_VERBOSE << "No Frixione isolation is done." << endl;

    //    logger << LOG_INFO << "i_a = " << i_a << "   oset.no_unrecombined_photon.size() = " << oset.no_unrecombined_photon.size() << endl;

    if (osi_photon_jet_algorithm){  //  All photons (i.e. un-recombined photons) are added to protojets if this option is chosen.
      for (int i_u = 0; i_u < oset.no_unrecombined_photon.size(); i_u++){
	int i_p = oset.no_unrecombined_photon[i_u];
	oset.particle_protojet.push_back(osi_particle_event[0][i_a][i_p]);
	oset.protojet_parton_origin.push_back(vector<int> (1, i_p));
	oset.protojet_flavour.push_back(vector<int> (1, osi_type_parton[i_a][i_p]));
      }
    }
  }

  
  
  //  logger << LOG_INFO << "i_a = " << i_a << "   oset.particle_protojet.size() = " << oset.particle_protojet.size() << endl;

  if (osi_switch_output_cutinfo){
    info_cut << endl;
    info_cut << "Summary after Frixione isolation:" << endl;
    for (int i_g = 0; i_g < osi_runtime_photon_isolation.size(); i_g++){
      // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_photon_isolation
      int j_g = osi_runtime_photon_isolation[i_g];  // j_g -> number in relevant_object_list
      int k_g = osi_equivalent_no_object[j_g];  // k_g -> number in object_list
      //      info_cut << "[" << setw(2) << i_a << "]   osi_esi.object_list_selection[runtime_photon_isolation[" << i_g << "] = " << j_g << "] = " << osi_esi.object_list_selection[j_g] << endl;
      info_cut << "[" << setw(2) << i_a << "]   osi_particle_event[" << setw(2) << k_g << " -> " << setw(10) << osi_esi.object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << osi_particle_event[k_g][i_a].size() << endl;
    }
    info_cut << "Protojets after Frixione isolation:" << endl;
    for (int i_p = 0; i_p < oset.particle_protojet.size(); i_p++){
      info_cut << "protojet[" << i_p << "] = " << oset.particle_protojet[i_p].momentum << "   flavour = " << oset.protojet_flavour[i_p][0] << endl;
    }
    info_cut << endl;
  }


  //////////////////////////////////////////////////
  //  elimination of protojets too close to beam  //
  //////////////////////////////////////////////////

  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   osi_parton_y_max = " << osi_parton_y_max << "   osi_parton_eta_max = " << osi_parton_eta_max << endl;}
  logger << LOG_DEBUG_VERBOSE << "jet algorithm: exclude protojet candidates" << endl;

  // exclude protojets which are too close to the beam (in terms of y or eta)
  if (osi_parton_y_max != 0. || osi_parton_eta_max != 0.){
    for (int j = oset.particle_protojet.size() - 1; j >= 0; j--){
      if (abs(oset.particle_protojet[j].rapidity) > osi_parton_y_max || abs(oset.particle_protojet[j].eta) > osi_parton_eta_max){
	oset.particle_protojet.erase(oset.particle_protojet.begin() + j);
	oset.protojet_flavour.erase(oset.protojet_flavour.begin() + j);
	oset.protojet_parton_origin.erase(oset.protojet_parton_origin.begin() + j);
      }
    }
  }

  //#include "user/specify.extra.exclusion.jet.algorithm.cxx"

  /////////////////////
  //  jet algorithm  //
  /////////////////////

  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before (apply jet_algorithm)):   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}
  if (osi_switch_output_cutinfo){
    for (int i_p = 0; i_p < oset.particle_protojet.size(); i_p++){
      info_cut << "[" << setw(2) << i_a << "]" << "   oset.particle_protojet[" << i_p << "]   pT = " << oset.particle_protojet[i_p].pT << "   ET = " << oset.particle_protojet[i_p].ET << "   flavour = " << oset.protojet_flavour[i_p][0] << "   origin = " << oset.protojet_parton_origin[i_p][0] << endl;
    }
  }


  
  logger << LOG_DEBUG_VERBOSE << "jet algorithm: combine protojets to jets" << endl;

  oset.particle_jet.clear();
  oset.jet_flavour.clear();
  oset.jet_parton_origin.clear();
  
  jet_algorithm_flavour(oset.particle_protojet, oset.protojet_flavour, oset.protojet_parton_origin, oset.particle_jet, oset.jet_flavour, oset.jet_parton_origin, oset);

  if (osi_switch_output_cutinfo){
    for (int i_p = 0; i_p < oset.particle_jet.size(); i_p++){
      stringstream jet_flavour_ss;
      jet_flavour_ss << "( ";
      for (int i_q = 0; i_q < oset.jet_flavour[i_p].size(); i_q++){
	jet_flavour_ss << oset.jet_flavour[i_p][i_q];
	if (i_q < oset.jet_flavour[i_p].size() - 1){jet_flavour_ss << " ";}
      }
      jet_flavour_ss << " )";
      
      stringstream jet_parton_origin_ss;
      jet_parton_origin_ss << "( ";
      for (int i_q = 0; i_q < oset.jet_parton_origin[i_p].size(); i_q++){
	jet_parton_origin_ss << oset.jet_parton_origin[i_p][i_q];
	if (i_q < oset.jet_parton_origin[i_p].size() - 1){jet_parton_origin_ss << " ";}
      }
      jet_parton_origin_ss << " )";
      
      info_cut << "[" << setw(2) << i_a << "]" << "   oset.particle_jet[" << i_p << "]   pT = " << oset.particle_jet[i_p].pT << "   ET = " << oset.particle_jet[i_p].ET << "   flavour = " << jet_flavour_ss.str() << "   origin = " << jet_parton_origin_ss.str() << endl;
      //      info_cut << "[" << setw(2) << i_a << "]" << "   oset.particle_jet[" << i_p << "]   pT = " << oset.particle_jet[i_p].pT << "   ET = " << oset.particle_jet[i_p].ET << "   flavour = " << oset.jet_flavour[i_p][0] << "   origin = " << oset.jet_parton_origin[i_p][0] << endl;
    }
  }

  //  logger << LOG_INFO << "i_a = " << i_a << "   oset.particle_jet.size() = " << oset.particle_jet.size() << endl;

  //#include "user/specify.add.excluded.jet.cxx"



  ////////////////////////////////////////////////////////////
  //  Frixione isolation - removal of jets close to photon  //
  ////////////////////////////////////////////////////////////

  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before (osi_frixione_isolation && osi_frixione_jet_removal)):   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}

  // sort out jets which are too close to an 'isolated photon' according to Frixione
  // might need to be adapted for photon_recombination & frixione_isolation !!!
  if (osi_frixione_isolation && osi_frixione_jet_removal){ // up to date ??? osi_equivalent_no_object[osi_no_relevant_object[...]] constructions !!!
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   osi_runtime_photon_isolation.size() = " << osi_runtime_photon_isolation.size() << endl;}
    
    // temporarily uncomment to see effect !!!
    if (osi_runtime_photon_isolation.size() != 0){//osi_no_relevant_object["photon"]
      logger << LOG_DEBUG_VERBOSE << "photon-isolation caused jet removal" << endl;
       
      for (int i_phot = 0; i_phot < osi_n_object[osi_access_object["photon"]][i_a]; i_phot++){
	for (int i_jet = oset.particle_jet.size() - 1; i_jet >= 0; i_jet--){
	  logger << LOG_DEBUG_VERBOSE << "sqrt(R2_eta(osi_particle_event[osi_equivalent_no_object[osi_no_relevant_object[photon]]][i_a][i_phot], oset.particle_jet[i_jet])) = " << sqrt(R2_eta(osi_particle_event[osi_access_object["photon"]][i_a][i_phot], oset.particle_jet[i_jet])) << "   frixione_delta_0 = " << osi_frixione_delta_0 << endl;
	  if (sqrt(R2_eta(osi_particle_event[osi_access_object["photon"]][i_a][i_phot], oset.particle_jet[i_jet])) < osi_frixione_delta_0){
	    logger << LOG_DEBUG_VERBOSE << "oset.particle_jet[" << i_jet << "] = " << oset.particle_jet[i_jet] << "   removed.   frixione_delta_0 = " << osi_frixione_delta_0 << endl;
	    oset.particle_jet.erase(oset.particle_jet.begin() + i_jet);
	    oset.jet_flavour.erase(oset.jet_flavour.begin() + i_jet);
	  }
	}
      }
    }
    
    for (int i_p = 0; i_p < oset.particle_jet.size(); i_p++){
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   oset.particle_jet[" << i_p << "]   pT = " << oset.particle_jet[i_p].pT << "   ET = " << oset.particle_jet[i_p].ET << "   flavour = " << oset.jet_flavour[i_p][0] << "   origin = " << oset.jet_parton_origin[i_p][0] << endl;}
    }
  }
  

  /////////////////////////////////////////////////
  //  jet algorithm - interpretation of outcome  //
  /////////////////////////////////////////////////

  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before oset.particle_jet_id):   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}

  logger << LOG_DEBUG_VERBOSE << "oset.particle_jet_id" << endl;
  //  vector<particle_id> oset.particle_jet_id(oset.particle_jet.size());
  ///  static vector<particle_id> oset.particle_jet_id;
  oset.particle_jet_id.clear();
  oset.particle_jet_id.resize(oset.particle_jet.size());

  for (int i_j = 0; i_j < oset.particle_jet.size(); i_j++){
    oset.particle_jet_id[i_j].xparticle = &oset.particle_jet[i_j];
    oset.particle_jet_id[i_j].content = oset.jet_flavour[i_j];

    // IR-safe defintion - b and bx are combined to light jet: switch needed !!!
    oset.particle_jet_id[i_j].identity = accumulate(oset.particle_jet_id[i_j].content.begin(), oset.particle_jet_id[i_j].content.end(), 0);
    if (oset.msi.M_b > 0. && oset.particle_jet_id[i_j].identity == 0){
      // no IR-safe defintion in case of massless b quarks - b and bx are combined to bottom jet: switch needed !!!
      for (int i_q = 0; i_q < oset.particle_jet_id[i_j].content.size(); i_q++){
	if (abs(oset.particle_jet_id[i_j].content[i_q]) == 5){oset.particle_jet_id[i_j].identity = 5; break;}
      }
    }
    
    // modification compared to IR-safe version of flavoured jet combination:
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   oset.particle_jet_id[" << i_j << "].content.size() = " << oset.particle_jet_id[i_j].content.size() << endl;}
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   oset.particle_jet_id[" << i_j << "].identity = " << oset.particle_jet_id[i_j].identity << endl;}
	
    // valid only for W+jets !!! does, however, not look wrong for other processes with photon recombination !!!
    // with this (E_phot -- E_had) criterion, it could happen that non-isolated photon collect enough pT to be considered a photon,
    // which, however, at present has no impact because the (properly isolated) photons are counted right after the Frixione isolation...
    // For processes without photons in the jet algorithm, nothing should happen here !!!
    
    // deactivate: this could allow to treat photons as jets:
    //    if (oset.particle_jet_id[i_j].content.size() == 1){continue;}
	
    if (oset.particle_jet_id[i_j].identity == 0){} // light jet or b-bx jet
    else if (oset.particle_jet_id[i_j].identity == 5 || // bjet_b
	     oset.particle_jet_id[i_j].identity == -5){continue;} // bjet_bx
    else {
      double E_phot = 0.;
      double E_had = 0.;
      int count_phot = 0;
      for (int i_l = 0; i_l < oset.particle_jet_id[i_j].content.size(); i_l++){
	if (oset.particle_jet_id[i_j].content[i_l] == 22){
	  E_phot += (osi_particle_event[0][i_a][oset.jet_parton_origin[i_j][i_l]].momentum).x0();
	  count_phot++;
	}
	else {
	  E_had += (osi_particle_event[0][i_a][oset.jet_parton_origin[i_j][i_l]].momentum).x0();
	}
      }

      if (osi_switch_output_cutinfo){
	info_cut << "E_phot = " << E_phot << endl;
	info_cut << "E_had = " << E_had << endl;
      }

      if (E_phot / (E_phot + E_had) >= osi_photon_E_threshold_ratio){
	//     if (E_phot / (E_phot + E_had) > osi_photon_E_threshold_ratio){
	// bug !!!      if (E_phot / (E_had + E_had) > osi_photon_E_threshold_ratio){
	oset.particle_jet_id[i_j].identity = 22;
      }
      else {
	oset.particle_jet_id[i_j].identity = oset.particle_jet_id[i_j].identity - count_phot * 22;
      }
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   oset.particle_jet_id[" << i_j << "].identity = " << oset.particle_jet_id[i_j].identity << endl;}
      logger << LOG_DEBUG_VERBOSE << "E_phot = " << E_phot << endl;
      logger << LOG_DEBUG_VERBOSE << "E_had = " << E_had << endl;
      logger << LOG_DEBUG_VERBOSE << "oset.particle_jet_id[" << i_j << "].identity = " << oset.particle_jet_id[i_j].identity << endl;
    }

    //#include "user/specify.jet.recombination.cxx"
  }
      
  sort(oset.particle_jet_id.begin(), oset.particle_jet_id.end(), idgreaterBypT);

  

  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   runtime_jet_recombination:   osi_runtime_jet_recombination.size() = " << osi_runtime_jet_recombination.size() << endl;}
      
  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before jet_algorithm):   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}

  logger << LOG_DEBUG_VERBOSE << "runtime_jet_recombination" << endl;
  for (int i_g = 0; i_g < osi_runtime_jet_recombination.size(); i_g++){
    int j_g = osi_runtime_jet_recombination[i_g];
    int k_g = osi_equivalent_no_object[j_g];
    logger << LOG_DEBUG_VERBOSE << "runtime_jet_recombination: i_g = " << i_g << "   j_g = " << j_g << "   k_g = " << k_g << endl;
    //    logger << LOG_DEBUG_VERBOSE << "osi_relevant_define_y[" << j_g << "] = " << osi_esi.pds[j_g].define_y << endl;
    //    logger << LOG_DEBUG_VERBOSE << "osi_relevant_define_eta[" << j_g << "] = " << osi_esi.pds[j_g].define_eta << endl;
    //    logger << LOG_DEBUG_VERBOSE << "osi_relevant_define_pT[" << j_g << "] = " << osi_esi.pds[j_g].define_pT << endl;
    //    logger << LOG_DEBUG_VERBOSE << "osi_relevant_define_ET[" << j_g << "] = " << osi_esi.pds[j_g].define_ET << endl;
    if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]   osi_esi.object_list_selection[runtime_jet_recombination[" << i_g << "] = " << j_g << "] = " << osi_esi.object_list_selection[j_g] << endl;}
    
    logger << LOG_DEBUG_VERBOSE << "oset.particle_jet_id.size() = " << oset.particle_jet_id.size() << endl;
    for (int i_j = 0; i_j < oset.particle_jet_id.size(); i_j++){
      logger << LOG_DEBUG_VERBOSE << "(*oset.particle_jet_id[" << i_j << "].xparticle) = " << (*oset.particle_jet_id[i_j].xparticle) << endl;
      /*
	logger << LOG_DEBUG_VERBOSE << "osi_no_relevant_object[photon] = " << osi_no_relevant_object["photon"] << endl;
	logger << LOG_DEBUG_VERBOSE << "k_g = " << k_g << endl;
	logger << LOG_DEBUG_VERBOSE << "j_g = " << j_g << endl;
	logger << LOG_DEBUG_VERBOSE << "oset.particle_jet_id[" << i_j << "].identity = " <<oset.particle_jet_id[i_j].identity  << endl;
      */
      // k_g (relevant_object) -> j_g (object)
      if      (j_g == osi_no_relevant_object["jet"] && oset.particle_jet_id[i_j].identity == 22){continue;}
      else if (j_g == osi_no_relevant_object["photon"] && oset.particle_jet_id[i_j].identity != 22){continue;}
      else if (j_g == osi_no_relevant_object["ljet"] && oset.particle_jet_id[i_j].identity != 0){continue;}
      else if (j_g == osi_no_relevant_object["bjet"] && abs(oset.particle_jet_id[i_j].identity) != 5){continue;}
      else if (j_g == osi_no_relevant_object["bjet_b"] && oset.particle_jet_id[i_j].identity != +5){continue;}
      else if (j_g == osi_no_relevant_object["bjet_bx"] && oset.particle_jet_id[i_j].identity != -5){continue;}
      
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << setprecision(5)
					      << "   y: " << setw(10) << abs((*oset.particle_jet_id[i_j].xparticle).rapidity) << " < " << setw(8) << osi_esi.pds[j_g].define_y 
					      << "   eta:  " << setw(10) << abs((*oset.particle_jet_id[i_j].xparticle).eta) << " < " << setw(8) << osi_esi.pds[j_g].define_eta 
					      << "   pT:  " << setw(10) << abs((*oset.particle_jet_id[i_j].xparticle).pT) << " > " << setw(8) << osi_esi.pds[j_g].define_pT 
					      << "   ET:  " << setw(10) << abs((*oset.particle_jet_id[i_j].xparticle).ET) << " > " << setw(8) << osi_esi.pds[j_g].define_ET 
					      << "   ---   " 
					      << (abs((*oset.particle_jet_id[i_j].xparticle).rapidity) < osi_esi.pds[j_g].define_y && 
						  abs((*oset.particle_jet_id[i_j].xparticle).eta) < osi_esi.pds[j_g].define_eta)
					      << "   ---   " 
					      << ((*oset.particle_jet_id[i_j].xparticle).pT >= osi_esi.pds[j_g].define_pT && 
						  (*oset.particle_jet_id[i_j].xparticle).ET >= osi_esi.pds[j_g].define_ET) << endl;}
      
      
      if (abs((*oset.particle_jet_id[i_j].xparticle).rapidity) < osi_esi.pds[j_g].define_y && 
	  abs((*oset.particle_jet_id[i_j].xparticle).eta) < osi_esi.pds[j_g].define_eta){ 
	if ((*oset.particle_jet_id[i_j].xparticle).pT >= osi_esi.pds[j_g].define_pT &&
	    (*oset.particle_jet_id[i_j].xparticle).ET >= osi_esi.pds[j_g].define_ET){
	  // not counted as an identified photon if Frixione isolation is used !!!
	  if (j_g == osi_no_relevant_object["photon"] && osi_frixione_isolation){}
	  else {
	    osi_n_object[k_g][i_a]++;
	  }
	  logger << LOG_DEBUG_VERBOSE << "osi_n_object[" << k_g << "][" << i_a << "] = " << osi_n_object[k_g][i_a] << endl;
	}
	osi_particle_event[k_g][i_a].push_back(*oset.particle_jet_id[i_j].xparticle);
	logger << LOG_DEBUG_VERBOSE << "osi_particle_event[" << k_g << "][" << i_a << "].size() = " << osi_particle_event[k_g][i_a].size() << endl;
      }
    }
    logger << LOG_DEBUG_VERBOSE << "osi_esi.pds[" << j_g << "].n_observed_min = " << osi_esi.pds[j_g].n_observed_min << endl;
    logger << LOG_DEBUG_VERBOSE << "osi_esi.pds[" << j_g << "].n_observed_max = " << osi_esi.pds[j_g].n_observed_max << endl;

    if (!(j_g == osi_no_relevant_object["photon"] && osi_frixione_isolation && osi_photon_photon_recombination)){
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   " << osi_esi.pds[j_g].n_observed_min << " <= osi_n_object[" << setw(2) <<k_g << "][" << setw(2) <<i_a << "] = " << osi_n_object[k_g][i_a] << " <= " << osi_esi.pds[j_g].n_observed_max << endl;}
      
      if ((osi_n_object[k_g][i_a] < osi_esi.pds[j_g].n_observed_min) || (osi_n_object[k_g][i_a] > osi_esi.pds[j_g].n_observed_max)){
	osi_cut_ps[i_a] = -1; 
	if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after runtime_jet_recombination" << endl;}
	logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after runtime_jet_recombination" << endl; 
      if (osi_switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
      }
    }
  }

  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   (Before osi_frixione_isolation && osi_photon_photon_recombination):   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}

    // Only applied for two photons: (combine with osi_photon_E_threshold_ratio = 1.!)
  static int photon_photon_counter = 0;
  if (osi_frixione_isolation && osi_photon_photon_recombination){
    int j_g = osi_no_relevant_object["photon"];
    int k_g = osi_access_object["photon"];
    int stop_photon_photon_recombination = 0;
    if (osi_particle_event[k_g][i_a].size() < 2){stop_photon_photon_recombination = 1;}
    
    int change_photon_photon_recombination = 0;
    //    if (k_g != osi_access_object["photon"]){logger << LOG_DEBUG << "k_g = " << k_g << " != " << osi_access_object["photon"] << " = osi_access_object[photon]" << endl;}
    
    while (!stop_photon_photon_recombination){
      double distance = osi_photon_photon_recombination_R;
      int i_rec = -1;
      int j_rec = -1;
      for (int i_p = 0; i_p < osi_particle_event[k_g][i_a].size(); i_p++){
	for (int j_p = i_p + 1; j_p < osi_particle_event[k_g][i_a].size(); j_p++){
	  double temp_distance = 0.;
	  if (osi_photon_R_definition == 0){temp_distance = R2_eta(osi_particle_event[k_g][i_a][i_p], osi_particle_event[k_g][i_a][j_p]);}
	  else if (osi_photon_R_definition == 1){temp_distance = R2_rapidity(osi_particle_event[k_g][i_a][i_p], osi_particle_event[k_g][i_a][j_p]);}
	  //	  logger << LOG_INFO << "photon_photon_distance = " << temp_distance << endl;
	  if (temp_distance < distance){
	    //	    logger << LOG_INFO << "photon_photon_distance = " << temp_distance << " < " << distance << endl;
	    distance = temp_distance;
	    i_rec = i_p;
	    j_rec = j_p;	      
	  }
	}
      }
      if (i_rec != -1 && j_rec != -1){
	change_photon_photon_recombination = 1;
	logger << LOG_DEBUG << "photon_photon_recombination applied (" << ++photon_photon_counter << "): distance = " << distance << endl;
	logger << LOG_DEBUG << "osi_particle_event[" << k_g << "][" << i_a << "][" << i_rec << "] = " << osi_particle_event[k_g][i_a][i_rec].momentum << endl;
	logger << LOG_DEBUG << "osi_particle_event[" << k_g << "][" << i_a << "][" << j_rec << "] = " << osi_particle_event[k_g][i_a][j_rec].momentum << endl;
	osi_particle_event[k_g][i_a][i_rec] = osi_particle_event[k_g][i_a][i_rec] + osi_particle_event[k_g][i_a][j_rec];
	osi_particle_event[k_g][i_a].erase(osi_particle_event[k_g][i_a].begin() + j_rec);
	if (j_rec < osi_n_object[k_g][i_a]){osi_n_object[k_g][i_a]--;}
      }
      else {
	stop_photon_photon_recombination = 1;
	// end while loop (to be added) !!!
      }
    }
    //      int old_n_photon = osi_n_object[k_g][i_a];
    if (change_photon_photon_recombination){
      osi_n_object[k_g][i_a] = 0;
      for (int i_p = 0; i_p < osi_particle_event[k_g][i_a].size(); i_p++){
	if (abs(osi_particle_event[0][i_a][i_p].rapidity) < osi_esi.pds[j_g].define_y && 
	    abs(osi_particle_event[0][i_a][i_p].eta) < osi_esi.pds[j_g].define_eta && 
	    osi_particle_event[0][i_a][i_p].pT >= osi_esi.pds[j_g].define_pT && 
	    osi_particle_event[0][i_a][i_p].ET >= osi_esi.pds[j_g].define_ET){
	  osi_n_object[k_g][i_a]++;
	}
      }
    }
    
    if ((osi_n_object[k_g][i_a] < osi_esi.pds[j_g].n_observed_min) || (osi_n_object[k_g][i_a] > osi_esi.pds[j_g].n_observed_max)){
      osi_cut_ps[i_a] = -1; 
      if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   cut after photon_photon_recombination" << endl;}
      logger << LOG_DEBUG_VERBOSE << "event at [i_a = " << i_a << "] cut after photon_photon_recombination" << endl; 
      if (osi_switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
      return;
    }
  }

    

  
  if (osi_switch_output_cutinfo){
    info_cut << endl;
    info_cut << "Summary after runtime_jet_recombination:" << endl;
    for (int i_g = 0; i_g < osi_runtime_jet_recombination.size(); i_g++){
      // j_g -> running number (directing to number in relevant_object_list) of objects contained in runtime_jet_recombination
      int j_g = osi_runtime_jet_recombination[i_g];  // j_g -> number in relevant_object_list
      int k_g = osi_equivalent_no_object[j_g];  // k_g -> number in object_list
      //      info_cut << "[" << setw(2) << i_a << "]   osi_esi.object_list_selection[runtime_jet_recombination[" << i_g << "] = " << j_g << "] = " << osi_esi.object_list_selection[j_g] << endl;
      info_cut << "[" << setw(2) << i_a << "]   osi_particle_event[" << setw(2) << k_g << " -> " << setw(10) << osi_esi.object_list_selection[j_g] << "][" << setw(2) << i_a << "].size() = " << osi_particle_event[k_g][i_a].size() << endl;
    }
    info_cut << endl;
  }

  ///


  // Old N-jettiness implementation shifted from here !!!

  


  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   generic event selection passed   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}
  //      if (osi_switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str(); }
  

  logger << LOG_DEBUG_VERBOSE << "after:  osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << endl;
  logger << LOG_DEBUG_VERBOSE << "osi_switch_qTcut = " << osi_switch_qTcut << endl;
  logger << LOG_DEBUG_VERBOSE << "osi_output_n_qTcut = " << osi_output_n_qTcut << endl;
  if (osi_switch_qTcut == 0){osi_cut_ps[i_a] = 0;}
  else if (osi_switch_qTcut == 1 && osi_output_n_qTcut == 1){osi_cut_ps[i_a] = 0;}
  else if (osi_switch_qTcut == 3 && osi_output_n_qTcut == 1){osi_cut_ps[i_a] = 0;}
  else if (osi_type_contribution == "CT" || 
	   osi_type_contribution == "CJ" || 
	   osi_type_contribution == "CT2" || 
	   osi_type_contribution == "CJ2" || 
	   osi_type_contribution == "L2CT" || 
	   osi_type_contribution == "L2CJ"){
    // Contributions with a Born phase-space, but qTcut dependence:
    osi_cut_ps[i_a] = osi_n_qTcut - 1;  // check if '- 1' is correct !!!
  }
  else {
    logger << LOG_DEBUG_VERBOSE << "osi_switch_qTcut = " << osi_switch_qTcut << endl;
    logger << LOG_DEBUG_VERBOSE << "osi_output_n_qTcut = " << osi_output_n_qTcut << endl;
    logger << LOG_DEBUG_VERBOSE << "osi_n_qTcut - 1 = " << osi_n_qTcut - 1 << endl;
    // osi_cut_ps[i_a] should remain as it is -> No need to re-evaluate !!!
    //    osi_cut_ps[i_a] = osi_n_qTcut - 1;  // check if '- 1' is correct !!!
  }
  
  if (oset.csi->class_contribution_NJcut_explicit){oset.event_selection_NJcut(i_a);}

  if (osi_switch_output_cutinfo){info_cut << "[" << setw(2) << i_a << "]" << "   generic event selection passed and osi_cut_ps[" << i_a << "] set   (osi_cut_ps[" << i_a << "] = " << osi_cut_ps[i_a] << ")." << endl;}
  if (osi_switch_output_cutinfo){logger << LOG_DEBUG << endl << info_cut.str();}
  

     
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;

}
