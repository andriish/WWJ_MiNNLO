#include "header.hpp"
void munich::integration_RA_QCD(){
  static Logger logger("munich::integration_RA_QCD");
  logger << LOG_INFO << "called" << endl;

  generic.determination_subprocess_psp(0, psi);
  generic.combination_subprocess_psp(0, psi, osi);

  // could be modified later ??? merge with MC-mappings of dipoles ???
  psi.MC_n_channel_phasespace[0] = generic.determination_MCchannels_psp(0, psi);
  dipole.push_back(dipole_set("ME2_R", csi.type_parton[0], csi.basic_type_parton[0], psi.symmetry_factor, psi.no_map[0], psi.o_map[0], psi.no_prc[0], psi.o_prc[0], psi.MC_n_channel_phasespace[0]));
  QCD_determine_dipoles(dipole_candidate, csi.type_parton[0], csi.basic_type_parton[0]);
  QCD_selection_dipoles(dipole, dipole_candidate, psi.phasespace_order_alpha_s[0] - 1, psi.phasespace_order_alpha_e[0], psi.phasespace_order_interference[0], psi.RA_singular_region, psi.RA_singular_region_name, psi.RA_singular_region_list, psi, generic);

  // Should be replaced !!!
  static int n_dipoles = dipole.size();
  osi.initialization_complete(isi, psi);

  /*
  psi.RA_dipole = &dipole;
  psi.initialization_complete(isi);
  */
  
  psi.initialization_complete_RA(isi, dipole);

  osi.initialization_integration(psi);

  observable_set save_osi = osi;
  phasespace_set save_psi = psi;
  runresumption_set rsi(osi, psi, save_osi, save_psi);

  calculate_ME2check_RA_QCD(psi, osi);

  osi.max_integrand = osi.sigma_normalization; // ???

  if (psi.n_events_max == 0){osi.int_end = 1;}
  if (osi.user.int_value[osi.user.int_map["rescaling_exponent"]] != 0){psi.hcf = psi.hcf * pow(osi.user.double_value[osi.user.double_map["rescaling_factor"]], osi.user.int_value[osi.user.int_map["rescaling_exponent"]]);}

  if (osi.int_end){osi.output_zero_contribution_complete(psi);}
  osi.initialization_runtime();
  while (osi.int_end == 0){
    rsi.perform_iteration_step();
    if (osi.int_end == 1){break;}

    psi.calculate_IS();

    osi.determine_p_parton(psi);
    // replace by proper nan-check !!!
    if (psi.xbp_all[0] != psi.xbp_all[0]){errorhandling_c_psp_initial(); continue;}
    psi.calculate_IS_RA(dipole);
    if (psi.RA_x_a == 0){generic.ac_psp_psp(0, psi.MC_phasespace.channel, psi);}
    else {psi.ac_psp_RA_dipoles();}
    psi.correct_phasespacepoint_real();
    for (int i_p = 0; i_p < osi.p_parton[0].size(); i_p++){osi.p_parton[0][psi.o_map[0][i_p]] = psi.xbp_all[0][intpow(2, i_p - 1)];}
    // replace by proper nan-check !!!
    if (psi.xbp_all[0] != psi.xbp_all[0]){errorhandling_c_xbpsp(); continue;}
    for (int ib = 1; ib < osi.p_parton[0].size(); ib++){logger << LOG_DEBUG_VERBOSE << "osi.p_parton[0][" << ib << "][0] = " << psi.xbp_all[0][intpow(2, ib - 1)] << "   " << sqrt(abs(psi.xbp_all[0][intpow(2, ib - 1)].m2())) << endl;}
    for (int ib = 0; ib < osi.p_parton[0].size(); ib++){logger << LOG_DEBUG_VERBOSE << "osi.p_parton[0][" << ib << "] = " << osi.p_parton[0][ib] << "   " << osi.p_parton[0][ib].m2() << "   " << sqrt(abs(osi.p_parton[0][ib].m2())) << endl;}

    psi.determine_dipole_phasespace_RA(dipole);
    for (int i_a = 1; i_a < n_dipoles; i_a++){for (int i_p = 0; i_p < osi.p_parton[i_a].size(); i_p++){osi.p_parton[i_a][i_p] = psi.xbp_all[i_a][intpow(2, i_p - 1)];}}
    psi.correct_phasespacepoint_dipole(); // Which phase-space is actually corrected here (psi -- osi) ???

    // replace by proper nan-check !!!
    if (osi.p_parton[0] != osi.p_parton[0]){errorhandling_c_psp(); continue;}
    perform_event_selection(osi, generic);
    if (osi.switch_RS == 1){for (int i_a = 1; i_a < n_dipoles; i_a++){osi.cut_ps[i_a] = -1;}}
    else if (osi.switch_RS == 2){osi.cut_ps[0] = -1;}
    
    osi.first_non_cut_ps = -1;
    for (int i_a = 0; i_a < osi.cut_ps.size(); i_a++){if (osi.cut_ps[i_a] > -1){osi.first_non_cut_ps = i_a; break;}}
    if (osi.first_non_cut_ps == -1){psi.handling_cut_psp(); continue;}
    // from qT subtraction contribution RRA:
    for (int i_a = 0; i_a < osi.cut_ps.size(); i_a++){if (osi.cut_ps[i_a] > -1){if (osi.change_cut[osi.cut_ps[i_a] + 1] == 0){osi.change_cut[osi.cut_ps[i_a] + 1] = 1;}}}

    if (osi.switch_console_output_tau_0){psi.output_check_tau_0();}
    if (osi.switch_output_testpoint){osi.output_testpoint(psi);}
    ///    if (psi.i_acc == psi.n_events_max){osi.int_end = 1;}
    ///
    generic.ag_psp_psp(0, 0, psi);
    psi.ag_psp_RA_dipoles();
    psi.calculate_g_tot();
    ///    psi.g_tot = 1.; ///
    
    if (munich_isnan(psi.g_tot)){errorhandling_RA_gtot(); continue;} 
    
    if (csi.type_contribution == "L2RA" || osi.user.string_value[osi.user.string_map["model"]] == "Bornloop"){
      osi.determine_techcut_RA(psi);
      //      if (psi.RA_techcut){psi.RA_techcut = 0; psi.handling_cut_psp();
      if (psi.RA_techcut){psi.RA_techcut = 0; psi.handling_techcut_psp(); continue;}
    }
    
    ///
    calculate_ME2_RA_QCD(osi);
    ///    for (int i_a = 1; i_a < osi.n_ps; i_a++){if (osi.cut_ps[i_a] > -1){osi.RA_ME2[i_a] = 1.;}}
 
    if (!(csi.type_contribution == "L2RA" || osi.user.string_value[osi.user.string_map["model"]] == "Bornloop")){
      osi.determine_techcut_RA(psi);
    }
    
    if (osi.user.switch_value[osi.user.switch_map["pT_4lep"]]){
      double temp_pT_4lep = (osi.particle_event[osi.access_object["lep"]][0][0].momentum
			     + osi.particle_event[osi.access_object["lep"]][0][1].momentum
			     + osi.particle_event[osi.access_object["lep"]][0][2].momentum
			     + osi.particle_event[osi.access_object["lep"]][0][3].momentum).pT();
      if (temp_pT_4lep < osi.user.double_value[osi.user.double_map["min_pT_4lep"]]){
	logger << LOG_INFO << right << setw(12) << psi.i_gen << " [" << setw(3) << psi.i_nan << "] " << " " << setw(10) << psi.i_acc << " (" << setw(7) << psi.i_tec << ") " << " " << setw(10) << psi.i_rej << " " << left << "   Matrix element set to zero due to pT_4lep = " << setw(8) << setprecision(4) << temp_pT_4lep << " ." << endl;
	psi.i_tec++;
	psi.RA_techcut = 0;
	//	psi.handling_cut_psp();
	psi.handling_techcut_psp();
	continue;
	
      }
    }
      
    if (psi.RA_techcut){psi.RA_techcut = 0; psi.handling_techcut_psp(); continue;}

    // Check for matrix elements that are considered unstable and thus set to zero:
    if (csi.type_contribution == "L2RA" || osi.user.string_value[osi.user.string_map["model"]] == "Bornloop"){
      for (int i_a = 0; i_a < n_dipoles; i_a++){
	//	logger << LOG_INFO << "osi.cut_ps[" << i_a << "] = " << osi.cut_ps[i_a] << "   osi.RA_ME2[" << i_a << "] = " << osi.RA_ME2[i_a] << endl;
	if (osi.cut_ps[i_a] != -1 && osi.RA_ME2[i_a] == 0.){
	  psi.RA_techcut = 1;
	  logger << LOG_INFO << right << setw(12) << psi.i_gen << " [" << setw(3) << psi.i_nan << "] " << " " << setw(10) << psi.i_acc << " (" << setw(7) << psi.i_tec << ") " << " " << setw(10) << psi.i_rej << " " << left << "   Matrix element set to zero in dipole no. " << setw(2) << i_a << "   -> psi.RA_techcut = " << psi.RA_techcut << endl;
	}
      }
    
      if (psi.RA_techcut){
	psi.i_tec++;
	psi.RA_techcut = 0;
	psi.handling_techcut_psp();
	continue;
      }
    }
    

    
    double temp_sum_RA_ME2 = accumulate(osi.RA_ME2.begin(), osi.RA_ME2.end(), 0.);
    if (munich_isinf(temp_sum_RA_ME2)){errorhandling_RA_me2(3); continue;}
    if (munich_isnan(temp_sum_RA_ME2)){errorhandling_RA_me2(0); continue;}
    if (!(osi.check_vanishing_ME2_end)){handling_vanishing_me2(); if (osi.flag_vanishing_ME2){continue;}}

    if (psi.g_tot == 0.){logger << LOG_INFO << "psi.g_tot == 0." << endl; errorhandling_RA_me2(1); continue;} // does that really happen?
    if (munich_isinf(psi.g_tot)){psi.handling_cut_psp(); continue;}

    for (int i_a = 0; i_a < n_dipoles; i_a++){
      if (osi.cut_ps[i_a] != -1){
        generic.calculate_dynamic_scale_RA(i_a, osi);
        generic.calculate_dynamic_scale_TSV(i_a, osi);
	osi.determine_scale_RA(i_a);
      }
    }

    // add proper errorhandling, also for mu_fact !!!
    // replace by proper nan-check !!!
    if (osi.switch_CV && osi.RA_value_factor_alpha_S != osi.RA_value_factor_alpha_S){errorhandling_alpha_S(); continue;}
    if (osi.switch_TSV && osi.value_relative_factor_alpha_S != osi.value_relative_factor_alpha_S){errorhandling_alpha_S(); continue;}

    if (osi.switch_moment){generic.moments(osi);}

    ///
    osi.calculate_pdf_LHAPDF_RA_CV();
    ///
    osi.calculate_pdf_LHAPDF_TSV();

    osi.determine_integrand_RA(psi);

    logger << LOG_DEBUG_VERBOSE << "i_acc = " << setw(8) << right << psi.i_acc << "   psi.MC_phasespace.channel = " << setw(8) << right << psi.MC_phasespace.channel << "   i_gen = " << setw(8) << right << psi.i_gen << "   integrand = " << left << setprecision(15) << setw(23) << osi.integrand << "   g_tot = " << left << setprecision(15) << setw(23) << psi.g_tot << endl;

    // introduce proper nan-check !!!
    if (osi.var_RA_ME2 != osi.var_RA_ME2){errorhandling_RA_me2(1); continue;}
    if (osi.switch_CV && osi.var_RA_ME2_CV != osi.var_RA_ME2_CV){errorhandling_RA_me2(1); continue;}
    //shifted:
    //    osi.determine_techcut_RA(psi);
    //    //    if (psi.RA_techcut){psi.RA_techcut = 0; psi.handling_cut_psp(); continue;}
    //    if (psi.RA_techcut){psi.RA_techcut = 0; psi.handling_techcut_psp(); continue;}
      
    // introduce proper nan-check !!!
    if (osi.var_RA_ME2_CV != osi.var_RA_ME2_CV){errorhandling_RA_me2(2); continue;}
    if (osi.switch_output_cancellation_check){osi.output_integrand_cancellation_check_RA(psi);}

    if (osi.switch_output_maxevent){osi.output_integrand_maximum_RA(psi);}
    //    logger << LOG_DEBUG << "i_acc = " << setw(8) << right << psi.i_acc << "   i_gen = " << setw(8) << right << psi.i_gen << "   integrand = " << left << setprecision(15) << setw(23) << osi.integrand << "   g_tot = " << left << setprecision(15) << setw(23) << psi.g_tot << endl;
    osi.determine_psp_weight_TSV(psi);
    osi.determine_psp_weight_RA(psi);

    if (osi.switch_distribution){osi.determine_distribution_complete();}
    static double optimization_modifier = 1.;
    if (osi.user.switch_value[osi.user.switch_map["optimization_modifier"]]){
      //      optimization_modifier = pow(osi.particle_event[osi.access_object["Vrec"]][0][0].pT, osi.user.switch_value[osi.user.switch_map["optimization_modifier"]]);
      optimization_modifier = pow(psi.x_pdf[0], osi.user.switch_value[osi.user.switch_map["optimization_modifier"]]);
      logger << LOG_DEBUG << "optimization_modifier = " << optimization_modifier << endl;
    }   
    psi.psp_MCweight_optimization(osi.integrand, osi.this_psp_weight, osi.this_psp_weight2, optimization_modifier);

    psi.i_acc++;
    osi.determine_runtime(psi);
  }
  osi.output_finalization_integration(psi);
}
