#include "header.hpp"

randommanager::randommanager(){}
//randommanager::randommanager() : logger(Logger("random manager")) {}

randommanager::randommanager(phasespace_set & _psi){
  //randommanager::randommanager(vector<double> sran, string _weightdir, string _processname, phasespace_set & _psi) : logger(Logger("random manager")) {
  static Logger logger("randommanager::randommanager");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  psi = &_psi;

  ///  weightdir=_weightdir;
  
  logger << LOG_DEBUG_VERBOSE << "weightdir = " << weightdir << endl;
  ///  processname=_processname;

  //  opt_end=true;
  // check if reasonable initialization
  end_optimization = 1;

  used_queue.resize(0);

  // Check meaning of queue_counter !!!
  queue_counter=0;

  // Check meaning of queue_threshold !!!
  queue_threshold=1e2;

  // Check order ??? proceeding before readin ???
  proceeding_save();

  logger << LOG_DEBUG << "weightdir = " << weightdir << endl;

  readin_weights();

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

randommanager::~randommanager(){
  static Logger logger("randommanager::~randommanager");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  //  for (int i_r = 0; i_r < random_variables.size(); i_r++) {
  //  FIXME: should the random manager be responsible for deleting the variables it manages?
  //  If so, we have to be careful that they actually still exists at this point
  //  delete random_variables[i_r];
  //  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

void randommanager::initialization_random_psp(int x_r){
  static Logger logger("randommanager::initialization_random_psp");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  
  ostringstream convert;

  ///  for (int i = 0; i < s.size(); i++){logger << LOG_DEBUG_VERBOSE << "s[" << i << "] = " << s[i] << endl;}

  if (random_psp[x_r].name == "") {
    convert << "unnamed_random_psp_" << random_psp.size();
    random_psp[x_r].name = convert.str();
  }
  /////  logger << LOG_DEBUG << "psi->rng_IS.size() = " << psi->rng_IS.size() << endl;

  ///  random_psp[x_r].imp_sampling=imp_sampling;

  /*///
  //  SK TODO: completely remove the individual s(3) vectors to generate random numbers
  //  Should be done completely in a single one, within randommanager !!!
  // decorrelate random sequences
  // TODO: seed uniformly!
  random_psp[x_r].s[0] = (random_psp[x_r].s[0]+s[0])/4*(1+cos(random_variables.size()+2));
  random_psp[x_r].s[1] = (random_psp[x_r].s[1]+s[1])/4*(1+cos(random_variables.size()+2));
  random_psp[x_r].s[2] = (random_psp[x_r].s[2]+s[2])/4*(1+cos(random_variables.size()+2));

  for (int i = 0; i < random_psp[x_r].s.size(); i++){logger << LOG_DEBUG << "random_psp[x_r].s[" << i << "] = " << random_psp[x_r].s[i] << endl;}
  */
  //  for (int i = 0; i < random_psp[x_r].s.size(); i++){logger << LOG_DEBUG_VERBOSE << "random_psp[x_r].s[" << i << "] = " << random_psp[x_r].s[i] << endl;}

  random_psp[x_r].proceeding_save();

  random_psp[x_r].manager = this;

  logger << LOG_DEBUG_VERBOSE << "readin.size() = " << readin.size() << endl;
									
  // temporary !!!
  random_psp[x_r].readin_weights(readin);

  ///  random_variables.push_back(new_var);

  logger << LOG_DEBUG << "new random variable registered " << setw(50) << random_psp[x_r].name << ": " << random_psp[x_r].n_events << ", " << random_psp[x_r].switch_IS_MC << ", " << random_psp[x_r].n_steps << ", " << random_psp[x_r].n_bins << endl;
  ///", " << random_psp[x_r].imp_sampling << endl;

  //  opt_end=false;
  end_optimization = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


/*
///void randommanager::register_variable(randomvariable* new_var, bool imp_sampling) {
void randommanager::register_variable(randomvariable* new_var){
  static Logger logger("randommanager::register_variable");
  logger << LOG_DEBUG << "called" << endl;
  
  ostringstream convert;

  ///  for (int i = 0; i < s.size(); i++){logger << LOG_DEBUG_VERBOSE << "s[" << i << "] = " << s[i] << endl;}

  if (new_var->name=="") {
    convert << "random_var" << random_variables.size();
    new_var->name = convert.str();
  }
  /////  logger << LOG_DEBUG << "psi->rng_IS.size() = " << psi->rng_IS.size() << endl;

  ///  new_var->imp_sampling=imp_sampling;
  */
  /*///
  //  SK TODO: completely remove the individual s(3) vectors to generate random numbers
  //  Should be done completely in a single one, within randommanager !!!
  // decorrelate random sequences
  // TODO: seed uniformly!
  new_var->s[0] = (new_var->s[0]+s[0])/4*(1+cos(random_variables.size()+2));
  new_var->s[1] = (new_var->s[1]+s[1])/4*(1+cos(random_variables.size()+2));
  new_var->s[2] = (new_var->s[2]+s[2])/4*(1+cos(random_variables.size()+2));

  for (int i = 0; i < new_var->s.size(); i++){logger << LOG_DEBUG << "new_var->s[" << i << "] = " << new_var->s[i] << endl;}

  *//*
  //  for (int i = 0; i < new_var->s.size(); i++){logger << LOG_DEBUG_VERBOSE << "new_var->s[" << i << "] = " << new_var->s[i] << endl;}

  new_var->proceeding_save();

  new_var->manager=this;

  new_var->readin_weights(readin);

  random_variables.push_back(new_var);

  logger << LOG_DEBUG << "new random variable registered " << setw(50) << new_var->name << ": " << new_var->n_events << ", " << new_var->switch_IS_MC << ", " << new_var->n_steps << ", " << new_var->n_bins << endl;
  ///", " << new_var->imp_sampling << endl;

  //  opt_end=false;
  end_optimization = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
*/

double randommanager::get_random(int x_a, int x_t, int x_i){
  static Logger logger("randommanager::get_random(int x_a, int x_t, int x_i)");
  logger << LOG_DEBUG_VERBOSE << "started" << endl;
  
  logger << LOG_DEBUG_VERBOSE << "x_a = " << x_a << "   x_t = " << x_t << "   x_i = " << x_i << endl;
  if (psi->switch_IS_mode_phasespace == 3 || psi->switch_IS_mode_phasespace == 4){
    logger << LOG_DEBUG_VERBOSE << "psi->switch_IS_mode_phasespace = " << psi->switch_IS_mode_phasespace << endl;
    logger << LOG_DEBUG_VERBOSE << "psi->MC_IS_name_type.size() = " << psi->MC_IS_name_type.size() << endl;
    logger << LOG_DEBUG_VERBOSE << "x_a = " << x_a << "   x_t = " << x_t << "   x_i = " << x_i << endl;
    logger << LOG_DEBUG_VERBOSE << "psi->MC_IS_name_type[" << x_a << "].size() = " << psi->MC_IS_name_type[x_a].size() << endl;
    logger << LOG_DEBUG_VERBOSE << "psi->MC_IS_name_type[" << x_a << "][" << x_t << "].size() = " << psi->MC_IS_name_type[x_a][x_t].size() << endl;
    logger << LOG_DEBUG_VERBOSE << "psi->MC_IS_name_type[" << x_a << "][" << x_t << "][" << x_i << "].size() = " << psi->MC_IS_name_type[x_a][x_t][x_i].size() << endl;
												    
    logger << LOG_DEBUG_POINT << "psi->MC_IS_name_type[" << x_a << " -- " << x_t << " -- " << x_i << "] = " << setw(50) << psi->MC_IS_name_type[x_a][x_t][x_i] << "   " << psi->map_type_to_list_MC_IS[vector<int> {x_a, x_t, x_i}] << endl;


	  ///    double temp_r = psi->phasespace_randoms[psi->map_type_to_list_MC_IS[vector<int> {x_a, x_t, x_i}]]->get_random();
    double temp_r = random_psp[psi->map_type_to_list_MC_IS[vector<int> {x_a, x_t, x_i}]].get_random();
    ///    double temp_r = psi->phasespace_randoms[psi->container_IS_startvalue[x_a][x_t] + x_i]->get_random();
    ///     logger << LOG_DEBUG << psi->phasespace_randoms[psi->container_IS_startvalue[x_a][x_t] + x_i]->name << "   psi->phasespace_randoms[" << psi->container_IS_startvalue[x_a][x_t] + x_i << "]->get_random() = " << temp_r << endl;
    
    return temp_r;
    ///    return psi->phasespace_randoms[psi->container_IS_startvalue[x_a][x_t] + x_i]->get_random();
  }
  else if (psi->switch_IS_mode_phasespace == 1 || psi->switch_IS_mode_phasespace == 3){
    // Needs to be re-activated (and checked) !!!
    //    logger << LOG_DEBUG << psi->phasespace_randoms[psi->container_IS_startvalue[x_a][x_t] + x_i]->name << "   counter_mode_12 = " << counter_mode_12 + 1 << "   psi->r.size() = " << psi->r.size() << "   psi->r[" << counter_mode_12 + 1 << "] = " << psi->r[counter_mode_12] << endl;
    //    logger << LOG_INFO << "psi->r[" << setw(2) << counter_mode_12 + 1 << "] = " << psi->r[counter_mode_12 + 1] << endl;
    return psi->r[++counter_mode_12];
  }
  else {
    logger << LOG_DEBUG << "counter_mode_12 = " << counter_mode_12 + 1 << "   psi->r.size() = " << psi->r.size() << "   psi->r[" << counter_mode_12 + 1 << "] = " << psi->r[counter_mode_12] << endl;
    return psi->r[++counter_mode_12];   
  }
  ///  double x_random = 0.;
  //  if (psi->container_IS_switch[psi->container_IS_startvalue[x_a][x_t] + x_i] == 1){
  ///  psi->phasespace_randoms[psi->container_IS_startvalue[x_a][x_t] + x_i]->get_random(x_random);
  //  psi.MC_g_IS_global *= g_IS_temp;
  //  }
  ///  return x_random;
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::output_used() {
  static Logger logger("randommanager::output_used");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++) {
    logger << LOG_DEBUG_VERBOSE << setw(50) << random_psp[i_r].name << "   used = " << random_psp[i_r].used << endl;
  }
  
  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

  void randommanager::psp_IS_optimization(double & psp_weight, double & psp_weight2) {
  static Logger logger("randommanager::psp_IS_optimization");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG_VERBOSE << "end_optimization = " << end_optimization << endl;

  if (end_optimization){return;}

  end_optimization = 1;

  logger << LOG_DEBUG_VERBOSE << "(random_psp.size() > queue_threshold) = " << (random_psp.size() > queue_threshold) << endl;

  
  // very confusing distinction !!! queue_threshold ???
  if (random_psp.size() > queue_threshold) {
    logger << LOG_DEBUG_VERBOSE << "queue_counter = " << queue_counter << endl;
    for (int i = 0; i < queue_counter; i++) {
      used_queue[i]->psp_IS_optimization(psp_weight);
      if (used_queue[i]->end_optimization == 0){
        end_optimization = 0;
      }
    }
  }
  else {
    //    logger << LOG_DEBUG << "random_psp.size() <= queue_threshold" << endl;
    /*
    int first = 0;
    int last = random_psp.size() - 1;
    for (int i = first; i < last + 1; i++) {
    */
    for (int i_r = 0; i_r < random_psp.size(); i_r++) {
      random_psp[i_r].psp_IS_optimization(psp_weight);
      if (random_psp[i_r].end_optimization == 0) {
        end_optimization = 0;
      }
    }
  }

  // ???
  queue_counter = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::increase_counter_n_acc(){
  static Logger logger("randommanager::increase_counter_n_acc");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}

  if (random_psp.size() > queue_threshold){
    for (int x_r = 0; x_r < queue_counter; x_r++){
      used_queue[x_r]->n_acc_channel[used_queue[x_r]->channel]++;
      if (used_queue[x_r]->end_optimization == 0) {
        used_queue[x_r]->used = false;
      }
    }
  }
  else {
    for (int x_r = 0; x_r < random_psp.size(); x_r++) {
      if (random_psp[x_r].used){
        random_psp[x_r].n_acc_channel[random_psp[x_r].channel]++;
        random_psp[x_r].used = false;
      }
    }
  }

  queue_counter = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::increase_cut_counter(){
  static Logger logger("randommanager::increase_cut_counter");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}

  if (random_psp.size() > queue_threshold){
    for (int x_r = 0; x_r < queue_counter; x_r++){
      used_queue[x_r]->n_rej_channel[used_queue[x_r]->channel]++;
      if (used_queue[x_r]->end_optimization == 0) {
        used_queue[x_r]->used = false;
      }
    }
  }
  else {
    for (int i_r = 0; i_r < random_psp.size(); i_r++) {
      if (random_psp[i_r].used) {
        random_psp[i_r].n_rej_channel[random_psp[i_r].channel]++;
        random_psp[i_r].used = false;
      }
    }
  }

  queue_counter = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::do_optimization_step(int events) {
  static Logger logger("randommanager::do_optimization_step");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}

  for (int i_r = 0; i_r < random_psp.size(); i_r++){
    random_psp[i_r].step_IS_optimization(events);
  }
  end_optimization = 1;
  for (int i_r = 0; i_r < random_psp.size(); i_r++){
    if (random_psp[i_r].end_optimization == 0){end_optimization = 0; break;}
  }

  if (end_optimization == 1){writeout_weights();}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::proceeding_out(ofstream &out_proceeding) {
  static Logger logger("randommanager::proceeding_out");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){
    random_psp[i_r].proceeding_out(out_proceeding);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::proceeding_in(int &proc, vector<string> &readin) {
  static Logger logger("randommanager::proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){
    random_psp[i_r].proceeding_in(proc, readin);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::check_proceeding_in(int &int_end, int &temp_check_size, vector<string> &readin) {
  static Logger logger("randommanager::check_proceeding_in");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  logger << LOG_DEBUG << "int_end = " << int_end << endl;
  logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << endl;
  logger << LOG_DEBUG << "readin.size() = " << readin.size() << endl;

  if (temp_check_size > readin.size()){
    int_end = 2;
    logger << LOG_DEBUG << "int_end = 2   temp_check_size = " << temp_check_size << " > " << readin.size() << " = readin.size()" << endl;
    return;
  }
  else {
    logger << LOG_DEBUG << "temp_check_size = " << temp_check_size << " <= " << readin.size() << " = readin.size()" << endl;
  }

  logger << LOG_DEBUG << "random_psp.size() = " << random_psp.size() << endl;
  
  for (int i_r = 0; i_r < random_psp.size(); i_r++){
    logger << LOG_DEBUG << "random_psp[" << i_r << "] = " << random_psp[i_r].name << "   n_bins = " << random_psp[i_r].n_bins << endl;
    random_psp[i_r].check_proceeding_in(int_end, temp_check_size, readin);
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::proceeding_save() {
  static Logger logger("randommanager::proceeding_save");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){
    random_psp[i_r].proceeding_save();
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::readin_weights(){
  static Logger logger("randommanager::readin_weights");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  /*
  string in_path = weightdir + "/weights_IS_MC_" + psi->csi->subprocess + ".dat";
  logger << LOG_DEBUG << "in_path = " << in_path << endl;
  ifstream readin_weights(in_path.c_str());
  */
  // temporary !!!
  
  ifstream readin_weights(psi->filename_IS_MCweight_in_contribution.c_str());

  if (readin_weights){
    char LineBuffer[128];
    while (readin_weights.getline(LineBuffer, 128)){readin.push_back(LineBuffer);}
    readin_weights.close();
    // temporary !!!
    //  writeout_weights();
  }
  else {
    logger << LOG_DEBUG << "weight file " << psi->filename_IS_MCweight_in_contribution << " could not be opened" << endl;
    readin_weights.close();
    return;
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::writeout_weights() {
  static Logger logger("randommanager::writeout_weights");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  ///  ofstream out_weights(("weights/weights_IS_MC_" + psi->csi->subprocess + ".dat").c_str());
  
  ofstream out_weights(psi->filename_IS_MCweight.c_str());
  //  ofstream out_weights(psi->filename_IS_MCweight_in_contribution.c_str());
  for (int i_r = 0; i_r < random_psp.size(); i_r++){
    random_psp[i_r].save_weights(out_weights);
  }
  end_optimization = 2;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::add_var_to_queue(randomvariable* this_random_psp){
  static Logger logger("randommanager::add_var_to_queue");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (end_optimization){return;}

  // check if this distinction makes sense !!!
  if (queue_counter >= used_queue.size()){
    used_queue.push_back(this_random_psp);
  }
  else {
    used_queue[queue_counter] = this_random_psp;
  }
  queue_counter++;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}


void randommanager::nancut(){
  static Logger logger("randommanager::nancut");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  for (int i_r = 0; i_r < random_psp.size(); i_r++){
    if (random_psp[i_r].used){
      random_psp[i_r].used = false;
    }
  }
  queue_counter = 0;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

