#include "header.hpp"
#include "ppemxnmnex04.subprocess.h"
void ppemxnmnex04_determination_no_subprocess_doublereal(int & no_map, vector<int> & o_map, int & no_prc, vector<int> & o_prc, double & symmetry_factor, vector<int> & tp, int basic_order_alpha_s, int basic_order_alpha_e, int basic_order_interference){
  static Logger logger("ppemxnmnex04_determination_no_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  no_map = -1;
  vector<int> out(9);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[7] = 7; out[8] = 8;}
      if (o == 1){out[7] = 8; out[8] = 7;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_map = 2; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_map = 2; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_map = 3; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_map = 3; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_map = 4; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   1)){no_map = 5; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   3)){no_map = 5; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   2)){no_map = 6; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   4)){no_map = 6; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   5)){no_map = 7; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -1)){no_map = 8; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -3)){no_map = 8; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -2)){no_map = 9; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -4)){no_map = 9; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -5)){no_map = 10; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   1)){no_map = 15; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   3)){no_map = 15; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   2)){no_map = 16; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   4)){no_map = 16; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   3)){no_map = 17; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   1)){no_map = 17; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   4)){no_map = 18; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   2)){no_map = 18; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   3)){no_map = 19; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   1)){no_map = 19; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   5)){no_map = 20; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   5)){no_map = 20; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_map = 21; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_map = 21; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_map = 22; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_map = 22; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_map = 23; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_map = 23; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_map = 24; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_map = 24; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_map = 25; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_map = 25; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_map = 26; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_map = 26; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_map = 27; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_map = 27; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){no_map = 28; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){no_map = 28; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -3)){no_map = 29; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -1)){no_map = 29; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -4)){no_map = 30; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -2)){no_map = 30; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -4)){no_map = 31; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -2)){no_map = 31; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -5)){no_map = 32; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -5)){no_map = 32; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   2)){no_map = 34; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   4)){no_map = 34; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   4)){no_map = 35; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   2)){no_map = 35; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   5)){no_map = 36; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   5)){no_map = 36; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -1)){no_map = 37; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -3)){no_map = 37; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -3)){no_map = 38; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -1)){no_map = 38; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_map = 39; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_map = 39; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_map = 40; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_map = 40; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_map = 41; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_map = 41; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_map = 42; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_map = 42; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_map = 43; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_map = 43; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_map = 44; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_map = 44; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -3)){no_map = 45; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -1)){no_map = 45; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -3)){no_map = 46; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -1)){no_map = 46; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -4)){no_map = 47; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -2)){no_map = 47; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -5)){no_map = 48; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -5)){no_map = 48; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==   5)){no_map = 50; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -1)){no_map = 51; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -3)){no_map = 51; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -2)){no_map = 52; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -4)){no_map = 52; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){no_map = 53; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){no_map = 54; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){no_map = 54; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){no_map = 55; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){no_map = 55; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){no_map = 56; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -1)){no_map = 58; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -3)){no_map = 58; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -2)){no_map = 59; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -4)){no_map = 59; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -3)){no_map = 60; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -1)){no_map = 60; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -4)){no_map = 61; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -2)){no_map = 61; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -3)){no_map = 62; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -1)){no_map = 62; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -5)){no_map = 63; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -5)){no_map = 63; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -2)){no_map = 65; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -4)){no_map = 65; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -4)){no_map = 66; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -2)){no_map = 66; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -5)){no_map = 67; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -5)){no_map = 67; break;}
      if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -5) && (tp[out[8]] ==  -5)){no_map = 69; break;}
    }
    if (no_map != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){o_map[o] = o;}
    else {o_map[o] = out[o];}
  }
  no_prc = no_map;
  o_prc = o_map;
  if (no_map == 0){}
  else if (no_map == 2){symmetry_factor = 1;}
  else if (no_map == 3){symmetry_factor = 1;}
  else if (no_map == 4){symmetry_factor = 1;}
  else if (no_map == 5){symmetry_factor = 1;}
  else if (no_map == 6){symmetry_factor = 1;}
  else if (no_map == 7){symmetry_factor = 1;}
  else if (no_map == 8){symmetry_factor = 1;}
  else if (no_map == 9){symmetry_factor = 1;}
  else if (no_map == 10){symmetry_factor = 1;}
  else if (no_map == 15){symmetry_factor = 2;}
  else if (no_map == 16){symmetry_factor = 1;}
  else if (no_map == 17){symmetry_factor = 1;}
  else if (no_map == 18){symmetry_factor = 1;}
  else if (no_map == 19){symmetry_factor = 1;}
  else if (no_map == 20){symmetry_factor = 1;}
  else if (no_map == 21){symmetry_factor = 2;}
  else if (no_map == 22){symmetry_factor = 1;}
  else if (no_map == 23){symmetry_factor = 1;}
  else if (no_map == 24){symmetry_factor = 1;}
  else if (no_map == 25){symmetry_factor = 1;}
  else if (no_map == 26){symmetry_factor = 1;}
  else if (no_map == 27){symmetry_factor = 1;}
  else if (no_map == 28){symmetry_factor = 1;}
  else if (no_map == 29){symmetry_factor = 1;}
  else if (no_map == 30){symmetry_factor = 1;}
  else if (no_map == 31){symmetry_factor = 1;}
  else if (no_map == 32){symmetry_factor = 1;}
  else if (no_map == 34){symmetry_factor = 2;}
  else if (no_map == 35){symmetry_factor = 1;}
  else if (no_map == 36){symmetry_factor = 1;}
  else if (no_map == 37){symmetry_factor = 1;}
  else if (no_map == 38){symmetry_factor = 1;}
  else if (no_map == 39){symmetry_factor = 2;}
  else if (no_map == 40){symmetry_factor = 1;}
  else if (no_map == 41){symmetry_factor = 1;}
  else if (no_map == 42){symmetry_factor = 1;}
  else if (no_map == 43){symmetry_factor = 1;}
  else if (no_map == 44){symmetry_factor = 1;}
  else if (no_map == 45){symmetry_factor = 1;}
  else if (no_map == 46){symmetry_factor = 1;}
  else if (no_map == 47){symmetry_factor = 1;}
  else if (no_map == 48){symmetry_factor = 1;}
  else if (no_map == 50){symmetry_factor = 2;}
  else if (no_map == 51){symmetry_factor = 1;}
  else if (no_map == 52){symmetry_factor = 1;}
  else if (no_map == 53){symmetry_factor = 2;}
  else if (no_map == 54){symmetry_factor = 1;}
  else if (no_map == 55){symmetry_factor = 1;}
  else if (no_map == 56){symmetry_factor = 1;}
  else if (no_map == 58){symmetry_factor = 2;}
  else if (no_map == 59){symmetry_factor = 1;}
  else if (no_map == 60){symmetry_factor = 1;}
  else if (no_map == 61){symmetry_factor = 1;}
  else if (no_map == 62){symmetry_factor = 1;}
  else if (no_map == 63){symmetry_factor = 1;}
  else if (no_map == 65){symmetry_factor = 2;}
  else if (no_map == 66){symmetry_factor = 1;}
  else if (no_map == 67){symmetry_factor = 1;}
  else if (no_map == 69){symmetry_factor = 2;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
void ppemxnmnex04_determination_subprocess_doublereal(int i_a, phasespace_set & psi){
  static Logger logger("ppemxnmnex04_determination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<int> tp = psi.csi->basic_type_parton[i_a];
  //  ppemxnmnex04_determination_no_subprocess_doublereal(psi.no_map[i_a], psi.o_map[i_a], psi.no_prc[i_a], psi.o_prc[i_a], psi.symmetry_factor, tp, psi.phasespace_order_alpha_s[i_a], psi.phasespace_order_alpha_e[i_a], psi.phasespace_order_interference[i_a]);

  psi.no_map[i_a] = -1;
  vector<int> out(9);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 2; o++){
      if (o == 0){out[7] = 7; out[8] = 8;}
      if (o == 1){out[7] = 8; out[8] = 7;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 2; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 2; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 3; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 3; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 4; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   1)){psi.no_map[i_a] = 5; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   3)){psi.no_map[i_a] = 5; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   2)){psi.no_map[i_a] = 6; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   4)){psi.no_map[i_a] = 6; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   5)){psi.no_map[i_a] = 7; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 8; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 8; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 9; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 9; break;}
      if ((tp[out[1]] ==   0) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 10; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   1)){psi.no_map[i_a] = 15; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   3)){psi.no_map[i_a] = 15; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   2)){psi.no_map[i_a] = 16; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   4)){psi.no_map[i_a] = 16; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   3)){psi.no_map[i_a] = 17; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   1)){psi.no_map[i_a] = 17; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   4)){psi.no_map[i_a] = 18; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   2)){psi.no_map[i_a] = 18; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   3)){psi.no_map[i_a] = 19; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   1)){psi.no_map[i_a] = 19; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==   5)){psi.no_map[i_a] = 20; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==   5)){psi.no_map[i_a] = 20; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){psi.no_map[i_a] = 21; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){psi.no_map[i_a] = 21; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 22; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 22; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 23; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 23; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 24; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 24; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 25; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 25; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 26; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 26; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 27; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 27; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 28; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 28; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 29; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 29; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 30; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 30; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 31; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 31; break;}
      if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 32; break;}
      if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 32; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   2)){psi.no_map[i_a] = 34; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   4)){psi.no_map[i_a] = 34; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   4)){psi.no_map[i_a] = 35; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   2)){psi.no_map[i_a] = 35; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==   5)){psi.no_map[i_a] = 36; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==   5)){psi.no_map[i_a] = 36; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 37; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 37; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 38; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 38; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){psi.no_map[i_a] = 39; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){psi.no_map[i_a] = 39; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 40; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 40; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 41; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 41; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 42; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 42; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 43; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 43; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 44; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 44; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 45; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 45; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 46; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 46; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 47; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 47; break;}
      if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 48; break;}
      if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 48; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==   5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==   5)){psi.no_map[i_a] = 50; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 51; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 51; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 52; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 52; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   0) && (tp[out[8]] ==   0)){psi.no_map[i_a] = 53; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   1) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 54; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   3) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 54; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   2) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 55; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   4) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 55; break;}
      if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==   5) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 56; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 58; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 58; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 59; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 59; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 60; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 60; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 61; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 61; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -3)){psi.no_map[i_a] = 62; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -1)){psi.no_map[i_a] = 62; break;}
      if ((tp[out[1]] ==  -1) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -1) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 63; break;}
      if ((tp[out[1]] ==  -3) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -3) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 63; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 65; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 65; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -4)){psi.no_map[i_a] = 66; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -2)){psi.no_map[i_a] = 66; break;}
      if ((tp[out[1]] ==  -2) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -2) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 67; break;}
      if ((tp[out[1]] ==  -4) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -4) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 67; break;}
      if ((tp[out[1]] ==  -5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -13) && (tp[5] ==  14) && (tp[6] == -12) && (tp[out[7]] ==  -5) && (tp[out[8]] ==  -5)){psi.no_map[i_a] = 69; break;}
    }
    if (psi.no_map[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){psi.o_map[i_a][o] = o;}
    else {psi.o_map[i_a][o] = out[o];}
  }
  psi.no_prc[i_a] = psi.no_map[i_a];
  psi.o_prc[i_a] = psi.o_map[i_a];
  if (psi.no_map[i_a] == 0){}
  else if (psi.no_map[i_a] == 2){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 3){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 4){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 5){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 6){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 7){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 8){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 9){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 10){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 15){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 16){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 17){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 18){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 19){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 20){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 21){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 22){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 23){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 24){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 25){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 26){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 27){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 28){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 29){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 30){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 31){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 32){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 34){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 35){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 36){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 37){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 38){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 39){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 40){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 41){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 42){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 43){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 44){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 45){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 46){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 47){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 48){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 50){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 51){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 52){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 53){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 54){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 55){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 56){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 58){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 59){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 60){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 61){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 62){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 63){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 65){psi.symmetry_factor = 2;}
  else if (psi.no_map[i_a] == 66){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 67){psi.symmetry_factor = 1;}
  else if (psi.no_map[i_a] == 69){psi.symmetry_factor = 2;}

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
void ppemxnmnex04_combination_subprocess_doublereal(int i_a, phasespace_set & psi, observable_set & osi){
  static Logger logger("ppemxnmnex04_combination_subprocess_doublereal");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (psi.no_map[i_a] == 0){}
  else if (psi.no_map[i_a] == 2){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   0,   0};   // g  g   -> e  mx nm nexd  dx 
    osi.combination_pdf[1] = { 1,   0,   0};   // g  g   -> e  mx nm nexs  sx 
  }
  else if (psi.no_map[i_a] == 3){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   0,   0};   // g  g   -> e  mx nm nexu  ux 
    osi.combination_pdf[1] = { 1,   0,   0};   // g  g   -> e  mx nm nexc  cx 
  }
  else if (psi.no_map[i_a] == 4){
    osi.combination_pdf.resize(1);
    osi.combination_pdf[0] = { 1,   0,   0};   // g  g   -> e  mx nm nexb  bx 
  }
  else if (psi.no_map[i_a] == 5){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   0,   1};   // g  d   -> e  mx nm nexg  d  
    osi.combination_pdf[1] = { 1,   0,   3};   // g  s   -> e  mx nm nexg  s  
    osi.combination_pdf[2] = {-1,   0,   1};   // d  g   -> e  mx nm nexg  d  
    osi.combination_pdf[3] = {-1,   0,   3};   // s  g   -> e  mx nm nexg  s  
  }
  else if (psi.no_map[i_a] == 6){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   0,   2};   // g  u   -> e  mx nm nexg  u  
    osi.combination_pdf[1] = { 1,   0,   4};   // g  c   -> e  mx nm nexg  c  
    osi.combination_pdf[2] = {-1,   0,   2};   // u  g   -> e  mx nm nexg  u  
    osi.combination_pdf[3] = {-1,   0,   4};   // c  g   -> e  mx nm nexg  c  
  }
  else if (psi.no_map[i_a] == 7){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   0,   5};   // g  b   -> e  mx nm nexg  b  
    osi.combination_pdf[1] = {-1,   0,   5};   // b  g   -> e  mx nm nexg  b  
  }
  else if (psi.no_map[i_a] == 8){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   0,  -1};   // g  dx  -> e  mx nm nexg  dx 
    osi.combination_pdf[1] = { 1,   0,  -3};   // g  sx  -> e  mx nm nexg  sx 
    osi.combination_pdf[2] = {-1,   0,  -1};   // dx g   -> e  mx nm nexg  dx 
    osi.combination_pdf[3] = {-1,   0,  -3};   // sx g   -> e  mx nm nexg  sx 
  }
  else if (psi.no_map[i_a] == 9){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   0,  -2};   // g  ux  -> e  mx nm nexg  ux 
    osi.combination_pdf[1] = { 1,   0,  -4};   // g  cx  -> e  mx nm nexg  cx 
    osi.combination_pdf[2] = {-1,   0,  -2};   // ux g   -> e  mx nm nexg  ux 
    osi.combination_pdf[3] = {-1,   0,  -4};   // cx g   -> e  mx nm nexg  cx 
  }
  else if (psi.no_map[i_a] == 10){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   0,  -5};   // g  bx  -> e  mx nm nexg  bx 
    osi.combination_pdf[1] = {-1,   0,  -5};   // bx g   -> e  mx nm nexg  bx 
  }
  else if (psi.no_map[i_a] == 15){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   1,   1};   // d  d   -> e  mx nm nexd  d  
    osi.combination_pdf[1] = { 1,   3,   3};   // s  s   -> e  mx nm nexs  s  
  }
  else if (psi.no_map[i_a] == 16){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,   2};   // d  u   -> e  mx nm nexd  u  
    osi.combination_pdf[1] = { 1,   3,   4};   // s  c   -> e  mx nm nexs  c  
    osi.combination_pdf[2] = {-1,   1,   2};   // u  d   -> e  mx nm nexd  u  
    osi.combination_pdf[3] = {-1,   3,   4};   // c  s   -> e  mx nm nexs  c  
  }
  else if (psi.no_map[i_a] == 17){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   1,   3};   // d  s   -> e  mx nm nexd  s  
    osi.combination_pdf[1] = { 1,   3,   1};   // s  d   -> e  mx nm nexd  s  
  }
  else if (psi.no_map[i_a] == 18){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,   4};   // d  c   -> e  mx nm nexd  c  
    osi.combination_pdf[1] = { 1,   3,   2};   // s  u   -> e  mx nm nexu  s  
    osi.combination_pdf[2] = {-1,   3,   2};   // u  s   -> e  mx nm nexu  s  
    osi.combination_pdf[3] = {-1,   1,   4};   // c  d   -> e  mx nm nexd  c  
  }
  else if (psi.no_map[i_a] == 19){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,   4};   // d  c   -> e  mx nm nexu  s  
    osi.combination_pdf[1] = { 1,   3,   2};   // s  u   -> e  mx nm nexd  c  
    osi.combination_pdf[2] = {-1,   3,   2};   // u  s   -> e  mx nm nexd  c  
    osi.combination_pdf[3] = {-1,   1,   4};   // c  d   -> e  mx nm nexu  s  
  }
  else if (psi.no_map[i_a] == 20){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,   5};   // d  b   -> e  mx nm nexd  b  
    osi.combination_pdf[1] = { 1,   3,   5};   // s  b   -> e  mx nm nexs  b  
    osi.combination_pdf[2] = {-1,   1,   5};   // b  d   -> e  mx nm nexd  b  
    osi.combination_pdf[3] = {-1,   3,   5};   // b  s   -> e  mx nm nexs  b  
  }
  else if (psi.no_map[i_a] == 21){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  mx nm nexg  g  
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  mx nm nexg  g  
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  mx nm nexg  g  
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  mx nm nexg  g  
  }
  else if (psi.no_map[i_a] == 22){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  mx nm nexd  dx 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  mx nm nexs  sx 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  mx nm nexd  dx 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  mx nm nexs  sx 
  }
  else if (psi.no_map[i_a] == 23){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  mx nm nexu  ux 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  mx nm nexc  cx 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  mx nm nexu  ux 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  mx nm nexc  cx 
  }
  else if (psi.no_map[i_a] == 24){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  mx nm nexs  sx 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  mx nm nexd  dx 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  mx nm nexs  sx 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  mx nm nexd  dx 
  }
  else if (psi.no_map[i_a] == 25){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  mx nm nexc  cx 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  mx nm nexu  ux 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  mx nm nexc  cx 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  mx nm nexu  ux 
  }
  else if (psi.no_map[i_a] == 26){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  mx nm nexb  bx 
    osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  mx nm nexb  bx 
    osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  mx nm nexb  bx 
    osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  mx nm nexb  bx 
  }
  else if (psi.no_map[i_a] == 27){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -2};   // d  ux  -> e  mx nm nexd  ux 
    osi.combination_pdf[1] = { 1,   3,  -4};   // s  cx  -> e  mx nm nexs  cx 
    osi.combination_pdf[2] = {-1,   1,  -2};   // ux d   -> e  mx nm nexd  ux 
    osi.combination_pdf[3] = {-1,   3,  -4};   // cx s   -> e  mx nm nexs  cx 
  }
  else if (psi.no_map[i_a] == 28){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -2};   // d  ux  -> e  mx nm nexs  cx 
    osi.combination_pdf[1] = { 1,   3,  -4};   // s  cx  -> e  mx nm nexd  ux 
    osi.combination_pdf[2] = {-1,   1,  -2};   // ux d   -> e  mx nm nexs  cx 
    osi.combination_pdf[3] = {-1,   3,  -4};   // cx s   -> e  mx nm nexd  ux 
  }
  else if (psi.no_map[i_a] == 29){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -3};   // d  sx  -> e  mx nm nexd  sx 
    osi.combination_pdf[1] = { 1,   3,  -1};   // s  dx  -> e  mx nm nexs  dx 
    osi.combination_pdf[2] = {-1,   3,  -1};   // dx s   -> e  mx nm nexs  dx 
    osi.combination_pdf[3] = {-1,   1,  -3};   // sx d   -> e  mx nm nexd  sx 
  }
  else if (psi.no_map[i_a] == 30){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -3};   // d  sx  -> e  mx nm nexu  cx 
    osi.combination_pdf[1] = { 1,   3,  -1};   // s  dx  -> e  mx nm nexc  ux 
    osi.combination_pdf[2] = {-1,   3,  -1};   // dx s   -> e  mx nm nexc  ux 
    osi.combination_pdf[3] = {-1,   1,  -3};   // sx d   -> e  mx nm nexu  cx 
  }
  else if (psi.no_map[i_a] == 31){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -4};   // d  cx  -> e  mx nm nexd  cx 
    osi.combination_pdf[1] = { 1,   3,  -2};   // s  ux  -> e  mx nm nexs  ux 
    osi.combination_pdf[2] = {-1,   3,  -2};   // ux s   -> e  mx nm nexs  ux 
    osi.combination_pdf[3] = {-1,   1,  -4};   // cx d   -> e  mx nm nexd  cx 
  }
  else if (psi.no_map[i_a] == 32){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   1,  -5};   // d  bx  -> e  mx nm nexd  bx 
    osi.combination_pdf[1] = { 1,   3,  -5};   // s  bx  -> e  mx nm nexs  bx 
    osi.combination_pdf[2] = {-1,   1,  -5};   // bx d   -> e  mx nm nexd  bx 
    osi.combination_pdf[3] = {-1,   3,  -5};   // bx s   -> e  mx nm nexs  bx 
  }
  else if (psi.no_map[i_a] == 34){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   2,   2};   // u  u   -> e  mx nm nexu  u  
    osi.combination_pdf[1] = { 1,   4,   4};   // c  c   -> e  mx nm nexc  c  
  }
  else if (psi.no_map[i_a] == 35){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   2,   4};   // u  c   -> e  mx nm nexu  c  
    osi.combination_pdf[1] = { 1,   4,   2};   // c  u   -> e  mx nm nexu  c  
  }
  else if (psi.no_map[i_a] == 36){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,   5};   // u  b   -> e  mx nm nexu  b  
    osi.combination_pdf[1] = { 1,   4,   5};   // c  b   -> e  mx nm nexc  b  
    osi.combination_pdf[2] = {-1,   2,   5};   // b  u   -> e  mx nm nexu  b  
    osi.combination_pdf[3] = {-1,   4,   5};   // b  c   -> e  mx nm nexc  b  
  }
  else if (psi.no_map[i_a] == 37){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -1};   // u  dx  -> e  mx nm nexu  dx 
    osi.combination_pdf[1] = { 1,   4,  -3};   // c  sx  -> e  mx nm nexc  sx 
    osi.combination_pdf[2] = {-1,   2,  -1};   // dx u   -> e  mx nm nexu  dx 
    osi.combination_pdf[3] = {-1,   4,  -3};   // sx c   -> e  mx nm nexc  sx 
  }
  else if (psi.no_map[i_a] == 38){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -1};   // u  dx  -> e  mx nm nexc  sx 
    osi.combination_pdf[1] = { 1,   4,  -3};   // c  sx  -> e  mx nm nexu  dx 
    osi.combination_pdf[2] = {-1,   2,  -1};   // dx u   -> e  mx nm nexc  sx 
    osi.combination_pdf[3] = {-1,   4,  -3};   // sx c   -> e  mx nm nexu  dx 
  }
  else if (psi.no_map[i_a] == 39){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  mx nm nexg  g  
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  mx nm nexg  g  
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  mx nm nexg  g  
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  mx nm nexg  g  
  }
  else if (psi.no_map[i_a] == 40){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  mx nm nexd  dx 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  mx nm nexs  sx 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  mx nm nexd  dx 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  mx nm nexs  sx 
  }
  else if (psi.no_map[i_a] == 41){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  mx nm nexu  ux 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  mx nm nexc  cx 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  mx nm nexu  ux 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  mx nm nexc  cx 
  }
  else if (psi.no_map[i_a] == 42){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  mx nm nexs  sx 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  mx nm nexd  dx 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  mx nm nexs  sx 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  mx nm nexd  dx 
  }
  else if (psi.no_map[i_a] == 43){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  mx nm nexc  cx 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  mx nm nexu  ux 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  mx nm nexc  cx 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  mx nm nexu  ux 
  }
  else if (psi.no_map[i_a] == 44){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  mx nm nexb  bx 
    osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  mx nm nexb  bx 
    osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  mx nm nexb  bx 
    osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  mx nm nexb  bx 
  }
  else if (psi.no_map[i_a] == 45){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -3};   // u  sx  -> e  mx nm nexu  sx 
    osi.combination_pdf[1] = { 1,   4,  -1};   // c  dx  -> e  mx nm nexc  dx 
    osi.combination_pdf[2] = {-1,   4,  -1};   // dx c   -> e  mx nm nexc  dx 
    osi.combination_pdf[3] = {-1,   2,  -3};   // sx u   -> e  mx nm nexu  sx 
  }
  else if (psi.no_map[i_a] == 46){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -4};   // u  cx  -> e  mx nm nexd  sx 
    osi.combination_pdf[1] = { 1,   4,  -2};   // c  ux  -> e  mx nm nexs  dx 
    osi.combination_pdf[2] = {-1,   4,  -2};   // ux c   -> e  mx nm nexs  dx 
    osi.combination_pdf[3] = {-1,   2,  -4};   // cx u   -> e  mx nm nexd  sx 
  }
  else if (psi.no_map[i_a] == 47){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -4};   // u  cx  -> e  mx nm nexu  cx 
    osi.combination_pdf[1] = { 1,   4,  -2};   // c  ux  -> e  mx nm nexc  ux 
    osi.combination_pdf[2] = {-1,   4,  -2};   // ux c   -> e  mx nm nexc  ux 
    osi.combination_pdf[3] = {-1,   2,  -4};   // cx u   -> e  mx nm nexu  cx 
  }
  else if (psi.no_map[i_a] == 48){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   2,  -5};   // u  bx  -> e  mx nm nexu  bx 
    osi.combination_pdf[1] = { 1,   4,  -5};   // c  bx  -> e  mx nm nexc  bx 
    osi.combination_pdf[2] = {-1,   2,  -5};   // bx u   -> e  mx nm nexu  bx 
    osi.combination_pdf[3] = {-1,   4,  -5};   // bx c   -> e  mx nm nexc  bx 
  }
  else if (psi.no_map[i_a] == 50){
    osi.combination_pdf.resize(1);
    osi.combination_pdf[0] = { 1,   5,   5};   // b  b   -> e  mx nm nexb  b  
  }
  else if (psi.no_map[i_a] == 51){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   5,  -1};   // b  dx  -> e  mx nm nexb  dx 
    osi.combination_pdf[1] = { 1,   5,  -3};   // b  sx  -> e  mx nm nexb  sx 
    osi.combination_pdf[2] = {-1,   5,  -1};   // dx b   -> e  mx nm nexb  dx 
    osi.combination_pdf[3] = {-1,   5,  -3};   // sx b   -> e  mx nm nexb  sx 
  }
  else if (psi.no_map[i_a] == 52){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   5,  -2};   // b  ux  -> e  mx nm nexb  ux 
    osi.combination_pdf[1] = { 1,   5,  -4};   // b  cx  -> e  mx nm nexb  cx 
    osi.combination_pdf[2] = {-1,   5,  -2};   // ux b   -> e  mx nm nexb  ux 
    osi.combination_pdf[3] = {-1,   5,  -4};   // cx b   -> e  mx nm nexb  cx 
  }
  else if (psi.no_map[i_a] == 53){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  mx nm nexg  g  
    osi.combination_pdf[1] = {-1,   5,  -5};   // bx b   -> e  mx nm nexg  g  
  }
  else if (psi.no_map[i_a] == 54){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  mx nm nexd  dx 
    osi.combination_pdf[1] = { 1,   5,  -5};   // b  bx  -> e  mx nm nexs  sx 
    osi.combination_pdf[2] = {-1,   5,  -5};   // bx b   -> e  mx nm nexd  dx 
    osi.combination_pdf[3] = {-1,   5,  -5};   // bx b   -> e  mx nm nexs  sx 
  }
  else if (psi.no_map[i_a] == 55){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  mx nm nexu  ux 
    osi.combination_pdf[1] = { 1,   5,  -5};   // b  bx  -> e  mx nm nexc  cx 
    osi.combination_pdf[2] = {-1,   5,  -5};   // bx b   -> e  mx nm nexu  ux 
    osi.combination_pdf[3] = {-1,   5,  -5};   // bx b   -> e  mx nm nexc  cx 
  }
  else if (psi.no_map[i_a] == 56){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  mx nm nexb  bx 
    osi.combination_pdf[1] = {-1,   5,  -5};   // bx b   -> e  mx nm nexb  bx 
  }
  else if (psi.no_map[i_a] == 58){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,  -1,  -1};   // dx dx  -> e  mx nm nexdx dx 
    osi.combination_pdf[1] = { 1,  -3,  -3};   // sx sx  -> e  mx nm nexsx sx 
  }
  else if (psi.no_map[i_a] == 59){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,  -1,  -2};   // dx ux  -> e  mx nm nexdx ux 
    osi.combination_pdf[1] = { 1,  -3,  -4};   // sx cx  -> e  mx nm nexsx cx 
    osi.combination_pdf[2] = {-1,  -1,  -2};   // ux dx  -> e  mx nm nexdx ux 
    osi.combination_pdf[3] = {-1,  -3,  -4};   // cx sx  -> e  mx nm nexsx cx 
  }
  else if (psi.no_map[i_a] == 60){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,  -1,  -3};   // dx sx  -> e  mx nm nexdx sx 
    osi.combination_pdf[1] = { 1,  -3,  -1};   // sx dx  -> e  mx nm nexdx sx 
  }
  else if (psi.no_map[i_a] == 61){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,  -1,  -4};   // dx cx  -> e  mx nm nexdx cx 
    osi.combination_pdf[1] = { 1,  -3,  -2};   // sx ux  -> e  mx nm nexux sx 
    osi.combination_pdf[2] = {-1,  -3,  -2};   // ux sx  -> e  mx nm nexux sx 
    osi.combination_pdf[3] = {-1,  -1,  -4};   // cx dx  -> e  mx nm nexdx cx 
  }
  else if (psi.no_map[i_a] == 62){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,  -1,  -4};   // dx cx  -> e  mx nm nexux sx 
    osi.combination_pdf[1] = { 1,  -3,  -2};   // sx ux  -> e  mx nm nexdx cx 
    osi.combination_pdf[2] = {-1,  -3,  -2};   // ux sx  -> e  mx nm nexdx cx 
    osi.combination_pdf[3] = {-1,  -1,  -4};   // cx dx  -> e  mx nm nexux sx 
  }
  else if (psi.no_map[i_a] == 63){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,  -1,  -5};   // dx bx  -> e  mx nm nexdx bx 
    osi.combination_pdf[1] = { 1,  -3,  -5};   // sx bx  -> e  mx nm nexsx bx 
    osi.combination_pdf[2] = {-1,  -1,  -5};   // bx dx  -> e  mx nm nexdx bx 
    osi.combination_pdf[3] = {-1,  -3,  -5};   // bx sx  -> e  mx nm nexsx bx 
  }
  else if (psi.no_map[i_a] == 65){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,  -2,  -2};   // ux ux  -> e  mx nm nexux ux 
    osi.combination_pdf[1] = { 1,  -4,  -4};   // cx cx  -> e  mx nm nexcx cx 
  }
  else if (psi.no_map[i_a] == 66){
    osi.combination_pdf.resize(2);
    osi.combination_pdf[0] = { 1,  -2,  -4};   // ux cx  -> e  mx nm nexux cx 
    osi.combination_pdf[1] = { 1,  -4,  -2};   // cx ux  -> e  mx nm nexux cx 
  }
  else if (psi.no_map[i_a] == 67){
    osi.combination_pdf.resize(4);
    osi.combination_pdf[0] = { 1,  -2,  -5};   // ux bx  -> e  mx nm nexux bx 
    osi.combination_pdf[1] = { 1,  -4,  -5};   // cx bx  -> e  mx nm nexcx bx 
    osi.combination_pdf[2] = {-1,  -2,  -5};   // bx ux  -> e  mx nm nexux bx 
    osi.combination_pdf[3] = {-1,  -4,  -5};   // bx cx  -> e  mx nm nexcx bx 
  }
  else if (psi.no_map[i_a] == 69){
    osi.combination_pdf.resize(1);
    osi.combination_pdf[0] = { 1,  -5,  -5};   // bx bx  -> e  mx nm nexbx bx 
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
