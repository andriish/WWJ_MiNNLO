{
  static int switch_cut_M_leplep = psi.user.switch_value[psi.user.switch_map["M_leplep"]];
  static double cut_min_M_leplep = psi.user.cut_value[psi.user.cut_map["M_leplep"]];

  static int switch_cut_M_leplepnunu = psi.user.switch_value[psi.user.switch_map["M_leplepnunu"]];
  static double cut_min_M_leplepnunu = psi.user.cut_value[psi.user.cut_map["min_M_leplepnunu"]];

  static int switch_cut_deltaZ_M_leplepnunu = psi.user.switch_value[psi.user.switch_map["delta_M_leplepnunu"]];
  static double cut_max_deltaZ_M_leplepnunu = psi.user.cut_value[psi.user.cut_map["max_delta_M_leplepnunu"]];
  
  if (switch_cut_M_leplep == 1){psi.sqrtsmin_opt[0][12] = cut_min_M_leplep;}

  if (switch_cut_M_leplepnunu == 1 && switch_cut_deltaZ_M_leplepnunu == 0){psi.sqrtsmin_opt[0][60] = cut_min_M_leplepnunu;}
  if (switch_cut_M_leplepnunu == 0 && switch_cut_deltaZ_M_leplepnunu == 1){psi.sqrtsmin_opt[0][60] = psi.M[23] - cut_max_deltaZ_M_leplepnunu;}
  if (switch_cut_M_leplepnunu == 1 && switch_cut_deltaZ_M_leplepnunu == 1){psi.sqrtsmin_opt[0][60] = max(cut_min_M_leplepnunu, psi.M[23] - cut_max_deltaZ_M_leplepnunu);}
}
