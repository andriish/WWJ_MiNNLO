#include "header.hpp"
#include "ppeexa03.subprocess.h"
#include "ppeexa03.phasespace.h"
#include "pplla23.amplitude.doublevirtual.h"
#include "gglla33.amplitude.doublevirtual.h"
int main(int argc, char *argv[]){
  cout << "BEGIN" << endl;

  munich munich(argc, argv, "pp-emepa+X");

  if (munich.subprocess != ""){
    // generic functions - relevant for all subcontributions
    munich.generic.particles = & ppeexa03_particles;
    munich.generic.cuts = & ppeexa03_cuts;
    munich.generic.cuts_test = & ppeexa03_cuts_test;
    munich.generic.calculate_dynamic_scale = & ppeexa03_calculate_dynamic_scale;
    munich.generic.calculate_dynamic_scale_TSV = & ppeexa03_calculate_dynamic_scale_TSV;
    munich.generic.moments = & ppeexa03_moments;
    if (munich.csi.type_contribution == "born" || 
        munich.csi.type_contribution == "VA" || 
        munich.csi.type_contribution == "CA" || 
        munich.csi.type_contribution == "VT" || 
        munich.csi.type_contribution == "CT" || 
        munich.csi.type_contribution == "L2I" || 
        munich.csi.type_contribution == "loop" || 
        munich.csi.type_contribution == "VT2" || 
        munich.csi.type_contribution == "CT2" || 
        munich.csi.type_contribution == "NLL_LO" || 
        munich.csi.type_contribution == "NLL_NLO" || 
        munich.csi.type_contribution == "NNLL_LO" || 
        munich.csi.type_contribution == "NNLL_NLO" || 
        munich.csi.type_contribution == "NNLL_NNLO" || 
        munich.csi.type_contribution == "L2VA" || 
        munich.csi.type_contribution == "L2CA" || 
        munich.csi.type_contribution == "L2VT" || 
        munich.csi.type_contribution == "L2CT"){
      // generic functions - relevant for all subcontributions located on born phase-space in order to determine subprocess information
      munich.generic.determination_subprocess_psp = & ppeexa03_determination_subprocess_born;
      munich.generic.combination_subprocess_psp = & ppeexa03_combination_subprocess_born;
      // generic functions - relevant for all subcontributions located on born phase-space in order to perform phase-space generation
      munich.generic.optimize_minv_psp = & ppeexa03_optimize_minv_born;
      munich.generic.determination_MCchannels_psp = & ppeexa03_determination_MCchannels_born;
      munich.generic.ax_psp_psp = & ppeexa03_ax_psp_born;
      munich.generic.ac_tau_psp_psp = & ppeexa03_ac_tau_psp_born;
      munich.generic.ac_psp_psp = & ppeexa03_ac_psp_born;
      munich.generic.ag_psp_psp = & ppeexa03_ag_psp_born;
      if (munich.csi.type_contribution == "CA"){munich.generic.phasespacepoint_psp = & ppeexa03_phasespacepoint_collinear;}
      else {munich.generic.phasespacepoint_psp = & ppeexa03_phasespacepoint_born;}
      if (munich.csi.type_contribution == "VT2" || 
          munich.csi.type_contribution == "VJ2" || 
          munich.csi.type_contribution == "NNLL_NNLO"){
        munich.generic.calculate_H2 = & pplla23_calculate_H2;
      }
      if (munich.csi.type_contribution == "L2VA" || 
          munich.csi.type_contribution == "L2VT" || 
          munich.csi.type_contribution == "L2VJ"){
        munich.generic.calculate_H1gg = & gglla33_calculate_H1;
      }
    }

    else if (munich.csi.type_contribution == "RA" || 
             munich.csi.type_contribution == "RT" || 
             munich.csi.type_contribution == "RVA" || 
             munich.csi.type_contribution == "RCA" || 
             munich.csi.type_contribution == "L2RA" || 
             munich.csi.type_contribution == "L2RT"){
      // generic functions - relevant for all subcontributions located on real phase-space in order to determine subprocess information
      munich.generic.determination_subprocess_psp = & ppeexa03_determination_subprocess_real;
      munich.generic.combination_subprocess_psp = & ppeexa03_combination_subprocess_real;
      // generic functions - relevant for all subcontributions located on real phase-space in order to perform phase-space generation
      munich.generic.determination_MCchannels_psp = & ppeexa03_determination_MCchannels_real;
      munich.generic.optimize_minv_psp = & ppeexa03_optimize_minv_real;
      munich.generic.ac_tau_psp_psp = & ppeexa03_ac_tau_psp_real;
      munich.generic.ax_psp_psp = & ppeexa03_ax_psp_real;
      munich.generic.ac_psp_psp = & ppeexa03_ac_psp_real;
      munich.generic.ag_psp_psp = & ppeexa03_ag_psp_real;
      munich.generic.calculate_dynamic_scale_RA = & ppeexa03_calculate_dynamic_scale_RA;
      munich.generic.determination_no_subprocess_dipole = & ppeexa03_determination_no_subprocess_born;
      munich.generic.determination_subprocess_dipole = & ppeexa03_determination_subprocess_born;
      munich.generic.determination_MCchannels_dipole = & ppeexa03_determination_MCchannels_born;
      munich.generic.ac_tau_psp_dipole = & ppeexa03_ac_tau_psp_born;
      munich.generic.ax_psp_dipole = & ppeexa03_ax_psp_born;
      munich.generic.ac_psp_dipole = & ppeexa03_ac_psp_born;
      munich.generic.ag_psp_dipole = & ppeexa03_ag_psp_born;
      if (munich.csi.type_contribution == "RCA" || munich.csi.type_contribution == "RCJ"){munich.generic.phasespacepoint_psp = & ppeexa03_phasespacepoint_realcollinear;}
      else {munich.generic.phasespacepoint_psp = & ppeexa03_phasespacepoint_real;}
    }

    else if (munich.csi.type_contribution == "RRA"){
      // generic functions - relevant for all subcontributions located on doublereal phase-space in order to determine subprocess information
      munich.generic.determination_subprocess_psp = & ppeexa03_determination_subprocess_doublereal;
      munich.generic.combination_subprocess_psp = & ppeexa03_combination_subprocess_doublereal;
      // generic functions - relevant for all subcontributions located on doublereal phase-space in order to perform phase-space generation
      munich.generic.determination_MCchannels_psp = & ppeexa03_determination_MCchannels_doublereal;
      munich.generic.ax_psp_psp = & ppeexa03_ax_psp_doublereal;
      munich.generic.ac_tau_psp_psp = & ppeexa03_ac_tau_psp_doublereal;
      munich.generic.ac_psp_psp = & ppeexa03_ac_psp_doublereal;
      munich.generic.ag_psp_psp = & ppeexa03_ag_psp_doublereal;
      munich.generic.optimize_minv_psp = & ppeexa03_optimize_minv_doublereal;
      munich.generic.calculate_dynamic_scale_RA = & ppeexa03_calculate_dynamic_scale_RA;
      munich.generic.determination_no_subprocess_dipole = & ppeexa03_determination_no_subprocess_real;
      munich.generic.determination_subprocess_dipole = & ppeexa03_determination_subprocess_real;
      munich.generic.determination_MCchannels_dipole = & ppeexa03_determination_MCchannels_real;
      munich.generic.ac_tau_psp_dipole = & ppeexa03_ac_tau_psp_real;
      munich.generic.ax_psp_dipole = & ppeexa03_ax_psp_real;
      munich.generic.ac_psp_dipole = & ppeexa03_ac_psp_real;
      munich.generic.ag_psp_dipole = & ppeexa03_ag_psp_real;
      munich.generic.phasespacepoint_psp = & ppeexa03_phasespacepoint_doublereal;

      munich.generic.determination_no_subprocess_doubledipole = & ppeexa03_determination_no_subprocess_born;
      munich.generic.determination_subprocess_doubledipole = & ppeexa03_determination_subprocess_born;
      munich.generic.determination_MCchannels_doubledipole = & ppeexa03_determination_MCchannels_born;
      munich.generic.ac_tau_psp_doubledipole = & ppeexa03_ac_tau_psp_born;
      munich.generic.ax_psp_doubledipole = & ppeexa03_ax_psp_born;
      munich.generic.ac_psp_doubledipole = & ppeexa03_ac_psp_born;
      munich.generic.ag_psp_doubledipole = & ppeexa03_ag_psp_born;
    }

    munich.run_initialization();
    munich.run_integration();

    cout << "END " << munich.csi.type_contribution << " " << munich.csi.type_correction << endl;
  }
  else {
    munich.generic.list_subprocess_born = & ppeexa03_list_subprocess_born;
    munich.generic.list_subprocess_V_QCD = & ppeexa03_list_subprocess_V_QCD;
    munich.generic.list_subprocess_C_QCD = & ppeexa03_list_subprocess_C_QCD;
    munich.generic.list_subprocess_R_QCD = & ppeexa03_list_subprocess_R_QCD;
    munich.generic.list_subprocess_V_QEW = & ppeexa03_list_subprocess_V_QEW;
    munich.generic.list_subprocess_C_QEW = & ppeexa03_list_subprocess_C_QEW;
    munich.generic.list_subprocess_R_QEW = & ppeexa03_list_subprocess_R_QEW;
    munich.generic.list_subprocess_V2_QCD = & ppeexa03_list_subprocess_V2_QCD;
    munich.generic.list_subprocess_C2_QCD = & ppeexa03_list_subprocess_C2_QCD;
    munich.generic.list_subprocess_RV_QCD = & ppeexa03_list_subprocess_RV_QCD;
    munich.generic.list_subprocess_RC_QCD = & ppeexa03_list_subprocess_RC_QCD;
    munich.generic.list_subprocess_RR_QCD = & ppeexa03_list_subprocess_RR_QCD;

    munich.get_summary();

    cout << "END RESULT/DISTRIBUTION" << endl;
  }
  munich.walltime_end();
  return 0;
}
