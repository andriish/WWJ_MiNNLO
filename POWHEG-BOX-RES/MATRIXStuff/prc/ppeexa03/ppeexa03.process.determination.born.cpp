#include "header.hpp"
#include "ppeexa03.subprocess.h"
void ppeexa03_determination_no_subprocess_born(int & no_map, vector<int> & o_map, int & no_prc, vector<int> & o_prc, double & symmetry_factor, vector<int> & tp, int basic_order_alpha_s, int basic_order_alpha_e, int basic_order_interference){
  static Logger logger("ppeexa03_determination_no_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  no_map = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (basic_order_alpha_s == 0 && basic_order_alpha_e == 3 && basic_order_interference == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_map = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_map = 3; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_map = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_map = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_map = 5; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_map = 6; break;}
      }
      else if (basic_order_alpha_s == 2 && basic_order_alpha_e == 3 && basic_order_interference == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){no_map = 1; break;}
      }
    }
    if (no_map != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){o_map[o] = o;}
    else {o_map[o] = out[o];}
  }
  no_prc = no_map;
  o_prc = o_map;
  if (basic_order_alpha_s == 0 && basic_order_alpha_e == 3 && basic_order_interference == 0){
    if (no_map == 0){}
    else if (no_map == 3){symmetry_factor = 1;}
    else if (no_map == 4){symmetry_factor = 1;}
    else if (no_map == 5){symmetry_factor = 1;}
    else if (no_map == 6){symmetry_factor = 1;}
  }
  else if (basic_order_alpha_s == 2 && basic_order_alpha_e == 3 && basic_order_interference == 0){
    if (no_map == 0){}
    else if (no_map == 1){symmetry_factor = 1;}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
void ppeexa03_determination_subprocess_born(int i_a, phasespace_set & psi){
  static Logger logger("ppeexa03_determination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<int> tp = psi.csi->basic_type_parton[i_a];
  //  ppeexa03_determination_no_subprocess_born(psi.no_map[i_a], psi.o_map[i_a], psi.no_prc[i_a], psi.o_prc[i_a], psi.symmetry_factor, tp, psi.phasespace_order_alpha_s[i_a], psi.phasespace_order_alpha_e[i_a], psi.phasespace_order_interference[i_a]);

  psi.no_map[i_a] = -1;
  vector<int> out(6);
  for (int i = 0; i < 2; i++){
    if (i == 0){out[1] = 1; out[2] = 2;}
    if (i == 1){out[1] = 2; out[2] = 1;}
    for (int o = 0; o < 1; o++){
      if (o == 0){}
      if (psi.phasespace_order_alpha_s[i_a] == 0 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   1) && (tp[out[2]] ==  -1) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){psi.no_map[i_a] = 3; break;}
        if ((tp[out[1]] ==   3) && (tp[out[2]] ==  -3) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){psi.no_map[i_a] = 3; break;}
        if ((tp[out[1]] ==   2) && (tp[out[2]] ==  -2) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){psi.no_map[i_a] = 4; break;}
        if ((tp[out[1]] ==   4) && (tp[out[2]] ==  -4) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){psi.no_map[i_a] = 4; break;}
        if ((tp[out[1]] ==   5) && (tp[out[2]] ==  -5) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){psi.no_map[i_a] = 5; break;}
        if ((tp[out[1]] ==  22) && (tp[out[2]] ==  22) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){psi.no_map[i_a] = 6; break;}
      }
      else if (psi.phasespace_order_alpha_s[i_a] == 2 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
        if ((tp[out[1]] ==   0) && (tp[out[2]] ==   0) && (tp[3] ==  11) && (tp[4] == -11) && (tp[5] ==  22)){psi.no_map[i_a] = 1; break;}
      }
    }
    if (psi.no_map[i_a] != -1){break;}
  }
  for (int o = 0; o < out.size(); o++){
    if (out[o] == 0){psi.o_map[i_a][o] = o;}
    else {psi.o_map[i_a][o] = out[o];}
  }
  psi.no_prc[i_a] = psi.no_map[i_a];
  psi.o_prc[i_a] = psi.o_map[i_a];
  if (psi.phasespace_order_alpha_s[i_a] == 0 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 3){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 4){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 5){psi.symmetry_factor = 1;}
    else if (psi.no_map[i_a] == 6){psi.symmetry_factor = 1;}
  }
  else if (psi.phasespace_order_alpha_s[i_a] == 2 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 1){psi.symmetry_factor = 1;}
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
void ppeexa03_combination_subprocess_born(int i_a, phasespace_set & psi, observable_set & osi){
  static Logger logger("ppeexa03_combination_subprocess_born");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  if (psi.phasespace_order_alpha_s[i_a] == 0 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 3){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   1,  -1};   // d  dx  -> e  ex a  
      osi.combination_pdf[1] = { 1,   3,  -3};   // s  sx  -> e  ex a  
      osi.combination_pdf[2] = {-1,   1,  -1};   // dx d   -> e  ex a  
      osi.combination_pdf[3] = {-1,   3,  -3};   // sx s   -> e  ex a  
    }
    else if (psi.no_map[i_a] == 4){
      osi.combination_pdf.resize(4);
      osi.combination_pdf[0] = { 1,   2,  -2};   // u  ux  -> e  ex a  
      osi.combination_pdf[1] = { 1,   4,  -4};   // c  cx  -> e  ex a  
      osi.combination_pdf[2] = {-1,   2,  -2};   // ux u   -> e  ex a  
      osi.combination_pdf[3] = {-1,   4,  -4};   // cx c   -> e  ex a  
    }
    else if (psi.no_map[i_a] == 5){
      osi.combination_pdf.resize(2);
      osi.combination_pdf[0] = { 1,   5,  -5};   // b  bx  -> e  ex a  
      osi.combination_pdf[1] = {-1,   5,  -5};   // bx b   -> e  ex a  
    }
    else if (psi.no_map[i_a] == 6){
      osi.combination_pdf.resize(1);
      osi.combination_pdf[0] = { 1,   7,   7};   // a  a   -> e  ex a  
    }
  }
  else if (psi.phasespace_order_alpha_s[i_a] == 2 && psi.phasespace_order_alpha_e[i_a] == 3 && psi.phasespace_order_interference[i_a] == 0){
    if (psi.no_map[i_a] == 0){}
    else if (psi.no_map[i_a] == 1){
      osi.combination_pdf.resize(1);
      osi.combination_pdf[0] = { 1,   0,   0};   // g  g   -> e  ex a  
    }
  }

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
