#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <vector>
#include <string>
#include <cstring>
#include <map>
#include <complex>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <time.h>
#include <dirent.h>
#include <cstdlib>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <limits>
#include <chrono>
#include <ctime>    
using namespace std;
typedef complex<double> double_complex;
#include "LHAPDF/LHAPDF.h"
#include "classes.h"
#include "routines.h"
#include "phasespace.h"
#include "varia.h"
#include "dipolesubtraction.h"
#include "qTsubtraction.h"
#include "NJsubtraction.h"
#include "const.cxx"
#include "gsl/gsl_sf_dilog.h"
#include "gsl/gsl_minmax.h"
#ifdef OPENLOOPS
#include "openloops.h"
#endif
#ifdef RECOLA
#include "recola.hpp"
#endif

using namespace mathconst;
using namespace physconst;

#ifdef RECOLA
using namespace Recola;
#endif

#define setdr right << setw(25) << setprecision(16) << showpoint
#define setdl left << setw(25) << setprecision(16) << showpoint
#define setsr right << setw(15) << setprecision(8) << showpoint
#define setsl left << setw(15) << setprecision(8) << showpoint

