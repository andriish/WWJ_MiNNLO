#include <classes.cxx>
#include <definitions.observable.set.cxx>
#include <header/ggllll34.amplitude.doublevirtual.h>

typedef enum {
    V_PHOT = 0,
    V_ZLL,
    V_WMIN,
    V_WPLU,
    V_ZNN
} TypeV;

void ggllll34_calculate_H1(observable_set & oset){
  static Logger logger("ggllll34_calculate_H1");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;
  logger << LOG_DEBUG_VERBOSE << "NEW VT2 implementation called" << endl;
  static int initialization = 1;
  static double c1ggdeltaf = 0;
  
  if (oset.switch_yuk == 0 or oset.order_y != 2 or oset.switch_OL == 1){
    ggllll34_calculate_amplitude_doublevirtual(oset);

    osi_QT_H1_delta = osi_QT_A1;

    osi_QT_H1_delta /= 2;
    
    osi_QT_H1_delta /= (osi_alpha_S * inv2pi * osi_QT_A0);
  }
  
  // depends on Higgs virtuality !! ?? !!

  if (initialization == 1){

    double  approxim = 1.; // 0 = large m_t limit, 1 = exact m_t dependence up to NLO, 2 = exact dependence on m_t and m_b up to NLO
    double M_H = osi_msi.M_H;
    double M_b = osi_msi.M_b;
  //  double M_b = 1.e-20;
  //double M_t = 1.e20;
    double M_t = osi_msi.M_t;
    double my_pi = 3.14159265359;
    int icross = 0; //icross =0 exact result
                  //  icross =1 large mtop limit
    double EPST = 1.e-8; // parameters needed to perform the analytical continuation of Logs
    double EPSV = 1.e-8;
    double REPS = 1.e-8;
    double XCVIR;

    int ibot = 1;  // What is this for
   
  // Save the data points

    double XX[462];
    double YAR[462];
    double YAI[462];
    double YHR[462];
    double YHI[462];

    int icount;

    double xx_val;
    ifstream input_xx;
    input_xx.open("/mnt/runs2/yook/develop_MATRIX/MATRIX/amp/ggllll34/out_xx.txt");
    string xx_line;
    icount = 0;
    while (input_xx >> xx_line){
      xx_val = atof(xx_line.c_str());
      XX[icount] = xx_val;
      icount++;
    }
    input_xx.close();
 
    double yar_val;
    ifstream input_yar;
    input_yar.open("/mnt/runs2/yook/develop_MATRIX/MATRIX/amp/ggllll34/out_yar.txt");
    string yar_line;
    icount = 0;
    while (input_yar >> yar_line){
      yar_val = atof(yar_line.c_str());
      YAR[icount] = yar_val;
      icount++;
    }
    input_yar.close();

    double yhr_val;
    ifstream input_yhr;
    input_yhr.open("/mnt/runs2/yook/develop_MATRIX/MATRIX/amp/ggllll34/out_yhr.txt");
    string yhr_line;
    icount = 0;
    while (input_yhr >> yhr_line){
      yhr_val = atof(yhr_line.c_str());
      YHR[icount] = yhr_val;
      icount++;
    }
    input_yhr.close();

    double yai_val;
    ifstream input_yai;
    input_yai.open("/mnt/runs2/yook/develop_MATRIX/MATRIX/amp/ggllll34/out_yai.txt");
    string yai_line;
    icount = 0;
    while (input_yai >> yai_line){
      yai_val = atof(yai_line.c_str());
      YAI[icount] = yai_val;
      icount++;
    }
    input_yai.close();
  
    double yhi_val;
    ifstream input_yhi;
    input_yhi.open("/mnt/runs2/yook/develop_MATRIX/MATRIX/amp/ggllll34/out_yhi.txt");
    string yhi_line;
    icount = 0;
    while (input_yhi >> yhi_line){
      yhi_val = atof(yhi_line.c_str());
      YHI[icount] = yhi_val;
      icount++;
    }
    input_yhi.close();

    if (icross == 0){
      double RHOT = pow(M_H, 2)/pow(M_t, 2);
      double RHOB = pow(M_H, 2)/pow(M_b, 2);
    
      if (ibot == 0){
	double fact = 1.;
	double facb = 1.;
      }
      else if (ibot == 1){
	double fact = 1.;
	double facb = 0.;
      }
      else {
	logger << LOG_DEBUG << "wrong ibot in setpar: " << ibot << endl;
      }
      double fact = 1.;
      double facb = 0.;
      double_complex CTAUb = 4. * pow(M_b, 2.)/pow(M_H, 2.)*double_complex(1.0,-1.* REPS) ;
      double_complex CTAUt = 4. * pow(M_t, 2.)/pow(M_H, 2.)*double_complex(1.0,-1.* REPS) ;
      double_complex CETAb = sqrt(1. - CTAUb);
      double_complex CETAt = sqrt(1. - CTAUt);
      double_complex CALPb = 1.0 + CETAb;
      double_complex CALPt = 1.0 + CETAt;
      double_complex CALMb = CTAUb/CALPb;
      double_complex CALMt = CTAUt/CALPt;
      double_complex CFb = -1. * pow(log(-CALPb/CALMb),2.)/4.0;
      double_complex CFt = -1. * pow(log(-CALPt/CALMt),2.)/4.0;
      double_complex CFBORNb = 1.5*CTAUb*(1.0+(1.0-CTAUb)*CFb);
      double_complex CFBORNt = 1.5*CTAUt*(1.0+(1.0-CTAUt)*CFt);

      double_complex CF0T=fact*CFBORNt; // TOP FORM FACTOR
      double_complex CF0B = facb*CFBORNb; // BOTTOM FORM FACTOR
      double_complex CF0=CF0T+CF0B*(approxim-1.); //TOTAL FORM FACTOR
      XCVIR = real((CKOF(RHOT, XX, YHR, YHI, YAR, YAI)*CF0T+(approxim-1)* CKOF(RHOB, XX, YHR, YHI, YAR, YAI)*CF0B)/CF0);
      int kord = 3;
      // logger << LOG_INFO << "XCVIR FULL = " << (CKOF(RHOT, XX, YHR, YHI, YAR, YAI)*CF0T+(approxim-1)* CKOF(RHOB, XX, YHR, YHI, YAR, YAI)*CF0B)/CF0 << endl;
      //  logger << LOG_INFO << " XCVIR " << XCVIR << endl;

    } 
    else{
      XCVIR = 5.5e0;
    }
  
    //c1ggdeltaf = XCVIR/2.e0+3.e0*pow(pi,2)/4.e0;
    c1ggdeltaf = XCVIR + 3.e0*pow(pi,2)/2.e0;
    double wbranch;
    double zbranch;
    wbranch = branch(oset, zbranch, wbranch);

    // logger << LOG_INFO << "wbranch: "<< wbranch << endl;
    c1ggdeltaf = c1ggdeltaf *wbranch;

    initialization = 0;
    
    logger << LOG_DEBUG << "c1ggdeltaf = "<< c1ggdeltaf << endl;
  }

  if (oset.switch_RCL == 1){
    

  if (oset.switch_yuk == 1 and oset.order_y == 2){
    osi_QT_H1_delta =  c1ggdeltaf;
  }

  else if (oset.switch_yuk == 1 and oset.order_y == 1){
    osi_QT_H1_delta = 0.5 * osi_QT_H1_delta + 0.5 * c1ggdeltaf;
  }

  }

    
 
  logger << LOG_DEBUG << "osi_QT_H1_delta = " << osi_QT_H1_delta << endl;

  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}

double fint(int KORD, double X, double XX[462], double YY[462]){
  double fint;
  double X0 = XX[1];
  double X1 = XX[2];
  double X2 = XX[3];
  double Y0 = YY[1];
  double Y1 = YY[2];
  double Y2 = YY[3];
  double D1F = (Y1-Y0)/(X1-X0);
  double D2F = (Y2-Y1)/(X2-X1);  
  double DX;
  double DF;

  if (X < X1){
    DX = X-X0;
    DF = D1F;
  }
  else {
    DX=X-X1;
    DF=D2F;
  }
  fint = Y1+DF*DX;
  if (KORD == 2){
    double A0 = (X-X1)*(X-X2)/(X0-X1)/(X0-X2);
    double A1 = (X-X0)*(X-X2)/(X1-X0)/(X1-X2);
    double A2 = (X-X0)*(X-X1)/(X2-X0)/(X2-X1);
    fint = A0*Y0+A1*Y1+A2*Y2;
  }
  
  return fint;
}

double fint1(int KORD, double X, double XP[462], double YP[462], int NP){
  int N1 = 3;
  int NX;
  double Fint1;
  double X0[462];
  double Y0[462];
  
  int i = 0;	
  while (i <= NP){
    if (X > XP[i]){
      NX = i;
    }
    i++;
      } 
 
  if (NX == NP){
    NX = NP - 1;
  }
  if (NX == 1){
    NX = 2;
  }
 
  i = 0;
  while (i <= 3){
    X0[i] = XP[NX + i - 2];
    i++;
      }

  i = 0;
  while (i <= 3){
    Y0[i] = YP[NX + i - 2];
    i++;
      }
  
  Fint1 = fint(KORD, X, X0, Y0);
  return Fint1;
}

double_complex CKOF(double RHO, double XX[462], double YHR[462], double YHI[462], double YAR[462], double YAI[462]){

  double_complex ckof;
  double EPST = 1.e-8; // parameters needed to perform the analytical continuation of Logs
  double EPSV = 1.e-8;
  double REPS = 1.e-8;
  int NCUT1 = 46;
  int NCUT2 = 54;
  int NCUT3 = 341;
  int NN = 999;
  int N = 461;
  double RHO2 = XX[NCUT3];
  double RHEP = 1.e-15;

  // scalar higgs
  if (RHO <= RHO2){
    int kord = 3; //WHAT IS THIS FOR
    double fint1R = fint1(kord, RHO,XX,YHR,N);
    double fint1H = fint1(kord, RHO,XX,YHI,N);
    ckof = {fint1R, fint1H};
  }
  else {
    double_complex temp1 =  {1.e0,-RHEP};
    double_complex CRHO = RHO2/temp1;
    double_complex CDLR = log(-CRHO);
    double_complex CLIM = 5.e0/36.e0*pow(CDLR, 2)-4.e0/3.e0*CDLR;
    double_complex temp2 = {YHR[NCUT3], YHI[NCUT3]};
    double_complex CONST = temp2 - CLIM;
    double_complex temp3 = {1.e0,-REPS};
    CRHO = RHO/temp3;
    CDLR = log(-CRHO);
    CLIM = 5.e0/36.e0*pow(CDLR, 2)-4.e0/3.e0*CDLR;
    ckof = CLIM + CONST;
    
  }
  double_complex temp4 = {pow(pi, 2), 0.};
  ckof = ckof - temp4;
  // cout << LOG_INFO << " (CKOF(RHOT, XX, YHR, YHI, YAR, YAI)" << ckof << endl;
  return ckof;
  
}

double branch(observable_set & oset, double brzee, double brwen){

  double hmass = osi_msi.M_H;
  double hwidth = osi_msi.Gamma_H;
  double bmass = osi_msi.M_b;
  //  double M_b = 1.e-20;
  //double M_t = 1.e20;
  double mtop = osi_msi.M_t;
  double zmass = osi_msi.M_Z;
  double zwidth = osi_msi.Gamma_Z;
  double wmass = osi_msi.M_W;
  double wwidth = osi_msi.Gamma_W;
  double my_pi = 3.14159265359;
  double aemmz = osi_msi.alpha_e_MZ;
  double Gf = osi_msi.G_F;
  double rt2 = 1.4142135624e0;
  double xw  = 4. * pi * aemmz/(8.0*pow(wmass,2)*Gf/rt2);
  double gwsq = 4. * pi*aemmz/xw;
  double esq = gwsq*xw;
  double gw = pow(gwsq, 0.5);
  double facz = esq/4.e0*zmass/(6.e0*pi);
  double facw = gwsq/8.e0*wmass/(6.e0*pi);
  double xsq = pow((wmass/mtop),2);
  // double factop = pow((gw/wmass), 2)*pow(mtop, 3)/(64.e0*pi)*pow((1.e0-xsq), 2)*(1.e0+2.e0*xsq);
  double sin2w = 2.*pow(xw*(1e0-xw), 0.5);
  double le = (-1.- 2.*(-1.)*xw)/sin2w;
  double re = (-2.*(-1.)*xw)/sin2w;
  double pwidth_e = facz*(pow(le, 2)+pow(re, 2));
  brzee = pwidth_e/zwidth;
  brwen = facw/wwidth;

  double wwbr;
  double x_w = 4.*pow(wmass, 2)/pow(hmass, 2);
  // if (x_w < 1.){
  //  wwbr = gwsq/64./pi*pow(hmass, 3)/pow(wmass, 2) *pow((1.0-x_w), 0.5)*(1.-x_w+0.75*pow(x_w, 2))/hwidth;
    // }
    //else {
    // wwbr = 0;
    // }
  wwbr = 0.2037;
  brwen = pow(brwen, 2)*wwbr;
  //cout << brwen << "wwbr: " << wwbr << endl;
  //cout << brwen << "branch: " << brwen << endl;
  return brwen;
}

double_complex ggllll34_computeM(int i1, int i2, int i5, int i6, int i7, int i8, int hel, const vector<vector<double_complex > > &za, const vector<vector<double_complex > > &zb, const vector<double_complex> &E) {
  static Logger logger("ggllll34_computeM");
  i5-=2;
  i6-=2;
  i7-=2;
  i8-=2;

//  cout << i1 << ", " << i2 << ", " << i5 << ", " << i6 << ", " << i7 << ", " << i8 << endl;

  double_complex result=0.0;
  result += E[0]*za[i5][i7]*zb[i6][i8];
  result += E[1]*za[i1][i5]*za[i1][i7]*zb[i1][i6]*zb[i1][i8];
  result += E[2]*za[i1][i5]*za[i2][i7]*zb[i1][i6]*zb[i2][i8];
  result += E[3]*za[i2][i5]*za[i1][i7]*zb[i2][i6]*zb[i1][i8];
  result += E[4]*za[i2][i5]*za[i2][i7]*zb[i2][i6]*zb[i2][i8];
  // multiply first bracket (...) of eqn. (3.12)
  result *= (zb[i2][i5]*za[i5][i1]+zb[i2][i6]*za[i6][i1]); // = |2 /p3 1>

  result += E[5]*za[i1][i5]*za[i1][i7]*zb[i1][i6]*zb[i2][i8];
  result += E[6]*za[i1][i5]*za[i1][i7]*zb[i2][i6]*zb[i1][i8];
  result += E[7]*za[i1][i5]*za[i2][i7]*zb[i2][i6]*zb[i2][i8];
  result += E[8]*za[i2][i5]*za[i1][i7]*zb[i2][i6]*zb[i2][i8];

  if (hel==0) {
    result *= (zb[i1][i5]*za[i5][i2]+zb[i1][i6]*za[i6][i2]) * za[i1][i2]/zb[i1][i2]; // = C_LL = |1 /p3 2> * <12>/[12]
  } else if (hel==1) {
    result *= (zb[i2][i5]*za[i5][i1]+zb[i2][i6]*za[i6][i1]); // = C_LR = |2 /p3 1>
  }

  logger << LOG_DEBUG_VERBOSE << "result=" << result << endl;
  return result;
}

void ggllll34_computeVVprimeHelAmplitudes(observable_set & oset, TypeV V1, TypeV V2, vector<fourvector> &p, const vector<vector<vector<double_complex> > > &E, vector<vector<vector<vector<vector<double_complex> > > > > &M_dressed) {
  // options for V1 and V2:
  // ----------------------
  // V_PHOT = 0: photon -> ll  decay
  // V_ZLL  = 1: Z   -> ll     decay
  // V_WMIN = 2: W^- -> l^- nu decay
  // V_WPLU = 3: W^+ -> l^+ nu decay
  // V_ZNN  = 4: Z   -> nu nu  decay

  double_complex cos_W = osi_msi.ccos_w;
  double_complex sin_W = osi_msi.csin_w;
  double M_Z = osi_msi.M_Z;
  double Gamma_Z = osi_msi.Gamma_Z;
  double M_W = osi_msi.M_W;
  double Gamma_W = osi_msi.Gamma_W;
  double Nf=osi_N_f;
  double_complex DV1,DV2; // propagators for the vector-boson decays
  double_complex LR56[2],LR78[2];
  double_complex Q_lep[4]; // charge left-handed [0] and right handed [1] particle of first decay 
                           // and the same for the second decay [2], [3]
  double_complex I_lep[4]; // isospin left-handed [0] and right handed [1] particle of first decay
                           // and the same for the second decay [2], [3]
  double ma2 = (p[3]+p[4]).m2();
  double mb2 = (p[5]+p[6]).m2();

  // set propagators (DV1), and couplings (LR56) for decay of first boson (V1)
  if (V1==0) { // photon -> ll
    DV1 = ma2; // photon propagator

    Q_lep[0]=-1;   // left-handed lepton charge
    Q_lep[1]=-1;   // right-handed lepton charge
    LR56[0] = Q_lep[0]; // left-handed  photon->ll coupling
    LR56[1] = Q_lep[1]; // right-handed photon->ll coupling
  } else if (V1==1 || V1 == 4) { // Z -> ll or Z -> nu nu
    DV1 = ma2-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z; // Z-boson propagator

    if (V1==1){ // Z -> ll
      Q_lep[0]=-1;   // left-handed lepton charge
      I_lep[0]=-0.5; // left-handed lepton isospin
      Q_lep[1]=-1;   // right-handed lepton charge
      I_lep[1]=0;    // right-handed lepton isospin
    } else if (V1==4){ // Z -> nu nu
      Q_lep[0]=0;    // left-handed neutrino charge
      I_lep[0]=0.5;  // left-handed neutrino isospin
      Q_lep[1]=0;    // no right-handed neutrino
      I_lep[1]=0;    // no right-handed neutrino
    }
    LR56[0] = (I_lep[0]-Q_lep[0]*sin_W*sin_W)/sin_W/cos_W; // left-handed  Z -> ll or Z -> nu nu coupling
    LR56[1] = (I_lep[1]-Q_lep[1]*sin_W*sin_W)/sin_W/cos_W; // right-handed Z -> ll or Z -> nu nu coupling
  } else if (V1 == 2 || V1 == 3) { // Z -> ll or Z -> nu nu
    DV1 = ma2-M_W*M_W+double_complex(0,1)*Gamma_W*M_W; // W-boson propagator

    LR56[0] = 1.0/sqrt(2)/sin_W; // left-handed  W -> l nu coupling
    LR56[1] = 0.0;               // right-handed W -> l nu coupling
  }
  // set propagators (DV2), and couplings (LR78) for decay of second boson (V2)
  if (V2==0) { // photon -> ll
    DV2 = mb2; // photon propagator

    Q_lep[2]=-1;   // left-handed lepton charge
    Q_lep[3]=-1;   // right-handed lepton charge
    LR78[0] = Q_lep[2]; // left-handed  photon->ll coupling
    LR78[1] = Q_lep[3]; // right-handed photon->ll coupling
  } else if (V2==1 || V2 == 4) { // W -> l nu decay
    DV2 = mb2-M_Z*M_Z+double_complex(0,1)*Gamma_Z*M_Z; // Z-boson propagator

    if (V2==1){ // Z -> ll
      Q_lep[2]=-1;   // left-handed lepton charge
      I_lep[2]=-0.5; // left-handed lepton isospin
      Q_lep[3]=-1;   // right-handed lepton charge
      I_lep[3]=0;    // right-handed lepton isospin
    } else if (V2==4){ // Z -> nu nu
      Q_lep[2]=0;    // left-handed neutrino charge
      I_lep[2]=0.5;  // left-handed neutrino isospin
      Q_lep[3]=0;    // no right-handed neutrino
      I_lep[3]=0;    // no right-handed neutrino
    }
    LR78[0] = (I_lep[2]-Q_lep[2]*sin_W*sin_W)/sin_W/cos_W; // left-handed  Z -> ll or Z -> nu nu coupling
    LR78[1] = (I_lep[3]-Q_lep[3]*sin_W*sin_W)/sin_W/cos_W; // right-handed Z -> ll or Z -> nu nu coupling
  } else if (V2 == 2 || V2 == 3) { // W -> l nu decay
    DV2 = mb2-M_W*M_W+double_complex(0,1)*Gamma_W*M_W; // W-boson propagator

    LR78[0] = 1.0/sqrt(2)/sin_W; // left-handed  W -> l nu coupling
    LR78[1] = 0.0;               // right-handed W -> l nu coupling
  }
  // decays: V -> particle anti-particle
  // left-handed V (0) : particle(L) anti-particle(R)
  // right-handed V (0) : particle(R) anti-particle(L)

  // set couplings (N_VV) of bosons to closed fermion loop
  double_complex N_VV=0.0;
  double_complex LZu=(0.5-2.0/3*sin_W*sin_W)/sin_W/cos_W;
  //  static double_complex LZu = osi_msi.cCminus_Zuu;
  double_complex LZd=(-0.5+1.0/3*sin_W*sin_W)/sin_W/cos_W;
  //  static double_complex LZd = osi_msi.cCminus_Zdd;
  double_complex RZu=-2.0/3*sin_W/cos_W;
  //  static double_complex RZu = osi_msi.cCminus_Zuu;
  double_complex RZd=1.0/3*sin_W/cos_W;
  //  static double_complex RZd = osi_msi.cCminus_Zdd;
  double_complex LW=1.0/sqrt(2)/sin_W;
  //  static double_complex LW = osi_msi.cCminus_W
  if (V1==0 && V2==0) { // gamma gamma
    N_VV = 2*(2.0/3)*(2.0/3)+(Nf-2)*(1.0/3)*(1.0/3);
  } else if (((V1==1 || V1==4) && V2==0) || (V1==0 && (V2==1 || V2==4))) { // Z gamma
    // note: sign mistake in 1503.04812, consistent with 1112.1531
    // MW: mistake in definition of L^gamma_f1f2 meant by this comment; 
    // there is indeed an additional minus sign in eqn. (3.15)?
    N_VV = 0.5*(2.0*(LZu+RZu)*2.0/3.0 + (Nf-2.0)*(LZd+RZd)*(-1.0/3.0));
  } else if ((V1==1 || V1==4) && (V2==1 || V2==4)) { // ZZ
    N_VV = 0.5*(2.0*(LZu*LZu+RZu*RZu)+(Nf-2.0)*(LZd*LZd+RZd*RZd));
  } else if ((V1==2 || V1==3) && (V2==2 || V2==3)) { // WW
    int Ng = Nf/2; // number of massless quark generations
    N_VV = 0.5*Ng*LW*LW;
  }

  // compute spinor products
  vector<vector<double_complex > > za, zb;
  vector<vector <double> > s_inv;
  calcSpinorProducts(p,za,zb,s_inv);

  // compute undressed amplitudes
  double_complex M_undressed[2][2][2][2][3];
  for (int loop=0; loop<2; loop++) {
    M_undressed[0][0][0][0][loop] = ggllll34_computeM(1,2,5,6,7,8,0,za,zb,E[loop][0]); // M_LLLL --> M_LL(LR)(LR)
    M_undressed[0][0][0][1][loop] = ggllll34_computeM(1,2,5,6,8,7,0,za,zb,E[loop][0]); // M_LLLR --> M_LL(LR)(RL)
    M_undressed[0][0][1][0][loop] = ggllll34_computeM(1,2,6,5,7,8,0,za,zb,E[loop][0]); // M_LLRL --> M_LL(RL)(LR)
    M_undressed[0][0][1][1][loop] = ggllll34_computeM(1,2,6,5,8,7,0,za,zb,E[loop][0]); // M_LLRR --> M_LL(RL)(RL)
    
    M_undressed[0][1][0][0][loop] = ggllll34_computeM(1,2,5,6,7,8,1,za,zb,E[loop][1]); // M_LRLL
    M_undressed[0][1][0][1][loop] = ggllll34_computeM(1,2,5,6,8,7,1,za,zb,E[loop][1]); // M_LRLR
    M_undressed[0][1][1][0][loop] = ggllll34_computeM(1,2,6,5,7,8,1,za,zb,E[loop][1]); // M_LRRL
    M_undressed[0][1][1][1][loop] = ggllll34_computeM(1,2,6,5,8,7,1,za,zb,E[loop][1]); // M_LRRR

    // get other amplitudes via charge conjucate [...]^C by replacing spinor products:
    // <ij> <-> [ij], i.e., replacing za <-> zb
    M_undressed[1][0][0][0][loop] = ggllll34_computeM(1,2,6,5,8,7,0,zb,za,E[loop][0]); // M_RLLL
    M_undressed[1][0][0][1][loop] = ggllll34_computeM(1,2,6,5,7,8,0,zb,za,E[loop][0]); // M_RLLR
    M_undressed[1][0][1][0][loop] = ggllll34_computeM(1,2,5,6,8,7,0,zb,za,E[loop][0]); // M_RLRL
    M_undressed[1][0][1][1][loop] = ggllll34_computeM(1,2,5,6,7,8,0,zb,za,E[loop][0]); // M_RLRR
     
    M_undressed[1][1][0][0][loop] = ggllll34_computeM(1,2,6,5,8,7,1,zb,za,E[loop][1]); // M_RRLL
    M_undressed[1][1][0][1][loop] = ggllll34_computeM(1,2,6,5,7,8,1,zb,za,E[loop][1]); // M_RRLR
    M_undressed[1][1][1][0][loop] = ggllll34_computeM(1,2,5,6,8,7,1,zb,za,E[loop][1]); // M_RRRL
    M_undressed[1][1][1][1][loop] = ggllll34_computeM(1,2,5,6,7,8,1,zb,za,E[loop][1]); // M_RRRR
  }

  // compute (and return) dressed amplitudes
  for (int loop=0; loop<2; loop++) {
    for (int h1=0; h1<2; h1++) {
      for (int h2=0; h2<2; h2++) {
        for (int h3=0; h3<2; h3++) {
	  for (int h4=0; h4<2; h4++) {
	    M_dressed[h1][h2][h3][h4][loop]  = N_VV*M_undressed[h1][h2][h3][h4][loop];
	    M_dressed[h1][h2][h3][h4][loop] *= (LR56[h3]*LR78[h4]/DV1/DV2);         
	    M_dressed[h1][h2][h3][h4][loop] *= double_complex(0,1);
	  }
        }
      }
    }
  }
}



void add_hel_amps(vector<vector<vector<vector<vector<double_complex> > > > > &M1, vector<vector<vector<vector<vector<double_complex> > > > > &M2) {
  for (int h1=0; h1<2; h1++) {
    for (int h2=0; h2<2; h2++) {
      for (int h3=0; h3<2; h3++) {
	for (int h4=0; h4<2; h4++) {
	  for (int loop=0; loop<2; loop++) {
	    M1[h1][h2][h3][h4][loop] += M2[h1][h2][h3][h4][loop];
	  }
	}
      }
    }
  }
}

void ggllll34_calculate_amplitude_doublevirtual(observable_set & oset) {
  static Logger logger("ggllll34_calculate_amplitude_doublevirtual");
  logger << LOG_DEBUG_VERBOSE << "called" << endl;

  vector<fourvector> p(osi_p_parton[0].size()); 
  vector<vector<vector<vector<vector<double_complex> > > > > M_dressed,M_dressed2;
  vector<vector<vector<vector<vector<double_complex> > > > > M_dressed_tmp;
  M_dressed.resize(2);
  M_dressed2.resize(2);
  M_dressed_tmp.resize(2);
  for (int h1=0; h1<2; h1++) {
    M_dressed[h1].resize(2);
    M_dressed2[h1].resize(2);
    M_dressed_tmp[h1].resize(2);
    for (int h2=0; h2<2; h2++) {
      M_dressed[h1][h2].resize(2);
      M_dressed2[h1][h2].resize(2);
      M_dressed_tmp[h1][h2].resize(2);
      for (int h3=0; h3<2; h3++) {
        M_dressed[h1][h2][h3].resize(2);
        M_dressed2[h1][h2][h3].resize(2);
        M_dressed_tmp[h1][h2][h3].resize(2);
	for (int h4=0; h4<2; h4++) {
	  M_dressed[h1][h2][h3][h4].resize(2,0.0);
	  M_dressed2[h1][h2][h3][h4].resize(2,0.0);
	  M_dressed_tmp[h1][h2][h3][h4].resize(2);
	}
      }
    }
  }

  double sym_factor=1;

  if (osi_name_process == "gg_emmupvmve~") {
    // WW
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][6];
    p[5]=osi_p_parton[0][5];
    p[6]=osi_p_parton[0][4];
  } else if (osi_name_process == "gg_emmumepmup") {
    // ZZ -> 2l 2l' (DF)
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][5];
    p[5]=osi_p_parton[0][4];
    p[6]=osi_p_parton[0][6];
  } else if (osi_name_process == "gg_emepveve~" || osi_name_process == "gg_emepvmvm~") {
    // ZZ -> 2l 2nu (SF and DF); WW contribution to SF below
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][4];
    p[5]=osi_p_parton[0][5];
    p[6]=osi_p_parton[0][6];
  } else if (osi_name_process == "gg_ememepep") {
    // ZZ -> 4l (SF)
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][5];
    p[5]=osi_p_parton[0][4];
    p[6]=osi_p_parton[0][6];

    sym_factor=1.0/4;
  }
  
  vector<vector<vector<double_complex> > > E(2,vector<vector<double_complex> >(2,vector<double_complex>(9)));
  double s = (p[1]+p[2]).m2();
  double t = (p[1]-(p[3]+p[4])).m2();
  double ma2 = (p[3]+p[4]).m2();
  double mb2 = (p[5]+p[6]).m2();
  
  logger << LOG_DEBUG << "sqrt(s)=" << sqrt(s) << ", sqrt(-t)=" << sqrt(-t) << ", sqrt(-u)=" << sqrt(-ma2-mb2+s+t) << ", ma=" << sqrt(ma2) << ", mb=" << sqrt(mb2) << endl;
  
  int static init=1;
  // initialize the form factor library
  if (init==1) {
    ggVVprime::getInstance().initAmplitude(osi_N_f);
    init=0;
  }

  //  bool debug = Log::getLogThreshold()<=LOG_DEBUG; // no debug mode implemented for ggVVamp
  
  // compute the form factors
  ggVVprime::getInstance().computeAmplitude(s,t,ma2,mb2);
  
  for (int loop=0; loop<2; loop++) {
    for (int hel=0; hel<2; hel++) {
      for (int i=1; i<=9; i++) {
        double Er,Ei;
        ggVVprime::getInstance().getAmplitude(loop,i,hel,Er,Ei);
        E[loop][hel][i-1] = double_complex(Er,Ei);
      }
    }
  }

  // compute dressed amplitudes for all contributions to respective process
  if (osi_name_process == "gg_emmupvmve~") { 
    // WW process
    // WW contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_WMIN, V_WPLU, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

  } else if (osi_name_process == "gg_emmumepmup" || osi_name_process == "gg_ememepep") {
    // ZZ -> 2l 2l' (DF) and ZZ -> 4l (SF) process; additional ZZ contribution to SF below
    // ZZ contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_ZLL, V_ZLL, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    // gamgam contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_PHOT, V_PHOT, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    // gamZ contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_PHOT, V_ZLL, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

    // Zgam contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_ZLL, V_PHOT, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

  } else if (osi_name_process == "gg_emepveve~" || osi_name_process == "gg_emepvmvm~") {
    // ZZ -> 2l 2nu (SF and DF); WW contribution to SF below
    // ZZ contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_ZLL, V_ZNN, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);
    
    // gamZ contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_PHOT, V_ZNN, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed,M_dressed_tmp);

  } else {
    assert(false);
  }
  
  // SF: take into account additional crossings
  if (osi_name_process == "gg_ememepep" || osi_name_process == "dd~_ememepep") {
    // SF: ZZ -> 4l
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][6];
    p[5]=osi_p_parton[0][4];
    p[6]=osi_p_parton[0][5];
    
    s = (p[1]+p[2]).m2();
    t = (p[1]-(p[3]+p[4])).m2();
    ma2 = (p[3]+p[4]).m2();
    mb2 = (p[5]+p[6]).m2();
    
    ggVVprime::getInstance().computeAmplitude(s,t,ma2,mb2);
    for (int loop=0; loop<2; loop++) {
      for (int hel=0; hel<2; hel++) {
	for (int i=1; i<=9; i++) {
	  double Er,Ei;
	  ggVVprime::getInstance().getAmplitude(loop,i,hel,Er,Ei);
	  E[loop][hel][i-1] = double_complex(Er,Ei);
	}
      }
    }

    // ZZ contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_ZLL, V_ZLL, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
    
    // gamgam contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_PHOT, V_PHOT, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

    // gamZ contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_PHOT, V_ZLL, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);    

    // Zgam contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_ZLL, V_PHOT, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);

  } else if (osi_name_process == "gg_emepveve~") {
    // SF: ZZ -> 2l 2nu
    p[0]=osi_p_parton[0][0];
    p[1]=osi_p_parton[0][1];
    p[2]=osi_p_parton[0][2];
    p[3]=osi_p_parton[0][3];
    p[4]=osi_p_parton[0][6];
    p[5]=osi_p_parton[0][5];
    p[6]=osi_p_parton[0][4];
    
    s = (p[1]+p[2]).m2();
    t = (p[1]-(p[3]+p[4])).m2();
    ma2 = (p[3]+p[4]).m2();
    mb2 = (p[5]+p[6]).m2();
    
    ggVVprime::getInstance().computeAmplitude(s,t,ma2,mb2);
    for (int loop=0; loop<2; loop++) {
      for (int hel=0; hel<2; hel++) {
	for (int i=1; i<=9; i++) {
	  double Er,Ei;
	  ggVVprime::getInstance().getAmplitude(loop,i,hel,Er,Ei);
	  E[loop][hel][i-1] = double_complex(Er,Ei);
	}
      }
    }

    // WW contribution
    ggllll34_computeVVprimeHelAmplitudes(oset, V_WMIN, V_WPLU, p, E, M_dressed_tmp);
    add_hel_amps(M_dressed2,M_dressed_tmp);
  }

  
  // square and add up helicity amplitudes
  osi_QT_A0=0;
  osi_QT_A1=0;
  osi_QT_A2=0;
  for (int h1=0; h1<2; h1++) {
    for (int h2=0; h2<2; h2++) {
      for (int h3=0; h3<2; h3++) {
	for (int h4=0; h4<2; h4++) {
	  osi_QT_A0 += norm(M_dressed[h1][h2][h3][h4][0])+norm(M_dressed2[h1][h2][h3][h4][0]);
	  osi_QT_A1 += 2*real(M_dressed[h1][h2][h3][h4][0]*conj(M_dressed[h1][h2][h3][h4][1]))+2*real(M_dressed2[h1][h2][h3][h4][0]*conj(M_dressed2[h1][h2][h3][h4][1]));
	  if (h3==h4) {
	    osi_QT_A0 -= 2.0*real(conj(M_dressed[h1][h2][h3][h4][0])*M_dressed2[h1][h2][h3][h4][0]);

	    osi_QT_A1 -= 2.0*real(conj(M_dressed[h1][h2][h3][h4][0])*M_dressed2[h1][h2][h3][h4][1]);
	    osi_QT_A1 -= 2.0*real(conj(M_dressed2[h1][h2][h3][h4][0])*M_dressed[h1][h2][h3][h4][1]);
	  }
        }
      }
    }
  }
  
  double alpha_e = osi_msi.alpha_e;

  osi_QT_A0 *= pow(4*M_PI*alpha_e,4);
  osi_QT_A0 /= (4*64); // averaging factor
  osi_QT_A0 *= 2; // color factor 1/2 d^AB * 1/2 d^AB
  osi_QT_A0 *= sym_factor;

  osi_QT_A1 *= pow(4*M_PI*alpha_e,4);
  osi_QT_A1 /= (4*64); // averaging factor
  osi_QT_A1 *= 2; // color factor 1/2 d^AB * 1/2 d^AB
  osi_QT_A1 *= sym_factor;

  // multiply with alpha_s^2 1-loop LO and alpha_s^3 2-loop NLO
  osi_QT_A0 *= pow(osi_alpha_S/2./M_PI,2);
  osi_QT_A1 *= pow(osi_alpha_S/2./M_PI,3);

  //  cout << pow(osi_alpha_S/2./M_PI,2) << endl;
  //  cout << osi_alpha_S << endl;

  // no idea why there is a factor of 4 missing:
  osi_QT_A0 *= 4.;
  osi_QT_A1 *= 4.;


  logger << LOG_DEBUG_VERBOSE << "finished" << endl;
}
