void ggllll34_calculate_H1(observable_set & oset);
void ggllll34_calculate_amplitude_doublevirtual(observable_set & oset);
double fint(int KORD, double X, double XX[462], double YY[462]);
double fint1(int KORD, double X, double XP[462], double YP[462], int NP);
double_complex CKOF(double RHO, double XX[462], double YHR[462], double YHI[462], double YAR[462], double YAI[462]);
double branch(observable_set & oset, double brzee, double brwen);

class ggVVprimePrivate;

class ggVVprime {
  public:
    static ggVVprime &getInstance() {
      static ggVVprime instance;
      return instance;
    }
    ~ggVVprime();
    void initAmplitude(int Nf);
    void computeAmplitude(double s, double t, double ma2, double mb2);
    void getAmplitude(int l, int i, int j, double &Er, double &Ei);
  private:
    ggVVprime();
    ggVVprime(ggVVprime const&);
    void operator=(ggVVprime const&);
    
    ggVVprimePrivate* mData;
};
