# import some basic crucial information from main (run_process)

from handle_output import *
out = print_output() # class for handling the on-screen output

from handle_input_matrix import *
inp = inputs()

from handle_input_files import *
inpf = input_files()

from handle_process import *
process_name = select_process() # class for process selection
#fold = process_folder() # class to create folder in run directory and handles all paths
