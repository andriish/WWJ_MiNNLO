#{{{ imports
import os
import copy
import shutil
from collections import defaultdict
from os.path import join as pjoin
from initialize_classes_matrix import out
#}}}
#{{{ def: line_prepender(filename, line)
def line_prepender(filename, line):
    # adds line at beginning of file
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)
#}}}
#{{{ def: multidim_dict(n)
def multidim_dict(n):
# extremely flexible multidimensional dictionary
  """ Creates an n-dimension dictionary where the n-th dimension is of type 'type'
  """  
  return defaultdict(lambda:multidim_dict(n-1))

#}}}
#{{{ def: include(filename)
def include(filename):
    if os.path.exists(filename): 
        #execfile(filename) # not phython 3 compatible
        exec(compile(source=open(filename).read(), filename=filename, mode='exec'))
    else:
        out.print_error_no_stop("Including file which does not exist: %s" % filename)
#}}}
#{{{ class: input_files()
class input_files():
#{{{ def: __init__(self,process_dir_in,no_run_folder_in)
    def __init__(self):
        self.process_inputs = ""
        self.process = ""
        self.LOgg = False
        self.NLOgg = False
        self.NLOEW = False
        self.FFS_list = []
        self.scale_dict = {}
        self.proc_dict = multidim_dict(2)
        self.cuts_dict = multidim_dict(2)
        # processes with Z bosons for rather unimportant switch_off_shell; hard-coded for now
        self.withZ = ["ppeeexex04","ppeexnenex04","ppeexnmnmx04","ppemexmx04"]
        # hard-code processes (except photon processes) which show switch_qT_accuracy 
        self.qtacc = ["ppeex02"]
#{{{    default settings for cuts on particle groups
        self.list_dict = {}
        self.list_dict["jet"] = ["25.","4.5","1.e99","0","99"]
        self.list_dict["default"] = ["0.","1.e99","1.e99","0","99"]
        self.list_dict["missing"] = ["0."]
#}}}
#{{{    mapping from particle shortcut to printout name
        self.map_particle_string = {}
        self.map_particle_string["jet"]     = "jet"
        self.map_particle_string["ljet"]    = "light jet"
        self.map_particle_string["bjet"]    = "bottom jet"
        self.map_particle_string["photon"]  = "photon"
        self.map_particle_string["lep"]     = "lepton"
        self.map_particle_string["e"]       = "electron"
        self.map_particle_string["em"]      = "electron"
        self.map_particle_string["ep"]      = "anti-electron"
        self.map_particle_string["mu"]      = "muon"
        self.map_particle_string["mum"]     = "muon"
        self.map_particle_string["mup"]     = "anit-muon"
        self.map_particle_string["tau"]     = "tau"
        self.map_particle_string["taum"]    = "tau"
        self.map_particle_string["taup"]    = "anti-tau"
        self.map_particle_string["lm"]      = "negatively-charged lepton"
        self.map_particle_string["lp"]      = "positively-charged lepton"
        self.map_particle_string["missing"] = "neutrino"
        self.map_particle_string["tjet"]    = "top jet"
        self.map_particle_string["top"]     = "top quark"
        self.map_particle_string["atop"]    = "anti-top quarl"
        self.map_particle_string["w"]       = "W-boson"
        self.map_particle_string["wm"]      = "W^- boson"
        self.map_particle_string["wp"]      = "W^+ boson"
        self.map_particle_string["z"]       = "Z boson"
        self.map_particle_string["h"]       = "Higgs boson"
        self.map_particle_string["nua"]     = "neutrino"
        self.map_particle_string["nu"]      = "neutrino"
        self.map_particle_string["nea"]     = "electron-neutrino"
        self.map_particle_string["ne"]      = "electron-neutrino"
        self.map_particle_string["nma"]     = "muon-neutrino"
        self.map_particle_string["nm"]      = "muon-neutrino"
        self.map_particle_string["nma"]     = "tau-neutrino"
        self.map_particle_string["nm"]      = "tau-neutrino"
        self.map_particle_string["nmx"]     = "anit-tau-neutrino"
#}}}
#}}}
#{{{ def: create_input_files(self,process_in,NLOgg_in,NLOEW_in)
    def create_input_files(self,input_path,process_in,LOgg_in,NLOgg_in,NLOEW_in,file_path_process_inputs_in = ""):
        # set routine inputs
        self.file_path_process_inputs = file_path_process_inputs_in
        self.process = process_in
        self.LOgg    = LOgg_in
        self.NLOgg   = NLOgg_in
        self.NLOEW   = NLOEW_in
        # read process inputs from process_inputs.py file
        if not os.path.exists(self.file_path_process_inputs):
            out.print_error("Could not find file with process inputs under path: %s. Exiting..." % self.file_path_process_inputs)
        try:
            include(self.file_path_process_inputs)
            self.FFS_list = FFS_list
            self.scale_dict = scale_dict
            self.proc_dict = proc_dict
            self.cuts_dict = cuts_dict
        except:
            pass
#{{{    example hard-coded input
        # self.user_cuts_list = []
        # self.user_cuts_list.append([["M_leplep","1","switch to turn on (1) and off (0) cuts on lepton-lepton invariant mass"], \
        #                             ["min_M_leplep","10.","requirement on lepton-lepton invariant mass (lower cut)"], \
        #                             ["max_M_leplep","1.e99","requirement on lepton-lepton invariant mass (upper cut)"]])
        # self.user_cuts_list.append([""])
        # self.user_cuts_list.append([["R_leplep","1","switch to turn on (1) and off (0) cuts on lepton-lepton separation"], \
        #                             ["min_R_leplep","0.1","requirement on lepton-lepton separation in y-phi-plane (lower cut)"]])
        # self.cuts_dict["ppemxnmnex04"]["user"] = self.user_cuts_list
#}}}
        # read process inputs from old parameter.dat files
        if not self.process in self.proc_dict or not self.process in self.cuts_dict:
            self.print_process_inputs_header(self.file_path_process_inputs)
            self.read_file_and_create_input(file_path.replace("/input_files/","/input_files_old/"),self.file_path_process_inputs)
            include(self.file_path_process_inputs)
            self.FFS_list = FFS_list
            self.scale_dict = scale_dict
            self.proc_dict = proc_dict
            self.cuts_dict = cuts_dict
        # initialize relevant parameter
        self.FFS = True if self.process in self.FFS_list else False
        self.LHAPDF_LO    = "NNPDF%s_%slo_as_0118%s%s" %  ("31" if self.NLOEW else "30","n" if self.NLOEW else "","_luxqed" if self.NLOEW else "","_nf_4" if self.FFS else "")
        self.LHAPDF_NLO   = "NNPDF%s_nlo_as_0118%s%s"  %  ("31" if self.NLOEW else "30","_luxqed" if self.NLOEW else "","_nf_4" if self.FFS else "")
        self.LHAPDF_NNLO  = "NNPDF%s_nnlo_as_0118%s%s" %  ("31" if self.NLOEW else "30","_luxqed" if self.NLOEW else "","_nf_4" if self.FFS else "")
        self.LHAPDF_NNNLO = self.LHAPDF_NNLO

        input_files_dir = pjoin(input_path,self.process)
        out.print_make("Creating input files for process %s inside folder %s..." % (self.process, input_files_dir))
        # create folder and default subfolder if not exists
        try:
            os.makedirs(input_files_dir)
        except:
            pass
        try:
            os.makedirs(pjoin(input_files_dir,"default.input.MATRIX"))
        except:
            pass
        # create relevant files
        self.create_parameter_dat(pjoin(input_files_dir,"default.input.MATRIX","parameter.dat"))
        self.create_model_dat(pjoin(input_files_dir,"default.input.MATRIX","model.dat"))
        # distribution.dat file only created if not exist, otherwise use the hand-coded version of the v1 release
        self.create_distribution_dat(pjoin(input_files_dir,"default.input.MATRIX","distribution.dat"))
        self.create_file_parameter_dat(pjoin(input_files_dir,"file_parameter.dat"))
        self.create_file_model_dat(pjoin(input_files_dir,"file_model.dat"))
        self.create_file_distribution_dat(pjoin(input_files_dir,"file_distribution.dat"))
#}}}
#{{{ def: init_parameter_dat(self)
    def init_parameter_dat(self):
#{{{    paramter.dat header
        self.init_parameter_string  = "##########################\n"
        self.init_parameter_string += "# MATRIX input parameter #\n"
        self.init_parameter_string += "##########################\n"
#}}}
        self.param_blocks = multidim_dict(2)
#{{{    general settings block
        self.param_blocks["general"]["header"] = "General run settings"
        list_general = []
        list_general.append(["process_class",self.proc_dict[self.process]["process_class"],"process id"])
        list_general.append(["E","6500.","energy per beam"])
        list_general.append(["coll_choice","1","(1) PP collider; (2) PPbar collider"])
        if self.FFS:   list_general.append(["flavor_scheme","1","(1) four-flavor scheme; (2) five-flavor scheme; note: use consistent PDFs"])
        if self.NLOEW: list_general.append(["photon_induced","1","switch to turn on (1) and off (0) photon-induced contributions"])
        if self.process in self.withZ: list_general.append(["switch_off_shell","0","switch for effective integration for off-shell Z bosons (eg, Higgs analysis)"])
        if self.NLOEW: list_general.append(["enhance_tails","0","switch to improve statistics in tail of distributions (a factor of two slower)"])
        self.param_blocks["general"]["input"] = list_general
#}}}
#{{{    scale settings block
        self.param_blocks["scale"]["header"] = "Scale settings"
        list_scale = []
        list_scale.append(["scale_ren","91.1876","renormalization (muR) scale"])
        list_scale.append(["scale_fact","91.1876","factorization (muF) scale"])
        list_scale.append(["dynamic_scale","1","dynamic ren./fac. scale"])
        for scale in self.scale_dict.get(self.process,[]):
            list_scale.append(["","",scale])
        # list_scale.append(["","",              "0: fixed scale above"])
        # list_scale.append(["","",              "1: invariant mass (Q) of system (of the colourless final states)"])
        # list_scale.append(["","",              "2: transverse mass (mT^2=Q^2+pT^2) of system (of the colourless final states)"])
        # list_scale.append(["","",              "3: sum of W-boson transverse masses computed with W pole mass:"])
        # list_scale.append(["","",              "   sqrt(M_W^2+pT_enu^2)+sqrt(M_W^2+pT_munu^2)"])
        # list_scale.append(["","",              "4: sum of W-boson transverse masses:"])
        # list_scale.append(["","",              "   sqrt(m_enu^2+pT_enu^2)+sqrt(m_munu^2+pT_munu^2)"])
        list_scale.append(["factor_central_scale","1","relative factor for central scale (important for dynamic scales)"])
        list_scale.append(["scale_variation","1","switch for muR/muF uncertainties (0) off; (1) 7-point (default); (2) 9-point variation"])
        list_scale.append(["variation_factor","2","symmetric variation factor; usually a factor of 2 up and down (default)"])
        self.param_blocks["scale"]["input"] = list_scale
#}}}
#{{{    order dependent settings block
        self.param_blocks["order"]["header"] = "Order-dependent run settings"
        list_order = []
        list_order.append(["# LO-run"])
        list_order.append(["run_LO","1","switch for LO cross section (1) on; (0) off"])
        list_order.append(["LHAPDF_LO",self.LHAPDF_LO,"LO LHAPDF set"])
        list_order.append(["PDFsubset_LO","0","member of LO PDF set"])
        list_order.append(["precision_LO","1.e-2","precision of LO cross section"])
        list_order.append([""])
        list_order.append(["# NLO-run"])
        list_order.append(["run_NLO_QCD","0","switch for NLO QCD cross section (1) on; (0) off"])
        if self.NLOEW: list_order.append(["run_NLO_EW","0","switch for NLO EW cross section (1) on; (0) off "])
        list_order.append(["LHAPDF_NLO",self.LHAPDF_NLO,"NLO LHAPDF set"])
        list_order.append(["PDFsubset_NLO","0","member of NLO PDF set"])
        list_order.append(["precision_NLO_QCD","1.e-2","precision of NLO QCD cross section"])
        if self.NLOEW: list_order.append(["precision_NLO_EW","1.e-2","precision of NLO EW correction"])
        list_order.append(["NLO_subtraction_method","1","switch to use (2) qT subtraction (1) Catani-Seymour at NLO"])
        list_order.append([""])
        list_order.append(["# NNLO-run"])
        list_order.append(["run_NNLO_QCD","0","switch for NNLO QCD cross section (1) on; (0) off "])
        if self.NLOEW: list_order.append(["add_NLO_EW","0","switch to add NLO EW cross section to NNLO run (1) on; (0) off"])
        if self.NLOEW: list_order.append(["","",           "note: can be added only if also running NNLO"])
        list_order.append(["LHAPDF_NNLO",self.LHAPDF_NNLO,"NNLO LHAPDF set"])
        list_order.append(["PDFsubset_NNLO","0","member of NNLO PDF set"])
        list_order.append(["precision_NNLO_QCD","1.e-2","precision of NNLO QCD cross section"])
        if self.NLOEW: list_order.append(["precision_added_EW","1.e-2","precision of NLO EW correction in NNLO run"])
        if self.LOgg and not self.NLOgg: list_order.append(["loop_induced","1","switch to turn on (1) and off (0) loop-induced gg channel"])
        if self.NLOgg: list_order.append(["loop_induced","2","switch for loop-induced gg (with NNLO settings): (0) off;"])
        if self.NLOgg: list_order.append(["","",           " (1) LO [NNLO contribution];   (2) NLO [N3LO contribution]"])
        if self.NLOgg: list_order.append(["","",           "(-1) only loop-induced gg LO; (-2) only loop-induced gg NLO  (run_NNLO_QCD=1 required!)"])
        if "a" in self.process or self.process in self.qtacc:
#            list_order.append([""]) # comment later !
            list_order.append(["switch_qT_accuracy","0","switch to improve qT-subtraction accuracy (slower numerical convergence)"])
        self.param_blocks["order"]["input"] = list_order
#}}}
#{{{    fiducial cuts (generic blocks and user cuts)
        # generic blocks
        self.param_blocks["cuts"]["header"] = "Settings for fiducial cuts"
        self.param_blocks["cuts"]["format"] = 1 # with single equal sign
        list_cuts = []
        list_cuts.append(["# Jet algorithm"])
        list_cuts.append(["jet_algorithm","3","(1) Cambridge-Aachen (2) kT (3) anti-kT"])
        list_cuts.append(["jet_R_definition","0","(0) pseudo-rapidity (1) rapidity"])
        list_cuts.append(["jet_R","0.4","DeltaR"])
        list_cuts.append([""])
        if "a" in self.process: # photon processes need photon isolation
            list_cuts.append(["# Frixione isolation"])
            list_cuts.append(["frixione_isolation","1","switch for Frixione isolation (0) off;"])
            list_cuts.append(["","","(1) with frixione_epsilon, used by ATLAS;"])
            list_cuts.append(["","","(2) with frixione_fixed_ET_max, used by CMS"])
            list_cuts.append(["frixione_n","1","exponent of delta-term"])
            list_cuts.append(["frixione_delta_0","0.4","maximal cone size"])
            list_cuts.append(["frixione_epsilon","0.5","photon momentum fraction"])
            list_cuts.append(["#frixione_fixed_ET_max","5","fixed maximal pT inside cone"])
            list_cuts.append([""])
        if self.NLOEW:
            list_cuts.append(["# Photon recombination (lepton dressing)"])
            list_cuts.append(["photon_recombination","1","switch for photon recombination (1) on; (0) off; must be on for EW runs"])
            list_cuts.append(["photon_R_definition","0","(0) pseudorap; (1) rapidity"])
            list_cuts.append(["photon_R","0.1","DeltaR: photon combined with charged particle when inside this radius"])
            list_cuts.append(["photon_E_threshold_ratio","1.","identify jet as photon: jet is photon, if photon-energy > ratio*jet-energy"])
            list_cuts.append([""])
        self.param_blocks["cuts"]["input"] = list_cuts
        def default(particle): return [particle]+self.list_dict.get(particle,self.list_dict["default"])
        list_blocks = []
        for block in self.cuts_dict[self.process]["block"]:
            if isinstance(block, basestring):
                list_blocks.append(default(block))
            elif isinstance(block, list):
                list_blocks.append(block)
            else:
                out.print_error("Something wrong with particle list for generic cuts in processes %s." % self.process)
        self.param_blocks["cuts"]["block"] = list_blocks

        # user defined cuts
        self.user_cut_list = []
#        self.user_cut_list.append([""])
        self.user_cut_list.append(["####################"])
        self.user_cut_list.append(["# User-defined cuts"])
        self.user_cut_list.append(["# (only used if defined in 'MATRIX/prc/$process/user/specify.cuts.cxx')"])
        # self.user_cut_list.append(["# Mandatory cuts for this process"])
        # self.user_cut_list.append(["# -- none"])
        self.user_cut_list.append(["#"])
        if not self.cuts_dict[self.process]["user"]:
            self.user_cut_list.append(["# no user-defined cuts implemented at the moment for this process"])
#        self.user_cut_list.append([""])
        self.param_blocks["cuts"]["user"] = self.user_cut_list+self.cuts_dict[self.process]["user"]
#}}}
#{{{    behavior block
        self.param_blocks["behav"]["header"] = "MATRIX behavior"
        self.param_blocks["behav"]["format"] = 1 # with single space around equal signs
        list_behav = []
        self.param_blocks["behav"]["input"] = list_behav
        list_behav.append(["max_time_per_job","12","very rough time(hours) one main run job shall take (default: 24h)"])
        list_behav.append(["","",                  "unreliable when < 1h, use as tuning parameter for degree of parallelization"])
        list_behav.append(["","",                  "note: becomes ineffective when job number > max_nr_parallel_jobs"])
        list_behav.append(["","",                  "      which is set in MATRIX_configuration file"])
        list_behav.append(["switch_distribution","1","switch to turn on (1) and off (0) distributions"])
        list_behav.append(["save_previous_result","1","switch to save previous result of this run (in result/\"run\"/saved_result_$i)"])
        list_behav.append(["save_previous_log","0","switch to save previous log of this run (in log/\"run\"/saved_result_$i)"])
        list_behav.append(["#include_pre_in_results","0","switch to (0) only include main run in results; (1) also all extrapolation (pre) runs;"])
        list_behav.append(["","",                        "crucial to set to 0 if re-running main with different inputs (apart from precision)"])
        list_behav.append(["","",                        "note: if missing (default) pre runs used if important for precision"])
        list_behav.append(["","",                        "(separately for each contribution)"])
        list_behav.append(["reduce_workload","0","switch to keep full job output (0), reduce (1) or minimize (2) workload on slow clusters"])
        list_behav.append(["random_seed","0","specify integer value (grid-/pre-run reproducible)"])
#}}}
#}}}
#{{{ def: parameter_header(self,string_in)
    def parameter_header(self,string_in):
        length = len(string_in)+2
        string_out  = "#" + "-"*length + "\\\n"
        string_out += "# "+string_in+" |\n"
        string_out += "#" + "-"*length + "/\n"
        return string_out
#}}}
#{{{ def: block(self,block)
    def block(self,block):
        string_out = ""
        if "header" in block: string_out = self.parameter_header(block["header"])
        maxlen_before = 18
        maxlen_total  = 29
        len_before = 0
        len_total = 0
        for line in block["input"]:
            if len(line[0]) < maxlen_before: len_before = max(len_before,len(line[0]))
#            if len(line[0])+len(line[1])+5 < maxlen_total: len_total  = max(len_total,len(line[0])+len(line[1])+5)
            try:
                len_total  = max(len_total,len(line[0])+len(line[1])+5)
            except:
                pass
        len_total = maxlen_total #min(len_total,maxlen_total)
        for line in block["input"]:
            if len(line) == 1:
                string_out += line[0]+"\n"
                continue
            if not line[0]:
                string_out += " "*len_total+"  #  "+line[2]+"\n"
                continue
            extra = 0
            if line[0].startswith("#"):
                extra= 1
            # for processes dependent settings take them from proc_dict
            line[1] = self.proc_dict.get(self.process).get(line[0],line[1])
            if block["format"] == 1:
                string_before = line[0]+" = "
                string_after  = line[1]+" "*(len_total+extra-len(string_before)-len(line[1]))
            elif len(line[0]) == len_before+1:
                string_before = line[0]+" "*(len_before-len(line[0]))+" =  "
                string_after  = line[1]+" "*(len_total+extra-len(string_before)-len(line[1]))
            elif len(line[0]) > len_before:
                string_before = line[0]+" "*(len_before-len(line[0]))+" = "
                string_after  = line[1]+" "*(len_total+extra-len(string_before)-len(line[1]))
            else:
                string_before = line[0]+" "*(len_before-len(line[0]))+"  =  "
                string_after  = line[1]+" "*(len_total+extra-len(string_before)-len(line[1]))
            string_out += string_before+string_after+"  #  "+line[2]+"\n"
        for block_list in block.get("block",[]): # loop through cut blocks, where available
            string_out += self.print_cut_block(block_list,len_total)
            string_out += "\n"

        # for fiducial cuts
#        maxlen_total  = 38
        len_total = 0
        for line in block.get("user",[]):
          for index, cut in enumerate(line):
            try:
                if index == 0: # first always a user_switch
                    len_total  = max(len_total,len(cut[0])+len(cut[1])+3)
                else: # the others user_cut
                    len_total  = max(len_total,len(cut[0])+len(cut[1])+3)
            except:
                pass
        for line in block["user"]:
            if len(line) == 1 and isinstance(line[0], basestring):
                string_out += line[0]+"\n"
                continue
            if not line[0]:
                string_out += " "*len_total+"  #  "+line[2]+"\n"
                continue
            extra = 0
            for index, cut in enumerate(line):
                if len(cut) == 1 and isinstance(cut[0], basestring):
                    string_out += cut[0]+"\n"
                    continue
                if not cut[0]:
                    string_out += " "*len_total+"  #  "+cut[2]+"\n"
                    continue
                if cut[0].startswith("#"):
                    extra= 1
                string_before = cut[0]+" = "
                string_after  = cut[1]+" "*(len_total+extra-len(string_before)-len(cut[1]))
                string_out += string_before+string_after+"  #  "+cut[2]+"\n"
            string_out += "\n"
        return string_out
#}}}
#{{{ def: print_cut_block(self,cut_list,len_total,format_in = 0)
    def print_cut_block(self,cut_list,len_total,format_in = "",len_before = "",len_after = ""):
        group = cut_list[0]
        string = self.map_particle_string[group]
        pT = cut_list[1]
        try:
            eta  = cut_list[2]
            y    = cut_list[3]
            nmin = cut_list[4]
            nmax = cut_list[5]
        except:
            pass
        try:
            def_pT  = "define_pT %s = %s" % (group,pT)
            def_eta = "define_eta %s = %s" % (group,eta)
            def_y   = "define_y %s = %s" % (group,y)
            def_min = "n_observed_min %s = %s" % (group,nmin)
            def_max = "n_observed_max %s = %s" % (group,nmax)
        except:
            pass
        if format_in ==1: # format for file_paramter.dat
            string_out = ""
            string_out += self.add(len_before,"define_pT %s"%group)+" = "+self.add(len_after)+pT+"\n"
            if len(cut_list) == 2: return string_out
            string_out += self.add(len_before,"define_eta %s"%group)+" = "+self.add(len_after)+eta+"\n"
            string_out += self.add(len_before,"define_y %s"%group)+" = "+self.add(len_after)+y+"\n"
            string_out += self.add(len_before,"n_observed_min %s"%group)+" = "+self.add(len_after)+nmin+"\n"
            string_out += self.add(len_before,"n_observed_max %s"%group)+" = "+self.add(len_after)+nmax+"\n"
        else: # format for paramter.dat
            string_out = "# %s%s cuts\n" % (string[0].upper(), string[1:])
            if group == "missing":
                string_out += def_pT+" "*(len_total-len(def_pT))  +"  #  requirement on transverse momentum of sum of all %ss (lower cut)\n" % string
            else:
                string_out += def_pT+" "*(len_total-len(def_pT))  +"  #  requirement on %s transverse momentum (lower cut)\n" % string
            if len(cut_list) == 2: return string_out
            string_out += def_eta+" "*(len_total-len(def_eta))+"  #  requirement on %s pseudo-rapidity (upper cut)\n" % string
            string_out += def_y+" "*(len_total-len(def_y))    +"  #  requirement on %s rapidity (upper cut)\n" % string
            string_out += def_min+" "*(len_total-len(def_min))+"  #  minimal number of observed %ss (with cuts above)\n" % string
            string_out += def_max+" "*(len_total-len(def_max))+"  #  maximal number of observed %ss (with cuts above)\n" % string
        return string_out
#}}}
#{{{ def: print_process_inputs_header(self,file_out)
    def print_process_inputs_header(self,file_out):
        header = False
        string_out  = "#{{{ header\n"
        string_out += "global FFS_list\n"
        string_out += "global scale_dict\n"
        string_out += "global proc_dict\n"
        string_out += "global cuts_dict\n"
        string_out += "FFS_list   = []\n"
        string_out += "scale_dict = {}\n"
        string_out += "proc_dict  = multidim_dict(2)\n"
        string_out += "cuts_dict  = multidim_dict(2)\n"
        string_out += "#}}}\n"
        if os.path.exists(file_out):
            with open(file_out) as f:
                if not "#{{{ header" in f.read():
                    line_prepender(file_out,string_out)
        else:
            with open(file_out, "w") as f:
                f.write(string_out)
#}}}
#{{{ def: read_file_and_create_input()
    def read_file_and_create_input(self,file_in,file_out=""):
        relevant_pars =["process_class","scale_ren","scale_fact","dynamic_scale","factor_central_scale"]
        group_dict = {}
        group_order_list = []
        switch_list = []
        relevant_par_out = ""
        switch = ""
        last_scale = False
        scale_comments = []
        last_cut = False
        temp_list = ""
        out_string = ""
        FFS = ""
        if file_out:
            indent = ""
        else:
            indent = "        "
        with open(file_in) as f:
            for line in f:
                line=line.strip()
                split = line.split("=")
                try:
                    param = split[0].strip()
                    value = split[1].strip()
                    comment = value.split("#")[1].strip()
                    value = value.split("#")[0].strip()
                except:
                    pass
                if line.startswith("#"):
                    if last_scale:
                        scale_comments.append(line.replace("#  ","").replace("# ","").replace("#",""))
                    if last_cut:
                        temp_list.append(["","",line.replace("#  ","").replace("# ","").replace("#","")])
                elif param == "dynamic_scale":
                    last_scale = True
                    last_cut   = False
                elif param.startswith("user_"):
                    last_cut   = True
                    last_scale = False
                else:
                    last_scale = False
                    last_cut   = False
                for par in relevant_pars:
                    if param == par:
                        relevant_par_out += indent+"self.proc_dict[\"%s\"][\"%s\"] = \"%s\"\n" % (self.process,par,value)
                if param == "flavor_scheme":
                    FFS = indent+"self.FFS_list.append(\"%s\")\n" % self.process
                if param.startswith("define_") or param.startswith("n_observed_"):
                    group = param.split()[1]
                    if not group in group_order_list:
                        group_order_list.append(group)
                    if not group in group_dict:
                        group_dict[group] = []
                        group_dict[group].append(group)
                    group_dict[group].append(value)
                if param.startswith("user_switch"):
                    if switch: switch_list.append(temp_list)
                    temp_list = [[param.strip(),value,comment]]
                    switch = param
                if param.startswith("user_cut"):
                    temp_list.append([param.strip(),value,comment])
                if param.startswith("user_int"):
                    temp_list.append([param.strip(),value,comment])
        if temp_list:
            switch_list.append(temp_list)
        if file_out:
            out_string += "#{{{ %s input\n" % self.process
        else:
            out_string += "#{{{    %s input\n" % self.process
        if FFS: out_string += FFS
        out_string += relevant_par_out
        out_string += indent+"self.scale_dict[\"%s\"] = %s\n" % (self.process,scale_comments)
        out_string += ""
        final_list = []
        for group in group_order_list:
            list_group = copy.copy(group_dict[group])
            list_group.pop(0)
            list_default = self.list_dict.get(group,self.list_dict["default"])
            if list_group == list_default:
                final_list.append(group)
            else:
                final_list.append(group_dict[group])
        out_string += indent+"self.cuts_dict[\"%s\"][\"block\"] = %s\n" % (self.process,str(final_list))
        out_string += ""
        out_string += indent+"self.cuts_dict[\"%s\"][\"user\"] = %s\n" % (self.process,str(switch_list))
        out_string += "#}}}\n"
        if file_out:
            with open(file_out, "a") as f:
                f.write(out_string.replace("self.",""))
        else:
            print out_string
#}}}
#{{{ def: create_parameter_dat(self,file_path)
    def create_parameter_dat(self,file_path):
        # starte creating new parameter.dat file
        self.init_parameter_dat()
#        print self.block(self.param_blocks["cuts"])
        string  = self.init_parameter_string
        string += "\n"
        string += self.block(self.param_blocks["general"])
        string += "\n\n"
        string += self.block(self.param_blocks["scale"])
        string += "\n\n"
        string += self.block(self.param_blocks["order"])
        string += "\n\n"
        string += self.block(self.param_blocks["cuts"])
        string += "\n"
        string += self.block(self.param_blocks["behav"])
        with open(file_path,'w') as f:
            f.write(string)
        return
#}}}
#{{{ def: create_model_dat(self,file_path)
    def create_model_dat(self,file_path):
        self.model_dict = {}
        if self.FFS:
            self.model_dict["M_b"] = "4.920000e+00"
        else:
            self.model_dict["M_b"] = "0.000000    "
        if "h" in self.process or "t" in self.process:
            self.model_dict["WT"] = "0.000000    "
        else:
            self.model_dict["WT"] = "1.442620e+00"
        if "h" in self.process or "w" in self.process or "z" in self.process or "t" in self.process:
            self.model_dict["WZ"] = "0.000000    "
            self.model_dict["WW"] = "0.000000    "
        else:
            self.model_dict["WZ"] = "2.495200e+00"
            self.model_dict["WW"] = "2.085400e+00"
        if "h" in self.process or "zz" in self.process or "wxw" in self.process:
            self.model_dict["WH"] = "0.000000    "
        else:
            self.model_dict["WH"] = "4.070000e-03"
        
        string = """##########################
# MATRIX model parameter #
##########################

#--------\\
# masses |
#--------/
Block MASS
  1 0.000000     # M_d 
  2 0.000000     # M_u
  3 0.000000     # M_s
  4 0.000000     # M_c
  5 %(M_b)s # M_b 
  6 1.732000e+02 # M_t
 11 0.000000     # M_e 
 12 0.000000     # M_ve
 13 0.000000     # M_mu
 14 0.000000     # M_vm
 15 1.777000e+00 # M_tau
 16 0.000000     # M_vt
 23 9.118760e+01 # M_Z 
 24 8.038500e+01 # M_W
 25 1.250000e+02 # M_H 

#-------------------\\
# inputs for the SM |
#-------------------/
Block SMINPUTS
  1 1.280000e+02 # 1/alpha_e(MZ)
  2 1.166390e-05 # G_F 
111 1.370360e+02 # 1/alpha_e(mu->0)

#------------------\\
# Yukawa couplings |
#------------------/
#Block YUKAWA 
#  5 4.750000e+00 # M_YB 
#  6 1.730000e+02 # M_YT 
# 15 1.777000e+00 # M_YTAU 

#---------------\\
# decays widths |
#---------------/
DECAY  6 %(WT)s # WT 
DECAY 23 %(WZ)s # WZ 
DECAY 24 %(WW)s # WW 
DECAY 25 %(WH)s # WH

#-----------\\
# EW inputs |
#-----------/
Block EWINPUTS
  1 1  # ew_scheme - determines scheme used for EW inputs
       # 0: alpha_e_0 scheme (alpha_e(mu->0) above used to determine inputs)
       # 1: G_mu scheme (G_F above used to determine inputs)
       # 2: alpha_e_MZ scheme (alpha_e(MZ) above used to determine inputs)
  3 1  # use_cms - switch for the complex mass scheme
       # 0: off
       # 1: on
       # 2: on, but alpha_e is determined through real parameters
""" % self.model_dict
  # 2 -1 # use_adapted_ew_coupling - can be used to dermine all couplings in a 
  #      # -1: off                 | scheme different from the above chosen 
  #      # 1-2: settings as above  | ew_scheme; standard setting: set them equal

        if self.process in ["ppw01","ppwx01","ppexne02","ppenex02"]:
            string += """

# NOTE: You can use EITHER Block CKM (full CKM) OR Block VCKMIN (1./2. generation mixing with Cabibbo angle).
#       The other one must be removed/commented. If you remove/comment both blocks a trivial CKM (no mixing) is used.

#------------\\
# CKM matrix |
#------------/
Block CKM
 11 0.974170e+00 # V_ud
 12 0.224800e+00 # V_us
 13 0.004090e+00 # V_ub
 21 0.220000e+00 # V_cd
 22 0.995000e+00 # V_cs
 23 0.040500e+00 # V_cb
 31 0.008200e+00 # V_td
 32 0.040000e+00 # V_ts
 33 1.009000e+00 # V_tb

#---------------\\
# Cabibbo angle |
#---------------/
#Block VCKMIN
#  1 0.227000e+00 # Cabibbo angle
"""
  
        with open(file_path,'w') as f:
            f.write(string)
        return
#}}}
#{{{ def: create_distribution_dat(self,file_path)
    def create_distribution_dat(self,file_path):
        string = """##################################
# MATRIX distribution definition #
##################################
#
# In this file you can customize the distributions created during the run (examples below)
# please read the INSTRUCTIONS at the END OF THIS FILE...
#
#------\\
# Info |
#------/
# Total rates and jet multiplicities (possibly within cuts) will automatically be included
# Add/remove arbitrary distribution-blocks, but always add/remove a full block.
#
#----------------------\\
# define distributions |
#----------------------/











#--------\\
# Syntax |
#--------/
# "distributionname"
# starts new distribution-block; user-defined, unique label for identification at end of run
#
#
# "distributiontype"
# specifies observable to be binned (some require more than one particle)
#
# pre-defined types: (custom definitions not yet supported)
# pT           -- transverse momentum of "particle 1"
#                 (scalar sum of pTs if more than one "particle" given)
# m            -- invariant mass of "particle 1"
# dm           -- invariant-mass difference between "particle 1" and "particle 2"
# absdm        -- absolute invariant-mass difference between "particle 1" and "particle 2"
# mmin         -- minimal invariant mass of "particle 1" and "particle 2"
# mmax         -- maximal invariant mass of "particle 1" and "particle 2"
# y            -- rapidy of "particle 1"
# absy         -- absolute rapidy of "particle 1"
# dy           -- rapidy difference between "particle 1" and "particle 2"
# absdy        -- absolute rapidy difference between "particle 1" and "particle 2"
# dabsy        -- difference between absolute rapidities of"particle 1" and "particle 2"
# absdabsy     -- absolute difference between absolute rapidities of "particle 1"
#                 and "particle 2"
# eta          -- pseudo-rapidy of "particle 1"
# abseta       -- absolute pseudo-rapidy of "particle 1"
# deta         -- pseudo-rapidy difference between "particle 1" and "particle 2"
# absdeta      -- absolute pseud-rapidy difference between "particle 1" and "particle 2"
# dabseta      -- difference between absolute pseudo-rapidities of"particle 1"
#                 and "particle 2"
# absdabseta   -- absolute difference between absolute pseudo-rapidities of "particle 1"
#                 and "particle 2"
# phi          -- transverse-angle (phi) of "particle 1", or phi difference between 
#                 "particle 1" and "particle 2" if two particles are given
# dR           -- distance (in y-phi-plane) between "particle 1" and "particle 2"
# dReta        -- distance (in eta-phi-plane) between "particle 1" and "particle 2"
# ET           -- transverse mass sqrt(m^2+pT^2) of "particle 1"
#                 (scalar sum of ETs if more than one "particle" given)
# mT           -- transverse mass: ET of "particle 1", if only one particle given
#                 experimental definition with missing energy, if more than one particle 
#                 given; all neutrinos (missing energie) go into "particle 1" all other 
#                 particles in "particle 2,3,4,..."
# 
# special distribution types:
# muR          -- Distribution of scale muR set in computation (no "particle" definition)
# muF          -- Distribution of scale muF set in computation (no "particle" definition)
# pTveto       -- Cumulative cross section sigma(pT<pTveto) as a function of pTveto, 
#                 where pT is the transverse momentum of "particle 1"
# multiplicity -- Distribution in the number of "particle 1"
#
# "particle 1", "particles 2"
# define particles for selected distribution; the value must be: "$particle $number", 
# where the $number denotes its ordering in pT (!!! never forget the $number!!!)
#
# $particle:
# photon -- photon (photon isolation in parameter.dat)
# lep    -- lepton
# lm     -- negatively charged lepton
# lp     -- positively charged lepton
# e      -- electron and positron
# em     -- electron
# ep     -- positron
# mu     -- muons (muon and anti-muon)
# tau    -- taus (tau and anti-tau)
# mum    -- muon
# mup    -- anti-muon
# taum   -- tau
# taup   -- anti-tau
# ljet   -- light jet (without b's, jet definition in parameter.dat)
# jet    -- normal jet (including b's, jet definition in parameter.dat)
# bjet   -- bottom jet (only b's, jet definition in parameter.dat)
# tjet   -- top quarks (top and anti-top)
# top    -- top quark
# atop   -- anti-top quark
# wm     -- W^- boson
# wp     -- W^+ boson
# z      -- Z boson
# h      -- Higgs boson
# nua    -- neutrinos (neutrino and anti-neutrino)
# nu     -- neutrino
# nux    -- anti-neutrino
# nea    -- electron-neutrinos (electron-neutrino and anti-electron-neutrino)
# ne     -- electron-neutrino
# nex    -- anti-electron-neutrino
# nma    -- muon-neutrinos (muon-neutrino and anti-muon-neutrino)
# nm     -- muon-neutrino
# nmx    -- anti-muon-neutrino
# nma    -- tau-neutrinos (tau-neutrino and anti-tau-neutrino)
# nm     -- tau-neutrino
# nmx    -- anti-tau-neutrino
# 
# $number:
# 1 -- hardest
# 2 -- second-hardest
# ...
#
# setting several values for same particle you add their 4-momenta 
# before computing observable ("distributiontype"), eg:"
# "
# distributiontype  =  pT
# particle 1        =  lep 1
# particle 1        =  lep 2
# "
# computes transverse momentum of sum of the 4-momenta of hardest and second-hardest lepton
# 
#
# binning (regular):
# "startpoint", "endpoint" define the range of the distribution
# specify "binwidth" OR "binnumber" to define equidistant bins
#
# if "binningtype = irregular" you can choose non-equidistant bins by definining edges of the bins:
# "
# edges = binedge1:binedge2:binedge3:...
# "
# where binedge1,2,3,... should be floats (eg, 0.:15.5:33.333:100.)
#
"""
        if not os.path.exists(file_path):
            with open(file_path,'w') as f:
                f.write(string)
        return
#}}}
#{{{ def: add(self,length,string):
    def add(self,length,string=""):
        return string+" "*(length-len(string))
#}}}
#{{{ def: print_user_cuts(self,user_cuts)
    def print_user_cuts(self,user_cuts,len_before,len_after):
#        len_total = len_before+len_after
        len_after_after = 61 - len_before - len_after
        string_out = ""
        for line in user_cuts:
            if len(line) == 1 and isinstance(line[0], basestring):
                continue
            if not line[0]:
                continue
            extra = 0
            for index, cut in enumerate(line):
                if cut[0] and not cut[0].startswith("#"):
                    string_out += self.add(len_before,cut[0])+" = "+self.add(len_after)+cut[1]+"\n"
#                string_out += self.add(len_before,cut[0])+" = "+self.add(len_after)+self.add(len_after_after,cut[1])+"%  "+cut[2]+"\n"
            string_out += "\n"
        return string_out
#}}}
#{{{ def: create_file_parameter_dat(self,file_path)
    def create_file_parameter_dat(self,file_path):
        len_before = 31
        len_after = 37-len_before
        printing = True
        default_blocks_printed = False
        cut_blocks_printed = False
        user_cuts_printed = False
        with open(file_path,'r') as f:
            string_out = ""
            for line in f:
                line_orig = copy.copy(line)
                line = line.strip()
                if line.startswith("type_perturbative_order"):
                     break
                if line.startswith("user_particle"):
                    printing = True
                elif line.startswith("jet_algorithm") or line.startswith("photon_") or line.startswith("jet_") or line.startswith("parton_") or line.startswith("frixione_"):
                    printing = False
                    if not default_blocks_printed:
                        for cutline in self.param_blocks["cuts"].get("input",[]):
                            if not cutline[0].startswith("#"):
                                if len(cutline) >= 2:
                                    if cutline[0]:
                                        string_out += self.add(len_before,cutline[0])+" = "+self.add(len_after)+cutline[1]+"\n"
                                else:
                                    string_out += self.add(len_before,cutline[0])+"\n"
                            default_blocks_printed = True
                elif line.startswith("define_") or line.startswith("n_observ"):
                    printing = False
                    if not user_cuts_printed:
                        if not cut_blocks_printed:
                            string_out += "\n"
                            for block_list in self.param_blocks["cuts"].get("block",[]): # loop through cut blocks, where available
                                string_out += self.print_cut_block(block_list,len_before+len_after,1,len_before,len_after)
                                string_out += "\n"
                                cut_blocks_printed = True
                elif line.startswith("user_"):
                    printing = False
                    if not user_cuts_printed:
                        string_out += self.print_user_cuts(self.param_blocks["cuts"].get("user",[]),len_before,len_after)
                        user_cuts_printed = True
                else:
                    if not printing and (line.startswith("#") or not line):
                        printing = False
                    else:
                        if printing == False:
                            string_out += "\n"
                        printing = True
                if (printing or line_orig[0] == "#") and  not line_orig.startswith(" "):
                    string_out += line.strip()+"\n"
            string_out += self.add(len_before,"type_perturbative_order")+" = "+self.add(len_after)+"LO"+"\n"
            string_out += self.add(len_before,"LHAPDFname")+" = "+self.add(len_after)+self.LHAPDF_LO+"\n"
            string_out += self.add(len_before,"LHAPDFsubset")+" = "+self.add(len_after)+"0"+"\n"
            string_out += "\n"
            string_out += self.add(len_before,"type_perturbative_order")+" = "+self.add(len_after)+"NLO"+"\n"
            string_out += self.add(len_before,"LHAPDFname")+" = "+self.add(len_after)+self.LHAPDF_NLO+"\n"
            string_out += self.add(len_before,"LHAPDFsubset")+" = "+self.add(len_after)+"0"+"\n"
            string_out += "\n"
            string_out += self.add(len_before,"type_perturbative_order")+" = "+self.add(len_after)+"NNLO"+"\n"
            string_out += self.add(len_before,"LHAPDFname")+" = "+self.add(len_after)+self.LHAPDF_NNLO+"\n"
            string_out += self.add(len_before,"LHAPDFsubset")+" = "+self.add(len_after)+"0"+"\n"
            string_out += "\n"
            string_out += self.add(len_before,"type_perturbative_order")+" = "+self.add(len_after)+"NNNLO"+"\n"
            string_out += self.add(len_before,"LHAPDFname")+" = "+self.add(len_after)+self.LHAPDF_NNNLO+"\n"
            string_out += self.add(len_before,"LHAPDFsubset")+" = "+self.add(len_after)+"0"+"\n"
            string_out += ""
            with open(file_path,'w') as f:
                f.write(string_out)
#}}}

#{{{ def: create_model_dat(self,file_path)
    def create_model_dat(self,file_path):
        self.model_dict = {}
        if self.FFS:
            self.model_dict["M_b"] = "4.920000e+00"
        else:
            self.model_dict["M_b"] = "0.000000    "
        if "h" in self.process or "t" in self.process:
            self.model_dict["WT"] = "0.000000    "
        else:
            self.model_dict["WT"] = "1.442620e+00"
        if "h" in self.process or "w" in self.process or "z" in self.process or "t" in self.process:
            self.model_dict["WZ"] = "0.000000    "
            self.model_dict["WW"] = "0.000000    "
        else:
            self.model_dict["WZ"] = "2.495200e+00"
            self.model_dict["WW"] = "2.085400e+00"
        if "h" in self.process or "zz" in self.process or "wxw" in self.process:
            self.model_dict["WH"] = "0.000000    "
        else:
            self.model_dict["WH"] = "4.070000e-03"
        
        string = """##########################
# MATRIX model parameter #
##########################

#--------\\
# masses |
#--------/
Block MASS
  1 0.000000     # M_d 
  2 0.000000     # M_u
  3 0.000000     # M_s
  4 0.000000     # M_c
  5 %(M_b)s # M_b 
  6 1.732000e+02 # M_t
 11 0.000000     # M_e 
 12 0.000000     # M_ve
 13 0.000000     # M_mu
 14 0.000000     # M_vm
 15 1.777000e+00 # M_tau
 16 0.000000     # M_vt
 23 9.118760e+01 # M_Z 
 24 8.038500e+01 # M_W
 25 1.250000e+02 # M_H 

#-------------------\\
# inputs for the SM |
#-------------------/
Block SMINPUTS
  1 1.280000e+02 # 1/alpha_e(MZ)
  2 1.166390e-05 # G_F 
111 1.370360e+02 # 1/alpha_e(mu->0)

#------------------\\
# Yukawa couplings |
#------------------/
#Block YUKAWA 
#  5 4.750000e+00 # M_YB 
#  6 1.730000e+02 # M_YT 
# 15 1.777000e+00 # M_YTAU 

#---------------\\
# decays widths |
#---------------/
DECAY  6 %(WT)s # WT 
DECAY 23 %(WZ)s # WZ 
DECAY 24 %(WW)s # WW 
DECAY 25 %(WH)s # WH

#-----------\\
# EW inputs |
#-----------/
Block EWINPUTS
  1 1  # ew_scheme - determines scheme used for EW inputs
       # 0: alpha_e_0 scheme (alpha_e(mu->0) above used to determine inputs)
       # 1: G_mu scheme (G_F above used to determine inputs)
       # 2: alpha_e_MZ scheme (alpha_e(MZ) above used to determine inputs)
  3 1  # use_cms - switch for the complex mass scheme
       # 0: off
       # 1: on
       # 2: on, but alpha_e is determined through real parameters
""" % self.model_dict
  # 2 -1 # use_adapted_ew_coupling - can be used to dermine all couplings in a 
  #      # -1: off                 | scheme different from the above chosen 
  #      # 1-2: settings as above  | ew_scheme; standard setting: set them equal

        if self.process in ["ppw01","ppwx01","ppexne02","ppenex02"]:
            string += """

# NOTE: You can use EITHER Block CKM (full CKM) OR Block VCKMIN (1./2. generation mixing with Cabibbo angle).
#       The other one must be removed/commented. If you remove/comment both blocks a trivial CKM (no mixing) is used.

#------------\\
# CKM matrix |
#------------/
Block CKM
 11 0.974170e+00 # V_ud
 12 0.224800e+00 # V_us
 13 0.004090e+00 # V_ub
 21 0.220000e+00 # V_cd
 22 0.995000e+00 # V_cs
 23 0.040500e+00 # V_cb
 31 0.008200e+00 # V_td
 32 0.040000e+00 # V_ts
 33 1.009000e+00 # V_tb

#---------------\\
# Cabibbo angle |
#---------------/
#Block VCKMIN
#  1 0.227000e+00 # Cabibbo angle
"""
  
        with open(file_path,'w') as f:
            f.write(string)
        return
#}}}

#{{{ def: create_file_model_dat(self,file_path)
    def create_file_model_dat(self,file_path):
        for param in self.model_dict:
            self.model_dict[param] = str(float(self.model_dict[param])).rstrip("0")
        string = """M_W                     =       80.385
M_Z                     =       91.1876
M_H                     =       125.
M_t                     =       173.2
M_b                     =       %(M_b)s
M_c                     =       0.
M_s                     =       0.
M_u                     =       0.
M_d                     =       0.
M_e                     =       0.
M_mu                    =       0.
M_tau                   =       0.
M_ve                    =       0.
M_vm                    =       0.
M_vt                    =       0.

G_F                     =       1.16639E-05
1/alpha_e_0             =       137.0360 
1/alpha_e_MZ            =       128. 

ew_scheme               =       1
use_adapted_ew_coupling =       -1
use_cms                 =       1

CKM_matrix              =       trivial
theta_c                 =       0.227 

contribution            =       LO
Gamma_W                 =       %(WW)s
Gamma_Z                 =       %(WZ)s
Gamma_t                 =       %(WT)s
Gamma_H                 =       %(WH)s

contribution            =       NLO   
Gamma_W                 =       %(WW)s
Gamma_Z                 =       %(WZ)s
Gamma_t                 =       %(WT)s
Gamma_H                 =       %(WH)s

contribution            =       NNLO  
Gamma_W                 =       %(WW)s
Gamma_Z                 =       %(WZ)s
Gamma_t                 =       %(WT)s
Gamma_H                 =       %(WH)s
""" % self.model_dict
        with open(file_path,'w') as f:
            f.write(string)
        return
#}}}
#{{{ def: create_file_distribution_dat(self,file_path)
    def create_file_distribution_dat(self,file_path):
        string = """# total cross section (possibly within cuts)
distributionname  =  total_rate
distributiontype  =  XS
startpoint        =  0.
binwidth          =  1
binnumber         =  1

# number of jets (possibly within cuts)
distributionname  =  n_jets
distributiontype  =  multiplicity
particle 1        =  jet 0
startpoint        =  0.
binwidth          =  1
binnumber         =  3
"""
        with open(file_path,'w') as f:
            f.write(string)
        return
#}}}
#}}}
