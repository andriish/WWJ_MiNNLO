#include <iostream>
#include <stdio.h>
#include <stdlib.h> //
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <vector>
#include <complex>
#include <mpi.h>
#include <math.h>
#include <ampVVprime.h>
using namespace std;

#include <grid_and_phsp.h>

struct{
int steps;
} ndivisions;

// *********************************************
// Specify here for which region you want to 
// compute the grid set
// *********************************************
int compute_grid_for_region=1;

void compute(int cx1, int cx2, const int steps,double ma2,double mb2,vector<vector<vector<double> > >phsp){
  int N_f=4;
  VVprime::getInstance().initAmplitude(N_f);

  int ii_cx3,ii_cx4;
  restart(cx1, cx2, steps, ii_cx3, ii_cx4);
  if(ii_cx3==steps+1 && ii_cx4==steps+1){
    cout<<"Nothing to be done!\n";
    return;
  }

  ofstream file[3][9];                                                                                       
  for(int type=0;type<3;++type){
    for(int index=0;index<9;++index){
      string pref="pwg-2loop";
      string id1=to_string(cx1),id2=to_string(cx2);
      string id3=to_string(type),id4=to_string(index);
      string suff(".dat");
      string att("-");
      string filetag=pref+att+id1+att+id2+att+id3+att+id4+suff;    
      file[type][index].open(filetag.c_str(),fstream::app);                                                                                         
    }                                                                                                                                                               
  }                                                                         


  for(int cx3=ii_cx3; cx3<=steps; ++cx3){ 
    for(int cx4=ii_cx4; cx4<=steps; ++cx4){ 
      double xx1=(double)cx1/steps;
      double xx2=(double)cx2/steps;
      double xx3=(double)cx3/steps;
      double xx4=(double)cx4/steps;
      VVprime::getInstance().computeAmplitude(phsp[cx3][cx4][0],phsp[cx3][cx4][1],ma2,mb2);
      for (int type=0; type<3; type++) {
	for (int i=0; i<9; i++) {
	  double Er,Ei;
	  VVprime::getInstance().getAmplitude(2,i+1,type,Er,Ei);
	  file[type][i]<<fixed<<noshowpos<<setw(5)<<setprecision(2)<<xx1<<" "<<xx2<<" "<<xx3<<" "<<xx4<<" "<<scientific<<showpos<<setw(18)<<setprecision(10)<<Er<<" "<<Ei<<"\n";
	}
      }      
      ii_cx4=0;      
    }
  }

  for(int type=0;type<3;++type){
    for(int index=0;index<9;++index){
      file[type][index].close();                                                                                         
    }                                                                                                                                                               
  }                                                                         

}



int main(int argc, char** argv)
{

  if(getenv("DIVISIONS")) {
    // DIVISIONS is set
    istringstream env(getenv("DIVISIONS"));
    env>>ndivisions.steps;
  } else {
    // DIVISIONS is not set
    ndivisions.steps=50; 
  }
  
  int task;
  istringstream itask(argv[1]);
  itask>>task;
  int size,rank;
  const int steps=ndivisions.steps; 

  MPI_Init (&argc, &argv); 
  MPI_Comm_size(MPI_COMM_WORLD, &size); //get number of processes per job
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // rank [0,size-1], where size is the SLURM ntasks, which has to satisfy the relation 
  // size=(steps+1)*(steps+1)/njobs, where njobs is the number of jobs in the SLURM 
  // job array. task = SLURM_ARRAY_TASK_ID ranging from [1,narray]
  int cx1=(size*(task-1)+rank)/(steps+1);
  int cx2=(size*(task-1)+rank)%(steps+1);

  long double y[4];
  y[0]=(long double)cx1/steps;
  y[1]=(long double)cx2/steps;
    
  vector<vector<vector<double> > >phsp(steps+1, vector<vector<double>>(steps+1,vector<double>(2)));
  double ma2,mb2;

  for(int cx3=0; cx3<=steps; ++cx3){ 
    y[2]=(long double)cx3/steps;
    for(int cx4=0; cx4<=steps; ++cx4){ 
      y[3]=(long double)cx4/steps;
      double s,t;
      find_phsp_point(y,ma2,mb2,s,t, compute_grid_for_region);
      phsp[cx3][cx4][0]=s;
      phsp[cx3][cx4][1]=t;
    }
  }

  compute(cx1,cx2,steps,ma2,mb2,phsp);

  MPI_Finalize();
  return(0);



};

  
