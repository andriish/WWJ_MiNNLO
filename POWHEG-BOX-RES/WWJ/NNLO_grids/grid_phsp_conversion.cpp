#include <iostream>
#include <stdio.h>
#include <stdlib.h> //
#include <time.h> //
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <vector>
#include <complex>
#include <math.h>
#include <chrono>
using namespace std; 

namespace input{
  long double Wmass,Wwidth;
  long double sbeam;
}


template <class T>
T read_input(const char* key,int & status)
{
  status=1;
  fstream file;
  file.open("gridinput",ios::in);
  T value;
  if(file.is_open())
    {
      string line;
      while(getline(file, line)){
	if(line[0]=='#'||line[0]==' '||line[0]=='\n'||line=="")
	  {continue;}
	istringstream iss(line);
	string tmp_key;
	iss>>tmp_key;
	if(tmp_key==key){
	  iss>>value;
	  status=0;
	  break;
	}
	status=-1;
      }
      file.close(); 
    }
  else
    {
      status=-1;
    }
  file.close(); 
  
  return value;
}


void set_by_region(long double &tinys1, long double &tinys2, long double &tinyt1, long double &tinyt2, double &pps, double &ppt, int reg)
{
  if(reg==1){
    pps=1.0;
    ppt=1.0;
    tinys2=0.95;
    tinys1=0.5e-1;
    tinyt2=0.9;
    tinyt1=1e-3;
  }
  else if(reg==2){
    pps=5.0;
    ppt=5.0;
    tinys2=1.-1e-5;
    tinys1=0.8;
    tinyt2=1.-1e-5;
    tinyt1=0.7;
  }
  else if(reg==3){
    pps=1.0;
    ppt=1.0;
    tinys2=0.95;
    tinys1=0.5e-1;
    tinyt2=1.-1e-3;
    tinyt1=0.7;
  }
  else if(reg==4){
    pps=5.0;
    ppt=1./pps;
    tinys2=1.-1e-5;
    tinys1=0.8;
    tinyt2=0.9;
    tinyt1=1e-4;
  }
}

void find_grid_point(double ma2,double mb2,double s,double t,double (&y)[4], int reg)
{
  int status;
  using namespace input;
  int static ini=true;
  if(ini){
    Wmass=read_input<long double>("Wmass",status);
    if(status==-1){ Wmass=80.3858;}
    Wwidth=read_input<long double>("Wwidth",status);
    if(status==-1){ Wwidth=2.08540;}
    sbeam=read_input<long double>("sbeam",status);
    if(status==-1){ sbeam=6500;}
    sbeam*=sbeam*4L;
    ini=false;
  }

  double p=3;
  long double masslow1=40,masshigh1=0.01*sbeam;
  long double zlow1=atanl((masslow1-powl(Wmass,2))/Wmass/Wwidth);
  long double zhigh1=atanl((masshigh1-powl(Wmass,2))/Wmass/Wwidth);
  long double zl1=(zhigh1)*pow(0.4,p)+(zlow1)*(1-pow(0.4,p));
  long double zh1=(zlow1)*pow(1-0.6,p)+(zhigh1)*(1-pow(1-0.6,p));
  long double z1=atanl((ma2-powl(Wmass,2))/Wmass/Wwidth);
  long double diff1=(z1-zlow1)/(zhigh1-zlow1);
  long double diff2=(z1-zhigh1)/(zlow1-zhigh1);
  if(diff1<0){y[0]=-2.;} // dummy grid-like value to be catched by the rescue system
  else if(diff2<0){y[0]=+2.;}
  else{
    if(z1<=zl1){if(diff1<1e-6){y[0]=0;}else{y[0]=pow(diff1,1/p);}}
    else if (z1>zl1 && z1<=zh1){y[0]=0.4+(0.6-0.4)*(z1-zl1)/(zh1-zl1);}
    else{if(diff2<1e-6){y[0]=0;}else{y[0]=1-pow(diff2,1/p);}}
    if(y[0]<1e-6)y[0]=0;
  }

  p=2.3;
  long double supp=powl(1-abs(ma2-powl(Wmass,2))/(ma2+powl(Wmass,2)),0.1);
  long double supp2=1-pow(abs(ma2-powl(Wmass,2))/(ma2+powl(Wmass,2)),0.02);
  long double masslow2=10l*supp+powl(Wmass,2)*(1-supp),masshigh2=10*masshigh1*supp2+powl(Wmass,2)*(1-supp2);
  long double zlow2=atanl((masslow2-powl(Wmass,2))/Wmass/Wwidth);
  long double zhigh2=atanl((masshigh2-powl(Wmass,2))/Wmass/Wwidth);
  long double zl2=(zhigh2)*pow(0.45,p)+(zlow2)*(1-pow(0.45,p));
  long double zh2=(zlow2)*pow(1-0.55,p)+(zhigh2)*(1-pow(1-0.55,p));
  long double z2=atanl((mb2-powl(Wmass,2))/Wmass/Wwidth);
  diff1=(z2-zlow2)/(zhigh2-zlow2);
  diff2=(z2-zhigh2)/(zlow2-zhigh2);
  if(diff1<0){y[1]=-2.;} // dummy grid-like value to be catched by the rescue system
  else if(diff2<0){y[1]=+2.;}
  else{
    if(z2<=zl2){if(abs(diff1)<1e-6){y[1]=0;}else{y[1]=pow(diff1,1/p);}}
    else if (z2>zl2 && z2<=zh2){y[1]=0.45+(0.55-0.45)*(z2-zl2)/(zh2-zl2);}
    else{if(abs(diff2)<1e-6){y[1]=1;}else{y[1]=1-pow(diff2,1/p);}}
    if(y[1]<1e-6)y[1]=0;
  }

  long double tinys1, tinys2, tinyt1, tinyt2;
  double pps, ppt;
  set_by_region(tinys1, tinys2, tinyt1, tinyt2, pps, ppt, reg);

  long double y2=sqrt(pow(s,2)+pow(ma2,2)+pow(mb2,2)-2*(s*ma2+s*mb2+ma2*mb2))/(s+ma2-mb2);
  if(abs(y2-tinys1)<1e-6){y[2]=0;}
  else{
    if((y2-tinys1)>0)
      {y[2]=pow((y2-tinys1)/(tinys2-tinys1),pps);}
    else //extrapolation below zero ...
      {
	if(reg==1){y[2]=-(tinys1-y2);}
	else{y[2]=-1;}
      }
  }
  if(y[2]<1e-6)y[2]=0;

  long double y3=0.5*(1-(2*t+s-ma2-mb2)/sqrt(pow(s,2)+pow(ma2,2)+pow(mb2,2)-2*(s*ma2+s*mb2+ma2*mb2)));
  if(abs(y3-tinyt1)<1e-6){y[3]=0;}
  else{
    if((y3-tinyt1)>0)
      {y[3]=pow((y3-tinyt1)/(tinyt2-tinyt1),ppt);}
    else //extrapolation below zero ...
      {
	if(reg==1){y[3]=-(tinyt1-y3);}
	else{y[3]=-1;}
      }
    if(abs(y[3])<1e-6)y[3]=0; 
  }

}


void find_phsp_point(long double y[4],double&  ma2,double&  mb2,double& s,double& t, int reg){
  int status;
  using namespace input;
  int static ini=true;
  if(ini){
    Wmass=read_input<long double>("Wmass",status);
    if(status==-1){ Wmass=80.3858;}
    Wwidth=read_input<long double>("Wwidth",status);
    if(status==-1){ Wwidth=2.08540;}
    sbeam=read_input<long double>("sbeam",status);
    if(status==-1){ sbeam=6500;}
    sbeam*=sbeam*4;
    ini=false;
  }
  
  double p=3;
  long double masslow1=40,masshigh1=0.01*sbeam;
  long double zlow1=atanl((masslow1-powl(Wmass,2))/Wmass/Wwidth);
  long double zhigh1=atanl((masshigh1-powl(Wmass,2))/Wmass/Wwidth);
  long double z1;
  long double zl1=(zhigh1)*pow(0.4,p)+(zlow1)*(1-pow(0.4,p));
  long double zh1=(zlow1)*pow(1-0.6,p)+(zhigh1)*(1-pow(1-0.6,p));
  if(y[0]<=0.4){z1=(zhigh1)*pow(y[0],p)+(zlow1)*(1-pow(y[0],p));}
  else if (y[0]>0.4 && y[0]<=0.6){z1=zl1+(zh1-zl1)*(y[0]-0.4)/(0.6-0.4);}
  else{z1=(zlow1)*pow(1-y[0],p)+(zhigh1)*(1-pow(1-y[0],p));}
  ma2=Wmass*Wwidth*tanl(z1)+powl(Wmass,2);

  p=2.3;
  double supp=pow(1-abs(ma2-powl(Wmass,2))/(ma2+powl(Wmass,2)),0.1);
  double supp2=1-pow(abs(ma2-powl(Wmass,2))/(ma2+powl(Wmass,2)),0.02);
  long double masslow2=10*supp+powl(Wmass,2)*(1-supp),masshigh2=10*masshigh1*supp2+powl(Wmass,2)*(1-supp2);
  long double zlow2=atanl((masslow2-powl(Wmass,2))/Wmass/Wwidth);
  long double zhigh2=atanl((masshigh2-powl(Wmass,2))/Wmass/Wwidth);
  long double z2;
  long double zl2=(zhigh2)*pow(0.45,p)+(zlow2)*(1-pow(0.45,p));
  long double zh2=(zlow2)*pow(1-0.55,p)+(zhigh2)*(1-pow(1-0.55,p));
  if(y[1]<=0.45){z2=(zhigh2)*pow(y[1],p)+(zlow2)*(1-pow(y[1],p));}
  else if (y[1]>0.45 && y[1]<=0.55){z2=zl2+(zh2-zl2)*(y[1]-0.45)/(0.55-0.45);}
  else{z2=(zlow2)*pow(1-y[1],p)+(zhigh2)*(1-pow(1-y[1],p));}
  mb2=Wmass*Wwidth*tanl(z2)+powl(Wmass,2);
 
  long double tinys1, tinys2, tinyt1, tinyt2;
  double pps, ppt;
  set_by_region(tinys1, tinys2, tinyt1, tinyt2, pps, ppt, reg);

  long double z3=tinys1+(tinys2-tinys1)*pow(y[2],1./pps);
  s=(ma2 + mb2 + (ma2 - mb2)*powl(z3,2) + 2l*sqrtl(ma2*mb2+ma2*(ma2-mb2)*powl(z3,2)) )/(1l-powl(z3,2));
  
  long double xx4;
  if(y[3]>0){xx4=tinyt1+(tinyt2-tinyt1)*pow(y[3],1./ppt);}
  else{xx4=y[3]+tinyt1;} //cos\theta range from 0 to tiny
  long double z4=1-2*xx4;
  t=(ma2*powl(z3,2)+(1l-powl(z3,2))*sqrtl((ma2*mb2+ma2*(ma2-mb2)*powl(z3,2))/powl((-1l+powl(z3,2)),2))
     -z3*z4*sqrtl(ma2*(ma2+mb2+powl(z3,2)*(ma2-mb2))+2l*ma2*sqrtl(ma2*(mb2+powl(z3,2)*(ma2-mb2)))))/(-1l+powl(z3,2));

}



void restart(int cx1, int cx2, const int steps, int &ii_cx3, int &ii_cx4){
  string pref("pwg-2loop-");
  string id1=to_string(cx1),id2=to_string(cx2);
  string suff("-0-0.dat");
  string att("-");
  string filename=pref+id1+att+id2+suff;
  fstream  file;
  file.open(filename.c_str(),ios::in);
  if(file.is_open()){
    bool empty=file.peek() == std::ifstream::traits_type::eof();
    if(empty){
      goto as_if_closed;
    }
    else{
      //Got to the last character before EOF
      file.seekg(-1, ios_base::end);
      if(file.peek() == '\n')
        {
          //Start searching for \n occurrences
          file.seekg(-1, ios_base::cur);
          int i = file.tellg();
          for(i;i > 0; i--)
            {
              if(file.peek() == '\n')
                {
                  //Found        
                  file.get();
                  break;
                }
              //Move one character back        
              file.seekg(i, ios_base::beg);
            }
        }
      string lastline;
      getline(file, lastline);

      int ii1,ii2;
      stringstream(lastline)>>ii1>>ii2>>ii_cx3>>ii_cx4;

      if(ii_cx4<steps){
        ++ii_cx4;
      }
      else if(ii_cx4==steps){
        if(ii_cx3==steps){
          ++ii_cx4;
          ++ii_cx3; //end of file
        }
        else{
	  ii_cx4=0;
	  ++ii_cx3;
        }
      }
    }
  }
  else{
  as_if_closed:
  ii_cx3=0;
  ii_cx4=0;
  }

  file.close();
}
