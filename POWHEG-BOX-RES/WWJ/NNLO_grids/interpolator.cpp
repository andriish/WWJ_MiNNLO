#include <iostream>
#include <stdio.h>
#include <stdlib.h> //
#include <time.h> //
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>
#include <vector>
#include <complex>
#include <math.h>
#include <chrono>


#include <btwxt.h>
#include <error.h>
#include <griddeddata.h>
using namespace Btwxt;

using namespace std;
#include <ampVVprime.h>

#include <grid_and_phsp.h>

//////////////////////////////////////////////////////////////////////
////////                INTERPOLATOR RUNNING MODE             ////////
//////////////////////////////////////////////////////////////////////
// '1' -> load just set og grids for 1 region (mainly for quick tests)
// '2' -> load full set of grids for 4 regions
// '3' -> load a subset of grids from all 4 regions and make use 
//        of crossing symmetry
//////////////////////////////////////////////////////////////////////
char interpolator_run_mode='3';

//////////////////////////////////////////////////////////////////////
////////                SELECT WHICH GRID TO USE              ////////
//////////////////////////////////////////////////////////////////////

bool choose_region(double ma2,double mb2, double s,double t, vector<double>&y, vector<double>&yt){
  double yyt[4];
  bool reg1=false;
  if( y[2]>0.85 && y[3]>0.75){	
    find_grid_point(ma2,mb2,s,t,yyt, 2);      
    if(yyt[2]<0||yyt[3]<0){
      goto REGION1t;
    }
  }
  else if(y[2]<=0.85 && y[3]>0.75){	
    find_grid_point(ma2,mb2,s,t,yyt, 3);      
    if(yyt[2]<0||yyt[3]<0){
      goto REGION1t;
    }
  }
  else if(y[2]>0.85 && y[3]<=0.75){	
    find_grid_point(ma2,mb2,s,t,yyt, 4);      
    if(yyt[2]<0||yyt[3]<0){
      goto REGION1t;
    }
  }
  else if(y[2]<=0.85 && y[3]<=0.75){
  REGION1t:
    reg1=true;
    yyt[0]=y[0];
    yyt[1]=y[1];
    yyt[2]=y[2];
    yyt[3]=y[3];
  }
  yt[0]=yyt[0];
  yt[1]=yyt[1];
  yt[2]=yyt[2];
  yt[3]=yyt[3];

  return reg1;
}


//////////////////////////////////////////////////////////////////////
////////        TOOLS FOR FORM FACTORS CROSSING SYMMETRY      ////////
//////////////////////////////////////////////////////////////////////

bool skip_C(int i){
  bool skip=false;
  if(i==2||i==3||i==4||i==6||i==8){skip=true;}
  return skip;
}

void convert_index_to_grid(int type,int index,int&ctype,int&cindex){
  if(type==1){
    ctype=0;
    cindex=index;
  } 
  else if(type==2){
    ctype=1;
    if(index==2){cindex=0;}
    else if(index==3){cindex=1;}
    else if(index==4){cindex=2;}
    else if(index==6){cindex=3;}
    else if(index==8){cindex=4;}
  } 
}

int convert_grid_to_index(int type,int index,int&ctype,int&cindex){
  if(type==0){
    ctype=1;
    cindex=index;
  } 
  else if(type==1){
    ctype=2;
    if(index==0){cindex=2;}
    else if(index==1){cindex=3;}
    else if(index==2){cindex=4;}
    else if(index==3){cindex=6;}
    else if(index==4){cindex=8;}
    else{return int(-1);}
  } 
  else{
    return int(-1);
  }
  return int(0);
}

void convert_index(int type,int index,int&ctype,int&cindex){
  // First convert from an A index the crossrelated B one
  int cctype,ccindex;
  if(type==0){cctype=1;} //A->B
  else if(type==2){cctype=type;} //C->C

  if(index==0){ccindex=3;}
  else if(index==1){ccindex=2;}
  else if(index==2){ccindex=1;}
  else if(index==3){ccindex=0;}
  else if(index==4){ccindex=4;}
  else if(index==5){ccindex=6;}
  else if(index==6){ccindex=5;}
  else if(index==7){ccindex=8;}
  else if(index==8){ccindex=7;}

  // Then use correct index to look into the grid
  convert_index_to_grid(cctype,ccindex,ctype,cindex);
}

//////////////////////////////////////////////////////////////////////

#include<cassert>
#include<sys/stat.h>
using namespace std;

bool print_assert(const bool assertion, string message){
  if(assertion){cout<<message<<endl;}
  return !assertion;
}

int load_region(int region,float**E=NULL){
  vector<double>axis0,axis1,axis2,axis3;
  std::pair<double, double> extrap_bounds{-0.5,1.01};
  GridAxis ax0,ax1,ax2,ax3;
  vector<GridAxis>grid(4);

  auto start = std::chrono::high_resolution_clock::now();

  string pref;
  string path=getenv("PWD");
  string path_to_grids=path+"/";
  string grid_dir="TWOLOOP_GRIDS_reg"+to_string(region)+"/";
  struct stat st;
  if(stat(string(path_to_grids+grid_dir).c_str(),&st)!=0){
    // If grids are not in current directory, look one level up
    string path_to_grids_old=path_to_grids;
    int pos=path.find_last_of('/');
    string url="https://wwwth.mpp.mpg.de/members/wieseman/download/codes/WW_MiNNLO/VVamp_interpolation_grids/WW_MiNNLO_2loop_grids.tar.gz";
    path_to_grids=path.substr(0,pos+1);   
    string error_message="\033[31m \n Error while loading grids for the interpolator \n\n The interpolation grids should be downloaded from \033[31;4m"+url+"\033[31;24m! \n "
      +" Since you are running from "+path+",\n make sure to have the grid folders at one of the "
      +" following locations:\n - "+path_to_grids_old+"\n - "+path_to_grids+"\n\n\033[0m";
    assert(print_assert((stat(string(path_to_grids+grid_dir).c_str(),&st)!=0),error_message));
  }  
  pref=path_to_grids+grid_dir+"pwg-2loop-merged-";
  cout<<" load2loopgrids: loading grids into memory \n";

  string line;	      
  double ma2,mb2,s,t;
  static bool first_call=true; // First function call
  static int nlines;
  static int nelements;
  static int nsteps;
  static int iter;
  static int iter_s;
  for(int type=0;type<3;++type){
    for(int index=0;index<9;++index){
      int ctype=type;
      int cindex=index;
      if(interpolator_run_mode=='3'){
	int status=convert_grid_to_index(type,index,ctype,cindex);
	if(status<0){break;}
      }
      string id1=to_string(ctype),id2=to_string(cindex);
      string suff("binary.dat");
      string att("-");
      string filetag=pref+id1+att+id2+att+suff;
      
      ifstream file(filetag.c_str(),ios::binary|ios::in);
      if(file.is_open()){
	if(type==0 && index==0 && first_call){
	  //Estimate size the first time
	  file.seekg(0,ios_base::end);
	  size_t size=file.tellg();
	  file.seekg(0,ios_base::beg);
	  nelements=size/sizeof(double);
	  nlines=nelements/2-2; // Each line contains real and imaginary part
	  nsteps=pow(nlines,0.25)-1;
	  iter_s=(sqrt(sqrt((double)nlines))-1.0);
	  first_call=false;
	  GriddedData::grid_dim=nlines;
	  return nlines;
	}
	vector<double> v(nelements);
	cout<<"\n Loading grids for 2loop type  E["<<id1<<"]["<<id2<<"]\n";
	file.read((char*)&v[0],nelements*sizeof(double));
	file.close();

	int c0=0,c1=0,c2=0,c3=0;
	int kk=-2;
	int kkk=0;
	int kkkk=0;
	for(int j=0; j<=nlines+1; ++j){
	  *(*E+nlines*(2*(index)+18*type)+j)=v[2*j];
	  *(*E+nlines*(2*(index)+18*type)+j+nlines)=v[2*j+1];
	}

      }

    }
  }
  
  //////////////////////////
  // AXES INITIALIZATION  //
  //////////////////////////
  static bool ini=true; // First time 
  if(ini){
    for(int cx=0; cx<=nsteps; ++cx){
      double xx=(double)cx/nsteps;
      axis0.push_back(xx);
      axis1.push_back(xx);
      axis2.push_back(xx);
      axis3.push_back(xx);
    }
    ini=false; //Initialize axes only for the first loading call
    ax0=GridAxis(axis0,Method::CONSTANT,Method::CUBIC,extrap_bounds); //ax, extrp,interp,interp_bound
    ax1=GridAxis(axis1,Method::CONSTANT,Method::CUBIC,extrap_bounds);
    ax2=GridAxis(axis2,Method::CONSTANT,Method::CUBIC,extrap_bounds);
    ax3=GridAxis(axis3,Method::CONSTANT,Method::CUBIC,extrap_bounds);
    grid={ax3,ax2,ax1,ax0};
    GriddedData::grid_axes=grid;
  }

  return 0;
}


void Btwxt_fit(double ma2,double mb2,double s, double t, vector<vector<vector<double> > > &E_ip1){

  double u=ma2+mb2-s-t;
  static bool ini=true;

  static int nlines=load_region(1);
  static int coeff_size;
  static float*E   ;
  static float*E_r2;
  static float*E_r3;
  static float*E_r4;

  if(ini){
    if(interpolator_run_mode=='1'||interpolator_run_mode=='2'){
      coeff_size=2*(9*3)*nlines; // A, B, C types
    }
    else{
      coeff_size=2*(9+5)*nlines; // 9 B types and 5 C types
    }
    E=new float[coeff_size];
    E_r2=new float[coeff_size];
    E_r3=new float[coeff_size];
    E_r4=new float[coeff_size];

    auto start = std::chrono::high_resolution_clock::now();                                                                                                             
     cout<<"\n  ******LOADING REGION 1 ********\n";
     load_region(1,&E);   

    if(interpolator_run_mode=='2'||interpolator_run_mode=='3'){
      cout<<"\n  ******LOADING REGION 2 ********\n";
      load_region(2,&E_r2);
      cout<<"\n  ******LOADING REGION 3 ********\n";
      load_region(3,&E_r3);
      cout<<"\n  ******LOADING REGION 4 ********\n";
      load_region(4,&E_r4);
    }
    auto finish = std::chrono::high_resolution_clock::now();                                                                                                             
    std::chrono::duration<double> elapsed = finish - start;                                                                                                            
    cout << "------------------------------------------------------------"<<endl;
    cout << "Time for loading grids: "<< elapsed.count()<<" s "<<endl;
    cout << "------------------------------------------------------------"<<endl;
    ini=false;

  }

  static RegularGridInterpolator interpolator(GriddedData(2));

  double yy[4],yyu[4];
  vector<double>y(4),yu(4);
  //find y from incoming phsp point
  find_grid_point(ma2,mb2,s,t,yy, 1);
  y[0]=yy[0];
  y[1]=yy[1];
  y[2]=yy[2];
  y[3]=yy[3];
  if(interpolator_run_mode=='3'){
    find_grid_point(ma2,mb2,s,u,yyu, 1);
    yu[0]=yyu[0];
    yu[1]=yyu[1];
    yu[2]=yyu[2];
    yu[3]=yyu[3];
  }


  if(interpolator_run_mode=='1'){
  //  *** RUN MODE 1 ***
    for(int type=0;type<3;++type){ //skip type 3
      for(int index=0;index<9;++index){
	vector<double> result(2);
	GriddedData::value_tables=E+nlines*(2*(index)+18*type);
	result = interpolator.get_values_at_target(y);
	E_ip1[0][type][index]=result[0];                                         
	E_ip1[1][type][index]=result[1];
      }
    }

  }
  else if(interpolator_run_mode=='2'){
  //  *** RUN MODE 2 ***
    vector<double>yyt(4);
    bool reg1=choose_region(ma2,mb2,s,t,y,yyt);

    for(int type=0;type<3;++type){ //skip type 3
      for(int index=0;index<9;++index){
	vector<double> result(2);
	if(y[2]>0.85 && y[3]>0.75 && !reg1){ // REGION 2
	  GriddedData::value_tables=E_r2+nlines*(2*(index)+18*type);
	}
	else if(y[2]<=0.85 && y[3]>0.75 && !reg1){  // REGION 3
	  GriddedData::value_tables=E_r3+nlines*(2*(index)+18*type);
	}
	else if(y[2]>0.85 && y[3]<=0.75 && !reg1){  // REGION 4	
	  GriddedData::value_tables=E_r4+nlines*(2*(index)+18*type);
	}
	else if(y[2]<=0.85 && y[3]<=0.75){ // REGION 1
	  GriddedData::value_tables=E+nlines*(2*(index)+18*type);
	}
	result=interpolator.get_values_at_target(yyt);
	E_ip1[0][type][index]=result[0];
	E_ip1[1][type][index]=result[1];
      }
    }

  }
  else if(interpolator_run_mode=='3'){
  //  *** RUN MODE 3 ***
    vector<double>yyt(4),yyu(4);
    bool reg1t=choose_region(ma2,mb2,s,t,y,yyt);
    bool reg1u=choose_region(ma2,mb2,s,u,yu,yyu);

    for(int type=1;type<3;++type){ //interpolate B and C 
      for(int index=0;index<9;++index){
	int ctype,cindex;
	if(type==2 && !skip_C(index)){continue;}
	else{convert_index_to_grid(type,index,ctype,cindex);}
	vector<double> result(2);
	if(y[2]>0.85 && y[3]>0.75 && !reg1t){ // REGION 2
	  GriddedData::value_tables=E_r2+nlines*(2*(cindex)+18*ctype);
	}
	else if(y[2]<=0.85 && y[3]>0.75 && !reg1t){  // REGION 3
	  GriddedData::value_tables=E_r3+nlines*(2*(cindex)+18*ctype);
	}
	else if(y[2]>0.85 && y[3]<=0.75 && !reg1t){  // REGION 4	
	  GriddedData::value_tables=E_r4+nlines*(2*(cindex)+18*ctype);
	}
	else if(y[2]<=0.85 && y[3]<=0.75){ // REGION 1
	  GriddedData::value_tables=E+nlines*(2*(cindex)+18*ctype);
	}
	result =interpolator.get_values_at_target(yyt);
	E_ip1[0][type][index]=result[0];
	E_ip1[1][type][index]=result[1];
      }
    }

    for(int type=0;type<3;++type){ //interpolate A and C 
      if(type==1){continue;}
      for(int index=0;index<9;++index){ 
	int ctype,cindex;
	if((type==0)||(type==2 && !skip_C(index))){convert_index(type,index,ctype,cindex);}
	else{continue;}
	vector<double> result(2);
	if(yu[2]>0.85 && yu[3]>0.75 && !reg1u){ // REGION 2
	  GriddedData::value_tables=E_r2+nlines*(2*(cindex)+18*ctype);
	}
	else if(yu[2]<=0.85 && yu[3]>0.75 && !reg1u){  // REGION 3
	  GriddedData::value_tables=E_r3+nlines*(2*(cindex)+18*ctype);
	}
	else if(yu[2]>0.85 && yu[3]<=0.75 && !reg1u){  // REGION 4	
	  GriddedData::value_tables=E_r4+nlines*(2*(cindex)+18*ctype);
	}
	else if(yu[2]<=0.85 && yu[3]<=0.75){ // REGION 1
	  GriddedData::value_tables=E+nlines*(2*(cindex)+18*ctype);
	}
	result =interpolator.get_values_at_target(yyu);
	E_ip1[0][type][index]=-result[0];
	E_ip1[1][type][index]=-result[1];
      }
    }

  }

  // Set type F_V
  E_ip1[0][3][0]=  0.;
  E_ip1[1][3][0]=  0.;
  E_ip1[0][3][1]=  0.;
  E_ip1[1][3][1]=  0.;
  E_ip1[0][3][2]=  0.;
  E_ip1[1][3][2]=  0.;
  E_ip1[0][3][3]=  0.;
  E_ip1[1][3][3]=  0.;
  E_ip1[0][3][4]=  -67.67805329;
  E_ip1[1][3][4]=  +217.8110789;
  E_ip1[0][3][5]=  +67.67805329;
  E_ip1[1][3][5]=  -217.8110789;
  E_ip1[0][3][6]=  +67.67805329;
  E_ip1[1][3][6]=  -217.8110789;
  E_ip1[0][3][7]=  -67.67805329;
  E_ip1[1][3][7]=  +217.8110789;
  E_ip1[0][3][8]=  -67.67805329;
  E_ip1[1][3][8]=  +217.8110789;
  
}

