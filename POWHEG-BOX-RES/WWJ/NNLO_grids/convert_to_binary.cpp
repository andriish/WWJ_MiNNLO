#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include<stdio.h>
#include<stdlib.h> 
#include<string.h>
#include<cassert>
#include<sys/stat.h>
using namespace std;

bool print_assert(const bool assertion, string message){
  if(assertion){cout<<message<<endl;}
  return !assertion;
}

void convert_grid(int region){
  string pref;
  string path=getenv("PWD");
  string path_to_grids=path+"/";
  string grid_dir="TWOLOOP_GRIDS_reg"+to_string(region)+"/";
  struct stat st;
  if(stat(string(path_to_grids+grid_dir).c_str(),&st)!=0){
    // If grids are not in the current directory, look one level up
    string path_to_grids_old=path_to_grids;
    int pos=path.find_last_of('/');
    path_to_grids=path.substr(0,pos+1);   
    string error_message="\033[31m \n Error while loading grids for binary conversion \n\n The interpolation grids should be generated using the executable computegrids (do 'make computegrids' \n from the directory of the process) and merged using the script merge.sh in NNLO_grids/COMPUTE_GRIDS/! \n Since you are running from "+path+",\n make sure to have the grid folders at one of the following locations:\n - "+path_to_grids_old+"\n - "+path_to_grids+"\n\n\033[0m";
    assert(print_assert((stat(string(path_to_grids+grid_dir).c_str(),&st)!=0),error_message));
  }  
  pref=path_to_grids+grid_dir+"pwg-2loop-merged-";

  int nlines;
  for(int type=0;type<3;++type){ 
    for(int index=0;index<9;++index){
      string id1=to_string(type),id2=to_string(index);
      string suff(".dat");
      string att("-");
      string filetag=pref+id1+att+id2+suff;
      ifstream file;
      file.open(filetag,ios::in);
      if(file.is_open()){
	if(type==0 && index==0){
	  // Count lines the first time
	  nlines=0;
	  char str[500];
	  while(file.peek()!=EOF){
	    file.getline(str,500);
	    nlines+=1;
	  }
	  file.seekg(0,ios_base::beg);
	}
	cout<<"\n Converting grids for 2loop type  E["<<id1<<"]["<<id2<<"]\n";
	string bsuff("binary");
	string bfiletag=pref+id1+att+id2+att+bsuff+suff;
	ofstream bfile(bfiletag,ios::binary|ios::out);
	for(int j=0; j<=nlines+1; ++j){
	  string line;
	  vector<double>x(4);
	  double Ert,Eit;
	  getline(file,line);
	  istringstream iss(line);
	  iss>>x[0]>>x[1]>>x[2]>>x[3]>>Ert>>Eit;
	  bfile.write((char*)&Ert,sizeof(double));
	  bfile.write((char*)&Eit,sizeof(double));
	}
	bfile.close();
      }
      file.close();
    }
  }

}

int main(){
  for(int i=1;i<=4;++i){convert_grid(i);}
}
