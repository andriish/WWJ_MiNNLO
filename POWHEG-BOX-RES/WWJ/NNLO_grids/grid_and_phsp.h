template <class T>T read_input(const char* key, int &status);

void find_grid_point(double ma2, double mb2, double s, double t, double (&y)[4], int reg);
void find_phsp_point(long double y[4], double &ma2, double &mb2, double &s, double &t, int reg);

void restart(int cx1, int cx2, const int steps, int &ii_cx3, int &ii_cx4);
