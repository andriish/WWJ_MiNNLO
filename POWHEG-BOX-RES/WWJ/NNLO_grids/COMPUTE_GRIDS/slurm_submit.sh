#!/bin/bash

min=1
max=153

if [[ $max -eq 153]]; then
    JOB_SCRIPT=./grid_run_17.sh
elif [[ $max -eq 17 ]]; then
    JOB_SCRIPT=./grid_run_153.sh
fi

JOBID=$(sbatch --array=$min-$max:1 ${JOB_SCRIPT} 2>&1 | awk '{print $(NF)}')
STRING=" Submitting job array ${JOBID} containing  ${max} jobs "
echo $STRING
