#!/bin/bash

for i in {0..3} ; do

    for j in {0..8} ; do
	merged="pwg-2loop-merged-${i}-${j}.dat"
	echo " "
	echo "Creating $merged file ... "
	size=$({ find . -maxdepth 1 -type f -name "pwg-2loop-*[0-9]-*[0-9]-${i}-${j}.dat" -printf '%s+'; echo 0;} | bc);	
	fallocate -l "$size" $merged
	find . -maxdepth 1 -type f -name "pwg-2loop-*[0-9]-*[0-9]-${i}-${j}.dat" -print0 | sort -zV | xargs -r0 cat 1<> $merged
	echo "... done"
    done

done
