      subroutine init_couplings
      implicit none
      include "coupl.inc"
      include 'PhysPars.h'
      include "pwhg_par.h"
      include "minnlo_flg.h"
      include "pwhg_flg.h"
      include "pwhg_physpar.h"

      integer j
      real * 8 powheginput
      external powheginput
c Avoid multiple calls to this subroutine. The parameter file is opened
c but never closed ...
      logical called
      data called/.false./
      save called
      if(called) then
         return
      else
         called=.true.
      endif
 
      ! there can be issues, especially when having Born cuts, when the 
      ! first 100 times the amplitude is zero, then it will set the whole
      ! amplitude falsely to zero; the following flag prevents this
      flg_cachediscardzeroamps = .false.

c     For V+J, the use of flg_doublefsr is recommended.
c     We make it the default, at least when minnlo is used.
      flg_doublefsr=.true.
      if(powheginput("#doublefsr").eq.0) flg_doublefsr=.false.      
      
      if(powheginput("#par_isrtinycsi").gt.0) 
     $     par_isrtinycsi = powheginput("#par_isrtinycsi")

      if(powheginput("#par_fsrtinycsi").gt.0) 
     $     par_fsrtinycsi = powheginput("#par_fsrtinycsi")

      if(powheginput("#par_isrtinyy").gt.0) 
     $     par_isrtinyy = powheginput("#par_isrtinyy")

      if(powheginput("#par_fsrtinyy").gt.0) 
     $     par_fsrtinyy = powheginput("#par_fsrtinyy")

!     1d-10 seems not to generate instability and it is enough                                              
!     for limits to work. Anyway, this very tiny values should just
!     be used for debugging. So, the verytinypars should be better 
!     replaced by the usage of tinyForLims 1d-10 in the powheg.input
      if(powheginput("#verytinypars").eq.1) then
         par_isrtinycsi = 1d-10
         par_isrtinyy = 1d-10
         par_fsrtinycsi = 1d-10
         par_fsrtinyy = 1d-10
      endif

      flg_minlo=.not.(powheginput('#minlo').eq.0)
      flg_minnlo=powheginput('#minnlo').eq.1
      flg_uubornonly=powheginput('#uubornonly').eq.1

c     If minlo is not present in the input card, but minnlo is                     
c     then set also flg_minlo to true, so that we are sure all is done             
c     properly in setlocalscales and for all other occurencies of                  
c     flg_minlo.                                                                   
      if(flg_minnlo) flg_minlo=.true.


*********************************************************
***********         MADGRAPH                 ************
*********************************************************
c Parameters are read from the MadGraph param_card.dat,
c except the strong coupling constant, which is defined
c somewhere else
      call setpara("param_card.dat",.true.)

      call madtophys

c$$$      physpar_ml(1)=0.511d-3
c$$$      physpar_ml(2)=0.1057d0
c$$$      physpar_ml(3)=1.777d0
c$$$      physpar_mq(1)=0.33d0     ! up
c$$$      physpar_mq(2)=0.33d0     ! down
c$$$      physpar_mq(3)=0.50d0     ! strange
c$$$      physpar_mq(4)=1.50d0     ! charm
c$$$      physpar_mq(5)=4.80d0     ! bottom
      call golem_initialize

c adapt to res framework -->
c set up masses and widths for resonance damping factors
      physpar_pdgmasses(24) = ph_Wmass
      physpar_pdgmasses(23) = ph_Zmass
      physpar_pdgmasses(5) = ph_bmass
      physpar_pdgmasses(6) = ph_tmass
      physpar_pdgmasses(22) = 0
      physpar_pdgwidths(24) = ph_Wwidth
      physpar_pdgwidths(23) = ph_Zwidth
      physpar_pdgwidths(6) = ph_twidth
      physpar_pdgwidths(22) = 0

      do j=1,physpar_npdg
         physpar_pdgmasses(-j) = physpar_pdgmasses(j)
         physpar_pdgwidths(-j) = physpar_pdgwidths(j)
      enddo

      physpar_phspmasses = physpar_pdgmasses
      physpar_phspwidths = physpar_pdgwidths

c$$$      physpar_phspmasses(6) = ph_tmass_phsp
c$$$      physpar_phspmasses(-6) = ph_tmass_phsp
c <-- adapt to res framework
! to avoid problems at event generation
      physpar_mq(4)= 1.40d0
      physpar_mq(5)= 4.92d0

      end


      subroutine lh_readin(param_name)
c overrides the lh_readin subroutine in MODEL/couplings.f;
c to make it work, rename or delete
c the lh_readin routine in MODEL/couplings.f
      implicit none
      character*(*) param_name
      include 'coupl.inc'
      include 'PhysPars.h'
      double precision  Two, Four, Rt2, Pi
      parameter( Two = 2.0d0, Four = 4.0d0 )
      parameter( Rt2   = 1.414213562d0 )
      parameter( Pi = 3.14159265358979323846d0 )

c     Common to lh_readin and printout
      double precision  alpha, gfermi, alfas, ewscheme
      double precision  mtMS,mbMS,mcMS,mtaMS!MSbar masses
      double precision  Vud,Vus,Vub,Vcd,Vcs,Vcb,Vtd,Vts,Vtb !CKM matrix elements
      common/values/    alpha,gfermi,alfas,
     &                  mtMS,mbMS,mcMS,mtaMS,
     &                  Vud,Vus,Vub,Vcd,Vcs,Vcb,Vtd,Vts,Vtb
c
      real * 8 powheginput
      external powheginput
c the only parameters relevant for this process are set
c via powheginput. All others are needed for the
c madgraph routines not to blow.

c$$$      alpha = powheginput('#alpha')
c$$$      if(powheginput('#alpha').lt.0d0) alpha = 1d0/132.167955100775d0 
      gfermi = powheginput('#gfermi')
      if(powheginput('#gfermi').lt.0d0) gfermi = 1.16639d-5
      alfas = 0.119d0
      zmass = powheginput('#zmass')
      if(powheginput('#zmass').lt.0d0) zmass = 91.18760d0  
      wmass = powheginput('#wmass')
      if(powheginput('#wmass').lt.0d0) wmass = 80.385d0
      tmass = powheginput('#tmass')
      if(powheginput('#tmass').lt.0d0) tmass = 173.2d0
      hmass = powheginput('#hmass')
      if(powheginput('#hmass').lt.0d0) hmass = 125d0
      bmass = powheginput('#bmass')
      if(powheginput('#bmass').lt.0d0) bmass = 4.92d0
      lmass = 0d0
      mcMS = 0d0
      mbMS = 0d0
      mtMS = 0d0 
      mtaMS = 0d0
      cmass = 0d0
      lmass = 0d0

      zwidth = powheginput('#zwidth')
      if(powheginput('#zwidth').lt.0d0) zwidth=2.4952d0
      wwidth = powheginput('#wwidth')
      if(powheginput('#wwidth').lt.0d0) wwidth=2.0854d0
      twidth = powheginput('#twidth')
      if(powheginput('#twidth').lt.0d0) twidth=1.442620d0
      hwidth = powheginput('#hwidth')
      if(powheginput('#hwidth').lt.0d0) hwidth = 0.407d-2

      ewscheme=powheginput('#ewscheme')
      if (ewscheme.lt.0) ewscheme = 1
      if (ewscheme.eq.1) then ! MW, MZ, Gmu scheme
!         if (powheginput("#complexGFermi").eq.0) then
         alpha=sqrt(2d0)/pi*gfermi*abs(wmass**2*(1d0-(wmass/zmass)**2))
! MW: complex masses not allowed in Gosam, in principle one could evaluate
!     alpha in principle with complex masses, but we don't provide that option
c$$$         else
c$$$            cwmass2=DCMPLX(wmass**2,-wwidth*wmass)
c$$$            czmass2=DCMPLX(zmass**2,-zwidth*zmass)
c$$$            alpha=sqrt(2d0)/pi*gfermi*abs(cwmass2*(1d0-cwmass2/czmass2))
c$$$         endif
      else ! MW, MZ, alpha scheme
         write(*,*) 'ERROR: Only Gmu scheme (ewscheme=1) tested so far.'
         stop
!         if (powheginput("#complexGFermi").eq.0) then
         gfermi=alpha/sqrt(2d0)*pi/(1d0-(wmass/zmass)**2)/wmass**2
c$$$         else
c$$$            cwmass2=DCMPLX(wmass**2,-wwidth*wmass)
c$$$            czmass2=DCMPLX(zmass**2,-zwidth*zmass)
c$$$            gfermi=alpha/sqrt(2d0)*pi/abs((1d0-cwmass2/czmass2)*cwmass2)
c$$$         endif
      endif
      

c     POWHEG CKM matrix
c
c        d     s     b
c    u
c    c
c    t

      !Vud=0.97428d0
      !Vus=0.2253d0
      !Vub=1d-10
      !Vcd=-0.2252d0
      !Vcs=0.97345d0
      !Vcb=1d-10
      !Vtd=1d-10
      !Vts=1d-10
      !Vtb=1d-10
      !
      !Vud=  0.97462d0
      !Vus=  0.2253d0
      !Vub=  1d-10
      !Vcd= -0.2253d0
      !Vcs=  0.97462d0
      !Vcb=1d-10
      !Vtd=1d-10
      !Vts=1d-10
      !Vtb=1d-10

      Vud=1d0
      Vus=0d0 !1d-10
      Vub=0d0 !1d-10
      Vcd=0d0 !1d-10
      Vcs=1d0
      Vcb=0d0 !1d-10
      Vtd=0d0 !1d-10
      Vts=0d0 !1d-10
      Vtb=1d0

c     !: for wwj, in the approximations we are working in, we can use a
c     diagonal CKM matrix. The result for the production cross section
c     is the same as if we had used a generic CKM. This is due to the
c     unitary of this matrix. The only place in the code where one needs
c     to really use the "true" CKM matrix is when generating
c     semileptonic or hadronic decays (see finalize_lh). randomizeckm
c     takes care of that.

      end

      subroutine set_ebe_couplings
      implicit none
      include 'pwhg_st.h'
      include 'pwhg_math.h'
      include "coupl.inc"
      logical ini
      save ini
      data ini/.true./
c QCD coupling constant
      G=sqrt(st_alpha*4d0*pi)
      GG(1)=-G
      GG(2)=-G

c HEFT coupling
      gh(1) = cmplx( g**2/4d0/PI/(3d0*PI*V), 0d0)
      gh(2) = cmplx( 0d0                   , 0d0)
      ga(1) = cmplx( 0d0                   , 0d0)
      ga(2) = cmplx( g**2/4d0/PI/(2d0*PI*V), 0d0)
      gh4(1) = G*gh(1)
      gh4(2) = G*gh(2)
      ga4(1) = G*ga(1)
      ga4(2) = G*ga(2)

      if(ini) then
         write(*,*) '-----------------------------'
         write(*,*) 'muF = ',sqrt(st_mufact2)
         write(*,*) 'muR,alphas = ',sqrt(st_muren2),st_alpha
         write(*,*) '-----------------------------'
         ini=.false.
      endif

      return
      end


      subroutine madtophys
      implicit none
      include 'coupl.inc'
      include 'PhysPars.h'
      include 'pwhg_math.h'
      real * 8 e_em,g_weak
c
c     Common to lh_readin and printout
c
      double precision  alpha, gfermi, alfas
      double precision  mtMS,mbMS,mcMS,mtaMS!MSbar masses
      double precision  Vud,Vus,Vub,Vcd,Vcs,Vcb,Vtd,Vts,Vtb !CKM matrix elements
      common/values/    alpha,gfermi,alfas,
     &                  mtMS,mbMS,mcMS,mtaMS,
     &                  Vud,Vus,Vub,Vcd,Vcs,Vcb,Vtd,Vts,Vtb

      e_em=gal(1)
      ph_unit_e=e_em
      ph_alphaem=e_em**2/(4*pi)
      ph_sthw2=1-(wmass/zmass)**2
      ph_sthw=sqrt(ph_sthw2)
      g_weak=e_em/ph_sthw
      ph_gfermi=sqrt(2d0)*g_weak**2/(8*wmass**2)

      ph_Zmass = zmass
      ph_Wmass = wmass
      ph_Hmass = hmass
      ph_Zwidth = zwidth
      ph_Wwidth = wwidth
      ph_Hwidth = hwidth
      ph_tmass = tmass
      ph_bmass = bmass

      ph_WmWw = ph_Wmass * ph_Wwidth
      ph_ZmZw = ph_Zmass * ph_Zwidth
      ph_Wmass2 = ph_Wmass**2
      ph_Zmass2 = ph_Zmass**2

c     CKM from PDG 2010 (eq. 11.27)
      ph_CKM(1,1)=Vud
      ph_CKM(1,2)=Vus
      ph_CKM(1,3)=Vub
      ph_CKM(2,1)=Vcd
      ph_CKM(2,2)=Vcs
      ph_CKM(2,3)=Vcb
      ph_CKM(3,1)=Vtd
      ph_CKM(3,2)=Vts
      ph_CKM(3,3)=Vtb

      write(*,*) '----------------------------------------'
      write(*,*) 'mw,gammaw ',ph_wmass,ph_wwidth
      write(*,*) 'mz,gammaz ',ph_zmass,ph_zwidth
      write(*,*) 'aeminv,sthw2,gweak,gfermi ',1./ph_alphaem,ph_sthw2,g_weak,ph_gfermi
      !write(*,*) 'CKM matrix'
      !write(*,*) ph_ckm(1,:)
      !write(*,*) ph_ckm(2,:)
      !write(*,*) ph_ckm(3,:)
      write(*,*) '----------------------------------------'
      
      end


      
C     initializes all the couplings in the  virtual, code
C     generated by GoSam and sets them equal to the
C     ones defined in the POWHEG BOX.              
      subroutine golem_initialize
      implicit none
      include 'PhysPars.h'
      include 'pwhg_st.h'
      include 'pwhg_rnd.h'
      integer ierr
      integer ioerr
      character * 20 param
      character * 20 value
      character * 50 line
      character * 29 path
      real * 8 powheginput
      external powheginput
      integer parallelstage,rndiwhichseed
      common/cpwhg_info/parallelstage,rndiwhichseed

      rndiwhichseed=rnd_iwhichseed
      parallelstage=powheginput('#parallelstage')
      
C     Parameter definition

      param = 'Nf='
      write(value,'(I1)') st_nlight
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

      param = 'alpha='
      write(value,'(F20.10)') ph_alphaem
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

      param = 'GF='
      write(value,'(F20.10)') ph_gfermi
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

      param = 'mZ='
      write(value,'(F20.10)') ph_Zmass
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

      param = 'mW='
      write(value,'(F20.10)') ph_Wmass
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)
      
      
      param = 'mH='
      write(value,'(F20.10)') ph_Hmass
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

      param = 'mC='
      write(value,'(F20.10)') ph_cmass
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

      param = 'mB='
      write(value,'(F20.10)') ph_bmass
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

c$$$      param = 'mBMS='
c$$$      write(value,'(F20.10)') ph_bmass
c$$$      line = trim(param)//trim(adjustl(value))//char(0)
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)

      param = 'mT='
      write(value,'(F20.10)') ph_tmass
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

c$$$      param = 'me='
c$$$      write(value,'(F20.10)') ph_emass
c$$$      line = trim(param)//trim(adjustl(value))//char(0)
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$
c$$$      param = 'mmu='
c$$$      write(value,'(F20.10)') ph_mumass
c$$$      line = trim(param)//trim(adjustl(value))//char(0)
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$
c$$$      param = 'mtau='
c$$$      write(value,'(F20.10)') ph_taumass
c$$$      line = trim(param)//trim(adjustl(value))//char(0)
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)

      param = 'wZ='
      write(value,'(F20.10)') ph_Zwidth
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

      param = 'wW='
      write(value,'(F20.10)') ph_Wwidth
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

      param = 'wH='
      write(value,'(F20.10)') ph_hwidth
      line = trim(param)//trim(adjustl(value))//char(0)
      call OLP_Option(line,ierr)
      call check_gosam_err(param,ierr)

c$$$      param = 'wB='
c$$$      write(value,'(F20.10)') ph_bwidth
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$
c$$$      param = 'wT='
c$$$      write(value,'(F20.10)') ph_twidth
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$
c$$$      param = 'wtau='
c$$$      write(value,'(F20.10)') ph_tauwidth
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$
c$$$      param = 'VUD='
c$$$      write(value,'(F20.10)') ph_CKM(1,1)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$
c$$$      param = 'CVDU='
c$$$      write(value,'(F20.10)') ph_CKM(1,1)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$
c$$$      param = 'VUS='
c$$$      write(value,'(F20.10)') ph_CKM(1,2)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'CVSU='
c$$$      write(value,'(F20.10)') ph_CKM(1,2)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'VUB='
c$$$      write(value,'(F20.10)') ph_CKM(1,3)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'CVBU='
c$$$      write(value,'(F20.10)') ph_CKM(1,3)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'VCD='
c$$$      write(value,'(F20.10)') ph_CKM(2,1)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'CVDC='
c$$$      write(value,'(F20.10)') ph_CKM(2,1)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'VCS='
c$$$      write(value,'(F20.10)') ph_CKM(2,2)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'CVSC='
c$$$      write(value,'(F20.10)') ph_CKM(2,2)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'VCB='
c$$$      write(value,'(F20.10)') ph_CKM(2,3)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'CVBC='
c$$$      write(value,'(F20.10)') ph_CKM(2,3)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'VTD='
c$$$      write(value,'(F20.10)') ph_CKM(3,1)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'CVDT='
c$$$      write(value,'(F20.10)') ph_CKM(3,1)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'VTS='
c$$$      write(value,'(F20.10)') ph_CKM(3,2)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'CVST='
c$$$      write(value,'(F20.10)') ph_CKM(3,2)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'VTB='
c$$$      write(value,'(F20.10)') ph_CKM(3,3)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
c$$$      
c$$$      param = 'CVBT='
c$$$      write(value,'(F20.10)') ph_CKM(3,3)
c$$$      line = trim(param)//trim(adjustl(value))
c$$$      call OLP_Option(line,ierr)
c$$$      call check_gosam_err(param,ierr)
      
C     Initialize virtual code
      
      path = '../GoSam_POWHEG/orderfile.olc'
      
      call OLP_Start(path,ioerr,parallelstage,rndiwhichseed)
      call check_gosam_err('olp_start routine',ierr)
      end
      
      
      subroutine check_gosam_err(param,ierr)
      implicit none
      character *(*) param
      integer ierr
      if (ierr.lt.0) then
         write(*,*)
     $        'GoSam '//param(1:len_trim(param))// ' reports an error.'
         write(*,*) 'The POWHEG BOX aborts'
         call exit(1)
      endif
      end
