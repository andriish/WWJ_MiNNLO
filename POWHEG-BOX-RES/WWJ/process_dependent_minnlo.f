      function get_nf_for_process()
      implicit none
      integer get_nf_for_process, nf
      nf = 4
      get_nf_for_process = nf
      end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!      DUMMY 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
      function impose_born_cut(p)
      implicit none
      logical impose_born_cut
      real * 8 p(0:3,6)
      impose_born_cut = .false.
      end

      ! This subroutine provides cuts for the projected phase space, when there are be                                                                         
      ! QED divergences in the matrix elements. At large pT those cuts are imposed on                                                                          
      ! contributions beyond accuracy. At low pT these or stronger cuts are already in                                                                         
      ! the analysis.                                                                                                                                           
      subroutine cuts_on_uuborn(p_uub,p_ub,cuts_passed)
      implicit none
      include 'nlegborn.h'
      integer, parameter :: nlegs=nlegbornexternal-1
      real*8 p_uub(0:3,nlegs),p_ub(0:3,nlegs)
      logical cuts_passed
      real*8 pt_gamma
      real*8 pt_gamma_ub
      real*8 pt_a_cut
      real*8 dotp, constant
      external dotp
      integer, save :: counter_cuts_on_uuborn=0
      real*8, save :: pt_gamma_sav=0d0
      cuts_passed=.true.
      end
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

      subroutine init_amplitude()
      implicit none
      external MATRIX_init_parameters
      include 'brinclude.h'
      include 'PhysPars.h'
      include 'pwhg_st.h'
      character*20 process_class
      integer nf_light
      logical cms
      logical select_MCFM
      real*8 powheginput
      external powheginput
      write(*,*)
      write(*,*) " ********* MiNNLO_PS ********** "
      write(*,*) " Initializing MATRIX parameters "
      write(*,*) " for the computation of the     "
      write(*,*) " 2-loop amplitude ...           "
      write(*,*) " ****************************** "
      write(*,*)
      process_class="pp-emmupvmve~+X" 
      nf_light=st_nlight
      cms=.true.
      ! initialize MCFM parameters
      if ( powheginput('#use_MCFM') .lt. 0 ) then
         select_MCFM = .true.
      else
         select_MCFM = powheginput('#use_MCFM') .eq. 1
      endif

      if(select_MCFM) cms=.false.

      call MATRIX_init_parameters(trim(process_class)//char(0),ph_Zmass,ph_Zwidth,   
     &     ph_Wmass,ph_Wwidth,ph_alphaem,br_nlegborn,nf_light,cms)          
      end

      subroutine init_process_dependent()
      implicit none
      real*8 bound
      character(1) b_exp
      common/MiNNLO_outliers/bound,b_exp
      
      ! Upper bound for values of res(j) for them to be consider  
      ! as anomalous ones
      bound=1d-4
      b_exp="4"
      
      ! Give an initial value for the Dterms 
      call init_masses_Dterms()
      end
         
      subroutine init_masses_Dterms()
      use internal_parameters, pi_hoppet => pi, cf_hoppet => cf, ca_hoppet => ca, tf_hoppet => tf
      implicit none
      include 'PhysPars.h'
      call set_masses(ph_Zmass)
      end

c DUMMY routine for bmass used in setlocalscales.f for Higgs
      subroutine bmass_in_minlo(bfact,alphas)
      implicit none
      double precision bfact, alphas, dummy
      dummy = 1337
      end

      function get_M_for_init_Dterms()
      implicit none
      double precision get_M_for_init_Dterms
      include 'PhysPars.h'
      include 'nlegborn.h'
      include 'pwhg_kn.h'
      include 'pwhg_flst.h'
      include 'pwhg_flg.h'
      integer nres,i
      logical ini,nores
      data ini/.true./
      save ini,nores,nres
      real * 8 powheginput
      external powheginput
      real*8 p(0:3)

      if (ini) then
         nores=powheginput('#nores').eq.1
         if(nores) then 
            nres=0
         else
            nres=nlegborn-nlegbornexternal
         endif
         ini=.false.
      endif

      p = 0d0 
      do i=(2+nres)+1,nlegborn
c     the sequence of colourless particles is unchanged in the Born and in the real flavour list
         if (abs(flst_born(i,1)).gt.6) then
            if(flg_minlo_real) then
               p(:)=p(:) + kn_cmpreal(:,i)
            else
               p(:)=p(:) + kn_cmpborn(:,i)
            endif
         endif
      enddo
      get_M_for_init_Dterms = sqrt(abs(p(0)**2-p(1)**2-p(2)**2-p(3)**2))
C     if flst_born is zero (1st call before before initialization of momenta)
C     then just set the scale to zmass 
      if (get_M_for_init_Dterms == 0d0) get_M_for_init_Dterms = ph_zmass 
      end

      subroutine get_uub_flav(flav,uub_flav)
      implicit none
      include 'nlegborn.h'
      integer flav(1:nlegborn)
      integer uub_flav(1:nlegbornexternal-1)
      uub_flav=0
      if(flav(1).ne.0)then
         uub_flav(1)=flav(1)
         uub_flav(2)=-uub_flav(1)
      elseif(flav(2).ne.0)then
         uub_flav(2)=flav(2)
         uub_flav(1)=-uub_flav(2)
      endif
      end

      ! This subroutine provides cuts for the projected phase space (using the technical
      ! or generation cuts, according to the value of the variable use_analysis_cuts, to 
      ! set by hand). This suborutine is used if we are debugging the phsp, the Born ME
      ! or the D3 terms by using the proper flags (which are enable by flg_uubornonly).
      ! This subroutine is called in setlocalscales, for ME and D3 debug, and in evaluubjacob,
      ! when performing phsp debug
      subroutine uub_phasespace_check(p_uub,makecuts)
      implicit none
      include 'nlegborn.h'
      integer, parameter :: nlegs=nlegbornexternal-1
      real*8 p_uub(0:3,nlegs)
      logical makecuts
      real*8 invmass,pt_gamma,deltaR_gamep,deltaR_gamem
      real*8,save:: pt_a_cut,deltaR_lepg_cut,invmass_cut
      real*8 dotp,powheginput
      external dotp,powheginput
      include 'pwhg_math.h'
      logical,save :: ini=.true.,nores
      integer,save::nres
      real*8,save::  rap_a_cut,
     &     pt_lept_cut,
     &      rap_lept_cut
      real*8 etaep,etaem,pt_ep,pt_em,etagam,
     &   deltaetagamep,
     &   deltaphigamep,
     &   deltaetagamem,
     &   deltaphigamem
      logical,save:: use_analysis_cuts=.true. ! DEFAULT .false.

      if(ini) then
         if(powheginput("#nores") == 1) then
            nores = .true.
            nres=0
         else
            nores = .false.
            nres=nlegborn-nlegbornexternal
         endif
         ini=.false.
      endif
            
      write(*,*) " Subroutine uub_phasespace_check, to be used for  "
      write(*,*) " debugging, has not been implemented for MINNLOPS"
      write(*,*) " WWJ ... stopping program"
      stop
      
      end


      subroutine get_B_V1_V2(pborn_UUB,msqB,msqV1,msqV2,nflav)
      use MiNLOdebug
      implicit none
      include 'nlegborn.h'
      include 'pwhg_st.h'
      include 'PhysPars.h'
      interface
         subroutine M2_qqbar(pphy,msqb,msqv1,msqv2,nflav)
         implicit none
         include 'nlegborn.h'
         integer, parameter :: nlegs=nlegbornexternal-1
         integer, intent(in) :: nflav
         real *8 pphy(0:3,nlegs)
         double precision,dimension(-nflav:nflav,-nflav:nflav):: msqB, msqV1
         double precision,dimension(-nflav:nflav,-nflav:nflav),optional::msqV2
         integer,save::ini=.true.
         end subroutine M2_qqbar
      end interface
      integer, parameter :: nlegs=nlegbornexternal-1
      integer,intent(in) :: nflav
      double precision,dimension(-nflav:nflav,-nflav:nflav):: msqB, msqV1
      double precision,dimension(-nflav:nflav,-nflav:nflav),optional::msqV2
      double precision pborn_UUB(0:3,nlegs)

      if(uubornonlyD1) then
         ! This scale is just for testing, but is                                                                                 
         ! process dependent                                                                                   
         scaleDtest=2*ph_Wmass
      endif

      msqB(:,:)  = 0d0
      msqV1(:,:) = 0d0
      if(present(msqv2))then
         msqV2(:,:) = 0d0
         call M2_qqbar(pborn_UUB,msqB,msqV1,msqV2=msqV2,nflav=nflav)
      else
         call M2_qqbar(pborn_UUB,msqB,msqV1,nflav=nflav)
      endif
      end


c     matrix elements to test UUB projection (blank for DY)                                                                   
      subroutine uub_for_minnlo(pphy,iborn,amp2)
      implicit none
      include 'pwhg_math.h'
      include 'pwhg_st.h'
      include 'PhysPars.h'
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      real* 8 amp2
      integer nlegs, nflav
      parameter (nlegs=4)
      integer j, k, bflav(1:nlegs), iborn
      real *8 pphy(0:3,nlegs), s, s34
      parameter (nflav=4)
      double precision msqb(-nflav:nflav,-nflav:nflav), msqV1(-nflav:nflav,-nflav:nflav), msqV2(-nflav:nflav,-nflav:nflav)    
      real *8, parameter :: zeta3=1.2020569031595942853997381615114d0
      write(*,*) 'uub_for_minnlo: not coded for WW production'
      call exit(-1)
      end



      subroutine M2_qqbar(pphy,msqb,msqv1,msqv2,nflav)
      use MiNLOdebug
      use MiNLO_cache
      use iso_c_binding, only: c_double
      implicit none
      include 'pwhg_math.h'
      include 'pwhg_st.h'
      include 'PhysPars.h'
      include 'pwhg_flst.h'
      include 'pwhg_flg.h'
      include 'pwhg_res.h'
      include 'pwhg_kn.h'
      integer,intent(in):: nflav
      integer, parameter :: nlegs_uub=nlegbornexternal-1
      integer i,j, k, bflav(1:nlegs_uub),kk,jj
      real *8 pphy(0:3,nlegs_uub), s, s34
      double precision,dimension(-nflav:nflav,-nflav:nflav):: msqb,msqV1
      double precision,dimension(-nflav:nflav,-nflav:nflav),optional::msqV2
      double precision,allocatable,dimension(:,:),save:: Id
      real *8, parameter :: zeta3=1.2020569031595942853997381615114d0
      real *8 zeta2,Polygamma21
      real*8 saved_stmuren2,dotp
      external dotp
      external MATRIX_update_kinematics
      external MATRIX_2loop
      
      real*8,dimension(-nflav:nflav,-nflav:nflav)::H1,H1_qT
      real*8,dimension(-nflav:nflav,-nflav:nflav)::H2,H2_qT
      real*8,save :: b0,Kg,DeltaI1,C1,C2
      logical, save :: ini=.true.
      
      logical nores
      integer nres
      save nores,nres
      real * 8 powheginput
      external powheginput
      logical,save :: select_MCFM

      integer mode,use_mcfm
      common/rescue_2loop/mode,use_mcfm
      integer index

      logical rescue_system
      logical,save:: test_interpolator=.false.
      
      logical,save:: use_interpolator

      if(ini)then
         b0=(11*ca-4d0*nflav/2d0)/6d0
         zeta2=pi**2/6d0
         Polygamma21=-2d0*zeta3
         Kg=(67d0/18d0-pi**2/6d0)*ca-10d0/9d0*nflav/2d0
         DeltaI1=cf*pi**2/2d0
         C1=-(cf/2d0)*pi**2/12d0
         C2= ((cf*(9d0*cf*pi**4+2d0*ca*(4856d0-603d0*pi**2+18d0*pi**4
     &        -2772d0*zeta3)+4d0*nflav*(-328d0+45d0*pi**2+252d0*zeta3)))/2592d0)/4d0
         ! Identity matrix
         if(allocated(Id))deallocate(Id)
         allocate(Id(-nflav:nflav,-nflav:nflav))
         do i=-nflav,nflav
            do j=-nflav,nflav
               if(i.ne.0.and.i.eq.-j) then
                  Id(i,j)=1d0
               else
                  Id(i,j)=0d0
               endif
            enddo
         enddo

         nores=powheginput('#nores').eq.1
         if(nores) then 
            nres=0
         else
            nres=nlegborn-nlegbornexternal
         endif
         
         ! initialize MCFM parameters
         if ( powheginput('#use_MCFM') .lt. 0 ) then
            use_mcfm = 1
            select_MCFM = .true.
         else
            use_mcfm    = powheginput('#use_MCFM')
            select_MCFM = use_mcfm .eq. 1
         endif
         
         if(.not.select_MCFM.and..not.flg_minnlo.and.flg_minlo)then
            write(*,*)""
            write(*,*)"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            write(*,*)" Trying to execute MiNLO with  "
            write(*,*)" MATRIX amplitudes... this will"
            write(*,*)" unnecessarily slow down the   "
            write(*,*)" code. Switching to MCFM ones: "
            write(*,*)"  -> use_MCFM set to true      "
            write(*,*)"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            write(*,*)""
            select_MCFM=.true.
         endif
         
         use_interpolator=powheginput('#use_interpolator').eq.1
         if(use_interpolator)then
            write(*,*)""
            write(*,*)"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            write(*,*)"        INTERPOLATOR switched on:"
            write(*,*)" the 2-loop contribution for the D3tems"
            write(*,*)" will be computed by means of an "
            write(*,*)" interpolator using precomputed 4D grids"
            write(*,*)"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            write(*,*)""
         endif

         ini=.false.
      endif

      msqb = 0d0
      msqv1 = 0d0
      H1_qT = 0d0
      H1 =0d0

      if(.not.select_MCFM.or.present(msqv2))then
         ! dummy alpha initialization, in order for MATRIX
         ! subroutines to work
         call MATRIX_update_alphaS(st_alpha)
      endif

      if(present(msqv2)) then
         H2_qT = 0d0
         H2=0d0
      endif
      
! build the born squared amplitude
      do j=-nflav,nflav
         if (j==0) cycle
         k=-j
         if(flg_smartMiNLO.and.msqb(j,k).ne.0d0)cycle
         bflav(1) = j
         bflav(2) = k
         bflav(3) = flst_born(3+nres,1)
         bflav(4) = flst_born(4+nres,1)
         bflav(5) = flst_born(5+nres,1)
         bflav(6) = flst_born(6+nres,1)
         if(.not.present(msqv2))then
              ! Just compute the terms needed by the Sudakov 
            if(select_MCFM)then
               ! Use MCFM for computing the Sudakov -> 2-loop not needed 
               call comp_born_virt_WW(pphy,bflav,msqb(j,k),msqv1(j,k))
               if(msqb(j,k).ne.0d0) then
                  H1_qT(j,k)=msqv1(j,k)/msqb(j,k)
               else
                  H1_qT(j,k)=0d0
               endif
  
               ! (2) The factors 1/2 and 1/4 account for 
               !  the different alpha_S/pi normalization 
               H1_qT(j,k)=(H1_qT(j,k))/2d0 
               ! If we had set st_muren2=-st_muren2, we should have still moved to the qT scheme by setting
               ! H1_qT=(H1_CS+2.*DeltaI1)/2. ! -> MCFM, not yet in qT scheme
            else
               ! Use MATRIX for computing the Sudakov -> 2-loop not needed 
               call MATRIX_update_kinematics(bflav,pphy)
               ! MATRIX H1 already in qT scheme
               call MATRIX_1loop(msqb(j,k),H1_qT(j,k))
            endif  
         else
            call MATRIX_update_kinematics(bflav,pphy)
            ! MATRIX H1/H2 already in qT scheme and H2 at scale s
            
            ! provide tree level and 1-loop pieces using MCFM
            call comp_born_virt_WW(pphy,bflav,msqb(j,k),msqv1(j,k))

            ! compute 2-loop contribution            
            if(use_interpolator)then
               mode=0
               call MATRIX_2loop(msqb(j,k),H1_qT(j,k),H2_qT(j,k))
               if(flg_smartMiNLO)call set_equivalent_flavour(H2_qT,nflav,H2_qT(j,k),j,k)
            
               if(test_interpolator)then
                  mode=-3
                  call MATRIX_2loop(msqb(j,k),H1_qT(j,k),H2_qT(j,k))
                  if(flg_smartMiNLO)call set_equivalent_flavour(H2_qT,nflav,H2_qT(j,k),j,k)
               endif

               rescue_system=((H2_qT(j,k).gt.500).or.(H2_qT(j,k).lt.-100))
               if(rescue_system.and.(.not.test_interpolator))then
                  mode=-3
                  call MATRIX_2loop(msqb(j,k),H1_qT(j,k),H2_qT(j,k))
                  if(flg_smartMiNLO)call set_equivalent_flavour(H2_qT,nflav,H2_qT(j,k),j,k)
               endif   
            else
               mode=-3
               call MATRIX_2loop(msqb(j,k),H1_qT(j,k),H2_qT(j,k))
               if(flg_smartMiNLO)call set_equivalent_flavour(H2_qT,nflav,H2_qT(j,k),j,k)
            endif

         endif         

         if(flg_smartMiNLO)then
            call set_equivalent_flavour(msqb,nflav,msqb(j,k),j,k)
            call set_equivalent_flavour(H1_qT,nflav,H1_qT(j,k),j,k)
         endif
      end do   

      ! (3) From qT to MiNNLOPS scheme
      H1=2d0*(H1_qT-2d0*C1*Id)
      msqV1=H1*msqb

      if(present(msqv2))then
         !H2=4d0*(H2_qT-C1**2-2d0*C2-2d0*C1*(H1_qT-2d0*C1))
         H2=4d0*(H2_qT-(C1**2+2d0*C2)*Id-C1*H1)
         msqV2=H2*msqb
      endif

      end


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
      subroutine comp_born_virt_WW(p_out,flavs_out,msqb,msqv)
      implicit none
      include 'pwhg_math.h'
      include 'nlegborn.h'
      real*8,intent(in):: p_out(0:3,nlegbornexternal-1) ! Assuming nlegborn-1=6 ! 
      real*8   p_in(0:3,nlegbornexternal-1) ! Assuming nlegborn-1=6 ! 
      integer,intent(in):: flavs_out(nlegbornexternal-1) ! Assuming nlegborn-1=6 !          
      integer  flavs_in(nlegbornexternal-1) ! Assuming nlegborn-1=6 !          
      real*8   msqb,msqv
      logical ini
      data ini/.true./
      save ini
      include 'ww_PhysPars.h'
      include 'ww_nlegborn.h'
      include 'pwhg_st.h'
      real * 8 ww_virtual,ww_born,ww_stmuren2_saved

      if(ini) then
         call ww_init_couplings(.false.) ! set to false to use SM coupling
         ini=.false.
      endif

      ! By setting mu_ren, we switch off the effect of the log(-sij/muren2)
      ! in the MCFM amplitude: (1) from Frixione to qT scheme
      ww_stmuren2_saved=st_muren2
      st_muren2 = ( (p_out(0,3)+p_out(0,4)+p_out(0,5)+p_out(0,6))**2
     $     - (p_out(1,3)+p_out(1,4)+p_out(1,5)+p_out(1,6))**2
     $     - (p_out(2,3)+p_out(2,4)+p_out(2,5)+p_out(2,6))**2
     $     - (p_out(3,3)+p_out(3,4)+p_out(3,5)+p_out(3,6))**2 )
      ! By setting st_muren2=-st_muren2, we completely switch off the contribution of xl12 in vpole function
      ! -> xl12=(0.,0.). Then, to get to the qT scheme, we have to add an extra cf*pi**2*Born.
      ! If we do NOT set st_muren=-st_muren2, we immediatly sit in the qT scheme, because xl12=(0.,-pi)

ccccccccccccccccccccccccccccccc
c     Flip p3 and p4, to match mcfm conventions:                                    
c     powheg list:                p1 p2 -> lbar nu   l nubar ...                    
c     mcfm list (ww_ routines):   p1 p2 -> nu   lbar l nubar ...                    
      p_in=p_out
      p_in(:,3)=p_out(:,4)
      p_in(:,4)=p_out(:,3)

      flavs_in(:)=flavs_out(:)
      flavs_in(3)=flavs_out(4)
      flavs_in(4)=flavs_out(3)
ccccccccccccccccccccccccccccccc
      ! Move from dimensional reduction (DR) to conventional
      ! dimensional regularization (CDR) scheme
      ! -> already done in the ww_setvirtual subroutine
      call ww_setvirtual(p_in,flavs_in,ww_virtual, ww_born,.false.)
!      B2correction=ww_virtual/ww_born
      msqb=ww_born

      msqv=ww_virtual
      st_muren2=ww_stmuren2_saved
      end
