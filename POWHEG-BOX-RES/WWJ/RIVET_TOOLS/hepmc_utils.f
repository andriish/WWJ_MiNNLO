
      subroutine init_hepmc(ii,seed)
      implicit none
      include 'pwhg_rwl.h'
      include 'pwhg_rnd.h'
      integer(kind=4), intent(out) :: ii
      character*20 seed
      ii=rwl_num_weights
      seed = trim(rnd_cwhichseed)//char(0)
      end subroutine

      subroutine get_weights_hepmc(ii,ffp)
      implicit none
      include 'pwhg_rwl.h'
      integer(kind=4), intent(in) :: ii
      real(kind=8), intent(out) :: ffp
      ffp=rwl_weights(ii+1)
      end subroutine
      
