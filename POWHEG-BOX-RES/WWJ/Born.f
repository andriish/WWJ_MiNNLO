      subroutine setborn(p,bflav,born,bornjk,bmunu)
c Wrapper subroutine to call the MadGraph Borns
c and set the event-by-event couplings constant
      implicit none
      include 'nlegborn.h'
      include 'pwhg_kn.h'
      real * 8 p(0:3,nlegbornexternal),bornjk(nlegbornexternal,nlegbornexternal)
      integer bflav(nlegbornexternal),i
      real * 8 bmunu(0:3,0:3,nlegbornexternal),born

      if(kn_jacborn.eq.0d0)then
         born=0d0
         bmunu=0d0
         bornjk=0d0
         return
      endif

      born=0d0
      bmunu=0d0
      bornjk=0d0

      call set_ebe_couplings
      call sborn_proc(p,bflav,born,bornjk,bmunu)

      end

      subroutine borncolour_lh
c Wrapper subroutine to call the MadGraph code to associate
c a (leading) color structure to an event.
      implicit none
      include 'nlegborn.h'
      include 'LesHouches.h'
      include 'pwhg_flst.h'
      include 'pwhg_rad.h'
      integer bflav(nlegbornexternal)
      integer mad_color(2,nlegbornexternal)
      integer i,j,k

      j=0
      do i=1,flst_bornlength(rad_ubornsubp)
         if (idup(i).eq.23 .or. abs(idup(i)).eq.24) cycle
         j=j+1
         if(idup(i).eq.21)then 
            bflav(j)=0
         else
            bflav(j)=idup(i)
         endif
      enddo
      call born_color(bflav,mad_color)
      do i=1,2
         k=0
         do j=1,flst_bornlength(rad_ubornsubp)
            k=k+1
            if(abs(idup(j)).eq.24.or.idup(j).eq.23)then
               icolup(:,j)=0
               k=k-1
            else
               icolup(i,j)=mad_color(i,k)
            endif
         enddo
      enddo
      end

      subroutine finalize_lh
      implicit none
      include 'nlegborn.h'
      include "LesHouches.h"
      integer decay 
      common /cdecay/decay
      integer iwp1,iwp2,iwm1,iwm2
      real * 8 random,ran  
      external random 
      real * 8 powheginput
      external powheginput
      logical ini,nores
      data ini/.true./
      save ini,nores
      integer jl,j

! Find lepton positions (which might vary depending on the
! resonance history)
! default flavour 
!         iwp1=-11
!         iwp2=12
!         iwm1=13
!         iwm2=-14
      do j=1,nlegreal
         ! Find first lepton position
         if(abs(idup(j)).eq.11 
     &        .or.abs(idup(j)).eq.12 
     &        .or.abs(idup(j)).eq.13 
     &        .or.abs(idup(j)).eq.14) then 
            jl=j  ! for WW either 5 (WW channel) or 6 (Z->WW channel)
            exit
         endif
      enddo


      if (decay == 11) then 
         iwp1=-11
         iwp2=12
         iwm1=11
         iwm2=-12
      elseif (decay == 22) then 
         iwp1=-13
         iwp2=14
         iwm1=13
         iwm2=-14
      elseif (decay == 33) then
         iwp1=-15
         iwp2=16
         iwm1=15
         iwm2=-16
      elseif (decay == 44) then 
         if(random().gt.0.5d0) then
            iwp1=-13
            iwp2=14
         else
            iwp1=-11
            iwp2=12
         endif
         if(random().gt.0.5d0) then
            iwm1=13
            iwm2=-14
         else
            iwm1=11
            iwm2=-12
         endif
      elseif (decay == 55) then 
         ran = random() 
         if(ran.gt.2d0/3d0) then
            iwp1=-15
            iwp2=16
         elseif(ran.gt.1d0/3d0) then
            iwp1=-13
            iwp2=14
         else
            iwp1=-11
            iwp2=12
         endif
         ran = random()
         if(ran.gt.2d0/3d0) then
            iwm1=15
            iwm2=-16
         elseif(ran.gt.1d0/3d0) then
            iwm1=13
            iwm2=-14
         else
            iwm1=11
            iwm2=-12
         endif         
      elseif (decay == 66) then 

C     Now decide the flavour of the UP-type quark (u or c) 
         if(random().gt.0.5d0) then
            iwp2 = 2            ! u  
            iwp1 = -1           ! db 
         else
            iwp2 = 4            ! c 
            iwp1 = -3           ! sb  
         endif
C     randomize over down type quark (e.g. in case of udb, pick udb or usb)
         call randomizeckm(iwp1,iwp1)  

         icolup(1,jl)= 0
         icolup(2,jl)= 503
         icolup(1,jl+1)= 503
         icolup(2,jl+1)= 0

C     do the same for W-
         if(random().gt.0.5d0) then
            iwm2 = -2           ! ubar  
            iwm1 = 1            ! d 
         else
            iwm2 = -4           ! cbar 
            iwm1 = 3            ! s
         endif
C     randomize over down type quark (e.g. in case of udb, pick udb or usb)
         call randomizeckm(iwm1,iwm1)  

         icolup(1,jl+2)= 504
         icolup(2,jl+2)= 0
         icolup(1,jl+3)= 0
         icolup(2,jl+3)= 504

      elseif (decay == 46) then 

C     decides if Wp or Wm decays hadronica/leptonically 
         if (random().gt. 0.5d0) then 
C     Wp -> had; wm -> lept 
            
C     Now decide the flavour of the UP-type quark (u or c) 
            if(random().gt.0.5d0) then
               iwp2 = 2  ! u  
               iwp1 = -1 ! db 
            else
               iwp2 = 4  ! c 
               iwp1 = -3 ! sb  
            endif
C     randomize over down type quark (e.g. in case of udb, pick udb or usb)
            call randomizeckm(iwp1,iwp1)  

            icolup(1,jl)= 0
            icolup(2,jl)= 503
            icolup(1,jl+1)= 503
            icolup(2,jl+1)= 0

C     decide the leptonic family in the final state
            if(random().gt.0.5d0) then
               iwm1=13
               iwm2=-14
            else
               iwm1=11
               iwm2=-12
            endif

         else
C     Wp -> lept; wm -> had 

C     decide flavour of the leptonic family
            if(random().gt.0.5d0) then
               iwp1=-13
               iwp2=14
            else
               iwp1=-11
               iwp2=12
            endif
            
C     Now decide the flavour of the UP-type quark (u or c) 
            if(random().gt.0.5d0) then
               iwm2 = -2        ! ubar  
               iwm1 = 1         ! d 
            else
               iwm2 = -4        ! cbar 
               iwm1 = 3         ! s
            endif
C     randomize over down type quark (e.g. in case of udb, pick udb or usb)
            call randomizeckm(iwm1,iwm1)  

            icolup(1,jl+2)= 504
            icolup(2,jl+2)= 0
            icolup(1,jl+3)= 0
            icolup(2,jl+3)= 504
           
         endif

      elseif (decay == 56) then 
         

C     decides if Wp or Wm decays hadronica/leptonically 
         if (random().gt. 0.5d0) then 
C     Wp -> had; wm -> lept 
            
C     Now decide the flavour of the UP-type quark (u or c) 
            if(random().gt.0.5d0) then
               iwp2 = 2  ! u  
               iwp1 = -1 ! db 
            else
               iwp2 = 4  ! c 
               iwp1 = -3 ! sb  
            endif
C     randomize over down type quark (e.g. in case of udb, pick udb or usb)
            call randomizeckm(iwp1,iwp1)  

            icolup(1,jl)= 0
            icolup(2,jl)= 503
            icolup(1,jl+1)= 503
            icolup(2,jl+1)= 0

C     decide the leptonic family in the final state
            ran = random()
            if(ran.gt.2d0/3d0) then
               iwm1=15
               iwm2=-16
            elseif(ran.gt.1d0/3d0) then
               iwm1=13
               iwm2=-14
            else
               iwm1=11
               iwm2=-12
            endif         
            
         else
C     Wp -> lept; wm -> had 

C     decide flavour of the leptonic family
            ran = random() 
            if(ran.gt.2d0/3d0) then
               iwp1=-15
               iwp2=16
            elseif(ran.gt.1d0/3d0) then
               iwp1=-13
               iwp2=14
            else
               iwp1=-11
               iwp2=12
            endif
            
C     Now decide the flavour of the UP-type quark (u or c) 
            if(random().gt.0.5d0) then
               iwm2 = -2        ! ubar  
               iwm1 = 1         ! d 
            else
               iwm2 = -4        ! cbar 
               iwm1 = 3         ! s
            endif
C     randomize over down type quark (e.g. in case of udb, pick udb or usb)
            call randomizeckm(iwm1,iwm1)  

            icolup(1,jl+2)= 504
            icolup(2,jl+2)= 0
            icolup(1,jl+3)= 0
            icolup(2,jl+3)= 504
            
         endif

      elseif (decay == 12) then 
!     not really needed (it's the default)
         iwp1=-11
         iwp2=12
         iwm1=13
         iwm2=-14
      elseif (decay == 21) then 
         iwp1=-13
         iwp2=14
         iwm1=11
         iwm2=-12
      else
         write(*,*) 'decay mode not allowed! Decay = ', decay 
         call exit(-1) 
      endif

C    now add the W+W- resonances

c     adapted to res framework -->
      if (ini) then
         nores=powheginput('#nores').eq.1
         ini=.false.
      endif
      
      if (nores) then
         idup(3)=iwp1
         idup(4)=iwp2
         idup(5)=iwm1
         idup(6)=iwm2
         call add_resonance(+24 ,3,4)
         call add_resonance(-24 ,6,7)
      else
         idup(jl)=iwp1
         idup(jl+1)=iwp2
         idup(jl+2)=iwm1
         idup(jl+3)=iwm2
      endif

      call check_leshouches
c     Give masses to final-state products
      call lhefinitemasses
      
      end
