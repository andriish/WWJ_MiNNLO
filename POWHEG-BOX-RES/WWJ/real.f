      subroutine setreal(p,rflav,amp2)
c Wrapper subroutine to call the MadGraph real emission matrix
c elements and set the event-by-event couplings constant
      implicit none
      include 'nlegborn.h'
      include 'pwhg_kn.h'
      include 'pwhg_math.h'
      include 'pwhg_st.h'
      include 'pwhg_flg.h'
      real * 8 p(0:3,nlegrealexternal)
      integer rflav(nlegrealexternal)
      real * 8 amp2


      if(kn_jacborn.eq.0d0 .and. flg_btildepart.ne.'R')then
         amp2=0d0
         return
      endif

      call set_ebe_couplings
      call sreal_proc(p,rflav,amp2)
c Cancel as/(2pi) associated with amp2. It will be put back by real_ampsq
      amp2 = amp2/(st_alpha/(2d0*pi))     
      if(amp2.ne.amp2) call increasecnt("R NaN")

      end



      subroutine regularcolour_lh
c Wrapper subroutine to call the MadGraph code to associate
c a (leading) color structure to an event.
      implicit none
      include 'nlegborn.h'
      include 'LesHouches.h'
      include 'pwhg_flst.h'
      include 'pwhg_rad.h'
      integer rflav(nlegrealexternal)
      integer mad_color(2,nlegrealexternal)
      integer i,j,k
      j=0
      do i=1,flst_regularlength(flst_iregularresgroup)
         if (idup(i).eq.23 .or. abs(idup(i)).eq.24) cycle
         j=j+1
         if(idup(i).eq.21)then
            rflav(j)=0
         else               
            rflav(j)=idup(i)
         endif
      enddo
      call real_color(rflav,mad_color)
      do i=1,2
         k=0
         do j=1,flst_regularlength(flst_iregularresgroup)
            k=k+1
            if(abs(idup(j)).eq.24.or.idup(j).eq.23)then
               icolup(:,j)=0
               k=k-1
            else
               icolup(i,j)=mad_color(i,k)
            endif
         enddo
      enddo
      end


