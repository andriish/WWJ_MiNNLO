      subroutine born_phsp(xborn)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_kn.h'
      include 'pwhg_flst.h'
      include 'pwhg_physpar.h'
      include 'PhysPars.h'
      real * 8  :: p(0:3,nlegborn)
      real * 8  :: cmp(0:3,nlegborn)
      real * 8  :: masses(nlegborn)
      real * 8  :: xborn(*)
      real * 8  :: powheginput, ptj
      integer   :: iborn,j
      logical, save :: ini=.true., nores
c     No resonance case                                                                                 
      integer flav(10),res(10),nres
      data flav/ -1, 1, 23, -24, 24, -11, 12, 13, -14, 0 /
      data res /  0, 0,  0,  3,   3,   5,  5,  4,   4, 0 /
      save flav,res,nres
      real*8 ptot
      real * 8 pborn(0:3,flst_numfsb+2)
      integer bflav(nlegborn),is_fs(nlegborn),isfslength
      real*8 p34(0:3),p56(0:3),m34,m56
      real*8 p3456(0:3),pt3456
      real*8 smin34,smax34,smin56,smax56
      logical bwwindows
      parameter (bwwindows=.false.)

      integer i

      if(ini) then
         if(powheginput("#nores") == 1) then
            nores = .true.
            nres=0
         else
            nores = .false.
            nres=1
         endif
         ini = .false.
      endif

      if(nores) then
c provide Born phase space for resonance unaware integration
         call genphasespace(xborn,10,flav,res,kn_beams,kn_jacborn,
     1        kn_xb1,kn_xb2,kn_sborn,cmp,p)

         kn_cmpborn(:,1:2) = cmp(:,1:2)
         kn_cmpborn(:,3:7) = cmp(:,6:10)

         kn_pborn(:,1:2) = p(:,1:2)
         kn_pborn(:,3:7) = p(:,6:10)

c     Make sure flst_ibornlength is set                   
         do iborn=1,flst_nborn
            if(flst_bornresgroup(iborn).eq.flst_ibornresgroup) exit
         enddo

         flst_ibornlength = flst_bornlength(iborn)
         flst_ireallength = flst_ibornlength + 1

         do j=1,flst_ibornlength
            kn_masses(j) = physpar_phspmasses(flst_born(j,iborn))
         enddo



      else
c generate Born phase space from resoance information

         do iborn=1,flst_nborn
            if(flst_bornresgroup(iborn).eq.flst_ibornresgroup) exit
         enddo

         flst_ibornlength = flst_bornlength(iborn)
         flst_ireallength = flst_ibornlength + 1

         do j=1,flst_ibornlength
            kn_masses(j) = physpar_phspmasses(flst_born(j,iborn))
         enddo
         

         call genphasespace(xborn,flst_ibornlength,flst_born(:,iborn),
     1        flst_bornres(:,iborn),kn_beams,kn_jacborn,
     1        kn_xb1,kn_xb2,kn_sborn,kn_cmpborn,kn_pborn)

         kn_minmass = 0d0 ! 2*ph_bmass 

      endif

       iborn=flst_ibornresgroup
       call getisfsparticles(flst_bornlength(iborn),flst_born(:,iborn),flst_bornres(:,iborn),   
     1      isfslength,is_fs)
       if(isfslength /= flst_numfsb+2) then
          write(*,*) ' Error in Born_phsp.f: length mismatch'
          call exit(-1)
       endif

       do j=1,isfslength
          pborn(:,j)=kn_cmpborn(:,is_fs(j))
       enddo 
       

! ***************  CUTS   ***************
       smin34=1d-3
       smax34=dsqrt(kn_sbeams)**2
       p34(:)=pborn(:,3)+pborn(:,4)
       m34=p34(0)**2-p34(1)**2-p34(2)**2-p34(3)**2
       smin56=1d-3
       smax56=dsqrt(kn_sbeams)**2
       p56(:)=pborn(:,5)+pborn(:,6)
       m56=p56(0)**2-p56(1)**2-p56(2)**2-p56(3)**2
       p3456(:)=p34(:)+p56(:)
       pt3456=dsqrt(p3456(1)**2+p3456(2)**2)
      
      if((m34.gt.smax34.or.m34.lt.smin34)
     & .or.(m56.gt.smax56.or.m56.lt.smin56)
     &      .or. (pt3456.lt.kn_ktmin)
     &      )then
       kn_jacborn=0d0
       return
      endif

c---if x's out of normal range abort                                                    
      if   ((kn_xb1 .gt. 1d0)
     & .or. (kn_xb2 .gt. 1d0)
     & .or. (kn_xb1 .lt. 1d-8)
     & .or. (kn_xb2 .lt. 1d-8)) then
!         write(*,*) ' error in Born phase space!, x1,x2 our of range'
!         write(*,*) xx(1),xx(2)
         kn_jacborn = 0
         return
      endif

      ptot = abs(sum(pborn(:,1)+pborn(:,2)-
     +        pborn(:,3)-pborn(:,4)-
     +        pborn(:,5)-pborn(:,6)-pborn(:,7)))
      if (ptot.gt.1d-7) then
         kn_jacborn = 0
         return
      endif

      end

      subroutine born_suppression(fact)
      implicit none
      real * 8 fact
      fact=1d0
      end

      subroutine rmn_suppression(fact)
      implicit none
      real * 8 fact
      fact=1d0
      end


      subroutine regular_suppression(fact)
      implicit none
      real * 8 fact
      call rmn_suppression(fact)
      end


      subroutine global_suppression(c,fact)
      implicit none
      character * 1 c
      real * 8 fact
      fact=1d0
      end


      subroutine set_fac_ren_scales(muf,mur)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'PhysPars.h'
      real * 8 muf,mur
      logical ini
      data ini/.true./
      integer runningscales
      real * 8 pww(0:3),pwm(0:3),pwp(0:3)
      real * 8 powheginput
      external powheginput
      save ini,runningscales
      include 'pwhg_flg.h'
      real * 8 p(0:3,flst_numfsr+2)
      integer j,iborn
      integer bflav(nlegborn),is_fs(nlegborn),isfslength

      if (ini) then
         runningscales=powheginput("#runningscales")

         if(powheginput("#minlo").eq.1) then
            write(*,*) '****************************************'
            write(*,*) '*******          MINLO ACTIVE    *******'
            write(*,*) '****************************************'
            write(*,*) '*******     FIXED SCALES!          *****'
            runningscales=0
         endif

         if (runningscales.eq.1) then
            write(*,*) '****************************************'
            write(*,*) '****************************************'
            write(*,*) '**  Factorization and renormalization **'
            write(*,*) '**  Scales mur=muf=M(WW)              **'
            write(*,*) '****************************************'
            write(*,*) '****************************************'
         elseif (runningscales.eq.2) then
            write(*,*) '****************************************'
            write(*,*) '****************************************'
            write(*,*) '**  Factorization and renormalization **'
            write(*,*) '**  Scales mur=muf=MT_Wp+MT_Wm        **'
            write(*,*) '****************************************'
            write(*,*) '****************************************'
         else
            write(*,*) '****************************************'
            write(*,*) '****************************************'
            write(*,*) '**   mur=muf=2MW                      **'
            write(*,*) '****************************************'
            write(*,*) '****************************************'                                                      
            runningscales=0
         endif
         ini=.false.
      endif

      if(runningscales.ne.0)then
         if(flg_btildepart == 'b')then
            iborn=flst_ibornresgroup
            call getisfsparticles(flst_bornlength(iborn),flst_born(:,iborn),
     1           flst_bornres(:,iborn),isfslength,is_fs)
            if(isfslength /= flst_numfsb+2) then
               write(*,*) ' Error in set_fac_ren_scales:'
               write(*,*) ' length mismatch'
               call exit(-1)
            endif
            do j=1,isfslength
               p(:,j)=kn_pborn(:,is_fs(j))
            enddo 
         elseif(flg_btildepart == 'r')then
            iborn=flst_ibornresgroup
            call getisfsparticles(flst_reallength(iborn),flst_real(:,iborn),
     1           flst_realres(:,iborn),isfslength,is_fs)
            if(isfslength /= flst_numfsr+2) then
               write(*,*) ' Error in set_fac_ren_scales:'
               write(*,*) ' length mismatch'
               call exit(-1)
            endif
            do j=1,isfslength
               p(:,j)=kn_preal(:,is_fs(j))
            enddo 
         elseif(flg_btildepart == 'R')then
            iborn=flst_iregularresgroup
            call getisfsparticles(flst_regularlength(iborn),flst_regular(:,iborn),
     1           flst_regularres(:,iborn),isfslength,is_fs)
            if(isfslength /= flst_numfsr+2) then
               write(*,*) ' Error in set_fac_ren_scales:'
               write(*,*) ' length mismatch'
               call exit(-1)
            endif
            do j=1,isfslength
               p(:,j)=kn_preal(:,is_fs(j))
            enddo 
         else
            write(*,*) "Case where flg_btildepart=",flg_btildepart
            write(*,*) "not supported ..."
            stop         
         endif

         if (runningscales.eq.1) then
            pww = p(:,3)+p(:,4)
     $           + p(:,5)+p(:,6)
            muf = sqrt(pww(0)**2-pww(1)**2-pww(2)**2-pww(3)**2)
            mur=muf
         elseif (runningscales.eq.2) then
            pwp = p(:,3)+p(:,4)
            pwm = p(:,5)+p(:,6)
            muf = sqrt(pwp(0)**2-pwp(3)**2)
            muf = muf+sqrt(pwm(0)**2-pwm(3)**2)
            mur=muf
         endif
      else
         muf=2d0*ph_wmass
         mur=2d0*ph_wmass
      endif
      end


