      subroutine btildecoll(xrad,rescoll,www)
      implicit none
      include 'nlegborn.h'
      include 'pwhg_flst.h'
      include 'pwhg_kn.h'
      include 'pwhg_br.h'
      include 'pwhg_math.h'
      include 'pwhg_st.h'
      include 'pwhg_em.h'
      include 'pwhg_flg.h'
      include 'pwhg_pdf.h'
      include 'pwhg_par.h'
      real * 8 xrad,rescoll(flst_nborn),www,rescfac
      real * 8 un
      parameter (un=1d0)
c     pdfb: born pdf's, pdfs: pdfs with scaled x->x/z
      real * 8
     1     pdfb(-pdf_nparton:pdf_nparton,2),
     3     pdfs(-pdf_nparton:pdf_nparton,2)
      real * 8 z,omzpgg,omzpqq,ppqq,omzpqg,ppqg,omzpgq,ppgq,
     1     sb,tot,plfrc,plfr0,z1,z2,xjac(2),rm(2),res(2),
     2     x,xjac0
      integer j,k,l,m,jb,fl(2),in,flavin,irad,in2,flavin2
      logical pwhg_isfinite
      real * 8 chargeofparticle,resgam(2),
     1     ch(2)
      external pwhg_isfinite,chargeofparticle
      real * 8 tmp
c     Statement Functions
c     omz:(1-z)*pgg(z), 2.106 of FNO2007
      omzpgg(z)=2*ca*(z+(1-z)**2/z+z*(1-z)**2)
c     2.103
      omzpqq(z)=cf*(1+z**2)
c     ppqq: (p prime qq) d pqq'/ d epsilon, from 2.103
      ppqq(z)=-cf*(1-z)
c     2.104
      omzpqg(z)=cf*(1+(1-z)**2)/z*(1-z)
      ppqg(z)=-cf*z
c     2.105
      omzpgq(z)=tf*(1-2*z*(1-z))*(1-z)
      ppgq(z)=-tf*2*z*(1-z)
c     log coeff., from 2.102, with partonic s=sb/z
      plfrc(z)=1/(1-z)*log(sb/z/st_mufact2)+2*log(1-z)/(1-z)
c     same, with soft limit s
      plfr0(z)=1/(1-z)*log(sb/st_mufact2)+2*log(1-z)/(1-z)
c     End Statement Functions
c     If there is no radiation from production, no remnants!
      do j=1,flst_nreson
         if(flst_reslist(j).eq.0) exit
      enddo
      if(j.eq.flst_nreson+1) then
c     no radiation from production!
         rescoll=0
         return
      endif

      if(flg_collremnsamp) then
         xjac0=630*(1-xrad)**4*xrad**4
         x=xrad**5*(70*xrad**4-315*xrad**3+540*xrad**2-420*xrad+126)
      else
         xjac0=1
         x=xrad
      endif
c     z1=1-par_isrtinycsi-(1-kn_xb1-par_isrtinycsi)*x
      z1=1-(1-kn_xb1)*par_isrtinycsi-(1-kn_xb1)*(1-par_isrtinycsi)*x
      xjac(1)=(1-kn_xb1)*xjac0
c     z2=1-par_isrtinycsi-(1-kn_xb2-par_isrtinycsi)*x
      z2=1-(1-kn_xb2)*par_isrtinycsi-(1-kn_xb2)*(1-par_isrtinycsi)*x
      xjac(2)=(1-kn_xb2)*xjac0

      sb=kn_sborn
      if(.not.flg_minlo) then
         rescfac = 1
      endif
c     loop over all Born configurations
      tot=0
      do jb=1,flst_nborn
c     the following is to be done once if minlo is not set
         if(flg_minlo.or.jb.eq.1) then
c     the following also sets st_mufact2 according to the underlying Born index jb,
c     sets rescfac to the rescaling factors needed by the virtual, real and collinear remnants
c     (if the flag argument 2 is given)
            if(flg_minlo) call setlocalscales(jb,2,rescfac)
c     See 7.224 and 7.225 of FNO2007
c     Remnant: 1/(1-z)_+=1/(1-z)_(1-x)+log(1-x) delta(1-z)
c     Remnant: log(1-z)/(1-z)_+ = log(1-z)/(1-z)_(1-x)+
c     log(1-x)^2/2 delta(1-z)
            rm(1) = log(1-kn_xb1)*log(sb/st_mufact2)
     1           + log(1-kn_xb1)**2
            rm(2) = log(1-kn_xb2)*log(sb/st_mufact2)
     1           + log(1-kn_xb2)**2
c     get pdfs at underlying born x values
            call pdfcall(1,kn_xb1,pdfb(:,1))
            call pdfcall(2,kn_xb2,pdfb(:,2))
c     get pdfs at underlying born x/z value values
            call pdfcall(1,kn_xb1/z1,pdfs(:,1))
            call pdfcall(2,kn_xb2/z2,pdfs(:,2)) 
         endif

         res(1)    = 0d0
         res(2)    = 0d0
         resgam(1) = 0d0
         resgam(2) = 0d0

         fl(1) = flst_born(1,jb)
         fl(2) = flst_born(2,jb)
         ch(1) = chargeofparticle(fl(1))
         ch(2) = chargeofparticle(fl(2))
         
         m=0
         do k=1,maxalr
            call getisr(jb, m, in, flavin, irad, in2, flavin2)
            if(m == -1) exit
            do l=1,2
               if(l == 2) then
                  if(in2 < 0) exit
                  in = in2
                  flavin = flavin2
               endif
               if(in == 1) then
                  z=z1
               else
                  z=z2
               endif

c     Identify the splitting contribution
c     g -> g g
c     FNO2007, 2.102
c     gg remnant
c     Remember 1/z factor, 4.23 and 7.206 of FNO2007
               if(flavin == 0 .and. fl(in) == 0 .and. irad == 0) then
                  res(in)=  omzpgg(z)*plfrc(z)/z  * pdfs(flavin,in)*xjac(in)
     1                 - omzpgg(un)*plfr0(z)     * pdfb(fl(in),in)*xjac(in)
     2                 + omzpgg(un)*rm(in)       * pdfb(fl(in),in)
               elseif(abs(flavin) <= 6 .and. flavin /= 0 .and. fl(in) == 0 .and. irad == flavin) then 
c     qg remnant
                  res(in)=res(in) + (omzpqg(z)*plfrc(z)
     1                 - ppqg(z))/z * pdfs(flavin,in) * xjac(in) 
               elseif( ((abs(flavin) <= 6 .and. flavin /= 0) .or. ! incoming quark
     1                  (abs(flavin) <= 15 .and. abs(flavin) >= 11)) ! incoming lepton
     2                 .and. fl(in).eq.22 .and. flavin == irad) then
c     qgamma remnant
                  resgam(in)=resgam(in) + (omzpqg(z)*plfrc(z)
     1                 - ppqg(z))/z * pdfs(flavin,in)* xjac(in)                  
     2                 * chargeofparticle(flavin)**2 / cf
                  if (pdf_dis_photon) then
                     resgam(in) = resgam(in)  + 
     1                    ( (9d0 + 5*z)/4. + 
     2                    (1d0 + z**2)/(1d0 - z)*
     3                    (-0.75d0 + log((1d0 - z)/z)) )*
     4                    (pdfs(flavin,in)/z-pdfb(flavin,in))
     5                    *  chargeofparticle(flavin)**2
                  endif

               elseif(fl(in) /= 0 .and. abs(fl(in)) <= 6 .and. flavin == fl(in) .and. irad == 0) then
c     qq remnant            
                  res(in) = res(in) + (omzpqq(z)*plfrc(z)
     1                 - ppqq(z))/z * pdfs(flavin,in)*xjac(in) 
     2                 - omzpqq(un)*plfr0(z)     * pdfb(fl(in),in)*xjac(in)
     3                 + omzpqq(un)*rm(in)           * pdfb(fl(in),in)
c     WEW, qq remnant with photon
               elseif( ( (fl(in) /= 0 .and. abs(fl(in)) <= 6) .or. ! is a quark
     1                 (abs(fl(in)) >= 11 .and. abs(fl(in)) <= 15) ) ! or a lepton
     2                 .and. flavin == fl(in) .and. irad == 22) then
                  resgam(in) = resgam(in) + ((omzpqq(z)*plfrc(z)
     1                 - ppqq(z))/z * pdfs(flavin,in)*xjac(in) 
     2                 - omzpqq(un)*plfr0(z)     * pdfb(fl(in),in)*xjac(in)
     3                 + omzpqq(un)*rm(in)       * pdfb(fl(in),in))
     4                 * ch(in)**2 /cf
c     gammaq DIS subtraction
                  if (pdf_dis_photon) then
                     resgam(in) = resgam(in)  - 
     1                    ( (9d0 + 5*z)/4 + 
     2                    (1d0 + z**2)/(1d0 - z)*
     3                    (-0.75d0 + log((1d0 - z)/z)) )*
     4                    (pdfs(flavin,in)/z-pdfb(fl(in),in)) * ch(in)**2
                  endif
               elseif(flavin == 0 .and. fl(in) /= 0 .and. abs(fl(in))<=6 .and. fl(in)+irad == 0) then
c     gq remnant
                  res(in)=res(in) + (omzpgq(z)*plfrc(z)
     1                 - ppgq(z))/z   * pdfs(flavin,in)*xjac(in)
               elseif(flavin == 22 .and. fl(in) /= 0 .and. fl(in)+irad == 0) then
c     gamma-q or gamma-l remnant; as gluon-quark remnant divided by tf, times
c     the charge square times 3 for colours in case of quarks
                  if(abs(fl(in))<=6) then
                     resgam(in) = resgam(in) + (omzpgq(z)*plfrc(z)
     1                    - ppgq(z))/z   * pdfs(flavin,in)*xjac(in) /tf *ch(in)**2 * 3
c     qgamma DIS subtraction 
                     if (pdf_dis_photon) then
                        resgam(in) = resgam(in)  - 3d0 *
     1                       ( log( (1d0-z)/z )*( z**2+(1d0-z)**2 )
     2                       - 8d0*z**2 + 8d0*z - 1d0  )*
     3                       (pdfs(flavin,in)/z - pdfb(fl(in),in)) * ch(in)**2
                     endif
                  elseif(abs(fl(in))>=11) then
                     resgam(in) = resgam(in)  + (omzpgq(z)*plfrc(z)
     1                    - ppgq(z))/z   * pdfs(flavin,in)*xjac(in) /tf *ch(in)**2
c     qgamma DIS subtraction 
                     if (pdf_dis_photon) then
                        resgam(in) = resgam(in)  - 1d0 *
     1                       ( log( (1d0-z)/z )*( z**2+(1d0-z)**2 )
     2                       - 8d0*z**2 + 8d0*z - 1d0  )*
     3                       (pdfs(flavin,in)/z - pdfb(fl(in),in)) * ch(in)**2
                     endif
                  endif
               else
                  write(*,*) ' sigcollremn : got ',flavin, '->' , fl(in),
     1                 ' + (radiated) ', irad
               endif
            enddo
         enddo

         if (flg_QEDonly) then
            res=0
         endif
         rescoll(jb)=( res(1)*pdfb(fl(2),2)+res(2)*pdfb(fl(1),1) )
     1        *br_born(jb)*st_alpha/(2*pi)*kn_jacborn * rescfac
         if(flg_with_em) then
c     WEW, add photon remnants       
            rescoll(jb)=rescoll(jb)
     1           +(resgam(1)*pdfb(fl(2),2)+resgam(2)*pdfb(fl(1),1))
     2           *br_born(jb)*em_alpha/(2*pi)*kn_jacborn * rescfac
         endif
         tot=tot+rescoll(jb)
      enddo
      if (.not.pwhg_isfinite(tot)) then
         do jb=1,flst_nborn
            rescoll(jb)=0d0
         enddo
         tot=0d0
      endif
      if(flg_nlotest) then
         if(flg_detailedNLO) then
            do jb=1,flst_nborn
c     contributions not belonging to the current resonance group turn out
c     to be zero anyhow, so the following if isn't really necessary ...
               if(flst_ibornresgroup == flst_bornresgroup(jb)) then
                  flst_currentuborn = jb
                  tmp = rescoll(jb)*www
                  if(pwhg_isfinite(tmp) .and. tmp /= 0) then
                     call analysis_driver(tmp,0)
                  endif
               endif
            enddo
         else
            tot=tot*www
            call analysis_driver(tot,0)
         endif
      endif
      contains
      subroutine getisr(iborn, m, in, iflavin, irad, in2, iflavin2)
      implicit none
      integer iborn, m, in, iflavin, irad, in2, iflavin2
      integer nregions,emitter,alr,j
      nregions = flst_born2alr(0,iborn)
      do j=m+1,nregions
         alr = flst_born2alr(j,iborn)
         emitter = flst_emitter(alr)
         if(emitter > 2) cycle
         irad = flst_alr(flst_alrlength(alr),alr)
         if(emitter == 0) then
            in = 1
            in2 = 2
            iflavin = flst_alr(1,alr)
            iflavin2 = flst_alr(2,alr)
            m=j
            return
         else
            in = emitter
            iflavin = flst_alr(in,alr)
            in2 = -1
            m=j
            return
         endif
      enddo
      m = -1
      end
      end



      function chargeofparticle(id)
c WEW,  chargeofparticle() added
c Returns the electric charge (in units of the positron charge)
c of particle id (pdg conventions, gluon is zero)
      implicit none
      real * 8 chargeofparticle
      integer id
      if(abs(id).gt.0.and.abs(id).le.6) then
         if(mod(abs(id),2).eq.0) then
            chargeofparticle =  2d0/3
         else
            chargeofparticle = -1d0/3
         endif
      elseif(abs(id).gt.10.and.abs(id).le.16) then
         if(mod(abs(id),2).ne.0) then
            chargeofparticle = -1d0
         else
            chargeofparticle = 0
         endif
      else
         chargeofparticle=0
      endif
      if(id<0) chargeofparticle = - chargeofparticle
      end
